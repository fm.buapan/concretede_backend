(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/app.component.html":
/*!**************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/app.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/confirm/confirm.component.html":
/*!**************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/confirm/confirm.component.html ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ng-template #view let-c=\"close\" let-d=\"dismiss\">\n\n</ng-template>\n<div class=\"row text-left\">\n  <div class=\"col-12\">\n    <div class=\"content-header\">การโอนเงินให้ผู้ขาย </div>\n    <!-- <p class=\"content-sub-header\">All table styles are inherited in Bootstrap 4, meaning any nested tables will be\n      styled in the same manner as the parent.</p> -->\n  </div>\n</div>\n<!--Basic Table Ends-->\n\n<!--Hoverable Rows Starts-->\n<section id=\"hoverable-rows\">\n  <div class=\"row text-left\">\n    <div class=\"col-sm-12\">\n      <div class=\"card\">\n        <div class=\"card-content\">\n\n          <div class=\"card-body\">\n            <div class=\"text-right\" style=\"margin-top: -10px; margin-bottom: 55px;\">\n              <form class=\"navbar-form navbar-right mt-1\" [formGroup]=\"searchForm\" (ngSubmit)=\"search()\">\n                <div class=\"position-relative has-icon-right\">\n                  <input class=\"form-control round\" formControlName=\"search\" placeholder=\"Search\" type=\"text\">\n                  <div class=\"form-control-position\">\n                    <i class=\"ft-search\"></i>\n                  </div>\n                </div>\n              </form>\n            </div>\n            <table class=\"table table-hover\">\n              <thead>\n                <tr>\n                  <th style=\"width: 50px;\">#</th>\n                  <th style=\"width: 120px;\">Order ID</th>\n                  <th>ชื่อ-สกุล</th>\n                  <!-- <th>เลขที่บัญชี</th> -->\n                  <th>ยอดเงินที่ต้องโอน</th>\n                  <th>สถานะ</th>\n                  <th >หลักฐาน</th>\n                  <!-- <th>วันที่โอน</th> -->\n                  <th style=\"width: 220px;\" class=\"text-center\">จัดการ</th>\n                </tr>\n              </thead>\n              <tbody>\n                <tr *ngFor=\"let d of order let i = index\">\n                  <td>{{i+1}}</td>\n                  <td>{{d.order_id}}</td>\n                  <td>{{d.sale_name}}</td>\n                  <!-- <td>\n                    <select *ngIf=\"d.bank != ''\" name=\"bank_selected\" #bbank (change)=\"sbank(bbank.value,d.order_id)\" [disabled]=\"d.status>3\" >\n                      <option [value]='noselect' >เลือกบัญชีธนาคาร</option>\n                      <option *ngFor=\"let b of d.bank\" [value]='b.bb_id'>{{b.no}} ({{b.bank}})</option>\n                    </select>\n                    <span *ngIf=\"d.bank == ''\">ไม่พบเลขที่บัญชี</span>\n                  </td> -->\n                  <td>{{d.com_price|number}} บาท </td>\n                  <!-- <td>1/1/2020</td> -->\n                  <td>\n                    <span *ngIf=\"d.status == 3\" class=\"badge badge-secondary\">รอการโอน</span>\n                    <span *ngIf=\"d.status == 4\" class=\"badge badge-success\">โอนสำเร็จ</span>\n                  </td>\n                  <td>\n                    <img style=\"width: 60px;\" *ngIf=\"d.status == 4\" src=\"{{d.pic}}\" (click)=\"open(d)\">\n                    <span *ngIf=\"d.status == 3\">รอการโอน</span>\n                  </td>\n                  <td>\n                    <button [disabled]=\"d.status>3 || admin.type > 2\" href=\"javascript:void(0)\"\n                      class=\"btn btn-raised mr-1 btn-primary btn-sm\" (click)=\"continue(d)\"><i class=\"fa fa-check\"></i>\n                      แนบหลักฐาน</button>\n                    <button [disabled]=\"true\" href=\"javascript:void(0)\" (click)=\"cancel()\"\n                      class=\"btn btn-raised mr-1 btn-sm btn-danger\"><i class=\"fa fa-times\"></i> ปฏิเสธ</button>\n                  </td>\n                </tr>\n\n              </tbody>\n            </table>\n            <div class=\"col-12 justify-content-center\">\n              <ngb-pagination [collectionSize]=\"all_page\" (pageChange)=\"change_page($event)\"\n                class=\"justify-content-center\" [pageSize]='20' [(page)]=\"page\"></ngb-pagination>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</section>\n<!--Hoverable rows Ends-->"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/confirm/confirm_details.html":
/*!************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/confirm/confirm_details.html ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"modal-header\">\r\n    <h4 class=\"modal-title\">การโอนเงินให้ผู้ขาย</h4>\r\n    <button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"close()\">\r\n        <span aria-hidden=\"true\">&times;</span>\r\n    </button>\r\n</div>\r\n<div class=\"modal-body\">\r\n\r\n    <div class=\"content\">\r\n        <select name=\"bank_selected\" #bbank (change)=\"sbank(bbank.value)\">\r\n            <option [value]='noselect'>เลือกบัญชีธนาคาร</option>\r\n            <option *ngFor=\"let b of data.bank\" [value]='b.bb_id'>{{b.no}} ({{b.bank}})</option>\r\n        </select>\r\n    </div><br>\r\n    <div>\r\n        <img [src]=\"url\">\r\n    </div><br>\r\n    <div>\r\n        <input style=\"display: none;\" #uploadImg id=\"slipinput\" type=\"file\" accept=\"image/*\"\r\n            (change)=\"changeImg($event)\" hidden>\r\n        <button type=\"button\" class=\"btn btn-raised mr-1 btn-primary\" (click)=\"uploadImg.click()\">แนบหลักฐาน</button>\r\n    </div>\r\n</div>\r\n<div class=\"modal-footer\">\r\n    <button type=\"button\" class=\"btn btn-raised mr-1 btn-primary\" (click)=\"confirm()\">ยืนยัน</button>\r\n    <button type=\"button\" class=\"btn btn-secondary btn-raised\" (click)=\"close()\">ปิดออก</button>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/customer/add_customer.html":
/*!**********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/customer/add_customer.html ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<form [formGroup]=\"customerForm\" (ngSubmit)=\"onReactiveFormSubmit()\" novalidate>\r\n    <div class=\"modal-header\">\r\n        <h4 class=\"modal-title\">แก้ไขข้อมูล</h4>\r\n        <button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"close()\">\r\n            <span aria-hidden=\"true\">&times;</span>\r\n        </button>\r\n    </div>\r\n    <div class=\"modal-body\">\r\n        <div class=\"w-100 text-center\">\r\n            <img class=\"avatar\" src=\"https://image.flaticon.com/icons/svg/194/194938.svg\" [src]=\"para.picture\">\r\n            <a class=\"change_img\" (click)=\"change_picture()\">\r\n                <i class=\"fa fa-camera\"></i>\r\n            </a>\r\n            <input type=\"file\" style=\"display: none;\" #file (change)=\"file_change($event)\">\r\n        </div>\r\n        <fieldset class=\"form-group mt-3\">\r\n            <label for=\"name\">ชื่อ - นามสกุล</label>\r\n            <input type=\"text\" id=\"name\"\r\n                [ngClass]=\"{'invalid':!customerForm.get('name').valid && (customerForm.get('name').dirty || customerForm.get('name').touched)}\"\r\n                formControlName=\"name\" name=\"name\" class=\"form-control round\">\r\n            <small class=\"form-text text-muted danger\"\r\n                *ngIf=\"!customerForm.get('name').valid && (customerForm.get('name').dirty || customerForm.get('name').touched)\">กรุณากรอก\r\n                ชื่อ - นามสกุล ให้ถูกต้อง</small>\r\n        </fieldset>\r\n        <fieldset class=\"form-group\">\r\n            <label for=\"email\">อีเมล์</label>\r\n            <input type=\"email\" id=\"email\"\r\n                [ngClass]=\"{'invalid':!customerForm.get('email').valid && (customerForm.get('email').dirty || customerForm.get('email').touched)}\"\r\n                formControlName=\"email\" name=\"email\" class=\"form-control round\">\r\n            <small class=\"form-text text-muted danger\"\r\n                *ngIf=\"!customerForm.get('email').valid && (customerForm.get('email').dirty || customerForm.get('email').touched)\">กรุณากรอกอีเมล์ให้ถูกต้อง</small>\r\n        </fieldset>\r\n        <fieldset class=\"form-group\">\r\n            <label for=\"tel\">เบอร์โทรศัพท์</label>\r\n            <input type=\"text\" id=\"phone\"\r\n                [ngClass]=\"{'invalid':!customerForm.get('phone').valid && (customerForm.get('phone').dirty || customerForm.get('phone').touched)}\"\r\n                formControlName=\"phone\" class=\"form-control round\">\r\n            <small class=\"form-text text-muted danger\"\r\n                *ngIf=\"!customerForm.get('phone').valid && (customerForm.get('phone').dirty || customerForm.get('phone').touched)\">กรุณากรอกเบอร์โทรศัพท์ให้ถูกต้อง</small>\r\n        </fieldset>\r\n        <fieldset class=\"form-group\">\r\n            <label for=\"tel\">รหัสผ่าน</label>\r\n            <input type=\"password\" id=\"password\"\r\n                formControlName=\"password\" class=\"form-control round\">\r\n        </fieldset>\r\n        <fieldset class=\"form-group\">\r\n            <label for=\"gender\">เพศ</label>\r\n            <select id=\"gender\" name=\"gender\" formControlName=\"gender\" class=\"form-control round\">\r\n                <option selected value=\"m\">ชาย</option>\r\n                <option value=\"f\">หญิง</option>\r\n            </select>\r\n        </fieldset>\r\n    </div>\r\n    <div class=\"modal-footer\">\r\n        <button type=\"button\" class=\"btn btn-secondary btn-raised\" (click)=\"close()\">ปิดออก</button>\r\n        <button type=\"submit\" class=\"btn btn-success mr-2 btn-raised \">บันทึก</button>\r\n    </div>\r\n</form>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/customer/customer.component.html":
/*!****************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/customer/customer.component.html ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ng-template #view let-c=\"close\" let-d=\"dismiss\">\n\n</ng-template>\n<div class=\"row text-left\">\n  <div class=\"col-12\">\n    <div class=\"content-header\">ลูกค้า</div>\n    <!-- <p class=\"content-sub-header\">All table styles are inherited in Bootstrap 4, meaning any nested tables will be\n      styled in the same manner as the parent.</p> -->\n  </div>\n</div>\n<!--Basic Table Ends-->\n\n<!--Hoverable Rows Starts-->\n<section id=\"hoverable-rows\">\n  <div class=\"row text-left\">\n    <div class=\"col-sm-12\">\n      <div class=\"card\">\n        <div class=\"card-content\">\n          <div class=\"card-body\">\n            <div class=\"text-right\" style=\"margin-top: -10px; margin-bottom: 55px;\">\n              <form class=\"navbar-form navbar-right mt-1\" [formGroup]=\"searchForm\" (ngSubmit)=\"search()\">\n                <div class=\"position-relative has-icon-right\">\n                  <input class=\"form-control round\" formControlName=\"search\" placeholder=\"Search\" type=\"text\">\n                  <div class=\"form-control-position\">\n                    <i class=\"ft-search\"></i>\n                  </div>\n                </div>\n              </form>\n              <!-- <a href=\"javascript:void(0)\" class=\"btn btn-raised mr-1 btn-round btn-primary\"><i class=\"fa fa-plus\"></i>\n                เพิ่มทีมงาน</a> -->\n            </div>\n\n            <table class=\"table table-hover\">\n              <thead>\n                <tr>\n                  <th style=\"width: 50px;\">ID</th>\n                  <th style=\"width: 60px;\" class=\"text-center\">ภาพ</th>\n                  <th>ชื่อ - สกุล</th>\n                  <th>ที่อยู่</th>\n                  <th>เบอร์โทรศัพท์</th>\n                  <th style=\"width: 200px;\" class=\"text-center\">จัดการ</th>\n                </tr>\n              </thead>\n              <tbody>\n                <tr *ngFor=\"let t of customers\">\n                  <th scope=\"row\">{{t.member_id}}</th>\n                  <td style=\"padding: 2px;\"><img style=\"width: 50px;\" [src]=\"t.picture\" src=\"{{t.picture}}\"></td>\n                  <td>{{t.name}}</td>\n                  <td>{{t.address}}</td>\n                  <td>{{t.phone}}</td>\n                  <td>\n                    <button href=\"javascript:void(0)\" class=\"btn btn-raised mr-1 btn-primary btn-sm\"\n                      (click)=\"view_profile(t)\"><i class=\"fa fa-eye\"></i> ดู</button>\n                    <a href=\"javascript:void(0)\" (click)=\"open(t)\" class=\"btn btn-raised mr-1 btn-sm btn-warning\"><i\n                        class=\"fa fa-edit\"></i> แก้ไข</a>\n                    <a href=\"javascript:void(0)\" (click)=\"delete_member()\"\n                      class=\"btn btn-raised mr-1 btn-sm btn-danger\"><i class=\"fa fa-trash-o\"></i> ลบ</a>\n                  </td>\n                </tr>\n\n              </tbody>\n            </table>\n            <div class=\"col-12 justify-content-center\">\n              <ngb-pagination [collectionSize]=\"all_page\" (pageChange)=\"change_page($event)\" class=\"justify-content-center\"\n               [pageSize]='20' [(page)]=\"page\"></ngb-pagination>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</section>\n<!--Hoverable rows Ends-->"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/customer/view_customer.html":
/*!***********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/customer/view_customer.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"modal-header\">\r\n    <h4 class=\"modal-title\">{{para.name}}</h4>\r\n    <button type=\"button\" class=\"close\" aria-label=\"Close\"  (click)=\"close()\">\r\n      <span aria-hidden=\"true\">&times;</span>\r\n    </button>\r\n  </div>\r\n  <div class=\"modal-body\">\r\n    <div class=\"w-100 text-center\">\r\n      <img style=\"width: 80px;\" src=\"{{para.picture}}\">\r\n    </div>\r\n\r\n    <div class=\"content\">\r\n      <dl>\r\n        <dt>ID </dt>\r\n        <dd>#{{para.member_id}}</dd>\r\n        <dt>ชื่อ - นามสกุล </dt>\r\n        <dd>{{para.name}}</dd>\r\n        <dt>อีเมล์ </dt>\r\n        <dd>{{para.email}}</dd>\r\n        <dt>เบอร์โทรศัพท์ </dt>\r\n        <dd>{{para.phone}}</dd>\r\n        <dt>ที่อยู่ </dt>\r\n        <dd>{{para.address}}</dd>\r\n      </dl>\r\n    </div>\r\n  </div>\r\n  <div class=\"modal-footer\">\r\n    <button type=\"button\" class=\"btn btn-secondary btn-raised\" (click)=\"close()\">ปิดออก</button>\r\n  </div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layouts/content/content-layout.component.html":
/*!*****************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layouts/content/content-layout.component.html ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"content-wrapper p-0\" [dir]=\"direction\">\n  <router-outlet></router-outlet>\n</div>\n\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layouts/full/full-layout.component.html":
/*!***********************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layouts/full/full-layout.component.html ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div\n  #wrapper\n  class=\"wrapper\"\n  [ngClass]=\"{\n    'nav-collapsed': iscollapsed,\n    'menu-collapsed': iscollapsed,\n    'sidebar-sm': isSidebar_sm,\n    'sidebar-lg': isSidebar_lg\n  }\"\n  [dir]=\"options.direction\"\n>\n  <div\n    #appSidebar\n    appSidebar\n    class=\"app-sidebar\"\n    (toggleHideSidebar)=\"toggleHideSidebar($event)\"\n    [ngClass]=\"{ 'hide-sidebar': hideSidebar }\"\n    data-active-color=\"white\"\n    [attr.data-background-color]=\"bgColor\"\n    [attr.data-image]=\"bgImage\"\n  >\n    <app-sidebar></app-sidebar>\n    <div class=\"sidebar-background\" #sidebarBgImage></div>\n  </div>\n  <app-navbar (toggleHideSidebar)=\"toggleHideSidebar($event)\"></app-navbar>\n  <div class=\"main-panel\">\n    <div class=\"main-content\">\n      <div class=\"content-wrapper\">\n        <div class=\"container-fluid\">\n          <router-outlet></router-outlet>\n        </div>\n      </div>\n    </div>\n    <app-footer></app-footer>\n  </div>\n  <app-notification-sidebar></app-notification-sidebar>\n  <!-- <app-customizer (directionEvent)=\"getOptions($event)\"></app-customizer> -->\n</div>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/news/add_news.html":
/*!**************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/news/add_news.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<form [formGroup]=\"newsForm\" (ngSubmit)=\"onReactiveFormSubmit()\" novalidate>\r\n    <div class=\"modal-header\">\r\n        <h4 class=\"modal-title\">เพิ่มข่าวสาร/โปรโมชั่น</h4>\r\n        <button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"close()\">\r\n            <span aria-hidden=\"true\">&times;</span>\r\n        </button>\r\n    </div>\r\n    <div class=\"modal-body\">\r\n        <fieldset class=\"form-group mt-3\" formGroupName=\"title\">\r\n            <label for=\"title\">หัวข้อข่าว (th) </label>\r\n            <input type=\"text\" formControlName=\"th\" name=\"th\" class=\"form-control round\">\r\n            <label for=\"title\">หัวข้อข่าว (en)</label>\r\n            <input type=\"text\" formControlName=\"en\" name=\"en\" class=\"form-control round\">\r\n            <!-- <small class=\"form-text text-muted danger\"\r\n                *ngIf=\"!productForm.get('p_name').valid && (productForm.get('p_name').dirty || productForm.get('p_name').touched)\">กรุณากรอกผู้ใช้งานให้ถูกต้อง</small> -->\r\n        </fieldset>\r\n        <fieldset class=\"form-group\" formGroupName=\"content\">\r\n            <label for=\"detail\">รายละเอียด (th)</label>\r\n            <ckeditor id=\"detailth\" formControlName=\"th\" [config]=\"editorConfig\" [editor]=\"Editor\"></ckeditor>\r\n            <!-- <textarea class=\"form-control\" id=\"detailth\" name=\"detailth\" formControlName=\"th\" rows=\"5\"></textarea> -->\r\n            \r\n            <!-- <script>\r\n                CKEDITOR.replace('detailth');\r\n\r\n                $(document).ready(function () {\r\n\r\n                    $.fn.modal.Constructor.prototype.enforceFocus = function () {\r\n                        modal_this = this\r\n                        $(document).on('focusin.modal', function (e) {\r\n                            if (modal_this.$element[0] !== e.target && !modal_this.$element.has(e.target).length\r\n                                // add whatever conditions you need here:\r\n                                &&\r\n                                !$(e.target.parentNode).hasClass('cke_dialog_ui_input_select') && !$(e.target.parentNode).hasClass('cke_dialog_ui_input_text')) {\r\n                                modal_this.$element.focus()\r\n                            }\r\n                        })\r\n                    };\r\n\r\n                });\r\n            </script> -->\r\n            <label for=\"detail\">รายละเอียด (en)</label>\r\n            <ckeditor id=\"dtailen\" formControlName=\"en\" [config]=\"editorConfig\" [editor]=\"Editor\"></ckeditor>\r\n            <!-- <textarea class=\"form-control\" id=\"dtailen\" formControlName=\"en\" rows=\"5\"></textarea> -->\r\n        </fieldset>\r\n        <fieldset formGroupName=\"show\">\r\n            <div>\r\n                <input formControlName=\"show\" type=\"checkbox\">\r\n                แสดงในข่าวสาร/โปรโมชั่น\r\n            </div>\r\n        </fieldset>\r\n    </div>\r\n    <div class=\"modal-footer\">\r\n        <button type=\"button\" class=\"btn btn-secondary btn-raised\" (click)=\"close()\">ปิดออก</button>\r\n        <button type=\"submit\" class=\"btn btn-success mr-2 btn-raised \">บันทึก</button>\r\n    </div>\r\n</form>\r\n<!-- <script>\r\n    $.fn.modal.Constructor.prototype.enforceFocus = function () {\r\n        modal_this = this\r\n        $(document).on('focusin.modal', function (e) {\r\n            if (modal_this.$element[0] !== e.target && !modal_this.$element.has(e.target).length\r\n                && !$(e.target.parentNode).hasClass('cke_dialog_ui_input_select')\r\n                && !$(e.target.parentNode).hasClass('cke_dialog_ui_input_text')) {\r\n                modal_this.$element.focus()\r\n            }\r\n        })\r\n    };\r\n</script> -->"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/news/news.component.html":
/*!********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/news/news.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ng-template #view let-c=\"close\" let-d=\"dismiss\">\n\n</ng-template>\n<div class=\"row text-left\">\n  <div class=\"col-12\">\n    <div class=\"content-header\">ข่าวสาร/โปรโมชั่น</div>\n    <!-- <p class=\"content-sub-header\">All table styles are inherited in Bootstrap 4, meaning any nested tables will be\n      styled in the same manner as the parent.</p> -->\n  </div>\n</div>\n<!--Basic Table Ends-->\n\n\n<!--Hoverable Rows Starts-->\n<section id=\"hoverable-rows\">\n  <div class=\"row text-left\">\n    <div class=\"col-sm-12\">\n      <div class=\"card\">\n        <div class=\"card-content\">\n          <div class=\"card-body\">\n            <div class=\"text-right navbar-right\" style=\"margin-top: -10px; margin-bottom: 10px;\">\n              <form class=\"navbar-form  mt-1 searchform\" [formGroup]=\"searchForm\" (ngSubmit)=\"search()\">\n                <div class=\"position-relative has-icon-right\">\n                  <input class=\"form-control round\" formControlName=\"search\" placeholder=\"Search\" type=\"text\">\n                  <div class=\"form-control-position\">\n                    <i class=\"ft-search\"></i>\n                  </div>\n                </div>\n              </form>\n              <button style=\"margin-bottom: 5px;\" class=\"btn btn-raised mr-1 btn-primary\" (click)=\"open({t_id:'0'})\"><i\n                  class=\"fa fa-plus\"></i>\n                เพิ่มข่าวสาร</button>\n            </div>\n            <table class=\"table table-hover\">\n              <thead>\n                <tr>\n                  <th style=\"width: 50px;\">#</th>\n                  <th>หัวข้อ</th>\n                  <th>เนื้อหา</th>\n                  <th>วันที่ลง</th>\n                  <th>สถานะ</th>\n                  <th style=\"width: 160px;\" class=\"text-center\">จัดการ</th>\n                </tr>\n              </thead>\n              <tbody>\n                <tr *ngFor=\"let t of news\">\n                  <th scope=\"row\">{{t.n_id}}</th>\n\n                  <td>{{t.title.th}}</td>\n                  <td ><p [innerHTML]=\"t.content.th\"></p></td>\n                  <td>{{t.date_time}}</td>\n                  <td>\n                    <span *ngIf=\"t.show==true\" class=\"badge badge-success\" >แสดง</span>\n                    <span *ngIf=\"t.show==false\" class=\"badge badge-secondary\">ซ่อน</span>\n                  </td>\n                  <td>\n                    <button href=\"javascript:void(0)\" (click)=\"open(t)\" class=\"btn btn-raised mr-1 btn-sm btn-warning\"><i\n                        class=\"fa fa-edit\"></i> แก้ไข</button>\n                    <button href=\"javascript:void(0)\" (click)=\"delete_member(t)\"\n                      class=\"btn btn-raised mr-1 btn-sm btn-danger\"><i class=\"fa fa-trash-o\"></i> ลบ</button>\n                  </td>\n                </tr>\n\n              </tbody>\n            </table>\n            <div class=\"col-12 justify-content-center\">\n              <ngb-pagination [collectionSize]=\"all_page\" ng-change=\"change_page(page)\" class=\"justify-content-center\"\n                [(page)]=\"page\"></ngb-pagination>\n\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</section>\n<!--Hoverable rows Ends-->"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/news/view_news.html":
/*!***************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/news/view_news.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"modal-header\">\r\n  <h4 class=\"modal-title\">ข่าวสาร #{{viewnews.n_id}}</h4>\r\n  <button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"close()\">\r\n    <span aria-hidden=\"true\">&times;</span>\r\n  </button>\r\n</div>\r\n<div class=\"modal-body\">\r\n  <div class=\"w-100 text-center\">\r\n    <img style=\"width: 80px;\" src=\"{{para.picture}}\">\r\n  </div>\r\n\r\n  <div class=\"content\">\r\n    <dl>\r\n      <dt>ชื่อผู้ใช้ </dt>\r\n      <dd>{{viewnews.n_id}}</dd>\r\n      <dt>หัวข้อ </dt>\r\n      <dd>{{viewnews.content.th}}</dd>\r\n      <dt>เนื้อหา </dt>\r\n      <dd>{{para.email}}</dd>\r\n      <dt>เบอร์โทรศัพท์ </dt>\r\n      <dd>{{para.phone}}</dd>\r\n      <dt>เพศ </dt>\r\n      <dd><span *ngIf=\"para.gender=='m'\">ชาย</span><span *ngIf=\"para.gender=='f'\">หญิง</span></dd>\r\n    </dl>\r\n  </div>\r\n</div>\r\n<div class=\"modal-footer\">\r\n  <button type=\"button\" class=\"btn btn-secondary btn-raised\" (click)=\"close()\">ปิดออก</button>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/order/add_order.html":
/*!****************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/order/add_order.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<form [formGroup]=\"teamForm\" (ngSubmit)=\"onReactiveFormSubmit()\" novalidate>\r\n    <div class=\"modal-header\">\r\n        <h4 class=\"modal-title\">เพิ่มทีมงานใหม่</h4>\r\n        <button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"close()\">\r\n            <span aria-hidden=\"true\">&times;</span>\r\n        </button>\r\n    </div>\r\n    <div class=\"modal-body\">\r\n        <div class=\"w-100 text-center\">\r\n            <img class=\"avatar\" src=\"https://image.flaticon.com/icons/svg/194/194938.svg\" [src]=\"para.picture\">\r\n            <a class=\"change_img\" (click)=\"change_picture()\">\r\n                <i class=\"fa fa-camera\"></i>\r\n            </a>\r\n            <input type=\"file\" style=\"display: none;\" #file (change)=\"file_change($event)\">\r\n        </div>\r\n        <fieldset class=\"form-group mt-3\">\r\n            <label for=\"username\">ชื่อผู้ใช้</label>\r\n            <input type=\"text\" id=\"username\"\r\n                [ngClass]=\"{'invalid':!teamForm.get('username').valid && (teamForm.get('username').dirty || teamForm.get('username').touched)}\"\r\n                formControlName=\"username\" name=\"username\" class=\"form-control round\">\r\n            <small class=\"form-text text-muted danger\"\r\n                *ngIf=\"!teamForm.get('username').valid && (teamForm.get('username').dirty || teamForm.get('username').touched)\">กรุณากรอกผู้ใช้งานให้ถูกต้อง</small>\r\n        </fieldset>\r\n        <fieldset class=\"form-group\">\r\n            <label for=\"password\">รหัสผ่าน</label>\r\n            <input type=\"password\" id=\"password\"\r\n                [ngClass]=\"{'invalid':!teamForm.get('password').valid && (teamForm.get('password').dirty || teamForm.get('password').touched)}\"\r\n                formControlName=\"password\" name=\"password\" class=\"form-control round\">\r\n            <small class=\"form-text text-muted danger\"\r\n                *ngIf=\"!teamForm.get('password').valid && (teamForm.get('password').dirty || teamForm.get('password').touched)\">กรุณากรอกรหัสผ่านให้ถูกต้อง</small>\r\n        </fieldset>\r\n        <fieldset class=\"form-group\">\r\n            <label for=\"name\">ชื่อ - นามสกุล</label>\r\n            <input type=\"text\" id=\"name\"\r\n                [ngClass]=\"{'invalid':!teamForm.get('name').valid && (teamForm.get('name').dirty || teamForm.get('name').touched)}\"\r\n                formControlName=\"name\" name=\"name\" class=\"form-control round\">\r\n            <small class=\"form-text text-muted danger\"\r\n                *ngIf=\"!teamForm.get('name').valid && (teamForm.get('name').dirty || teamForm.get('name').touched)\">กรุณากรอก\r\n                ชื่อ - นามสกุล ให้ถูกต้อง</small>\r\n        </fieldset>\r\n        <fieldset class=\"form-group\">\r\n            <label for=\"email\">อีเมล์</label>\r\n            <input type=\"email\" id=\"email\"\r\n                [ngClass]=\"{'invalid':!teamForm.get('email').valid && (teamForm.get('email').dirty || teamForm.get('email').touched)}\"\r\n                formControlName=\"email\" name=\"email\" class=\"form-control round\">\r\n            <small class=\"form-text text-muted danger\"\r\n                *ngIf=\"!teamForm.get('email').valid && (teamForm.get('email').dirty || teamForm.get('email').touched)\">กรุณากรอกอีเมล์ให้ถูกต้อง</small>\r\n        </fieldset>\r\n        <fieldset class=\"form-group\">\r\n            <label for=\"tel\">เบอร์โทรศัพท์</label>\r\n            <input type=\"text\" id=\"phone\"\r\n                [ngClass]=\"{'invalid':!teamForm.get('phone').valid && (teamForm.get('phone').dirty || teamForm.get('phone').touched)}\"\r\n                formControlName=\"phone\" class=\"form-control round\">\r\n            <small class=\"form-text text-muted danger\"\r\n                *ngIf=\"!teamForm.get('phone').valid && (teamForm.get('phone').dirty || teamForm.get('phone').touched)\">กรุณากรอกเบอร์โทรศัพท์ให้ถูกต้อง</small>\r\n        </fieldset>\r\n        <fieldset class=\"form-group\">\r\n            <label for=\"gender\">เพศ</label>\r\n            <select id=\"gender\" name=\"gender\" formControlName=\"gender\" class=\"form-control round\">\r\n                <option selected value=\"m\">ชาย</option>\r\n                <option value=\"f\">หญิง</option>\r\n            </select>\r\n        </fieldset>\r\n        <fieldset class=\"form-group\">\r\n            <label for=\"type\">ประเภทผู้ใช้งาน</label>\r\n            <select id=\"type\" name=\"type\" formControlName=\"type\" class=\"form-control round\">\r\n                <option selected value=\"1\">Admin</option>\r\n                <option value=\"2\">User</option>\r\n                <option value=\"3\">Accounting</option>\r\n            </select>\r\n        </fieldset>\r\n    </div>\r\n    <div class=\"modal-footer\">\r\n        <button type=\"button\" class=\"btn btn-secondary btn-raised\" (click)=\"close()\">ปิดออก</button>\r\n        <button type=\"submit\" class=\"btn btn-success mr-2 btn-raised \">บันทึก</button>\r\n    </div>\r\n</form>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/order/order.component.html":
/*!**********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/order/order.component.html ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ng-template #view let-c=\"close\" let-d=\"dismiss\">\n\n</ng-template>\n<div class=\"row text-left\">\n  <div class=\"col-12\">\n    <div class=\"content-header\">ออเดอร์</div>\n    <!-- <p class=\"content-sub-header\">All table styles are inherited in Bootstrap 4, meaning any nested tables will be\n      styled in the same manner as the parent.</p> -->\n  </div>\n</div>\n<!--Basic Table Ends-->\n\n<!--Hoverable Rows Starts-->\n<section id=\"hoverable-rows\">\n  <div class=\"row text-left\">\n    <div class=\"col-sm-12\">\n      <div class=\"card\">\n        <div class=\"card-content\">\n\n          <div class=\"card-body\">\n            <div class=\"text-right\" style=\"margin-top: -10px; margin-bottom: 55px;\">\n              <form class=\"navbar-form navbar-right mt-1\" [formGroup]=\"searchForm\" (ngSubmit)=\"search()\">\n                <div class=\"position-relative has-icon-right\">\n                  <input class=\"form-control round\" formControlName=\"search\" placeholder=\"Search\" type=\"text\">\n                  <div class=\"form-control-position\">\n                    <i class=\"ft-search\"></i>\n                  </div>\n                </div>\n              </form>\n\n            </div>\n\n            <table class=\"table table-hover\">\n              <thead>\n                <tr>\n                  <th style=\"width: 50px;\">ID</th>\n                  <th style=\"width: 80px;\" class=\"text-center\">ภาพ</th>\n                  <th>ชื่อสินค้า</th>\n                  <th>จำนวน</th>\n                  <th>สถานที่ส่ง</th>\n                  <th>สถานะ</th>\n                  <th style=\"width: 200px;\" class=\"text-center\">จัดการ</th>\n                </tr>\n              </thead>\n              <tbody>\n                <tr *ngFor=\"let t of orders\">\n                  <th scope=\"row\">{{t.order_id}}</th>\n                  <td style=\"padding: 2px;\"><img style=\"width: 80px;\" src=\"{{t.p_img}}\"></td>\n                  <td>{{t.p_name}}</td>\n                  <td>{{t.count}} คิว</td>\n                  <td>{{t.address}}</td>\n                  <td>\n                    <span *ngIf=\"t.status=='-2'\" class=\"badge badge-danger\">ชำระเงินไม่สำเร็จ</span>\n                    <span *ngIf=\"t.status=='-1'\" class=\"badge badge-dark\">ถูกยกเลิก</span>\n                    <span *ngIf=\"t.status=='0'\" class=\"badge badge-secondary\">รอการชำระเงิน</span>\n                    <span *ngIf=\"t.status=='1'\" class=\"badge badge-primary\">รอการจัดส่ง</span>\n                    <span *ngIf=\"t.status=='2'\" class=\"badge badge-primary\">กำลังส่ง</span>\n                    <span *ngIf=\"t.status=='3'\" class=\"badge badge-success\">ได้รับแล้ว</span>\n                    <span *ngIf=\"t.status=='4'\" class=\"badge badge-success\">สำเร็จ</span>\n                  </td>\n                  <td>\n                    <button href=\"javascript:void(0)\" class=\"btn btn-raised mr-1 btn-primary btn-sm\"\n                      (click)=\"view_profile(t.order_id)\"><i class=\"fa fa-eye\"></i> รายละเอียด</button>\n                    <button href=\"javascript:void(0)\" (click)=\"cancel_order(t.order_id)\"\n                      [disabled]=\"t.status=='-1'\" class=\"btn btn-raised mr-1 btn-sm btn-danger\"><i class=\"fa fa-times\"></i> ยกเลิก</button>\n                  </td>\n                </tr>\n\n              </tbody>\n            </table>\n            <div class=\"col-12 justify-content-center\">\n              <ngb-pagination [collectionSize]=\"all_page\" (pageChange)=\"change_page($event)\" class=\"justify-content-center\"\n              [pageSize]='20' [(page)]=\"page\"></ngb-pagination>\n\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</section>\n<!--Hoverable rows Ends-->"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/order/view_order.html":
/*!*****************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/order/view_order.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"modal-header\">\r\n  <h4 class=\"modal-title\">Order ID #{{details.order_data.order_id}} \r\n    <span *ngIf=\"details.order_data.status=='-1'\">ถูกยกเลิก</span>\r\n    <span *ngIf=\"details.order_data.status=='0'\">รอการชำระเงิน</span>\r\n    <span *ngIf=\"details.order_data.status=='1'\">รอการจัดส่ง</span>\r\n    <span *ngIf=\"details.order_data.status=='2'\">สำเร็จแล้ว</span>\r\n  </h4>\r\n  <button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"close()\">\r\n    <span aria-hidden=\"true\">&times;</span>\r\n  </button>\r\n</div>\r\n<div class=\"modal-body\">\r\n  <div class=\"content\">\r\n    <dl>\r\n      <dt>Order ID </dt>\r\n      <dd>#{{details.order_data.order_id}}</dd>\r\n      <dt>ลูกค้า </dt>\r\n      <dd>{{details.order_data.name}}</dd>\r\n      <dt>สินค้า </dt>\r\n      <dd>{{details.order_data.p_name.th}}</dd>\r\n      <dt>จำนวน </dt>\r\n      <dd>{{details.order_data.count}} คิว</dd>\r\n      <dt>การขนส่ง </dt>\r\n      <dd>\r\n        <span *ngIf=\"details.order_data.car=='1'\">รถโม่เล็ก จำนวน {{details.order_data.num_diff}} รอบ</span>\r\n        <span *ngIf=\"details.order_data.car=='2'\">รถโม่ใหญ่ จำนวน {{details.order_data.num_diff}} รอบ</span>\r\n      </dd>\r\n      <dt>วันที่จัดส่ง </dt>\r\n      <dd>{{details.order_data.send_date}}</dd>\r\n      <dt>เวลาที่จัดส่ง </dt>\r\n      <dd>{{details.order_data.send_time}}</dd>\r\n      <dt>สถานที่จัดส่ง </dt>\r\n      <dd>{{details.order_data.address}} ตำบล{{details.order_data.subdistrict}} อำเภอ{{details.order_data.district}} จังหวัด{{details.order_data.province}}</dd>\r\n    </dl>\r\n  </div>\r\n  <table class=\"table table-hover\">\r\n    <thead>\r\n      <tr>\r\n        <th>#</th>\r\n        <th>ชื่อผู้ประมูล</th>\r\n        <th>ราคา/คิว</th>\r\n        <th>ค่าขนส่ง/รอบ</th>\r\n        <th>ราคารวม</th>\r\n        <th>รวมทั้งสิ้น(+{{details.order_data.rate}}%)</th>\r\n        <th>วัน-เวลาที่ประมูล</th>\r\n      </tr>\r\n    </thead>\r\n    <tbody>\r\n      <tr *ngFor=\"let t of action;let i = index\" [ngClass]=\"{'win': i == 0}\" >\r\n        <td>{{i+1}}</td>\r\n        <td>{{t.name}}</td>\r\n        <td>{{t.ap_price|number}} บาท</td>\r\n        <td>{{t.ap_diff_price|number}} บาท</td>\r\n        <td>{{t.com_price|number}} บาท</td>\r\n        <td>{{t.sumprice|number}} บาท</td>\r\n        <td>{{t.create_date}}</td>\r\n      </tr>\r\n    </tbody>\r\n  </table>\r\n</div>\r\n<div class=\"modal-footer\">\r\n  <button type=\"button\" class=\"btn btn-secondary btn-raised\" (click)=\"close()\" >ปิดออก</button>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/payment/payment.component.html":
/*!**************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/payment/payment.component.html ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ng-template #view let-c=\"close\" let-d=\"dismiss\">\n\n</ng-template>\n<div class=\"row text-left\">\n  <div class=\"col-12\">\n    <div class=\"content-header\">การชำระเงิน</div>\n    <!-- <p class=\"content-sub-header\">All table styles are inherited in Bootstrap 4, meaning any nested tables will be\n      styled in the same manner as the parent.</p> -->\n  </div>\n</div>\n<!--Basic Table Ends-->\n\n<!--Hoverable Rows Starts-->\n<section id=\"hoverable-rows\">\n  <div class=\"row text-left\">\n    <div class=\"col-sm-12\">\n      <div class=\"card\">\n        <div class=\"card-content\">\n\n          <div class=\"card-body\">\n            <div class=\"text-right\" style=\"margin-top: -10px; margin-bottom: 55px;\">\n              <form class=\"navbar-form navbar-right mt-1\" [formGroup]=\"searchForm\" (ngSubmit)=\"search()\">\n                <div class=\"position-relative has-icon-right\">\n                  <input class=\"form-control round\" formControlName=\"search\" placeholder=\"Search\" type=\"text\">\n                  <div class=\"form-control-position\">\n                    <i class=\"ft-search\"></i>\n                  </div>\n                </div>\n              </form>\n            </div>\n            <table class=\"table table-hover\">\n              <thead>\n                <tr>\n                  <th style=\"width: 50px;\">#</th>\n                  <th style=\"width: 80px;\">Order ID</th>\n                  <th style=\"width: 80px;\" class=\"text-center\">รูปภาพ</th>\n                  <th>สินค้า</th>\n                  <th>จำนวน</th>\n                  <th>ชื่อ-สกุล</th>\n                  <th>ราคาสุทธิ</th>\n                  <th style=\"width: 200px;\">สถานะการชำระเงิน</th>\n                  <th style=\"width: 180px;\" class=\"text-center\">จัดการ</th>\n                </tr>\n              </thead>\n              <tbody>\n                <!-- <tr *ngFor=\"let t of orders\"> -->\n                <tr *ngFor=\"let p of payments.data; let i = index\" >\n                  <th scope=\"row\">{{i+1}}</th>\n                  <th scope=\"row\">{{p.order_id}}</th>\n                  <td (click)=\"test(p)\" style=\"padding: 2px;\"><img style=\"width: 80px;\"\n                      src=\"{{p.picture}}\"></td>\n                  <td>{{p.p_name}}</td>\n                  <td>{{p.count}} คิว</td>\n                  <td>{{p.name}}</td>\n                  <td>{{p.price|number}} บาท</td>\n                  <td>\n                    <span *ngIf=\"p.status=='-2'\" class=\"badge badge-danger\">ปฏิเสธแล้ว</span>\n                    <span *ngIf=\"p.status=='-1'\" class=\"badge badge-dark\">ถูกยกเลิก</span>\n                    <span *ngIf=\"p.status=='0'\" class=\"badge badge-secondary\">รอการตรวจสอบ</span>\n                    <span *ngIf=\"p.status=='1'\" class=\"badge badge-success\">ยืนยันแล้ว</span>\n                    <span *ngIf=\"p.status=='2'\" class=\"badge badge-primary\">กำลังส่ง</span>\n                    <span *ngIf=\"p.status=='3'\" class=\"badge badge-success\">ได้รับแล้ว</span>\n                    <span *ngIf=\"p.status=='4'\" class=\"badge badge-success\">สำเร็จแล้ว</span>\n                  </td>\n                  <td>\n                    <!-- <fieldset class=\"form-group\">\n                      <select class=\"form-control\" id=\"basicSelect\">\n                        <option>รอการยืนยัน</option>\n                        <option>ยืนยัน</option>\n                        <option>แก้ไข</option>\n                        <option>ยกเลิก</option>\n                      </select>\n                    </fieldset> -->\n                    <button [disabled]=\"p.status!='0' || p.order_status=='-1' || admin_type > 2\" href=\"javascript:void(0)\" class=\"btn btn-raised mr-1 btn-primary btn-sm\"\n                      (click)=\"confirm(p.pm_id)\"><i class=\"fa fa-check\"></i> ยืนยัน</button>\n                    <button [disabled]=\"p.status!='0' || p.order_status=='-1' || admin_type > 2\" href=\"javascript:void(0)\" (click)=\"cancel(p.pm_id)\"\n                      class=\"btn btn-raised mr-1 btn-sm btn-danger\"><i class=\"fa fa-times\"></i> ปฏิเสธ</button>\n                  </td>\n                </tr>\n\n              </tbody>\n            </table>\n            <div class=\"col-12 justify-content-center\">\n              <ngb-pagination [collectionSize]=\"all_page\" (pageChange)=\"change_page($event)\" class=\"justify-content-center\"\n               [pageSize]='20' [(page)]=\"page\"></ngb-pagination>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</section>\n<!--Hoverable rows Ends-->"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/payment/payment_details.html":
/*!************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/payment/payment_details.html ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"modal-header\">\r\n    <h4 class=\"modal-title\">Payment ID #{{payment.pm_id}}\r\n        <!-- <span *ngIf=\"details.order_data.status=='-1'\">ถูกยกเลิก</span>\r\n      <span *ngIf=\"details.order_data.status=='0'\">รอการชำระเงิน</span>\r\n      <span *ngIf=\"details.order_data.status=='1'\">รอการจัดส่ง</span>\r\n      <span *ngIf=\"details.order_data.status=='2'\">สำเร็จแล้ว</span> -->\r\n    </h4>\r\n    <button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"close()\">\r\n        <span aria-hidden=\"true\">&times;</span>\r\n    </button>\r\n</div>\r\n<div class=\"modal-body\">\r\n    <div class=\"content\">\r\n        <dl>\r\n            <dt>หลักฐานการชำระ </dt>\r\n            <dd><img style=\"width: 420px;\"\r\n                src=\"{{payment.picture}}\"></dd>\r\n            <dt>Payment ID </dt>\r\n            <dd>#{{payment.pm_id}} อัพโหลดเมื่อวันที่ {{payment.time_upload| date:'shortDate'}} เวลา {{payment.time_upload| date:'shortTime'}}</dd>\r\n            <dt>Order ID </dt>\r\n            <dd>#{{payment.order_id}}</dd>\r\n            <dt>ชื่อลูกค้า </dt>\r\n            <dd>{{payment.name}}</dd>\r\n            <dt>การชำระเงิน </dt>\r\n            <dd>\r\n                <span *ngIf=\"payment.order_status == 0\">รอการชำระเงิน</span>\r\n                <span *ngIf=\"payment.order_status == -1\">ยกเลิกแล้ว โดย {{payment.admin}} เมื่อวันที่ {{payment.time_check| date:'shortDate'}} เวลา {{payment.time_check| date:'shortTime'}}</span>\r\n                <span *ngIf=\"payment.order_status == -2\">ถูกปฏิเสธ โดย {{payment.admin}} เมื่อวันที่ {{payment.time_check| date:'shortDate'}} เวลา {{payment.time_check| date:'shortTime'}}</span>\r\n                <span *ngIf=\"payment.order_status == 1\">ยืนยันแล้ว โดย {{payment.admin}} เมื่อวันที่ {{payment.time_check| date:'shortDate'}} เวลา {{payment.time_check| date:'shortTime'}}</span>\r\n            </dd>\r\n            <dt>เบอร์โทรลูกค้า </dt>\r\n            <dd>{{payment.phone}}</dd>\r\n            <dt>email ลูกค้า </dt>\r\n            <dd>{{payment.email}}</dd>\r\n            <dt>ชื่อผู้ขาย </dt>\r\n            <dd>{{payment.sale_name}}</dd>\r\n            <dt>เบอร์โทรผู้ขาย </dt>\r\n            <dd>{{payment.sale_phone}}</dd>\r\n            <dt>สินค้า </dt>\r\n            <dd>{{payment.p_name}}</dd>\r\n            <dt>จำนวน </dt>\r\n            <dd>{{payment.count}} คิว</dd>\r\n            <dt>ราคาสุทธิ </dt>\r\n            <dd>{{payment.price|number}} บาท\r\n                <!-- <span *ngIf=\"details.order_data.car=='1'\">รถโม่เล็ก</span>\r\n          <span *ngIf=\"details.order_data.car=='2'\">รถโม่ใหญ่</span> -->\r\n            </dd>\r\n            <dt>วันที่จัดส่ง </dt>\r\n            <dd>{{payment.send_date}}</dd>\r\n            <dt>เวลาที่จัดส่ง </dt>\r\n            <dd>{{payment.send_time}}</dd>\r\n            <dt>สถานที่จัดส่ง </dt>\r\n            <dd>{{payment.address}}</dd>\r\n        </dl>\r\n    </div>\r\n</div>\r\n<div class=\"modal-footer\">\r\n    <button [disabled]=\"payment.order_status!='0' || admin_type > 2\" href=\"javascript:void(0)\" class=\"btn btn-raised mr-1 btn-primary \"\r\n        (click)=\"confirm(payment.pm_id)\"><i class=\"fa fa-check\"></i> ยืนยัน</button>\r\n    <button [disabled]=\"payment.order_status!='0' || admin_type > 2\" href=\"javascript:void(0)\" (click)=\"cancel(payment.pm_id)\"\r\n        class=\"btn btn-raised mr-1 btn-danger\"><i class=\"fa fa-times\"></i> ปฏิเสธ</button>\r\n    <button class=\"btn btn-secondary btn-raised\" (click)=\"close()\">ปิดออก</button>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/product/add_product.html":
/*!********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/product/add_product.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<form [formGroup]=\"productForm\" (ngSubmit)=\"onReactiveFormSubmit()\" novalidate>\r\n    <div class=\"modal-header\">\r\n        <h4 class=\"modal-title\">เพิ่มสินค้า</h4>\r\n        <button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"close()\">\r\n            <span aria-hidden=\"true\">&times;</span>\r\n        </button>\r\n    </div>\r\n    <div class=\"modal-body\">\r\n        <div class=\"w-100 text-center\">\r\n            <img class=\"avatar\" (click)=\"change_picture()\"\r\n                src=\"http://www.suzukainternational.co.th/wp-content/uploads/2015/07/ECES-2-300x300.jpg\"\r\n                [src]=\"para.p_img\">\r\n            <!-- <a class=\"change_img\" (click)=\"change_picture()\">\r\n                <i class=\"fa fa-camera\"></i>\r\n            </a> -->\r\n            <input type=\"file\" style=\"display: none;\" #file (change)=\"file_change($event)\">\r\n        </div>\r\n        <fieldset class=\"form-group mt-3\" formGroupName=\"p_name\">\r\n            <label for=\"p_name\">ชื่อสินค้า (th) </label>\r\n            <input type=\"text\" formControlName=\"th\" name=\"p_nameth\" class=\"form-control round\">\r\n            <label for=\"p_name\">ชื่อสินค้า (en)</label>\r\n            <input type=\"text\" formControlName=\"en\" name=\"p_nameen\" class=\"form-control round\">\r\n            <!-- <small class=\"form-text text-muted danger\"\r\n                *ngIf=\"!productForm.get('p_name').valid && (productForm.get('p_name').dirty || productForm.get('p_name').touched)\">กรุณากรอกผู้ใช้งานให้ถูกต้อง</small> -->\r\n        </fieldset>\r\n        <fieldset class=\"form-group\" formGroupName=\"how_to_use\">\r\n            <label for=\"password\">วิธีใช้งาน (th)</label>\r\n            <ckeditor class=\"myclass\" id=\"detailth\" formControlName=\"th\" [config]=\"editorConfig\" [editor]=\"Editor\"></ckeditor>\r\n            <!-- <textarea class=\"form-control\" formControlName=\"th\" rows=\"5\"></textarea> -->\r\n            <label for=\"password\">วิธีใช้งาน (en)</label>\r\n            <ckeditor class=\"myclass\" id=\"detailen\" formControlName=\"en\" [config]=\"editorConfig\" [editor]=\"Editor\"></ckeditor>\r\n            <!-- <textarea class=\"form-control\" formControlName=\"en\" rows=\"5\"></textarea> -->\r\n        </fieldset>\r\n        <fieldset class=\"form-group\" formGroupName=\"default_price\">\r\n            <label for=\"price\" class=\"newF\">ราคาต่อคิว (บาท)</label>\r\n            <input type=\"text\" formControlName=\"price\" name=\"price\" class=\"form-control newT round\">\r\n            <label for=\"diff\" class=\"newF\">ราคาขนส่งต่อเทียว (รถโม่เล็ก)</label>\r\n            <input type=\"text\" formControlName=\"diff_sm\" name=\"diff_sm\" class=\"form-control newT round\">.\r\n            <label for=\"diff\" class=\"newF\">ราคาขนส่งต่อเทียว (รถโม่ใหญ่)</label>\r\n            <input type=\"text\" formControlName=\"diff_lg\" name=\"diff_lg\" class=\"form-control newT round\">\r\n\r\n        </fieldset>\r\n        <fieldset formGroupName=\"show\">\r\n            <div>\r\n                <label for=\"price\" class=\"newF\">ลำดับการแสดง</label>\r\n                <input type=\"number\" formControlName=\"sort\" name=\"sort\" class=\"form-control newT round\">\r\n            </div>\r\n            <div>\r\n                <input formControlName=\"show\" type=\"checkbox\">\r\n                แสดงในรายการสินค้า\r\n            </div>\r\n        </fieldset>\r\n        <!-- <fieldset class=\"form-group\">\r\n            <label for=\"name\">ชื่อ - นามสกุล</label>\r\n            <input type=\"text\" id=\"name\"\r\n                [ngClass]=\"{'invalid':!productForm.get('name').valid && (productForm.get('name').dirty || productForm.get('name').touched)}\"\r\n                formControlName=\"name\" name=\"name\" class=\"form-control round\">\r\n            <small class=\"form-text text-muted danger\"\r\n                *ngIf=\"!productForm.get('name').valid && (productForm.get('name').dirty || productForm.get('name').touched)\">กรุณากรอก\r\n                ชื่อ - นามสกุล ให้ถูกต้อง</small>\r\n        </fieldset>\r\n        <fieldset class=\"form-group\">\r\n            <label for=\"email\">อีเมล์</label>\r\n            <input type=\"email\" id=\"email\"\r\n                [ngClass]=\"{'invalid':!productForm.get('email').valid && (productForm.get('email').dirty || productForm.get('email').touched)}\"\r\n                formControlName=\"email\" name=\"email\" class=\"form-control round\">\r\n            <small class=\"form-text text-muted danger\"\r\n                *ngIf=\"!productForm.get('email').valid && (productForm.get('email').dirty || productForm.get('email').touched)\">กรุณากรอกอีเมล์ให้ถูกต้อง</small>\r\n        </fieldset>\r\n        <fieldset class=\"form-group\">\r\n            <label for=\"tel\">เบอร์โทรศัพท์</label>\r\n            <input type=\"text\" id=\"phone\"\r\n                [ngClass]=\"{'invalid':!productForm.get('phone').valid && (productForm.get('phone').dirty || productForm.get('phone').touched)}\"\r\n                formControlName=\"phone\" class=\"form-control round\">\r\n            <small class=\"form-text text-muted danger\"\r\n                *ngIf=\"!productForm.get('phone').valid && (productForm.get('phone').dirty || productForm.get('phone').touched)\">กรุณากรอกเบอร์โทรศัพท์ให้ถูกต้อง</small>\r\n        </fieldset>\r\n        <fieldset class=\"form-group\">\r\n            <label for=\"gender\">เพศ</label>\r\n            <select id=\"gender\" name=\"gender\" formControlName=\"gender\" class=\"form-control round\">\r\n                <option selected value=\"m\">ชาย</option>\r\n                <option value=\"f\">หญิง</option>\r\n            </select>\r\n        </fieldset>\r\n        <fieldset class=\"form-group\">\r\n            <label for=\"type\">ประเภทผู้ใช้งาน</label>\r\n            <select id=\"type\" name=\"type\" formControlName=\"type\" class=\"form-control round\">\r\n                <option selected value=\"1\">Admin</option>\r\n                <option value=\"2\">User</option>\r\n                <option value=\"3\">Accounting</option>\r\n            </select>\r\n        </fieldset> -->\r\n    </div>\r\n    <div class=\"modal-footer\">\r\n        <button type=\"button\" class=\"btn btn-secondary btn-raised\" (click)=\"close()\">ปิดออก</button>\r\n        <button type=\"submit\" class=\"btn btn-success mr-2 btn-raised \">บันทึก</button>\r\n    </div>\r\n</form>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/product/product.component.html":
/*!**************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/product/product.component.html ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row text-left\">\n  <div class=\"col-12\">\n    <div class=\"content-header\">สินค้า</div>\n    <!-- <p class=\"content-sub-header\">All table styles are inherited in Bootstrap 4, meaning any nested tables will be\n      styled in the same manner as the parent.</p> -->\n  </div>\n</div>\n<!--Basic Table Ends-->\n\n<!--Hoverable Rows Starts-->\n<section id=\"hoverable-rows\">\n  <div class=\"row text-left\">\n    <div class=\"col-sm-12\">\n      <div class=\"card\">\n        <div class=\"card-content\">\n\n          <div class=\"card-body\">\n            <div class=\"text-right navbar-right\" style=\"margin-top: -10px; margin-bottom: 10px;\">\n\n              <form class=\"navbar-form  mt-1 searchform\" [formGroup]=\"searchForm\" (ngSubmit)=\"search()\">\n                <div class=\"position-relative has-icon-right\">\n                  <input class=\"form-control round\" formControlName=\"search\" placeholder=\"Search\" type=\"text\">\n                  <div class=\"form-control-position\">\n                    <i class=\"ft-search\"></i>\n                  </div>\n\n                </div>\n\n              </form>\n              <button style=\"margin-bottom: 5px;\" class=\"btn btn-raised mr-1 btn-primary\" (click)=\"open({p_id:'0'})\"><i\n                  class=\"fa fa-plus\"></i>\n                เพิ่มสินค้า</button>\n\n            </div>\n\n            <table class=\"table table-hover\">\n              <thead>\n                <tr>\n                  <th style=\"width: 50px;\">#</th>\n                  <th style=\"width: 120px;\">ลำดับแสดงผล</th>\n                  <th style=\"width: 60px;\" class=\"text-center\">ภาพ</th>\n                  <th>ชื่อสินค้า</th>\n                  <th>รายละเอียด</th>\n                  <th>สถานะ</th>\n                  <th style=\"width: 145px;\" class=\"text-center\">จัดการ</th>\n                </tr>\n              </thead>\n              <tbody>\n                <tr *ngFor=\"let t of products ; let i = index\">\n                  <th scope=\"row\">{{i+1}}</th>\n                  <th scope=\"row\">{{t.sort}}</th>\n                  <td style=\"padding: 2px;\"><img style=\"width: 150px;border-radius: 5px;\" src=\"{{t.p_img}}\"></td>\n                  <td>{{t.p_name.th}}</td>\n                  <td>\n                    <div  [innerHTML]=\"t.how_to_use.th\"></div>\n                  </td>\n                  <td>\n                    <span *ngIf=\"t.show==true\" class=\"badge badge-success\">แสดง</span>\n                    <span *ngIf=\"t.show==false\" class=\"badge badge-secondary\">ซ่อน</span>\n                  </td>\n                  <td>\n                    <!-- <button href=\"javascript:void(0)\" class=\"btn btn-raised mr-1 btn-primary btn-sm\"\n                      (click)=\"view_profile(t)\"><i class=\"fa fa-eye\"></i> ดู</button> -->\n                    <a href=\"javascript:void(0)\" (click)=\"open(t)\" class=\"btn btn-raised mr-1 btn-sm btn-warning\"><i\n                        class=\"fa fa-edit\"></i> แก้ไข</a>\n                    <a href=\"javascript:void(0)\" (click)=\"delete_product(t)\"\n                      class=\"btn btn-raised mr-1 btn-sm btn-danger\"><i class=\"fa fa-trash-o\"></i> ลบ</a>\n                  </td>\n                </tr>\n\n              </tbody>\n            </table>\n            <div class=\"col-12 justify-content-center\">\n              <ngb-pagination [collectionSize]=\"all_page\" (pageChange)=\"change_page($event)\" class=\"justify-content-center\"\n               [pageSize]='20' [(page)]=\"page\"></ngb-pagination>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</section>\n<!--Hoverable rows Ends-->"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/product/view_product.html":
/*!*********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/product/view_product.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"modal-header\">\r\n  <h4 class=\"modal-title\">อาทิตย์ มะณีศรี</h4>\r\n  <button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"close()\">\r\n    <span aria-hidden=\"true\">&times;</span>\r\n  </button>\r\n</div>\r\n<div class=\"modal-body\">\r\n  <div class=\"w-100 text-center\">\r\n    <img style=\"width: 80px;\" src=\"{{para.picture}}\">\r\n  </div>\r\n\r\n  <div class=\"content\">\r\n    <dl>\r\n      <dt>ชื่อผู้ใช้ </dt>\r\n      <dd>{{para.username}}</dd>\r\n      <dt>ชื่อ - นามสกุล </dt>\r\n      <dd>{{para.name}}</dd>\r\n      <dt>อีเมล์ </dt>\r\n      <dd>{{para.email}}</dd>\r\n      <dt>เบอร์โทรศัพท์ </dt>\r\n      <dd>{{para.phone}}</dd>\r\n      <dt>เพศ </dt>\r\n      <dd><span *ngIf=\"para.gender=='m'\">ชาย</span><span *ngIf=\"para.gender=='f'\">หญิง</span></dd>\r\n    </dl>\r\n  </div>\r\n</div>\r\n<div class=\"modal-footer\">\r\n  <button type=\"button\" class=\"btn btn-secondary btn-raised\" (click)=\"close()\">ปิดออก</button>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/saller/add_saller.html":
/*!******************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/saller/add_saller.html ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<form [formGroup]=\"sallerForm\" (ngSubmit)=\"onReactiveFormSubmit()\" novalidate>\r\n    <div class=\"modal-header\">\r\n        <h4 class=\"modal-title\">เพิ่มทีมงานใหม่</h4>\r\n        <button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"close()\">\r\n            <span aria-hidden=\"true\">&times;</span>\r\n        </button>\r\n    </div>\r\n    <div class=\"modal-body\">\r\n        <div class=\"w-100 text-center\">\r\n            <img class=\"avatar\" src=\"https://image.flaticon.com/icons/svg/194/194938.svg\" [src]=\"para.picture\">\r\n            <a class=\"change_img\" (click)=\"change_picture()\">\r\n                <i class=\"fa fa-camera\"></i>\r\n            </a>\r\n            <input type=\"file\" style=\"display: none;\" #file (change)=\"file_change($event)\">\r\n        </div>\r\n        <fieldset class=\"form-group mt-3\">\r\n            <label for=\"username\">ชื่อผู้ใช้</label>\r\n            <input type=\"text\" id=\"username\"\r\n                [ngClass]=\"{'invalid':!sallerForm.get('username').valid && (sallerForm.get('username').dirty || sallerForm.get('username').touched)}\"\r\n                formControlName=\"username\" name=\"username\" class=\"form-control round\">\r\n            <small class=\"form-text text-muted danger\"\r\n                *ngIf=\"!sallerForm.get('username').valid && (sallerForm.get('username').dirty || sallerForm.get('username').touched)\">กรุณากรอกผู้ใช้งานให้ถูกต้อง</small>\r\n        </fieldset>\r\n        <fieldset class=\"form-group\">\r\n            <label for=\"password\">รหัสผ่าน</label>\r\n            <input type=\"password\" id=\"password\"\r\n                [ngClass]=\"{'invalid':!sallerForm.get('password').valid && (sallerForm.get('password').dirty || sallerForm.get('password').touched)}\"\r\n                formControlName=\"password\" name=\"password\" class=\"form-control round\">\r\n            <small class=\"form-text text-muted danger\"\r\n                *ngIf=\"!sallerForm.get('password').valid && (sallerForm.get('password').dirty || sallerForm.get('password').touched)\">กรุณากรอกรหัสผ่านให้ถูกต้อง</small>\r\n        </fieldset>\r\n        <fieldset class=\"form-group\">\r\n            <label for=\"name\">ชื่อ - นามสกุล</label>\r\n            <input type=\"text\" id=\"name\"\r\n                [ngClass]=\"{'invalid':!sallerForm.get('name').valid && (sallerForm.get('name').dirty || sallerForm.get('name').touched)}\"\r\n                formControlName=\"name\" name=\"name\" class=\"form-control round\">\r\n            <small class=\"form-text text-muted danger\"\r\n                *ngIf=\"!sallerForm.get('name').valid && (sallerForm.get('name').dirty || sallerForm.get('name').touched)\">กรุณากรอก\r\n                ชื่อ - นามสกุล ให้ถูกต้อง</small>\r\n        </fieldset>\r\n        <fieldset class=\"form-group\">\r\n            <label for=\"email\">อีเมล์</label>\r\n            <input type=\"email\" id=\"email\"\r\n                [ngClass]=\"{'invalid':!sallerForm.get('email').valid && (sallerForm.get('email').dirty || sallerForm.get('email').touched)}\"\r\n                formControlName=\"email\" name=\"email\" class=\"form-control round\">\r\n            <small class=\"form-text text-muted danger\"\r\n                *ngIf=\"!sallerForm.get('email').valid && (sallerForm.get('email').dirty || sallerForm.get('email').touched)\">กรุณากรอกอีเมล์ให้ถูกต้อง</small>\r\n        </fieldset>\r\n        <fieldset class=\"form-group\">\r\n            <label for=\"tel\">เบอร์โทรศัพท์</label>\r\n            <input type=\"text\" id=\"phone\"\r\n                [ngClass]=\"{'invalid':!sallerForm.get('phone').valid && (sallerForm.get('phone').dirty || sallerForm.get('phone').touched)}\"\r\n                formControlName=\"phone\" class=\"form-control round\">\r\n            <small class=\"form-text text-muted danger\"\r\n                *ngIf=\"!sallerForm.get('phone').valid && (sallerForm.get('phone').dirty || sallerForm.get('phone').touched)\">กรุณากรอกเบอร์โทรศัพท์ให้ถูกต้อง</small>\r\n        </fieldset>\r\n        <fieldset class=\"form-group\">\r\n            <label for=\"gender\">เพศ</label>\r\n            <select id=\"gender\" name=\"gender\" formControlName=\"gender\" class=\"form-control round\">\r\n                <option selected value=\"m\">ชาย</option>\r\n                <option value=\"f\">หญิง</option>\r\n            </select>\r\n        </fieldset>\r\n        <fieldset class=\"form-group\">\r\n            <label for=\"type\">ประเภทผู้ใช้งาน</label>\r\n            <select id=\"type\" name=\"type\" formControlName=\"type\" class=\"form-control round\">\r\n                <option selected value=\"1\">Admin</option>\r\n                <option value=\"2\">User</option>\r\n                <option value=\"3\">Accounting</option>\r\n            </select>\r\n        </fieldset>\r\n    </div>\r\n    <div class=\"modal-footer\">\r\n        <button type=\"button\" class=\"btn btn-secondary btn-raised\" (click)=\"close()\">ปิดออก</button>\r\n        <button type=\"submit\" class=\"btn btn-success mr-2 btn-raised \">บันทึก</button>\r\n    </div>\r\n</form>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/saller/saller.component.html":
/*!************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/saller/saller.component.html ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ng-template #view let-c=\"close\" let-d=\"dismiss\">\n\n</ng-template>\n<div class=\"row text-left\">\n  <div class=\"col-12\">\n    <div class=\"content-header\">ผู้ขาย</div>\n    <!-- <p class=\"content-sub-header\">All table styles are inherited in Bootstrap 4, meaning any nested tables will be\n      styled in the same manner as the parent.</p> -->\n  </div>\n</div>\n<!--Basic Table Ends-->\n\n<!--Hoverable Rows Starts-->\n<section id=\"hoverable-rows\">\n  <div class=\"row text-left\">\n    <div class=\"col-sm-12\">\n      <div class=\"card\">\n        <div class=\"card-content\">\n\n          <div class=\"card-body\">\n            <div class=\"text-right\" style=\"margin-top: -10px; margin-bottom: 55px;\">\n              <form class=\"navbar-form navbar-right mt-1\" [formGroup]=\"searchForm\" (ngSubmit)=\"search()\">\n                <div class=\"position-relative has-icon-right\">\n                  <input class=\"form-control round\" formControlName=\"search\" placeholder=\"Search\" type=\"text\">\n                  <div class=\"form-control-position\">\n                    <i class=\"ft-search\"></i>\n                  </div>\n                </div>\n              </form>\n              <!-- <a href=\"javascript:void(0)\" class=\"btn btn-raised mr-1 btn-round btn-primary\"><i class=\"fa fa-plus\"></i>\n                เพิ่มทีมงาน</a> -->\n            </div>\n\n            <table class=\"table table-hover\">\n              <thead>\n                <tr>\n                  <th style=\"width: 50px;\">#</th>\n                  <th style=\"width: 60px;\" class=\"text-center\">ภาพ</th>\n                  <th>ชื่อ - สกุล</th>\n                  <th>ที่อยู่</th>\n                  <th>เบอร์โทรศัพท์</th>\n                  <th>สถานะ</th>\n                  <th style=\"width: 200px;\" class=\"text-center\">จัดการ</th>\n                </tr>\n              </thead>\n              <tbody>\n                <tr *ngFor=\"let t of sallers\">\n                  <th scope=\"row\">{{t.member_id}}</th>\n                  <td style=\"padding: 2px;\"><img style=\"width: 50px;\" src=\"{{t.picture}}\"></td>\n                  <td>{{t.name}}</td>\n                  <td>{{t.address}}</td>\n                  <td>{{t.phone}}</td>\n                  <td>\n                    <span *ngIf=\"t.approve == 0\" class=\"badge badge-secondary\">รอการอนุมัติ</span>\n                    <span *ngIf=\"t.approve == 1\" class=\"badge badge-success\">อนุมัติแล้ว</span>\n                  </td>\n                  <td>\n                    <button href=\"javascript:void(0)\" class=\"btn btn-raised mr-1 btn-primary btn-sm\"\n                      (click)=\"view_profile(t)\"><i class=\"fa fa-eye\"></i> ดู</button>\n                    <a href=\"javascript:void(0)\" (click)=\"open(t)\" class=\"btn btn-raised mr-1 btn-sm btn-warning\"><i\n                        class=\"fa fa-edit\"></i> แก้ไข</a>\n                    <a href=\"javascript:void(0)\" (click)=\"delete_member()\"\n                      class=\"btn btn-raised mr-1 btn-sm btn-danger\"><i class=\"fa fa-trash-o\"></i> ลบ</a>\n                  </td>\n                </tr>\n\n              </tbody>\n            </table>\n            <div class=\"col-12 justify-content-center\">\n              <ngb-pagination [collectionSize]=\"all_page\" (pageChange)=\"change_page($event)\" class=\"justify-content-center\"\n               [pageSize]='20' [(page)]=\"page\"></ngb-pagination>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</section>\n<!--Hoverable rows Ends-->"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/saller/view_saller.html":
/*!*******************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/saller/view_saller.html ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"modal-header\">\r\n  <h4 class=\"modal-title\">{{para.name}}</h4>\r\n  <button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"close()\">\r\n    <span aria-hidden=\"true\">&times;</span>\r\n  </button>\r\n</div>\r\n<div class=\"modal-body\">\r\n  <div class=\"w-100 text-center\">\r\n    <img style=\"width: 80px;\" src=\"{{para.picture}}\">\r\n  </div>\r\n\r\n  <div class=\"content\">\r\n    <dl>\r\n      <dt>ชื่อ - นามสกุล </dt>\r\n      <dd>{{para.name}}</dd>\r\n      <dt>อีเมล์ </dt>\r\n      <dd>{{para.email}}</dd>\r\n      <dt>เบอร์โทรศัพท์ </dt>\r\n      <dd>{{para.phone}}</dd>\r\n      <dt>สถานะ </dt>\r\n      <dd>\r\n        <span *ngIf=\"para.status == 1\">ปกติ</span>\r\n      </dd>\r\n    </dl>\r\n    <div class=\"row\">\r\n      <div class=\"col\">\r\n        <h4>บัตรประชาชน</h4>\r\n        <span *ngFor=\"let item of verify.type1; let i = index\">\r\n          <a href=\"{{item.picture}}\" target=\"_blank\" [ngClass]=\"item.style\">{{i+1}}.บัตรประชาชน({{i+1}})</a>\r\n          <button [disabled]=\"item.status!='0'\" style=\"margin-left: 5px;\" class=\"btn btn-raised mr-1 btn-primary btn-sm\" (click)=\"pass(item.mv_id)\"><i\r\n              class=\"fa fa-check\"></i></button>\r\n          <button [disabled]=\"item.status!='0'\" (click)=\"eject(item.mv_id)\" class=\"btn btn-raised mr-1 btn-sm btn-danger\"><i\r\n              class=\"fa fa-remove\"></i></button><br>\r\n        </span>\r\n      </div>\r\n      <div class=\"col\">\r\n        <h4>หนังสือแต่งตั้ง</h4>\r\n        <span *ngFor=\"let item of verify.type2; let i = index\">\r\n          <a href=\"{{item.picture}}\" target=\"_blank\" [ngClass]=\"item.style\">{{i+1}}.หนังสือแต่งตั้ง({{i+1}})</a>\r\n          <button [disabled]=\"item.status!='0'\" style=\"margin-left: 5px;\" class=\"btn btn-raised mr-1 btn-primary btn-sm\" (click)=\"pass(item.mv_id)\"><i\r\n              class=\"fa fa-check\"></i></button>\r\n          <button [disabled]=\"item.status!='0'\" (click)=\"eject(item.mv_id)\" class=\"btn btn-raised mr-1 btn-sm btn-danger\"><i\r\n              class=\"fa fa-remove\"></i></button><br>\r\n        </span>\r\n      </div>\r\n      <div class=\"col\">\r\n        <h4>หนังสือรับรอง</h4>\r\n        <span *ngFor=\"let item of verify.type3; let i = index\">\r\n          <a href=\"{{item.picture}}\" target=\"_blank\" [ngClass]=\"item.style\" >{{i+1}}.หนังสือรับรอง({{i+1}})</a>\r\n          <button [disabled]=\"item.status!='0'\" style=\"margin-left: 5px;\" class=\"btn btn-raised mr-1 btn-primary btn-sm\" (click)=\"pass(item.mv_id)\"><i\r\n              class=\"fa fa-check\"></i></button>\r\n          <button [disabled]=\"item.status!='0'\" (click)=\"eject(item.mv_id)\" class=\"btn btn-raised mr-1 btn-sm btn-danger\"><i\r\n              class=\"fa fa-remove\"></i></button><br>\r\n        </span>\r\n      </div>\r\n    </div>\r\n    <!-- <table style=\"width: 100%; border:dashed\">\r\n      <thead>\r\n        <th>บัตรประชาชน</th>\r\n        <th>หนังสือแต่งตั้ง</th>\r\n        <th>หนังสือรับรอง</th>\r\n      </thead>\r\n      <tbody>\r\n        <tr>\r\n          <td>\r\n            <span *ngFor=\"let item of verify.type1; let i = index\">{{i+1}}. <a href=\"{{item.picture}}\"\r\n                target=\"_blank\">บัตรประชาชน({{i+1}})</a> <br></span>\r\n            <div style=\"padding-left: 5px;\">\r\n              <button href=\"javascript:void(0)\" class=\"btn btn-raised mr-1 btn-primary btn-sm\"\r\n                (click)=\"view_profile(t)\"><i class=\"fa fa-eye\"></i> ผ่าน</button>\r\n              <a href=\"javascript:void(0)\" (click)=\"open(t)\" class=\"btn btn-raised mr-1 btn-sm btn-warning\"><i\r\n                  class=\"fa fa-edit\"></i> ไม่ผ่าน</a>\r\n            </div>\r\n          </td>\r\n          <td>\r\n            <span *ngFor=\"let item of verify.type2; let i = index\">{{i+1}}. <a href=\"{{item.picture}}\"\r\n                target=\"_blank\">หนังสือแต่งตั้ง({{i+1}})</a><br></span>\r\n          </td>\r\n          <td>\r\n            <span *ngFor=\"let item of verify.type3; let i = index\">{{i+1}}. <a href=\"{{item.picture}}\"\r\n                target=\"_blank\">หนังสือรับรอง({{i+1}})</a><br></span>\r\n          </td>\r\n        </tr>\r\n      </tbody>\r\n    </table> -->\r\n  </div>\r\n</div>\r\n<div class=\"modal-footer\">\r\n  <button type=\"button\" [disabled]=\"verify.pass == '-1' || para.approve == '1'\" class=\"btn btn-raised mr-1 btn-primary\" (click)=\"approve(para.member_id)\">อนุมัติ</button>\r\n  <button type=\"button\" class=\"btn btn-secondary btn-raised\" (click)=\"close()\">ปิดออก</button>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/score/score.component.html":
/*!**********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/score/score.component.html ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ng-template #view let-c=\"close\" let-d=\"dismiss\">\n\n</ng-template>\n<div class=\"row text-left\">\n  <div class=\"col-12\">\n    <div class=\"content-header\">เบิกแต้มสะสม</div>\n    <!-- <p class=\"content-sub-header\">All table styles are inherited in Bootstrap 4, meaning any nested tables will be\n      styled in the same manner as the parent.</p> -->\n  </div>\n</div>\n<!--Basic Table Ends-->\n\n<!--Hoverable Rows Starts-->\n<section id=\"hoverable-rows\">\n  <div class=\"row text-left\">\n    <div class=\"col-sm-12\">\n      <div class=\"card\">\n        <div class=\"card-content\">\n\n          <div class=\"card-body\">\n            <div class=\"text-right\" style=\"margin-top: -10px; margin-bottom: 55px;\">\n              <form class=\"navbar-form navbar-right mt-1\" [formGroup]=\"searchForm\" (ngSubmit)=\"search()\">\n                <div class=\"position-relative has-icon-right\">\n                  <input class=\"form-control round\" formControlName=\"search\" placeholder=\"Search\" type=\"text\">\n                  <div class=\"form-control-position\">\n                    <i class=\"ft-search\"></i>\n                  </div>\n                </div>\n              </form>\n            </div>\n            <table class=\"table table-hover\">\n              <thead>\n                <tr>\n                  <th style=\"width: 50px;\">#</th>\n                  <!-- <th style=\"width: 50px;\">ID</th> -->\n                  <th>ชื่อ-สกุล</th>\n                  <!-- <th>เลขที่บัญชี</th> -->\n                  <th>ยอดเงินที่ต้องโอน</th>\n                  <th>สถานะ</th>\n                  <th style=\"width: 220px;\" class=\"text-center\">จัดการ</th>\n                </tr>\n              </thead>\n              <tbody>\n                <tr *ngFor=\"let d of score let i = index\">\n                  <td>{{i+1}}</td>\n                  <!-- <td>{{d.score_id}}</td> -->\n                  <td>{{d.name}}</td>\n                  <!-- <td>\n                    <select *ngIf=\"d.bank != ''\" name=\"bank_selected\" #bbank (change)=\"sbank(bbank.value,d.score_id)\" [disabled]=\"d.confirm!=0\" >\n                      <option [value]='noselect' >เลือกบัญชีธนาคาร</option>\n                      <option *ngFor=\"let b of d.bank\" [value]='b.bb_id'>{{b.no}} ({{b.bank}})</option>\n                    </select>\n                    <span *ngIf=\"d.bank == ''\">ไม่พบเลขที่บัญชี</span>\n                  </td> -->\n                  <td>{{d.exp|number}} บาท </td>\n                  <td>\n                    <span *ngIf=\"d.confirm == -1\" class=\"badge badge-danger\">ยกเลิกแล้ว</span>\n                    <span *ngIf=\"d.confirm == 0\" class=\"badge badge-secondary\">รอดำเนินการ</span>\n                    <span *ngIf=\"d.confirm == 1\" class=\"badge badge-success\">โอนสำเร็จ</span>\n                  </td>\n                  <td>\n                    <div>\n                      <input style=\"display: none;\" #uploadImg id=\"slipinput\" type=\"file\" accept=\"image/*\"\n                      (change)=\"changeImg($event)\" hidden>\n                      <!-- <i class=\"ft-image mr-1\" (click)=\"uploadImg.click()\"></i> -->\n                    </div>\n                    <button [disabled]=\"d.confirm!=0 || admin.type > 2 \" href=\"javascript:void(0)\"\n                      class=\"btn btn-raised mr-1 btn-primary btn-sm\" (click)=\"continue(d)\"><i class=\"fa fa-check\"></i>\n                      แนบหลักฐาน</button>\n                    <button [disabled]=\"true\" href=\"javascript:void(0)\" (click)=\"cancel()\"\n                      class=\"btn btn-raised mr-1 btn-sm btn-danger\"><i class=\"fa fa-times\"></i> ปฏิเสธ</button>\n                  </td>\n                </tr>\n\n              </tbody>\n            </table>\n            <div class=\"col-12 justify-content-center\">\n              <ngb-pagination [collectionSize]=\"all_page\" (pageChange)=\"change_page($event)\"\n                class=\"justify-content-center\" [pageSize]='20' [(page)]=\"page\"></ngb-pagination>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</section>\n<!--Hoverable rows Ends-->"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/score/score_details.html":
/*!********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/score/score_details.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"modal-header\">\r\n    <h4 class=\"modal-title\">เบิกแต้มจำนวน {{data.exp}}</h4>\r\n    <button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"close()\">\r\n      <span aria-hidden=\"true\">&times;</span>\r\n    </button>\r\n  </div>\r\n  <div class=\"modal-body\">\r\n  \r\n    <div class=\"content\">\r\n        <select *ngIf=\"data.bank != ''\" name=\"bank_selected\" #bbank (change)=\"sbank(bbank.value)\"\r\n        [disabled]=\"data.confirm!=0\">\r\n        <option [value]='noselect'>เลือกบัญชีธนาคาร</option>\r\n        <option *ngFor=\"let b of data.bank\" [value]='b.bb_id'>{{b.no}} ({{b.bank}})</option>\r\n    </select>\r\n    <span *ngIf=\"data.bank == ''\">ไม่พบเลขที่บัญชี</span>\r\n    </div><br>\r\n    <div>\r\n        <img [src]=\"url\">\r\n    </div><br>\r\n    <div>\r\n        <input style=\"display: none;\" #uploadImg id=\"slipinput\" type=\"file\" accept=\"image/*\"\r\n        (change)=\"changeImg($event)\" hidden>\r\n        <button type=\"button\"  class=\"btn btn-raised mr-1 btn-primary\"(click)=\"uploadImg.click()\">แนบหลักฐาน</button>\r\n      </div>\r\n  </div>\r\n  <div class=\"modal-footer\">\r\n    <button type=\"button\"  class=\"btn btn-raised mr-1 btn-primary\" (click)=\"confirm()\">ยืนยัน</button>\r\n    <button type=\"button\" class=\"btn btn-secondary btn-raised\" (click)=\"close()\">ปิดออก</button>\r\n  </div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/settings/add_bank.html":
/*!******************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/settings/add_bank.html ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<form [formGroup]=\"bankForm\" (ngSubmit)=\"onBankSubmit()\" novalidate>\r\n    <div class=\"modal-header\">\r\n        <h4 class=\"modal-title\">เพิ่มบัญชีธนาคาร</h4>\r\n        <button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"close()\">\r\n            <span aria-hidden=\"true\">&times;</span>\r\n        </button>\r\n    </div>\r\n    <div class=\"modal-body\">\r\n\r\n        <label for=\"no\">เลขที่บัญชี </label>\r\n        <input type=\"text\" formControlName=\"no\" name=\"no\" class=\"form-control round\">\r\n        <label for=\"name\">ชื่อบัญชี</label>\r\n        <input type=\"text\" formControlName=\"name\" name=\"name\" class=\"form-control round\">\r\n        <label for=\"bank\">ธนาคาร</label>\r\n        <input type=\"text\" formControlName=\"bank\" name=\"bank\" class=\"form-control round\">\r\n        <!-- <small class=\"form-text text-muted danger\"\r\n                *ngIf=\"!productForm.get('p_name').valid && (productForm.get('p_name').dirty || productForm.get('p_name').touched)\">กรุณากรอกผู้ใช้งานให้ถูกต้อง</small> -->\r\n\r\n\r\n        <input formControlName=\"show\" type=\"checkbox\" >\r\n        แสดง\r\n    </div>\r\n    <div class=\"modal-footer\">\r\n        <button type=\"button\" class=\"btn btn-secondary btn-raised\" (click)=\"close()\">ปิดออก</button>\r\n        <button type=\"submit\" class=\"btn btn-success mr-2 btn-raised \">บันทึก</button>\r\n    </div>\r\n</form>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/settings/settings.component.html":
/*!****************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/settings/settings.component.html ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ng-template #view let-c=\"close\" let-d=\"dismiss\">\n\n</ng-template>\n<div class=\"row text-left\">\n  <div class=\"col-12\">\n    <div class=\"content-header\">ตั้งค่า</div>\n    <!-- นโยบายความเป็นส่วนตัว + เงื่อนไขการให้บริการ TH/EN -->\n    <!-- <p class=\"content-sub-header\">All table styles are inherited in Bootstrap 4, meaning any nested tables will be\n      styled in the same manner as the parent.</p> -->\n  </div>\n</div>\n<!--Basic Table Ends-->\n<!--Hoverable Rows Starts-->\n<section id=\"hoverable-rows\">\n  <div class=\"row text-left\">\n    <div class=\"col-sm-12\">\n      <div class=\"card\">\n        <div class=\"card-content\">\n          <div class=\"card-body\">\n            <div class=\"card-content\">\n              <div class=\"under-line\">\n                <h4 *ngIf=\"setting_genaral\">ตั้งค่าทั่วไป<i class=\"ft-chevrons-down go-right\"\n                    (click)=\"show_setting(1)\"></i></h4>\n                <h4 *ngIf=\"!setting_genaral\">ตั้งค่าทั่วไป<i class=\"ft-chevrons-up go-right\"\n                    (click)=\"show_setting(1)\"></i></h4>\n              </div>\n              <div class=\"card-body genaral\" [hidden]=\"setting_genaral\">\n                <label>แก้ไขล่าสุดเมื่อ : {{settingAll.up_date | date}}</label>\n                <ngb-tabset type=\"pills\">\n                  <ngb-tab>\n                    <ng-template ngbTabTitle>นโยบายความเป็นส่วนตัว</ng-template>\n                    <ng-template ngbTabContent>\n                      <div class=\"row\">\n                        <div class=\"col-sm-8\">\n                          <label>ภาษาไทย</label>\n                        </div>\n                      </div>\n                      <div>\n                        <ckeditor [config]=\"editorConfig\" [editor]=\"Editor\" [(ngModel)]=\"settingAll.policy.th\"\n                          tagName=\"policy\" (change)=\"onChangePolicyTH($event)\"></ckeditor>\n                      </div>\n                      <div class=\"row\">\n                        <div class=\"col-sm-8\">\n                          <label>ภาษาอังกฤษ</label>\n                        </div>\n                      </div>\n                      <div>\n                        <ckeditor [config]=\"editorConfig\" [editor]=\"Editor\" [(ngModel)]=\"settingAll.policy.en\"\n                          name=\"service\" (change)=\"onChangePolicyEN($event)\"></ckeditor>\n                      </div>\n                    </ng-template>\n                  </ngb-tab>\n                  <ngb-tab>\n                    <ng-template ngbTabTitle>เงื่อนไขการให้บริการ</ng-template>\n                    <ng-template ngbTabContent>\n                      <div class=\"row\">\n                        <div class=\"col-sm-8\">\n                          <label>ภาษาไทย</label>\n                        </div>\n                      </div>\n                      <div>\n                        <ckeditor [config]=\"editorConfig\" [editor]=\"Editor\" [(ngModel)]=\"settingAll.service.th\"\n                          tagName=\"policy\" (change)=\"onChangeServiceTH($event)\"></ckeditor>\n                      </div>\n                      <div class=\"row\">\n                        <div class=\"col-sm-8\">\n                          <label>ภาษาอังกฤษ</label>\n                        </div>\n                      </div>\n                      <div>\n                        <ckeditor [config]=\"editorConfig\" [editor]=\"Editor\" [(ngModel)]=\"settingAll.service.en\"\n                          name=\"service\" (change)=\"onChangeServiceEN($event)\"></ckeditor>\n                      </div>\n                    </ng-template>\n                  </ngb-tab>\n                </ngb-tabset>\n                <div>\n                  <a href=\"javascript:void(0)\" class=\"btn btn-success mr-2 btn-raised \"\n                    (click)=\"save()\">บันทึกการตั้งค่าทั่วไป</a>\n                </div>\n              </div>\n              <div class=\"under-line\">\n                <h4 *ngIf=\"setting_rate\">อัตราส่วนราคา<i class=\"ft-chevrons-down go-right\"\n                    (click)=\"show_setting(2)\"></i></h4>\n                <h4 *ngIf=\"!setting_rate\">อัตราส่วนราคา<i class=\"ft-chevrons-up go-right\" (click)=\"show_setting(2)\"></i>\n                </h4>\n              </div>\n              <div class=\"card-body rate\" [hidden]=\"setting_rate\">\n                <label>แก้ไขล่าสุดเมื่อ : {{now_rate.up_date | date}}</label>\n                <form [formGroup]=\"rate\" (ngSubmit)=\"onReactiveFormSubmit()\" novalidate>\n                  <div>\n                    <fieldset class=\"form-group\">\n                      <label for=\"atOrder\" class=\"right\">อัตราส่วนบวกเพิ่มจากรายการ ( หน่วยเป็น % ) </label>\n                      <input type=\"number\" formControlName=\"atOrder\" name=\"atOrder\" class=\"form-control\">\n                      <label for=\"cus_rate\" class=\"right\">อัตราส่วนคืนให้ลูกค้า ( หน่วยเป็น % )</label>\n                      <input type=\"number\" formControlName=\"cus_rate\" name=\"cus_rate\" class=\"form-control\">\n                      <label for=\"sale_rate\" class=\"right\">อัตราส่วนคืนให้ผู้ขาย ( หน่วยเป็น % )</label>\n                      <input type=\"number\" formControlName=\"sale_rate\" name=\"sale_rate\" class=\"form-control\">\n                    </fieldset>\n                  </div>\n                  <div>\n                    <button type=\"submit\" class=\"btn btn-success mr-2 btn-raised \">บันทึกอัตราส่วนราคา</button>\n                  </div>\n                </form>\n              </div>\n              <div class=\"under-line\">\n                <h4 *ngIf=\"setting_account\">ตั้งค่าบัญชี<i class=\"ft-chevrons-down go-right\"\n                    (click)=\"show_setting(3)\"></i></h4>\n                <h4 *ngIf=\"!setting_account\">ตั้งค่าบัญชี<i class=\"ft-chevrons-up go-right\"\n                    (click)=\"show_setting(3)\"></i></h4>\n              </div>\n              <div class=\"card-body account\" [hidden]=\"setting_account\">\n                <!-- <label>แก้ไขล่าสุดเมื่อ : {{now_rate.up_date | date}}</label> -->\n                <button style=\"margin-bottom: 5px;\" class=\"btn btn-raised mr-1 btn-primary butt-right\"\n                  (click)=\"open({bb_id:'0'})\"><i class=\"fa fa-plus\"></i>\n                  เพิ่มบัญชี</button>\n                <table class=\"table table-hover\">\n                  <thead>\n                    <tr>\n                      <th style=\"width: 50px;\">#</th>\n                      <th>เลขที่บัญชี</th>\n                      <th>ชื่อบัญชี</th>\n                      <th>ธนาคาร</th>\n                      <th>สถานะ</th>\n                      <th style=\"width: 145px;\" class=\"text-center\">จัดการ</th>\n                    </tr>\n                  </thead>\n                  <tbody>\n                    <tr *ngFor=\"let b of bank; let i=index\">\n                      <th scope=\"row\">{{i+1}}</th>\n                      <td>{{b.no}}</td>\n                      <td>{{b.name}}</td>\n                      <td>{{b.bank}}</td>\n                      <td>\n                        <span *ngIf=\"b.show == 1\" class=\"badge badge-success\">แสดง</span>\n                        <span *ngIf=\"b.show == 0\" class=\"badge badge-secondary\">ซ่อน</span>\n                      </td>\n                      <td>\n                        <!-- <button href=\"javascript:void(0)\" class=\"btn btn-raised mr-1 btn-primary btn-sm\"\n                            (click)=\"view_profile(t)\"><i class=\"fa fa-eye\"></i> ดู</button> -->\n                        <a href=\"javascript:void(0)\" (click)=\"open(b)\" class=\"btn btn-raised mr-1 btn-sm btn-warning\"><i\n                            class=\"fa fa-edit\"></i> แก้ไข</a>\n                        <a href=\"javascript:void(0)\" (click)=\"delete(b)\"\n                          class=\"btn btn-raised mr-1 btn-sm btn-danger\"><i class=\"fa fa-trash-o\"></i> ลบ</a>\n                      </td>\n                    </tr>\n\n                  </tbody>\n                </table>\n              </div>\n              <div class=\"under-line\">\n                <h4 *ngIf=\"setting_location\">ตั้งค่าพื้นที่เปิดให้บริการ<i class=\"ft-chevrons-down go-right\"\n                    (click)=\"show_setting(4)\"></i></h4>\n                <h4 *ngIf=\"!setting_location\">ตั้งค่าพื้นที่เปิดให้บริการ<i class=\"ft-chevrons-up go-right\"\n                    (click)=\"show_setting(4)\"></i></h4>\n              </div>\n              <div class=\"card-body account\" [hidden]=\"setting_location\">\n                <table class=\"table table-hover\">\n                  <thead>\n                    <tr>\n                      <th style=\"width: 50px;\">#</th>\n                      <th>จังหวัด</th>\n                      <th>สถานะ</th>\n                      <th style=\"width: 145px;\" class=\"text-center\">จัดการ</th>\n                    </tr>\n                  </thead>\n                  <tbody>\n                    <tr *ngFor=\"let p of provinces; let i=index\">\n                      <th scope=\"row\">{{i+1}}</th>\n                      <td>{{p.name_th}}</td>\n                      <td>\n                        <span *ngIf=\"p.hidden == 0\" class=\"badge badge-success\">เปิดให้บริการ</span>\n                        <span *ngIf=\"p.hidden == 1\" class=\"badge badge-secondary\">ปิดให้บริการ</span>\n                      </td>\n                      <td>\n                        <!-- <button href=\"javascript:void(0)\" class=\"btn btn-raised mr-1 btn-primary btn-sm\"\n                            (click)=\"view_profile(t)\"><i class=\"fa fa-eye\"></i> ดู</button> -->\n                        <button href=\"javascript:void(0)\" (click)=\"pOpen(p)\" [disabled]=\"p.hidden == 0\" class=\"btn btn-raised mr-1 btn-sm btn-warning\"><i\n                            class=\"fa fa-edit\"></i> เปิด</button>\n                        <button href=\"javascript:void(0)\" (click)=\"pClose(p)\" [disabled]=\"p.hidden == 1\"\n                          class=\"btn btn-raised mr-1 btn-sm btn-danger\"><i class=\"fa fa-trash-o\"></i> ปิด</button>\n                      </td>\n                    </tr>\n\n                  </tbody>\n                </table>\n              </div>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</section>\n<!--Hoverable rows Ends-->"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/shared/customizer/customizer.component.html":
/*!***************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/shared/customizer/customizer.component.html ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--Theme customizer Starts-->\n<div #customizer class=\"customizer border-left-blue-grey border-left-lighten-4 d-none d-sm-none d-md-block\" [ngClass]=\"{open: isOpen}\">\n  <a class=\"customizer-close\" (click)=\"closeCustomizer()\">\n    <i class=\"ft-x font-medium-3\"></i>\n  </a>\n  <a class=\"customizer-toggle bg-danger\" id=\"customizer-toggle-icon\" (click)=\"toggleCustomizer()\">\n    <i class=\"ft-settings font-medium-4 fa fa-spin white align-middle\"></i>\n  </a>\n  <div [perfectScrollbar] class=\"customizer-content p-3 ps-container ps-theme-dark text-left\" data-ps-id=\"df6a5ce4-a175-9172-4402-dabd98fc9c0a\">\n    <h4 class=\"text-uppercase mb-0 text-bold-400\">Theme Customizer</h4>\n    <p>Customize &amp; Preview in Real Time</p>\n    <hr>\n\n    <!-- Layout Options-->\n    <h6 class=\"text-center text-bold-500 mb-3 text-uppercase\">Layout Options</h6>\n    <div class=\"layout-switch ml-4\">\n      <div class=\"custom-control custom-radio d-inline-block custom-control-inline light-layout\">\n        <input class=\"custom-control-input\" id=\"ll-switch\" type=\"radio\" name=\"layout-switch\" [checked]=\"config.layout.variant === 'Light'\"  (click)=\"onLightLayout()\" />\n        <label class=\"custom-control-label\" for=\"ll-switch\">Light</label>\n      </div>\n      <div class=\"custom-control custom-radio d-inline-block custom-control-inline dark-layout\">\n        <input class=\"custom-control-input\" id=\"dl-switch\" type=\"radio\" name=\"layout-switch\" [checked]=\"config.layout.variant === 'Dark'\" (click)=\"onDarkLayout()\" />\n        <label class=\"custom-control-label\" for=\"dl-switch\">Dark</label>\n      </div>\n      <div class=\"custom-control custom-radio d-inline-block custom-control-inline transparent-layout\">\n        <input class=\"custom-control-input\" id=\"tl-switch\" type=\"radio\" name=\"layout-switch\" [checked]=\"config.layout.variant === 'Transparent'\" (click)=\"onTransparentLayout()\" />\n        <label class=\"custom-control-label\" for=\"tl-switch\">Transparent</label>\n      </div>\n    </div>\n    <hr />\n\n    <!-- Sidebar Options Starts-->\n    <h6 class=\"text-center text-bold-500 mb-3 text-uppercase sb-options\">Sidebar Color Options</h6>\n    <div class=\"cz-bg-color sb-color-options\">\n      <div class=\"row p-1\">\n        <div class=\"col\"><span class=\"gradient-pomegranate d-block rounded-circle\" style=\"width:20px; height:20px;\"\n            data-bg-color=\"pomegranate\" [ngClass]=\"{'selected': selectedBgColor === 'pomegranate'}\" (click)=\"changeSidebarBgColor('pomegranate')\"></span></div>\n        <div class=\"col\"><span class=\"gradient-king-yna d-block rounded-circle\" style=\"width:20px; height:20px;\"\n            data-bg-color=\"king-yna\" [ngClass]=\"{'selected': selectedBgColor === 'king-yna'}\" (click)=\"changeSidebarBgColor('king-yna')\"></span></div>\n        <div class=\"col\"><span class=\"gradient-ibiza-sunset d-block rounded-circle\" style=\"width:20px; height:20px;\"\n            data-bg-color=\"ibiza-sunset\" [ngClass]=\"{'selected': selectedBgColor === 'ibiza-sunset'}\" (click)=\"changeSidebarBgColor('ibiza-sunset')\"></span></div>\n        <div class=\"col\"><span class=\"gradient-flickr d-block rounded-circle\" style=\"width:20px; height:20px;\"\n            data-bg-color=\"flickr\" [ngClass]=\"{'selected': selectedBgColor === 'flickr'}\" (click)=\"changeSidebarBgColor('flickr')\"></span></div>\n        <div class=\"col\"><span class=\"gradient-purple-bliss d-block rounded-circle\" style=\"width:20px; height:20px;\"\n            data-bg-color=\"purple-bliss\" [ngClass]=\"{'selected': selectedBgColor === 'purple-bliss'}\" (click)=\"changeSidebarBgColor('purple-bliss')\"></span></div>\n        <div class=\"col\"><span class=\"gradient-man-of-steel d-block rounded-circle\" style=\"width:20px; height:20px;\"\n            data-bg-color=\"man-of-steel\" [ngClass]=\"{'selected': selectedBgColor === 'man-of-steel'}\" (click)=\"changeSidebarBgColor('man-of-steel')\"></span></div>\n        <div class=\"col\"><span class=\"gradient-purple-love d-block rounded-circle\" style=\"width:20px; height:20px;\"\n            data-bg-color=\"purple-love\" [ngClass]=\"{'selected': selectedBgColor === 'purple-love'}\" (click)=\"changeSidebarBgColor('purple-love')\"></span></div>\n      </div>\n      <div class=\"row p-1\">\n        <div class=\"col\"><span class=\"bg-black d-block rounded-circle\" style=\"width:20px; height:20px;\" data-bg-color=\"black\" [ngClass]=\"{'selected': selectedBgColor === 'black'}\" (click)=\"changeSidebarBgColor('black')\"></span></div>\n        <div class=\"col\"><span class=\"bg-grey d-block rounded-circle\" style=\"width:20px; height:20px;\" data-bg-color=\"white\" [ngClass]=\"{'selected': selectedBgColor === 'white'}\" (click)=\"changeSidebarBgColor('white')\"></span></div>\n        <div class=\"col\"><span class=\"bg-primary d-block rounded-circle\" style=\"width:20px; height:20px;\" data-bg-color=\"primary\" [ngClass]=\"{'selected': selectedBgColor === 'primary'}\" (click)=\"changeSidebarBgColor('primary')\"></span></div>\n        <div class=\"col\"><span class=\"bg-success d-block rounded-circle\" style=\"width:20px; height:20px;\" data-bg-color=\"success\" [ngClass]=\"{'selected': selectedBgColor === 'success'}\" (click)=\"changeSidebarBgColor('success')\"></span></div>\n        <div class=\"col\"><span class=\"bg-warning d-block rounded-circle\" style=\"width:20px; height:20px;\" data-bg-color=\"warning\" [ngClass]=\"{'selected': selectedBgColor === 'warning'}\" (click)=\"changeSidebarBgColor('warning')\"></span></div>\n        <div class=\"col\"><span class=\"bg-info d-block rounded-circle\" style=\"width:20px; height:20px;\" data-bg-color=\"info\" [ngClass]=\"{'selected': selectedBgColor === 'info'}\" (click)=\"changeSidebarBgColor('info')\"></span></div>\n        <div class=\"col\"><span class=\"bg-danger d-block rounded-circle\" style=\"width:20px; height:20px;\" data-bg-color=\"danger\" [ngClass]=\"{'selected': selectedBgColor === 'danger'}\" (click)=\"changeSidebarBgColor('danger')\"></span></div>\n      </div>\n    </div>\n    <!-- Sidebar Options Ends-->\n    <!-- Transparent Layout Bg color Options-->\n    <h6 class=\"text-center text-bold-500 mb-3 text-uppercase tl-color-options d-none\">Background Colors</h6>\n    <div class=\"cz-tl-bg-color d-none\">\n      <div class=\"row p-1\">\n        <div class=\"col\"><span class=\"bg-hibiscus d-block rounded-circle\" style=\"width:20px; height:20px;\"\n            data-bg-color=\"bg-hibiscus\" [ngClass]=\"{'selected': selectedTLBgColor === 'bg-hibiscus'}\" (click)=\"changeSidebarTLBgColor('bg-hibiscus')\"></span></div>\n        <div class=\"col\"><span class=\"bg-purple-pizzazz d-block rounded-circle\" style=\"width:20px; height:20px;\"\n            data-bg-color=\"bg-purple-pizzazz\" [ngClass]=\"{'selected': selectedTLBgColor === 'bg-purple-pizzazz'}\" (click)=\"changeSidebarTLBgColor('bg-purple-pizzazz')\"></span></div>\n        <div class=\"col\"><span class=\"bg-blue-lagoon d-block rounded-circle\" style=\"width:20px; height:20px;\"\n            data-bg-color=\"bg-blue-lagoon\" [ngClass]=\"{'selected': selectedTLBgColor === 'bg-blue-lagoon'}\" (click)=\"changeSidebarTLBgColor('bg-blue-lagoon')\"></span></div>\n        <div class=\"col\"><span class=\"bg-electric-violet d-block rounded-circle\" style=\"width:20px; height:20px;\"\n            data-bg-color=\"bg-electric-violet\" [ngClass]=\"{'selected': selectedTLBgColor === 'bg-electric-violet'}\" (click)=\"changeSidebarTLBgColor('bg-electric-violet')\"></span></div>\n        <div class=\"col\"><span class=\"bg-portage d-block rounded-circle\" style=\"width:20px; height:20px;\" data-bg-color=\"bg-portage\" [ngClass]=\"{'selected': selectedTLBgColor === 'bg-portage'}\" (click)=\"changeSidebarTLBgColor('bg-portage')\"></span></div>\n        <div class=\"col\"><span class=\"bg-tundora d-block rounded-circle\" style=\"width:20px; height:20px;\" data-bg-color=\"bg-tundora\" [ngClass]=\"{'selected': selectedTLBgColor === 'bg-tundora'}\" (click)=\"changeSidebarTLBgColor('bg-tundora')\"></span></div>\n      </div>\n    </div>\n    <!-- Transparent Layout Bg color Ends-->\n    <hr />\n    <!--Sidebar BG Image Starts-->\n    <h6 class=\"text-center text-bold-500 mb-3 text-uppercase sb-bg-img\">Sidebar Bg Image</h6>\n    <div class=\"cz-bg-image row sb-bg-img\">\n      <div class=\"col-sm-2 mb-3\"><img class=\"rounded\" src=\"assets/img/sidebar-bg/01.jpg\" width=\"90\"  [ngClass]=\"{'selected': selectedBgImage === 'assets/img/sidebar-bg/01.jpg'}\" (click)=\"changeSidebarBgImage('assets/img/sidebar-bg/01.jpg')\" /></div>\n      <div class=\"col-sm-2 mb-3\"><img class=\"rounded\" src=\"assets/img/sidebar-bg/02.jpg\" width=\"90\"  [ngClass]=\"{'selected': selectedBgImage === 'assets/img/sidebar-bg/02.jpg'}\" (click)=\"changeSidebarBgImage('assets/img/sidebar-bg/02.jpg')\" /></div>\n      <div class=\"col-sm-2 mb-3\"><img class=\"rounded\" src=\"assets/img/sidebar-bg/03.jpg\" width=\"90\"  [ngClass]=\"{'selected': selectedBgImage === 'assets/img/sidebar-bg/03.jpg'}\" (click)=\"changeSidebarBgImage('assets/img/sidebar-bg/03.jpg')\" /></div>\n      <div class=\"col-sm-2 mb-3\"><img class=\"rounded\" src=\"assets/img/sidebar-bg/04.jpg\" width=\"90\"  [ngClass]=\"{'selected': selectedBgImage === 'assets/img/sidebar-bg/04.jpg'}\" (click)=\"changeSidebarBgImage('assets/img/sidebar-bg/04.jpg')\" /></div>\n      <div class=\"col-sm-2 mb-3\"><img class=\"rounded\" src=\"assets/img/sidebar-bg/05.jpg\" width=\"90\"  [ngClass]=\"{'selected': selectedBgImage === 'assets/img/sidebar-bg/05.jpg'}\" (click)=\"changeSidebarBgImage('assets/img/sidebar-bg/05.jpg')\" /></div>\n      <div class=\"col-sm-2 mb-3\"><img class=\"rounded\" src=\"assets/img/sidebar-bg/06.jpg\" width=\"90\"  [ngClass]=\"{'selected': selectedBgImage === 'assets/img/sidebar-bg/06.jpg'}\" (click)=\"changeSidebarBgImage('assets/img/sidebar-bg/06.jpg')\" /></div>\n    </div>\n    <!-- Transparent BG Image Ends-->\n    <div class=\"tl-bg-img d-none\">\n      <h6 class=\"text-center text-bold-500 text-uppercase\">Background Images</h6>\n      <div class=\"cz-tl-bg-image row\">\n        <div class=\"col-sm-3\"><img class=\"rounded bg-glass-1\" src=\"assets/img/gallery/bg-glass-1.jpg\" width=\"90\"  [ngClass]=\"{'selected': selectedTLBgImage === 'assets/img/gallery/bg-glass-1.jpg'}\" (click)=\"changeSidebarTLBgImage('assets/img/gallery/bg-glass-1.jpg', 'bg-glass-1')\" /></div>\n        <div class=\"col-sm-3\"><img class=\"rounded bg-glass-2\" src=\"assets/img/gallery/bg-glass-2.jpg\" width=\"90\"  [ngClass]=\"{'selected': selectedTLBgImage === 'assets/img/gallery/bg-glass-2.jpg'}\" (click)=\"changeSidebarTLBgImage('assets/img/gallery/bg-glass-2.jpg', 'bg-glass-2')\" /></div>\n        <div class=\"col-sm-3\"><img class=\"rounded bg-glass-3\" src=\"assets/img/gallery/bg-glass-3.jpg\" width=\"90\"  [ngClass]=\"{'selected': selectedTLBgImage === 'assets/img/gallery/bg-glass-3.jpg'}\" (click)=\"changeSidebarTLBgImage('assets/img/gallery/bg-glass-3.jpg', 'bg-glass-3')\" /></div>\n        <div class=\"col-sm-3\"><img class=\"rounded bg-glass-4\" src=\"assets/img/gallery/bg-glass-4.jpg\" width=\"90\"  [ngClass]=\"{'selected': selectedTLBgImage === 'assets/img/gallery/bg-glass-4.jpg'}\" (click)=\"changeSidebarTLBgImage('assets/img/gallery/bg-glass-4.jpg', 'bg-glass-4')\" /></div>\n      </div>\n    </div>\n\n    <hr />\n    <!-- Transparent BG Image Ends-->\n    <!--Sidebar BG Image Ends-->\n\n    <!--Sidebar BG Image Toggle Starts-->\n    <div class=\"togglebutton toggle-sb-bg-img\">\n      <div class=\"switch switch border-0 d-flex justify-content-between w-100\">\n        <span>Sidebar Bg Image</span>\n        <div class=\"float-right\">\n          <div class=\"custom-control custom-checkbox mb-2 mr-sm-2 mb-sm-0\">\n            <input type=\"checkbox\" class=\"custom-control-input cz-bg-image-display\" checked id=\"sidebar-bg-img\" (change)=\"bgImageDisplay($event)\">\n            <label class=\"custom-control-label d-block\" for=\"sidebar-bg-img\"></label>\n          </div>\n        </div>\n      </div>\n      <hr>\n    </div>\n    <!--Sidebar BG Image Toggle Ends-->\n\n    <!--Compact Menu Starts-->\n    <div class=\"togglebutton\">\n      <div class=\"switch switch border-0 d-flex justify-content-between w-100\">\n        <span>Compact Menu</span>\n        <div class=\"float-right\">\n          <div class=\"custom-control custom-checkbox mb-2 mr-sm-2 mb-sm-0\">\n            <input type=\"checkbox\" [checked]=\"config.layout.sidebar.collapsed\" class=\"custom-control-input cz-compact-menu\" id=\"cz-compact-menu\" (change)=\"toggleCompactMenu($event)\">\n            <label class=\"custom-control-label d-block\" for=\"cz-compact-menu\"></label>\n          </div>\n        </div>\n      </div>\n    </div>\n    <!--Compact Menu Ends-->\n    <hr>\n\n    <!--RTL Starts-->\n    <div class=\"togglebutton\">\n      <div class=\"switch switch border-0 d-flex justify-content-between w-100\">\n        <span>RTL</span>\n        <div class=\"float-right\">\n          <div class=\"custom-control custom-checkbox mb-2 mr-sm-2 mb-sm-0\">\n            <input type=\"checkbox\" [checked]=\"options.direction == 'rtl' ? 'checked' : false\" class=\"custom-control-input cz-rtl\"\n              id=\"cz-rtl\" (change)=\"options.direction = (options.direction == 'rtl' ? 'ltr' : 'rtl'); sendOptions()\">\n            <label class=\"custom-control-label d-block\" for=\"cz-rtl\"></label>\n          </div>\n        </div>\n      </div>\n    </div>\n    <!--RTL Ends-->\n    <hr>\n\n    <!--Sidebar Width Starts-->\n    <div>\n      <label for=\"cz-sidebar-width\">Sidebar Width</label>\n      <select id=\"cz-sidebar-width\" #width class=\"custom-select cz-sidebar-width float-right\" (change)=\"changeSidebarWidth(width.value)\">\n        <option value=\"sidebar-sm\" [selected] = \"size === 'sidebar-sm'\">Small</option>\n        <option value=\"sidebar-md\" [selected] = \"size === 'sidebar-md'\">Medium</option>\n        <option value=\"sidebar-lg\" [selected] = \"size === 'sidebar-lg'\">Large</option>\n      </select>\n    </div>\n    <!--Sidebar Width Ends-->\n  </div>\n</div>\n<!--Theme customizer Ends-->\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/shared/footer/footer.component.html":
/*!*******************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/shared/footer/footer.component.html ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--Footer Starts-->\n<footer>\n    <div class=\"container-fluid\">\n        <p class=\"copyright text-center\">\n                Copyright  &copy;  {{currentDate | date: 'yyyy'}} <a id=\"pixinventLink\" href=\"https://themeforest.net/user/pixinvent/portfolio?ref=pixinvent\">PIXINVENT</a>, All rights reserved.          \n        </p>\n        \n    </div>\n</footer>\n<!--Footer Ends-->"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/shared/navbar/navbar.component.html":
/*!*******************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/shared/navbar/navbar.component.html ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- Navbar (Header) Starts -->\n<nav class=\"header-navbar navbar navbar-expand-lg navbar-light bg-faded\">\n    <div class=\"container-fluid\">\n        <div class=\"navbar-header\">\n            <button type=\"button\" class=\"navbar-toggle d-lg-none float-left\" data-toggle=\"collapse\" (click)=\"toggleSidebar()\">\n                <span class=\"sr-only\">Toggle navigation</span>\n                <span class=\"icon-bar\"></span>\n                <span class=\"icon-bar\"></span>\n                <span class=\"icon-bar\"></span>\n            </button>\n            <span class=\"d-lg-none navbar-right navbar-collapse-toggle\">\n                <a class=\"open-navbar-container\" (click)=\"isCollapsed = !isCollapsed\" [attr.aria-expanded]=\"!isCollapsed\" aria-controls=\"navbarSupportedContent\">\n                    <i class=\"ft-more-vertical\"></i>\n                </a>\n            </span>\n            <!-- <form class=\"navbar-form navbar-right mt-1\" role=\"search\">\n                <div class=\"position-relative has-icon-right\">\n                    <input type=\"text\" class=\"form-control round\" placeholder=\"Search\">\n                    <div class=\"form-control-position\">\n                        <i class=\"ft-search\"></i>\n                    </div>\n                </div>\n            </form> -->\n           \n        </div>\n        <div class=\"navbar-container\">\n            <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\" [ngbCollapse]=\"isCollapsed\">\n                <ul class=\"navbar-nav\">\n                    <li class=\"nav-item mr-2  d-none d-lg-block\">\n                        <a href=\"javascript:;\" class=\"nav-link\" id=\"navbar-fullscreen\" appToggleFullscreen (click)=\"ToggleClass()\">                            \n                            <i class=\"{{toggleClass}} font-medium-3 blue-grey darken-4\"></i>\n                            <p class=\"d-none\">fullscreen</p>\n                        </a>\n                    </li>\n                    <!-- <li class=\"nav-item\" ngbDropdown [placement]=\"placement\">\n                        <a class=\"nav-link position-relative\" id=\"dropdownLang\" ngbDropdownToggle>\n                            <i class=\"ft-flag font-medium-3 blue-grey darken-4\"></i>\n                            <p class=\"d-none\">Language</p>\n                        </a>\n                        <div ngbDropdownMenu aria-labelledby=\"dropdownLang\" class=\"dropdownLang text-left\">                           \n                            <a class=\"dropdown-item py-1 lang\" href=\"javascript:;\" (click)=\"ChangeLanguage('en')\">\n                                <img src=\"./assets/img/flags/us.png\" alt=\"English\" class=\"langimg\">\n                                <span>English</span>\n                            </a>\n                            <a class=\"dropdown-item py-1 lang\" href=\"javascript:;\" (click)=\"ChangeLanguage('es')\">\n                                <img src=\"./assets/img/flags/es.png\" alt=\"Spanish\" class=\"langimg\">\n                                <span>Spanish</span>\n                            </a> \n                            <a class=\"dropdown-item py-1 lang\" href=\"javascript:;\" (click)=\"ChangeLanguage('pt')\">\n                                <img src=\"./assets/img/flags/br.png\" alt=\"Portuguese\" class=\"langimg\">\n                                <span>Portuguese</span>\n                            </a> \n                            <a class=\"dropdown-item py-1 lang\" href=\"javascript:;\" (click)=\"ChangeLanguage('de')\">\n                                <img src=\"./assets/img/flags/de.png\" alt=\"German\" class=\"langimg\">\n                                <span>German</span>\n                            </a>                            \n                        </div>\n                    </li> -->\n                    <li class=\"nav-item\" ngbDropdown [placement]=\"placement\">\n                        <a class=\"nav-link position-relative\" id=\"dropdownBasic2\" ngbDropdownToggle>\n                            <i class=\"ft-bell font-medium-3 blue-grey darken-4\"></i>\n                            <span class=\"notification badge badge-pill badge-light\">1</span>\n                            <p class=\"d-none\">Notifications</p>\n                        </a>\n                        <div ngbDropdownMenu aria-labelledby=\"dropdownBasic2\" class=\"notification-dropdown\">\n                            <div class=\"noti-list\" [perfectScrollbar]>\n                                <a class=\"dropdown-item noti-container py-3 border-bottom border-bottom-blue-grey border-bottom-lighten-4\">\n                                    <i class=\"ft-bell info float-left d-block font-large-1 mt-1 mr-2\"></i>\n                                    <span class=\"noti-wrapper\">\n                                        <span class=\"noti-title line-height-1 d-block text-bold-400 info\">แจ้งเตือนการชำระเงิน</span>\n                                        <span class=\"noti-text\">มีการแจ้งชำระเงินจากนายป้อม เวลา 12:00 น.</span>\n                                    </span>\n                                </a>\n                            </div>\n                            <a class=\"noti-footer primary text-center d-block border-top border-top-blue-grey border-top-lighten-4 text-bold-400 py-1\">อ่านแล้วทั้งหมด</a>\n                        </div>\n                    </li>\n                    <li class=\"nav-item\" ngbDropdown [placement]=\"placement\">\n                        <a class=\"nav-link position-relative\" id=\"dropdownBasic3\" ngbDropdownToggle>\n                            <i class=\"ft-user font-medium-3 blue-grey darken-4\"></i>\n                            <a class=\"user_name\"> {{admin.name}}</a>\n                        </a>\n                        <div ngbDropdownMenu aria-labelledby=\"dropdownBasic3\" class=\"text-left\">                           \n                            <a class=\"dropdown-item py-1\" href=\"javascript:;\" >\n                                <i class=\"ft-settings mr-2\"></i>\n                                <span>Settings</span>\n                            </a>\n                            <div class=\"dropdown-divider\"></div>\n                            <a class=\"dropdown-item\" href=\"javascript:;\" (click)=\"logout()\">\n                                <i class=\"ft-power mr-2\"></i>\n                                <span>Logout</span>\n                            </a>\n                        </div>\n                    </li>\n                    <!-- <li class=\"nav-item d-none d-lg-block\">\n                        <a class=\"nav-link position-relative notification-sidebar-toggle\" (click)=\"toggleNotificationSidebar();\">\n                            <i class=\"ft-align-left font-medium-3 blue-grey darken-4\"></i>\n                            <p class=\"d-none\">Notifications Sidebar</p>\n                        </a>\n                    </li> -->\n                   \n                </ul>\n            </div>\n        </div>\n    </div>\n</nav>\n<!-- Navbar (Header) Ends -->"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/shared/notification-sidebar/notification-sidebar.component.html":
/*!***********************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/shared/notification-sidebar/notification-sidebar.component.html ***!
  \***********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- //////////////////////////////////////////////////////////////////////////// -->\n<!-- START Notification Sidebar -->\n<aside #sidebar id=\"notification-sidebar\" class=\"notification-sidebar d-none d-sm-none d-md-block\">\n  <a class=\"notification-sidebar-close\" (click)=\"onClose()\">\n    <i class=\"ft-x font-medium-3\"></i>\n  </a>\n  <div class=\"side-nav notification-sidebar-content\" [perfectScrollbar]>\n    <div class=\"row\">\n      <div class=\"col-12 mt-1\">\n        <ngb-tabset>\n          <ngb-tab>\n            <ng-template ngbTabTitle><b>Activity</b></ng-template>\n            <ng-template ngbTabContent>\n              <div id=\"activity\" class=\"col-12 timeline-left\">\n                <h6 class=\"mt-1 mb-3 text-bold-400 text-left\">RECENT ACTIVITY</h6>\n                <div id=\"timeline\" class=\"timeline-left timeline-wrapper\">\n                  <ul class=\"timeline\">\n                    <li class=\"timeline-line\"></li>\n                    <li class=\"timeline-item text-left text-left\">\n                      <div class=\"timeline-badge\">\n                        <span class=\"bg-purple bg-lighten-1\" data-toggle=\"tooltip\" data-placement=\"right\"\n                          title=\"Portfolio project work\"><i class=\"ft-shopping-cart\"></i></span>\n                      </div>\n                      <div class=\"col s9 recent-activity-list-text\">\n                        <a href=\"#\" class=\"deep-purple-text medium-small\">just now</a>\n                        <p class=\"mt-0 mb-2 fixed-line-height font-weight-300 medium-small\">Jim Doe Purchased new\n                          equipments for zonal office.</p>\n                      </div>\n                    </li>\n                    <li class=\"timeline-item text-left\">\n                      <div class=\"timeline-badge\">\n                        <span class=\"bg-info bg-lighten-1\" data-toggle=\"tooltip\" data-placement=\"right\"\n                          title=\"Portfolio project work\"><i class=\"fa fa-plane\"></i></span>\n                      </div>\n                      <div class=\"col s9 recent-activity-list-text\">\n                        <a href=\"#\" class=\"cyan-text medium-small\">Yesterday</a>\n                        <p class=\"mt-0 mb-2 fixed-line-height font-weight-300 medium-small\">Your Next flight for USA\n                          will be on 15th August 2015.</p>\n                      </div>\n                    </li>\n                    <li class=\"timeline-item text-left\">\n                      <div class=\"timeline-badge\">\n                        <span class=\"bg-success bg-lighten-1\" data-toggle=\"tooltip\" data-placement=\"right\"\n                          title=\"Portfolio project work\"><i class=\"ft-mic\"></i></span>\n                      </div>\n                      <div class=\"col s9 recent-activity-list-text\">\n                        <a href=\"#\" class=\"green-text medium-small\">5 Days Ago</a>\n                        <p class=\"mt-0 mb-2 fixed-line-height font-weight-300 medium-small\">Natalya Parker Send you a\n                          voice mail for next conference.</p>\n                      </div>\n                    </li>\n                    <li class=\"timeline-item text-left\">\n                      <div class=\"timeline-badge\">\n                        <span class=\"bg-warning bg-lighten-1\" data-toggle=\"tooltip\" data-placement=\"right\"\n                          title=\"Portfolio project work\"><i class=\"ft-map-pin\"></i></span>\n                      </div>\n                      <div class=\"col s9 recent-activity-list-text\">\n                        <a href=\"#\" class=\"amber-text medium-small\">1 Week Ago</a>\n                        <p class=\"mt-0 mb-2 fixed-line-height font-weight-300 medium-small\">Jessy Jay open a new store\n                          at S.G Road.</p>\n                      </div>\n                    </li>\n                    <li class=\"timeline-item text-left\">\n                      <div class=\"timeline-badge\">\n                        <span class=\"bg-red bg-lighten-1\" data-toggle=\"tooltip\" data-placement=\"right\"\n                          title=\"Portfolio project work\"><i class=\"ft-inbox\"></i></span>\n                      </div>\n                      <div class=\"col s9 recent-activity-list-text\">\n                        <a href=\"#\" class=\"deep-orange-text medium-small\">2 Week Ago</a>\n                        <p class=\"mt-0 mb-2 fixed-line-height font-weight-300 medium-small\">voice mail for conference.\n                        </p>\n                      </div>\n                    </li>\n                    <li class=\"timeline-item text-left\">\n                      <div class=\"timeline-badge\">\n                        <span class=\"bg-cyan bg-lighten-1\" data-toggle=\"tooltip\" data-placement=\"right\"\n                          title=\"Portfolio project work\"><i class=\"ft-mic\"></i></span>\n                      </div>\n                      <div class=\"col s9 recent-activity-list-text\">\n                        <a href=\"#\" class=\"brown-text medium-small\">1 Month Ago</a>\n                        <p class=\"mt-0 mb-2 fixed-line-height font-weight-300 medium-small\">Natalya Parker Send you a\n                          voice mail for next conference.</p>\n                      </div>\n                    </li>\n                    <li class=\"timeline-item text-left\">\n                      <div class=\"timeline-badge\">\n                        <span class=\"bg-amber bg-lighten-1\" data-toggle=\"tooltip\" data-placement=\"right\"\n                          title=\"Portfolio project work\"><i class=\"ft-map-pin\"></i></span>\n                      </div>\n                      <div class=\"col s9 recent-activity-list-text\">\n                        <a href=\"#\" class=\"deep-purple-text medium-small\">3 Month Ago</a>\n                        <p class=\"mt-0 mb-2 fixed-line-height font-weight-300 medium-small\">Jessy Jay open a new store\n                          at S.G Road.</p>\n                      </div>\n                    </li>\n                    <li class=\"timeline-item text-left\">\n                      <div class=\"timeline-badge\">\n                        <span class=\"bg-grey bg-lighten-1\" data-toggle=\"tooltip\" data-placement=\"right\"\n                          title=\"Portfolio project work\"><i class=\"ft-inbox\"></i></span>\n                      </div>\n                      <div class=\"col s9 recent-activity-list-text\">\n                        <a href=\"#\" class=\"grey-text medium-small\">1 Year Ago</a>\n                        <p class=\"mt-0 mb-2 fixed-line-height font-weight-300 medium-small\">voice mail for conference.\n                        </p>\n                      </div>\n                    </li>\n                  </ul>\n                </div>\n              </div>\n            </ng-template>\n          </ngb-tab>\n          <ngb-tab>\n            <ng-template ngbTabTitle><b>Chat</b></ng-template>\n            <ng-template ngbTabContent>\n              <div id=\"chatapp\" class=\"col-12\">\n                <h6 class=\"mt-1 mb-3 text-bold-400 text-left\">RECENT CHAT</h6>\n                <div class=\"collection border-none\">\n                  <div class=\"media mb-1\">\n                    <a>\n                      <img alt=\"96x96\" class=\"media-object d-flex mr-3 bg-primary height-50 rounded-circle\"\n                        src=\"assets/img/portrait/small/avatar-s-12.png\">\n                    </a>\n                    <div class=\"media-body text-left\">\n                      <div class=\"clearfix\">\n                        <h4 class=\"font-medium-1 primary mt-1 mb-0 mr-auto float-left\">Elizabeth Elliott </h4>\n                        <span class=\"medium-small float-right blue-grey-text text-lighten-3\">5.00 AM</span>\n                      </div>\n                      <p class=\"text-muted font-small-3\">Thank you </p>\n                    </div>\n                  </div>\n                  <div class=\"media mb-1\">\n                    <a>\n                      <img alt=\"96x96\" class=\"media-object d-flex mr-3 bg-primary height-50 rounded-circle\"\n                        src=\"assets/img/portrait/small/avatar-s-6.png\">\n                    </a>\n                    <div class=\"media-body text-left\">\n                      <div class=\"clearfix\">\n                        <h4 class=\"font-medium-1 primary mt-1 mb-0 mr-auto float-left\">Mary Adams </h4>\n                        <span class=\"medium-small float-right blue-grey-text text-lighten-3\">4.14 AM</span>\n                      </div>\n                      <p class=\"text-muted font-small-3\">Hello Boo </p>\n                    </div>\n                  </div>\n                  <div class=\"media mb-1\">\n                    <a>\n                      <img alt=\"96x96\" class=\"media-object d-flex mr-3 bg-primary height-50 rounded-circle\"\n                        src=\"assets/img/portrait/small/avatar-s-11.png\">\n                    </a>\n                    <div class=\"media-body text-left\">\n                      <div class=\"clearfix\">\n                        <h4 class=\"font-medium-1 primary mt-1 mb-0 mr-auto float-left\">Caleb Richards </h4>\n                        <span class=\"medium-small float-right blue-grey-text text-lighten-3\">9.00 PM</span>\n                      </div>\n                      <p class=\"text-muted font-small-3\">Keny ! </p>\n                    </div>\n                  </div>\n                  <div class=\"media mb-1\">\n                    <a>\n                      <img alt=\"96x96\" class=\"media-object d-flex mr-3 bg-primary height-50 rounded-circle\"\n                        src=\"assets/img/portrait/small/avatar-s-18.png\">\n                    </a>\n                    <div class=\"media-body text-left\">\n                      <div class=\"clearfix\">\n                        <h4 class=\"font-medium-1 primary mt-1 mb-0 mr-auto float-left\">June Lane </h4>\n                        <span class=\"medium-small float-right blue-grey-text text-lighten-3\">4.14 AM</span>\n                      </div>\n                      <p class=\"text-muted font-small-3\">Ohh God </p>\n                    </div>\n                  </div>\n                  <div class=\"media mb-1\">\n                    <a>\n                      <img alt=\"96x96\" class=\"media-object d-flex mr-3 bg-primary height-50 rounded-circle\"\n                        src=\"assets/img/portrait/small/avatar-s-1.png\">\n                    </a>\n                    <div class=\"media-body text-left\">\n                      <div class=\"clearfix\">\n                        <h4 class=\"font-medium-1 primary mt-1 mb-0 mr-auto float-left\">Edward Fletcher </h4>\n                        <span class=\"medium-small float-right blue-grey-text text-lighten-3\">5.15 PM</span>\n                      </div>\n                      <p class=\"text-muted font-small-3\">Love you </p>\n                    </div>\n                  </div>\n                  <div class=\"media mb-1\">\n                    <a>\n                      <img alt=\"96x96\" class=\"media-object d-flex mr-3 bg-primary height-50 rounded-circle\"\n                        src=\"assets/img/portrait/small/avatar-s-2.png\">\n                    </a>\n                    <div class=\"media-body text-left\">\n                      <div class=\"clearfix\">\n                        <h4 class=\"font-medium-1 primary mt-1 mb-0 mr-auto float-left\">Crystal Bates </h4>\n                        <span class=\"medium-small float-right blue-grey-text text-lighten-3\">8.00 AM</span>\n                      </div>\n                      <p class=\"text-muted font-small-3\">Can we </p>\n                    </div>\n                  </div>\n                  <div class=\"media mb-1\">\n                    <a>\n                      <img alt=\"96x96\" class=\"media-object d-flex mr-3 bg-primary height-50 rounded-circle\"\n                        src=\"assets/img/portrait/small/avatar-s-3.png\">\n                    </a>\n                    <div class=\"media-body text-left\">\n                      <div class=\"clearfix\">\n                        <h4 class=\"font-medium-1 primary mt-1 mb-0 mr-auto float-left\">Nathan Watts </h4>\n                        <span class=\"medium-small float-right blue-grey-text text-lighten-3\">9.53 PM</span>\n                      </div>\n                      <p class=\"text-muted font-small-3\">Great! </p>\n                    </div>\n                  </div>\n                  <div class=\"media mb-1\">\n                    <a>\n                      <img alt=\"96x96\" class=\"media-object d-flex mr-3 bg-primary height-50 rounded-circle\"\n                        src=\"assets/img/portrait/small/avatar-s-15.png\">\n                    </a>\n                    <div class=\"media-body text-left\">\n                      <div class=\"clearfix\">\n                        <h4 class=\"font-medium-1 primary mt-1 mb-0 mr-auto float-left\">Willard Wood </h4>\n                        <span class=\"medium-small float-right blue-grey-text text-lighten-3\">4.20 AM</span>\n                      </div>\n                      <p class=\"text-muted font-small-3\">Do it </p>\n                    </div>\n                  </div>\n                  <div class=\"media mb-1\">\n                    <a>\n                      <img alt=\"96x96\" class=\"media-object d-flex mr-3 bg-primary height-50 rounded-circle\"\n                        src=\"assets/img/portrait/small/avatar-s-19.png\">\n                    </a>\n                    <div class=\"media-body text-left\">\n                      <div class=\"clearfix\">\n                        <h4 class=\"font-medium-1 primary mt-1 mb-0 mr-auto float-left\">Ronnie Ellis </h4>\n                        <span class=\"medium-small float-right blue-grey-text text-lighten-3\">5.30 PM</span>\n                      </div>\n                      <p class=\"text-muted font-small-3\">Got that </p>\n                    </div>\n                  </div>\n                  <div class=\"media mb-1\">\n                    <a>\n                      <img alt=\"96x96\" class=\"media-object d-flex mr-3 bg-primary height-50 rounded-circle\"\n                        src=\"assets/img/portrait/small/avatar-s-14.png\">\n                    </a>\n                    <div class=\"media-body text-left\">\n                      <div class=\"clearfix\">\n                        <h4 class=\"font-medium-1 primary mt-1 mb-0 mr-auto float-left\">Gwendolyn Wood </h4>\n                        <span class=\"medium-small float-right blue-grey-text text-lighten-3\">4.34 AM</span>\n                      </div>\n                      <p class=\"text-muted font-small-3\">Like you </p>\n                    </div>\n                  </div>\n                  <div class=\"media mb-1\">\n                    <a>\n                      <img alt=\"96x96\" class=\"media-object d-flex mr-3 bg-primary height-50 rounded-circle\"\n                        src=\"assets/img/portrait/small/avatar-s-13.png\">\n                    </a>\n                    <div class=\"media-body text-left\">\n                      <div class=\"clearfix\">\n                        <h4 class=\"font-medium-1 primary mt-1 mb-0 mr-auto float-left\">Daniel Russell </h4>\n                        <span class=\"medium-small float-right blue-grey-text text-lighten-3\">12.00 AM</span>\n                      </div>\n                      <p class=\"text-muted font-small-3\">Thank you </p>\n                    </div>\n                  </div>\n                  <div class=\"media mb-1\">\n                    <a>\n                      <img alt=\"96x96\" class=\"media-object d-flex mr-3 bg-primary height-50 rounded-circle\"\n                        src=\"assets/img/portrait/small/avatar-s-22.png\">\n                    </a>\n                    <div class=\"media-body text-left\">\n                      <div class=\"clearfix\">\n                        <h4 class=\"font-medium-1 primary mt-1 mb-0 mr-auto float-left\">Sarah Graves </h4>\n                        <span class=\"medium-small float-right blue-grey-text text-lighten-3\">11.14 PM</span>\n                      </div>\n                      <p class=\"text-muted font-small-3\">Okay you </p>\n                    </div>\n                  </div>\n                  <div class=\"media mb-1\">\n                    <a>\n                      <img alt=\"96x96\" class=\"media-object d-flex mr-3 bg-primary height-50 rounded-circle\"\n                        src=\"assets/img/portrait/small/avatar-s-9.png\">\n                    </a>\n                    <div class=\"media-body text-left\">\n                      <div class=\"clearfix\">\n                        <h4 class=\"font-medium-1 primary mt-1 mb-0 mr-auto float-left\">Andrew Hoffman </h4>\n                        <span class=\"medium-small float-right blue-grey-text text-lighten-3\">7.30 PM</span>\n                      </div>\n                      <p class=\"text-muted font-small-3\">Can do </p>\n                    </div>\n                  </div>\n                  <div class=\"media mb-1\">\n                    <a>\n                      <img alt=\"96x96\" class=\"media-object d-flex mr-3 bg-primary height-50 rounded-circle\"\n                        src=\"assets/img/portrait/small/avatar-s-20.png\">\n                    </a>\n                    <div class=\"media-body text-left\">\n                      <div class=\"clearfix\">\n                        <h4 class=\"font-medium-1 primary mt-1 mb-0 mr-auto float-left\">Camila Lynch </h4>\n                        <span class=\"medium-small float-right blue-grey-text text-lighten-3\">2.00 PM</span>\n                      </div>\n                      <p class=\"text-muted font-small-3\">Leave it </p>\n                    </div>\n                  </div>\n                </div>\n              </div>\n            </ng-template>\n          </ngb-tab>\n          <ngb-tab>\n            <ng-template ngbTabTitle><b>Settings</b></ng-template>\n            <ng-template ngbTabContent>\n              <div id=\"settings\" class=\"col-12\">\n                <h6 class=\"mt-1 mb-3 text-bold-400 text-left\">GENERAL SETTINGS</h6>\n                <ul class=\"list-unstyled\">\n                  <li class=\"text-left\">\n                    <div class=\"togglebutton\">\n                      <div class=\"switch d-flex justify-content-between w-100\">\n                        <span class=\"text-bold-500\">Notifications</span>\n                        <div class=\"notification-cb\">\n                          <div class=\"custom-control custom-checkbox mb-2 mr-sm-2 mb-sm-0\">\n                            <input checked=\"checked\" class=\"custom-control-input \" type=\"checkbox\"\n                              id=\"notifications1\">\n                            <label class=\"custom-control-label d-block\" for=\"notifications1\"></label>\n                          </div>\n                        </div>\n                      </div>\n                    </div>\n                    <p>Use checkboxes when looking for yes or no answers.</p>\n                  </li>\n                  <li class=\"text-left\">\n                    <div class=\"togglebutton\">\n                      <div class=\"switch d-flex justify-content-between w-100\">\n                        <span class=\"text-bold-500\">Show recent activity</span>\n                        <div class=\"notification-cb\">\n                          <div class=\"custom-control custom-checkbox mb-2 mr-sm-2 mb-sm-0\">\n                            <input checked=\"checked\" class=\"custom-control-input \" type=\"checkbox\"\n                              id=\"recent-activity1\">\n                            <label class=\"custom-control-label d-block\" for=\"recent-activity1\"></label>\n                          </div>\n                        </div>\n                      </div>\n                    </div>\n                    <p>The for attribute is necessary to bind our custom checkbox with the input.</p>\n                  </li>\n                  <li class=\"text-left\">\n                    <div class=\"togglebutton\">\n                      <div class=\"switch  d-flex justify-content-between w-100\">\n                        <span class=\"text-bold-500\">Notifications</span>\n                        <div class=\"notification-cb\">\n                          <div class=\"custom-control custom-checkbox mb-2 mr-sm-2 mb-sm-0\">\n                            <input class=\"custom-control-input \" type=\"checkbox\" id=\"notifications2\">\n                            <label class=\"custom-control-label d-block\" for=\"notifications2\"></label>\n                          </div>\n                        </div>\n                      </div>\n                    </div>\n                    <p>Use checkboxes when looking for yes or no answers.</p>\n                  </li>\n                  <li class=\"text-left\">\n                    <div class=\"togglebutton\">\n                      <div class=\"switch d-flex justify-content-between w-100\">\n                        <span class=\"text-bold-500\">Show recent activity</span>\n                        <div class=\"notification-cb\">\n                          <div class=\"custom-control custom-checkbox mb-2 mr-sm-2 mb-sm-0\">\n                            <input class=\"custom-control-input \" type=\"checkbox\"\n                              id=\"recent-activity2\">\n                            <label class=\"custom-control-label d-block\" for=\"recent-activity2\"></label>\n                          </div>\n                        </div>\n                      </div>\n                    </div>\n                    <p>The for attribute is necessary to bind our custom checkbox with the input.</p>\n                  </li>\n                  <li class=\"text-left\">\n                    <div class=\"togglebutton\">\n                      <div class=\"switch d-flex justify-content-between w-100\">\n                        <span class=\"text-bold-500\">Show your emails</span>\n                        <div class=\"notification-cb\">\n                          <div class=\"custom-control custom-checkbox mb-2 mr-sm-2 mb-sm-0\">\n                            <input class=\"custom-control-input \" type=\"checkbox\" id=\"show-emails\">\n                            <label class=\"custom-control-label d-block\" for=\"show-emails\"></label>\n                          </div>\n                        </div>\n                      </div>\n                    </div>\n                    <p>Use checkboxes when looking for yes or no answers.</p>\n                  </li>\n                  <li class=\"text-left\">\n                    <div class=\"togglebutton\">\n                      <div class=\"switch d-flex justify-content-between w-100\">\n                        <span class=\"text-bold-500\">Show Task statistics</span>\n                        <div class=\"notification-cb\">\n                          <div class=\"custom-control custom-checkbox mb-2 mr-sm-2 mb-sm-0\">\n                            <input class=\"custom-control-input \" type=\"checkbox\" id=\"show-stats\">\n                            <label class=\"custom-control-label d-block\" for=\"show-stats\"></label>\n                          </div>\n                        </div>\n                      </div>\n                    </div>\n                    <p>The for attribute is necessary to bind our custom checkbox with the input.</p>\n                  </li>\n                </ul>\n              </div>\n            </ng-template>\n          </ngb-tab>\n        </ngb-tabset>\n      </div>\n    </div>\n  </div>\n</aside>\n<!-- END Notification Sidebar -->"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/shared/sidebar/sidebar.component.html":
/*!*********************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/shared/sidebar/sidebar.component.html ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- Sidebar Header starts -->\n<div class=\"sidebar-header\">\n    <div class=\"logo clearfix\">\n        <a [routerLink]=\"['/']\" class=\"logo-text float-left\">\n            <div class=\"logo-img\">\n                <img [src]=\"logoUrl\" />\n            </div>\n            <span class=\"text align-middle\">concrete</span>\n        </a>\n        <a class=\"nav-toggle d-none d-sm-none d-md-none d-lg-block\" id=\"sidebarToggle\" href=\"javascript:;\">\n            <i #toggleIcon appSidebarToggle class=\"ft-toggle-right toggle-icon\" data-toggle=\"expanded\"></i>\n        </a>\n        <a class=\"nav-close d-block d-md-block d-lg-none d-xl-none\" id=\"sidebarClose\" href=\"javascript:;\">\n            <i class=\"ft-x\"></i>\n        </a>\n    </div>\n</div>\n<!-- Sidebar Header Ends -->\n\n<!-- Sidebar Content starts -->\n<div class=\"sidebar-content\"  [perfectScrollbar]>\n    <div class=\"nav-container\">\n        <ul class=\"navigation\" appSidebarList>\n            <!-- First level menu -->\n             <li appSidebarlink level=\"{{depth + 1}}\" (toggleEmit)=\"handleToggle($event)\" (click)=\"toggleSlideInOut()\" [routePath]=\"menuItem.path\" [classes]=\"menuItem.class\" [title]=\"menuItem.title\"  [parent]=\"\" *ngFor=\"let menuItem of menuItems\"   [ngClass]=\"{'has-sub': menuItem.class === 'has-sub' ? true: false, 'open': activeTitles.includes(menuItem.title) && !nav_collapsed_open ? true : false, 'nav-collapsed-open': nav_collapsed_open && activeTitles.includes(menuItem.title)}\" [routerLinkActive]=\"menuItem.submenu.length != 0 ? '' : 'active'\"\n                [routerLinkActiveOptions]=\"{exact: true}\">\n                <a appSidebarAnchorToggle [routerLink]=\"menuItem.class === '' ? [menuItem.path] : null\" *ngIf=\"!menuItem.isExternalLink; else externalLinkBlock\">\n                    <i [ngClass]=\"[menuItem.icon]\"></i>\n                    <span class=\"menu-title\">{{menuItem.title | translate }}</span>\n                    <span *ngIf=\"menuItem.badge != '' \" [ngClass]=\"[menuItem.badgeClass]\">{{menuItem.badge}}</span>\n                </a>\n                <ng-template #externalLinkBlock>\n                    <a [href]=\"[menuItem.path]\" target=\"_blank\">\n                        <i [ngClass]=\"[menuItem.icon]\"></i>\n                        <span class=\"menu-title\">{{menuItem.title | translate }}</span>\n                        <span *ngIf=\"menuItem.badge != '' \" [ngClass]=\"[menuItem.badgeClass]\">{{menuItem.badge}}</span>\n                    </a>\n                </ng-template>\n                <!-- Second level menu -->\n                <ul class=\"menu-content\" *ngIf=\"menuItem.submenu.length > 0\" [@slideInOut]=\"activeTitles.includes(menuItem.title) ? true : false\">\n                    <li appSidebarlink level=\"{{depth + 2}}\" (toggleEmit)=\"handleToggle($event)\" [routePath]=\"menuSubItem.path\" [classes]=\"menuSubItem.class\" [title]=\"menuSubItem.title\" [parent]=\"menuItem.title\" *ngFor=\"let menuSubItem of menuItem.submenu\" [routerLinkActive]=\"menuSubItem.submenu.length > 0 ? '' : 'active'\" [ngClass]=\"{'has-sub': menuSubItem.class === 'has-sub' ? true: false, 'open': activeTitles.includes(menuSubItem.title) && !nav_collapsed_open ? true : false, 'nav-collapsed-open': nav_collapsed_open && activeTitles.includes(menuSubItem.title)}\">\n                        <a appSidebarAnchorToggle [routerLink]=\"menuSubItem.submenu.length > 0 ? null : [menuSubItem.path]\" *ngIf=\"!menuSubItem.isExternalLink; else externalSubLinkBlock\">\n                            <i [ngClass]=\"[menuSubItem.icon]\"></i>\n                            <span class=\"menu-title\">{{menuSubItem.title | translate }}</span>\n                            <span *ngIf=\"menuSubItem.badge != '' \" [ngClass]=\"[menuSubItem.badgeClass]\">{{menuSubItem.badge}}</span>\n                        </a>\n                        <ng-template #externalSubLinkBlock>\n                            <a [href]=\"[menuSubItem.path]\">\n                                <i [ngClass]=\"[menuSubItem.icon]\"></i>\n                                <span class=\"menu-title\">{{menuSubItem.title | translate }}</span>\n                                <span *ngIf=\"menuSubItem.badge != '' \" [ngClass]=\"[menuSubItem.badgeClass]\">{{menuSubItem.badge}}</span>\n                            </a>\n                        </ng-template>\n                        <!-- Third level menu -->\n                        <ul class=\"menu-content\" *ngIf=\"menuSubItem.submenu.length > 0\" [@slideInOut]=\"activeTitles.includes(menuSubItem.title) ? true : false\">\n                            <li appSidebarlink level=\"{{depth + 3}}\" [routePath]=\"menuSubsubItem.path\" [classes]=\"menuSubsubItem.class\" [title]=\"menuSubsubItem.title\" [parent]=\"menuSubItem.title\" *ngFor=\"let menuSubsubItem of menuSubItem.submenu\" routerLinkActive=\"active\" [routerLinkActiveOptions]=\"{exact: true}\"\n                                [ngClass]=\"[menuSubsubItem.class]\">\n                                <a appSidebarAnchorToggle [routerLink]=\"[menuSubsubItem.path]\" *ngIf=\"!menuSubsubItem.isExternalLink; else externalSubSubLinkBlock\">\n                                    <i [ngClass]=\"[menuSubsubItem.icon]\"></i>\n                                    <span class=\"menu-title\">{{menuSubsubItem.title | translate }}</span>\n                                    <span *ngIf=\"menuSubsubItem.badge != '' \" [ngClass]=\"[menuSubsubItem.badgeClass]\">{{menuSubsubItem.badge}}</span>\n                                </a>\n                                <ng-template #externalSubSubLinkBlock>\n                                    <a [href]=\"[menuSubsubItem.path]\">\n                                        <i [ngClass]=\"[menuSubsubItem.icon]\"></i>\n                                        <span class=\"menu-title\">{{menuSubsubItem.title | translate }}</span>\n                                        <span *ngIf=\"menuSubsubItem.badge != '' \" [ngClass]=\"[menuSubsubItem.badgeClass]\">{{menuSubsubItem.badge}}</span>\n                                    </a>\n                                </ng-template>\n                            </li>\n                        </ul>\n                    </li>\n                </ul>\n            </li>\n        </ul>\n    </div>\n</div>\n<!-- Sidebar Content Ends -->\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/team/add_team.html":
/*!**************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/team/add_team.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<form [formGroup]=\"teamForm\" (ngSubmit)=\"onReactiveFormSubmit()\" novalidate>\r\n    <div class=\"modal-header\">\r\n        <h4 class=\"modal-title\">เพิ่มทีมงานใหม่</h4>\r\n        <button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"close()\">\r\n            <span aria-hidden=\"true\">&times;</span>\r\n        </button>\r\n    </div>\r\n    <div class=\"modal-body\">\r\n        <div class=\"w-100 text-center\">\r\n            <img class=\"avatar\" src=\"https://image.flaticon.com/icons/svg/194/194938.svg\" [src]=\"para.picture\">\r\n            <a class=\"change_img\" (click)=\"change_picture()\">\r\n                <i class=\"fa fa-camera\"></i>\r\n            </a>\r\n            <input type=\"file\" style=\"display: none;\" #file (change)=\"file_change($event)\">\r\n        </div>\r\n        <fieldset class=\"form-group mt-3\">\r\n            <label for=\"username\">ชื่อผู้ใช้</label>\r\n            <input type=\"text\" id=\"username\"\r\n                [ngClass]=\"{'invalid':!teamForm.get('username').valid && (teamForm.get('username').dirty || teamForm.get('username').touched)}\"\r\n                formControlName=\"username\" name=\"username\" class=\"form-control round\">\r\n            <small class=\"form-text text-muted danger\"\r\n                *ngIf=\"!teamForm.get('username').valid && (teamForm.get('username').dirty || teamForm.get('username').touched)\">กรุณากรอกผู้ใช้งานให้ถูกต้อง</small>\r\n        </fieldset>\r\n        <fieldset class=\"form-group\">\r\n            <label for=\"password\">รหัสผ่าน</label>\r\n            <input type=\"password\" id=\"password\"\r\n                [ngClass]=\"{'invalid':!teamForm.get('password').valid && (teamForm.get('password').dirty || teamForm.get('password').touched)}\"\r\n                formControlName=\"password\" name=\"password\" class=\"form-control round\">\r\n            <small class=\"form-text text-muted danger\"\r\n                *ngIf=\"!teamForm.get('password').valid && (teamForm.get('password').dirty || teamForm.get('password').touched)\">กรุณากรอกรหัสผ่านให้ถูกต้อง</small>\r\n        </fieldset>\r\n        <fieldset class=\"form-group\">\r\n            <label for=\"name\">ชื่อ - นามสกุล</label>\r\n            <input type=\"text\" id=\"name\"\r\n                [ngClass]=\"{'invalid':!teamForm.get('name').valid && (teamForm.get('name').dirty || teamForm.get('name').touched)}\"\r\n                formControlName=\"name\" name=\"name\" class=\"form-control round\">\r\n            <small class=\"form-text text-muted danger\"\r\n                *ngIf=\"!teamForm.get('name').valid && (teamForm.get('name').dirty || teamForm.get('name').touched)\">กรุณากรอก\r\n                ชื่อ - นามสกุล ให้ถูกต้อง</small>\r\n        </fieldset>\r\n        <fieldset class=\"form-group\">\r\n            <label for=\"email\">อีเมล์</label>\r\n            <input type=\"email\" id=\"email\"\r\n                [ngClass]=\"{'invalid':!teamForm.get('email').valid && (teamForm.get('email').dirty || teamForm.get('email').touched)}\"\r\n                formControlName=\"email\" name=\"email\" class=\"form-control round\">\r\n            <small class=\"form-text text-muted danger\"\r\n                *ngIf=\"!teamForm.get('email').valid && (teamForm.get('email').dirty || teamForm.get('email').touched)\">กรุณากรอกอีเมล์ให้ถูกต้อง</small>\r\n        </fieldset>\r\n        <fieldset class=\"form-group\">\r\n            <label for=\"tel\">เบอร์โทรศัพท์</label>\r\n            <input type=\"text\" id=\"phone\"\r\n                [ngClass]=\"{'invalid':!teamForm.get('phone').valid && (teamForm.get('phone').dirty || teamForm.get('phone').touched)}\"\r\n                formControlName=\"phone\" class=\"form-control round\">\r\n            <small class=\"form-text text-muted danger\"\r\n                *ngIf=\"!teamForm.get('phone').valid && (teamForm.get('phone').dirty || teamForm.get('phone').touched)\">กรุณากรอกเบอร์โทรศัพท์ให้ถูกต้อง</small>\r\n        </fieldset>\r\n        <fieldset class=\"form-group\">\r\n            <label for=\"gender\">เพศ</label>\r\n            <select id=\"gender\" name=\"gender\" formControlName=\"gender\" class=\"form-control round\">\r\n                <option selected value=\"m\">ชาย</option>\r\n                <option value=\"f\">หญิง</option>\r\n            </select>\r\n        </fieldset>\r\n        <fieldset class=\"form-group\">\r\n            <label for=\"type\">ประเภทผู้ใช้งาน</label>\r\n            <select id=\"type\" name=\"type\" formControlName=\"type\" class=\"form-control round\">\r\n                <option value=\"1\" [disabled]=\"admin_type > 1\">Admin</option>\r\n                <option selected value=\"2\">User</option>\r\n                <option value=\"3\">Accounting</option>\r\n            </select>\r\n        </fieldset>\r\n    </div>\r\n    <div class=\"modal-footer\">\r\n        <button type=\"button\" class=\"btn btn-secondary btn-raised\" (click)=\"close()\">ปิดออก</button>\r\n        <button type=\"submit\" class=\"btn btn-success mr-2 btn-raised \">บันทึก</button>\r\n    </div>\r\n</form>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/team/team.component.html":
/*!********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/team/team.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ng-template #view let-c=\"close\" let-d=\"dismiss\">\n\n</ng-template>\n<div class=\"row text-left\">\n  <div class=\"col-12\">\n    <div class=\"content-header\">ทีมงาน</div>\n    <!-- <p class=\"content-sub-header\">All table styles are inherited in Bootstrap 4, meaning any nested tables will be\n      styled in the same manner as the parent.</p> -->\n  </div>\n</div>\n<!--Basic Table Ends-->\n\n\n<!--Hoverable Rows Starts-->\n<section id=\"hoverable-rows\">\n  <div class=\"row text-left\">\n    <div class=\"col-sm-12\">\n      <div class=\"card\">\n        <div class=\"card-content\">\n\n          <div class=\"card-body\">\n\n            <div class=\"text-right navbar-right\" style=\"margin-top: -10px; margin-bottom: 10px;\">\n\n              <form class=\"navbar-form  mt-1 searchform\" [formGroup]=\"searchForm\" (ngSubmit)=\"search()\">\n                <div class=\"position-relative has-icon-right\">\n                  <input class=\"form-control round\" formControlName=\"search\" placeholder=\"Search\" type=\"text\">\n                  <div class=\"form-control-position\">\n                    <i class=\"ft-search\"></i>\n                  </div>\n               \n                </div>\n               \n              </form>\n              <button [disabled]=\"admin_type > 2\" style=\"margin-bottom: 5px;\" class=\"btn btn-raised mr-1 btn-primary\" (click)=\"open({t_id:'0'})\"><i\n                class=\"fa fa-plus\"></i>\n              เพิ่มทีมงาน</button>\n             \n            </div>\n\n\n            <table class=\"table table-hover\">\n              <thead>\n                <tr>\n                  <th style=\"width: 50px;\">#</th>\n                  <th style=\"width: 60px;\" class=\"text-center\">ภาพ</th>\n                  <th>ชื่อ - สกุล</th>\n                  <th>ชื่อเข้าใช้งาน</th>\n                  <th>เบอร์โทรศัพท์</th>\n                  <th style=\"width: 200px;\" class=\"text-center\">จัดการ</th>\n                </tr>\n              </thead>\n              <tbody>\n                <tr *ngFor=\"let t of teams\">\n                  <th scope=\"row\">{{t.t_id}}</th>\n                  <td style=\"padding: 2px;\"><img style=\"width: 50px;border-radius: 50%;\" src=\"{{t.picture}}\"></td>\n                  <td>{{t.name}}</td>\n                  <td>{{t.username}}</td>\n                  <td>{{t.phone}}</td>\n                  <td>\n                    <button href=\"javascript:void(0)\" class=\"btn btn-raised mr-1 btn-primary btn-sm\"\n                      (click)=\"view_profile(t)\"><i class=\"fa fa-eye\"></i> ดู</button>\n                    <button href=\"javascript:void(0)\" (click)=\"open(t)\" [disabled]=\"admin_type>2 || admin_type>t.type\" class=\"btn btn-raised mr-1 btn-sm btn-warning\"><i\n                        class=\"fa fa-edit\"></i> แก้ไข</button>\n                    <button href=\"javascript:void(0)\" [disabled]=\"admin_type>2 || admin_type>t.type\" (click)=\"delete_member(t)\"\n                      class=\"btn btn-raised mr-1 btn-sm btn-danger\"><i class=\"fa fa-trash-o\"></i> ลบ</button>\n                  </td>\n                </tr>\n\n              </tbody>\n            </table>\n            <div class=\"col-12 justify-content-center\">\n              <ngb-pagination [collectionSize]=\"all_page\" ng-change=\"change_page(page)\" class=\"justify-content-center\"\n                [(page)]=\"page\"></ngb-pagination>\n\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</section>\n<!--Hoverable rows Ends-->"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/team/view_team.html":
/*!***************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/team/view_team.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"modal-header\">\r\n  <h4 class=\"modal-title\">{{para.username}}</h4>\r\n  <button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"close()\">\r\n    <span aria-hidden=\"true\">&times;</span>\r\n  </button>\r\n</div>\r\n<div class=\"modal-body\">\r\n  <div class=\"w-100 text-center\">\r\n    <img style=\"width: 80px;\" src=\"{{para.picture}}\">\r\n  </div>\r\n\r\n  <div class=\"content\">\r\n    <dl>\r\n      <dt>ชื่อผู้ใช้ </dt>\r\n      <dd>{{para.username}}</dd>\r\n      <dt>ชื่อ - นามสกุล </dt>\r\n      <dd>{{para.name}}</dd>\r\n      <dt>อีเมล์ </dt>\r\n      <dd>{{para.email}}</dd>\r\n      <dt>เบอร์โทรศัพท์ </dt>\r\n      <dd>{{para.phone}}</dd>\r\n      <dt>เพศ </dt>\r\n      <dd><span *ngIf=\"para.gender=='m'\">ชาย</span><span *ngIf=\"para.gender=='f'\">หญิง</span></dd>\r\n    </dl>\r\n  </div>\r\n</div>\r\n<div class=\"modal-footer\">\r\n  <button type=\"button\" class=\"btn btn-secondary btn-raised\" (click)=\"close()\">ปิดออก</button>\r\n</div>"

/***/ }),

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _layouts_full_full_layout_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./layouts/full/full-layout.component */ "./src/app/layouts/full/full-layout.component.ts");
/* harmony import */ var _layouts_content_content_layout_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./layouts/content/content-layout.component */ "./src/app/layouts/content/content-layout.component.ts");
/* harmony import */ var _shared_routes_full_layout_routes__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./shared/routes/full-layout.routes */ "./src/app/shared/routes/full-layout.routes.ts");
/* harmony import */ var _shared_routes_content_layout_routes__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./shared/routes/content-layout.routes */ "./src/app/shared/routes/content-layout.routes.ts");
/* harmony import */ var _shared_auth_auth_guard_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./shared/auth/auth-guard.service */ "./src/app/shared/auth/auth-guard.service.ts");








var appRoutes = [
    {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full',
    },
    { path: '', component: _layouts_full_full_layout_component__WEBPACK_IMPORTED_MODULE_3__["FullLayoutComponent"], data: { title: 'full Views' },
        children: _shared_routes_full_layout_routes__WEBPACK_IMPORTED_MODULE_5__["Full_ROUTES"], canActivate: [_shared_auth_auth_guard_service__WEBPACK_IMPORTED_MODULE_7__["AuthGuard"]] },
    { path: '', component: _layouts_content_content_layout_component__WEBPACK_IMPORTED_MODULE_4__["ContentLayoutComponent"], data: { title: 'content Views' },
        children: _shared_routes_content_layout_routes__WEBPACK_IMPORTED_MODULE_6__["CONTENT_ROUTES"], canActivate: [_shared_auth_auth_guard_service__WEBPACK_IMPORTED_MODULE_7__["AuthGuard"]] },
    {
        path: '**',
        redirectTo: 'pages/error'
    }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(appRoutes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");




var AppComponent = /** @class */ (function () {
    function AppComponent(router, routeractive) {
        this.router = router;
        this.routeractive = routeractive;
    }
    AppComponent.prototype.ngAfterContentInit = function () {
        console.log(this.routeractive.toString());
    };
    AppComponent.prototype.ngOnInit = function () {
        this.subscription = this.router.events
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["filter"])(function (event) { return event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_2__["NavigationEnd"]; }))
            .subscribe(function () { return window.scrollTo(0, 0); });
    };
    AppComponent.prototype.ngOnDestroy = function () {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    };
    AppComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] }
    ]; };
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! raw-loader!./app.component.html */ "./node_modules/raw-loader/index.js!./src/app/app.component.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: createTranslateLoader, AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "createTranslateLoader", function() { return createTranslateLoader; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _shared_shared_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./shared/shared.module */ "./src/app/shared/shared.module.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _agm_core__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @agm/core */ "./node_modules/@agm/core/index.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var _ngx_translate_http_loader__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @ngx-translate/http-loader */ "./node_modules/@ngx-translate/http-loader/fesm5/ngx-translate-http-loader.js");
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/fesm5/store.js");
/* harmony import */ var ngx_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ngx-perfect-scrollbar */ "./node_modules/ngx-perfect-scrollbar/dist/ngx-perfect-scrollbar.es5.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _layouts_content_content_layout_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./layouts/content/content-layout.component */ "./src/app/layouts/content/content-layout.component.ts");
/* harmony import */ var _layouts_full_full_layout_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./layouts/full/full-layout.component */ "./src/app/layouts/full/full-layout.component.ts");
/* harmony import */ var ng2_dragula__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ng2-dragula */ "./node_modules/ng2-dragula/dist/fesm5/ng2-dragula.js");
/* harmony import */ var _shared_auth_auth_service__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./shared/auth/auth.service */ "./src/app/shared/auth/auth.service.ts");
/* harmony import */ var _shared_auth_auth_guard_service__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./shared/auth/auth-guard.service */ "./src/app/shared/auth/auth-guard.service.ts");
/* harmony import */ var _team_team_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./team/team.component */ "./src/app/team/team.component.ts");
/* harmony import */ var _saller_saller_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./saller/saller.component */ "./src/app/saller/saller.component.ts");
/* harmony import */ var _customer_customer_component__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./customer/customer.component */ "./src/app/customer/customer.component.ts");
/* harmony import */ var angular_web_storage__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! angular-web-storage */ "./node_modules/angular-web-storage/fesm5/angular-web-storage.js");
/* harmony import */ var _product_product_component__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./product/product.component */ "./src/app/product/product.component.ts");
/* harmony import */ var _angular_fire__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! @angular/fire */ "./node_modules/@angular/fire/index.js");
/* harmony import */ var _angular_fire_database__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! @angular/fire/database */ "./node_modules/@angular/fire/database/index.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _order_order_component__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! ./order/order.component */ "./src/app/order/order.component.ts");
/* harmony import */ var _news_news_component__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! ./news/news.component */ "./src/app/news/news.component.ts");
/* harmony import */ var _settings_settings_component__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! ./settings/settings.component */ "./src/app/settings/settings.component.ts");
/* harmony import */ var _ckeditor_ckeditor5_angular__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! @ckeditor/ckeditor5-angular */ "./node_modules/@ckeditor/ckeditor5-angular/fesm5/ckeditor-ckeditor5-angular.js");
/* harmony import */ var _payment_payment_component__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! ./payment/payment.component */ "./src/app/payment/payment.component.ts");
/* harmony import */ var _confirm_confirm_component__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(/*! ./confirm/confirm.component */ "./src/app/confirm/confirm.component.ts");
/* harmony import */ var _score_score_component__WEBPACK_IMPORTED_MODULE_34__ = __webpack_require__(/*! ./score/score.component */ "./src/app/score/score.component.ts");
/* harmony import */ var ngx_lightbox__WEBPACK_IMPORTED_MODULE_35__ = __webpack_require__(/*! ngx-lightbox */ "./node_modules/ngx-lightbox/index.js");
/* harmony import */ var ngx_lightbox__WEBPACK_IMPORTED_MODULE_35___default = /*#__PURE__*/__webpack_require__.n(ngx_lightbox__WEBPACK_IMPORTED_MODULE_35__);




































var DEFAULT_PERFECT_SCROLLBAR_CONFIG = {
    suppressScrollX: true,
    wheelPropagation: false
};
function createTranslateLoader(http) {
    return new _ngx_translate_http_loader__WEBPACK_IMPORTED_MODULE_11__["TranslateHttpLoader"](http, "./assets/i18n/", ".json");
}
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [_app_component__WEBPACK_IMPORTED_MODULE_14__["AppComponent"], _layouts_full_full_layout_component__WEBPACK_IMPORTED_MODULE_16__["FullLayoutComponent"], _order_order_component__WEBPACK_IMPORTED_MODULE_28__["OrderComponent"], _layouts_content_content_layout_component__WEBPACK_IMPORTED_MODULE_15__["ContentLayoutComponent"], _team_team_component__WEBPACK_IMPORTED_MODULE_20__["NgbdModalContent"], _news_news_component__WEBPACK_IMPORTED_MODULE_29__["NewsComponent"],
                _team_team_component__WEBPACK_IMPORTED_MODULE_20__["TeamComponent"], _product_product_component__WEBPACK_IMPORTED_MODULE_24__["ProductComponent"], _team_team_component__WEBPACK_IMPORTED_MODULE_20__["viewModalContent"], _saller_saller_component__WEBPACK_IMPORTED_MODULE_21__["SallerComponent"], _customer_customer_component__WEBPACK_IMPORTED_MODULE_22__["CustomerComponent"], _saller_saller_component__WEBPACK_IMPORTED_MODULE_21__["NgbdModalContentSaller"],
                _saller_saller_component__WEBPACK_IMPORTED_MODULE_21__["viewModalContentSaller"], _customer_customer_component__WEBPACK_IMPORTED_MODULE_22__["NgbdModalContentCustomer"], _customer_customer_component__WEBPACK_IMPORTED_MODULE_22__["viewModalContentCustomer"], _payment_payment_component__WEBPACK_IMPORTED_MODULE_32__["viewModalPayment"], _product_product_component__WEBPACK_IMPORTED_MODULE_24__["viewModalProduct"], _product_product_component__WEBPACK_IMPORTED_MODULE_24__["NgbdModalProduct"], _order_order_component__WEBPACK_IMPORTED_MODULE_28__["viewModalOrder"], _order_order_component__WEBPACK_IMPORTED_MODULE_28__["NgbdModalOrder"], _settings_settings_component__WEBPACK_IMPORTED_MODULE_30__["settingsComponent"],
                _news_news_component__WEBPACK_IMPORTED_MODULE_29__["NgbdModalNews"], _news_news_component__WEBPACK_IMPORTED_MODULE_29__["viewModalNews"], _payment_payment_component__WEBPACK_IMPORTED_MODULE_32__["PaymentComponent"], _settings_settings_component__WEBPACK_IMPORTED_MODULE_30__["NgbdModalBank"], _confirm_confirm_component__WEBPACK_IMPORTED_MODULE_33__["ConfirmComponent"], _score_score_component__WEBPACK_IMPORTED_MODULE_34__["ScoreComponent"], _score_score_component__WEBPACK_IMPORTED_MODULE_34__["viewModalScore"], _confirm_confirm_component__WEBPACK_IMPORTED_MODULE_33__["viewModalConfirm"]
            ],
            entryComponents: [_team_team_component__WEBPACK_IMPORTED_MODULE_20__["NgbdModalContent"], _team_team_component__WEBPACK_IMPORTED_MODULE_20__["viewModalContent"], _saller_saller_component__WEBPACK_IMPORTED_MODULE_21__["NgbdModalContentSaller"],
                _saller_saller_component__WEBPACK_IMPORTED_MODULE_21__["viewModalContentSaller"], _customer_customer_component__WEBPACK_IMPORTED_MODULE_22__["NgbdModalContentCustomer"], _settings_settings_component__WEBPACK_IMPORTED_MODULE_30__["NgbdModalBank"],
                _customer_customer_component__WEBPACK_IMPORTED_MODULE_22__["viewModalContentCustomer"], _payment_payment_component__WEBPACK_IMPORTED_MODULE_32__["viewModalPayment"], _product_product_component__WEBPACK_IMPORTED_MODULE_24__["viewModalProduct"], _score_score_component__WEBPACK_IMPORTED_MODULE_34__["viewModalScore"], _confirm_confirm_component__WEBPACK_IMPORTED_MODULE_33__["viewModalConfirm"],
                _product_product_component__WEBPACK_IMPORTED_MODULE_24__["NgbdModalProduct"], _order_order_component__WEBPACK_IMPORTED_MODULE_28__["viewModalOrder"], _order_order_component__WEBPACK_IMPORTED_MODULE_28__["NgbdModalOrder"], _news_news_component__WEBPACK_IMPORTED_MODULE_29__["NgbdModalNews"], _news_news_component__WEBPACK_IMPORTED_MODULE_29__["viewModalNews"]],
            imports: [
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_3__["BrowserAnimationsModule"],
                _ckeditor_ckeditor5_angular__WEBPACK_IMPORTED_MODULE_31__["CKEditorModule"],
                _ngrx_store__WEBPACK_IMPORTED_MODULE_12__["StoreModule"].forRoot({}),
                _app_routing_module__WEBPACK_IMPORTED_MODULE_5__["AppRoutingModule"],
                _shared_shared_module__WEBPACK_IMPORTED_MODULE_6__["SharedModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormsModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__["NgbModalModule"],
                angular_web_storage__WEBPACK_IMPORTED_MODULE_23__["AngularWebStorageModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_9__["HttpClientModule"],
                _angular_fire__WEBPACK_IMPORTED_MODULE_25__["AngularFireModule"].initializeApp(_environments_environment__WEBPACK_IMPORTED_MODULE_27__["environment"].firebase),
                _angular_fire_database__WEBPACK_IMPORTED_MODULE_26__["AngularFireDatabaseModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["ReactiveFormsModule"],
                ngx_toastr__WEBPACK_IMPORTED_MODULE_7__["ToastrModule"].forRoot(),
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__["NgbModule"].forRoot(),
                ngx_lightbox__WEBPACK_IMPORTED_MODULE_35__["LightboxModule"],
                _ngx_translate_core__WEBPACK_IMPORTED_MODULE_10__["TranslateModule"].forRoot({
                    loader: {
                        provide: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_10__["TranslateLoader"],
                        useFactory: createTranslateLoader,
                        deps: [_angular_common_http__WEBPACK_IMPORTED_MODULE_9__["HttpClient"]]
                    }
                }),
                _agm_core__WEBPACK_IMPORTED_MODULE_8__["AgmCoreModule"].forRoot({
                    apiKey: "YOUR KEY"
                }),
                ngx_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_13__["PerfectScrollbarModule"]
            ],
            providers: [
                _shared_auth_auth_service__WEBPACK_IMPORTED_MODULE_18__["AuthService"],
                _shared_auth_auth_guard_service__WEBPACK_IMPORTED_MODULE_19__["AuthGuard"],
                ng2_dragula__WEBPACK_IMPORTED_MODULE_17__["DragulaService"],
                {
                    provide: ngx_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_13__["PERFECT_SCROLLBAR_CONFIG"],
                    useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
                },
                { provide: ngx_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_13__["PERFECT_SCROLLBAR_CONFIG"], useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG }
            ],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_14__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/confirm/confirm.component.scss":
/*!************************************************!*\
  !*** ./src/app/confirm/confirm.component.scss ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".btn[class*=\"btn-\"] {\n  margin-bottom: 0rem; }\n\n.justify-content-center {\n  display: flex !important;\n  justify-content: center !important; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29uZmlybS9DOlxcZGV2ZWxvcGVyXFxiYWNrZW5kL3NyY1xcYXBwXFxjb25maXJtXFxjb25maXJtLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksbUJBQW1CLEVBQUE7O0FBRXJCO0VBQ0Usd0JBQXdCO0VBQ3hCLGtDQUFrQyxFQUFBIiwiZmlsZSI6InNyYy9hcHAvY29uZmlybS9jb25maXJtLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmJ0bltjbGFzcyo9XCJidG4tXCJdIHtcclxuICAgIG1hcmdpbi1ib3R0b206IDByZW07XHJcbiAgfVxyXG4gIC5qdXN0aWZ5LWNvbnRlbnQtY2VudGVyIHtcclxuICAgIGRpc3BsYXk6IGZsZXggIWltcG9ydGFudDtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyICFpbXBvcnRhbnQ7XHJcbiAgfSJdfQ== */"

/***/ }),

/***/ "./src/app/confirm/confirm.component.ts":
/*!**********************************************!*\
  !*** ./src/app/confirm/confirm.component.ts ***!
  \**********************************************/
/*! exports provided: viewModalConfirm, ConfirmComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "viewModalConfirm", function() { return viewModalConfirm; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConfirmComponent", function() { return ConfirmComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var app_webapi_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! app/webapi.service */ "./src/app/webapi.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _angular_fire_database__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/fire/database */ "./node_modules/@angular/fire/database/index.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var ngx_lightbox__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-lightbox */ "./node_modules/ngx-lightbox/index.js");
/* harmony import */ var ngx_lightbox__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(ngx_lightbox__WEBPACK_IMPORTED_MODULE_7__);








var viewModalConfirm = /** @class */ (function () {
    function viewModalConfirm(activeModal, api, ref, firebase) {
        this.activeModal = activeModal;
        this.api = api;
        this.ref = ref;
        this.firebase = firebase;
        this.data = {
            sale_id: 0
        };
        this.admin = { name: '' };
        this.admin_type = 0;
        this.imagePath = '';
        this.url = '';
        this.formData = new FormData();
        this.bb_id = '';
    }
    viewModalConfirm.prototype.close = function () {
        this.activeModal.dismiss(this.data);
    };
    viewModalConfirm.prototype.ngOnInit = function () {
        var _this = this;
        console.log(this.data);
        this.admin = this.api.storage_get('logindata');
        this.api.getData('Confirm/salebank/' + this.data.sale_id).then(function (res) {
            _this.data.bank = res;
            console.log(res);
        });
    };
    viewModalConfirm.prototype.sbank = function (bb_id) {
        this.bb_id = bb_id;
        console.log(bb_id);
    };
    viewModalConfirm.prototype.confirm = function () {
        var _this = this;
        console.log(this.reader);
        if (this.bb_id != '') {
            if (this.reader != undefined) {
                this.api.postData('Confirm/confirm_sale', { img: this.reader.result, type: this.type, order_id: this.data.order_id, bb_id: this.bb_id, admin: this.admin.t_id, price: this.data.com_price }).then(function (res) {
                    if (res == 'success') {
                        _this.api.fb_set('order');
                        sweetalert2__WEBPACK_IMPORTED_MODULE_6___default.a.fire('สำเร็จ!', '', 'success');
                        setTimeout(function () {
                            _this.close();
                        }, 1000);
                    }
                    else {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_6___default.a.fire('สำเร็จ!', '', 'warning');
                        setTimeout(function () {
                            _this.close();
                        }, 1000);
                    }
                });
            }
            else {
                sweetalert2__WEBPACK_IMPORTED_MODULE_6___default.a.fire('กรุณาเลือกหลักฐานก่อน', '', 'warning');
            }
        }
        else {
            sweetalert2__WEBPACK_IMPORTED_MODULE_6___default.a.fire('กรุณาเลือกบัญชีธนาคารก่อน', '', 'warning');
        }
    };
    viewModalConfirm.prototype.cancel = function (pm_id) {
        // console.log('Cancel by admin ID =',this.admin.t_id,' pm_id =',pm_id);
        // this.api.postData('Payment/confirm_payment/',{pm_id:pm_id,id:this.admin.t_id,confirm:-1}).then((res)=>{
        //   this.close()
        // })
    };
    viewModalConfirm.prototype.changeImg = function (event) {
        var _this = this;
        var data = {
            member_id: '',
            message: ''
        };
        console.log(event);
        this.formData.append('avatar', event.target.files[0]);
        this.reader = new FileReader();
        this.reader.onload = function (event) {
            _this.url = event.target.result;
            _this.ref.detectChanges();
        };
        this.reader.readAsDataURL(event.target.files[0]);
        this.type = event.target.files[0].type;
        console.log(this.reader.result);
        setTimeout(function () {
            // this.api.postData("Confirm/upload", { img: this.reader.result, type: this.type, member_id:member_id }).then((res: any) => {
            // })
        }, 1000);
    };
    viewModalConfirm.ctorParameters = function () { return [
        { type: _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__["NgbActiveModal"] },
        { type: app_webapi_service__WEBPACK_IMPORTED_MODULE_2__["WebapiService"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"] },
        { type: _angular_fire_database__WEBPACK_IMPORTED_MODULE_5__["AngularFireDatabase"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], viewModalConfirm.prototype, "data", void 0);
    viewModalConfirm = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'view-modal-content',
            template: __webpack_require__(/*! raw-loader!./confirm_details.html */ "./node_modules/raw-loader/index.js!./src/app/confirm/confirm_details.html"),
            styles: [__webpack_require__(/*! ./confirm_details.scss */ "./src/app/confirm/confirm_details.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__["NgbActiveModal"],
            app_webapi_service__WEBPACK_IMPORTED_MODULE_2__["WebapiService"],
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"],
            _angular_fire_database__WEBPACK_IMPORTED_MODULE_5__["AngularFireDatabase"]])
    ], viewModalConfirm);
    return viewModalConfirm;
}());

var ConfirmComponent = /** @class */ (function () {
    function ConfirmComponent(api, modalService, firebase, lightbox) {
        this.api = api;
        this.modalService = modalService;
        this.firebase = firebase;
        this.lightbox = lightbox;
        this.all_page = 0;
        this.buff = {
            bb_id: '',
            order_id: ''
        };
        this.admin = { name: '' };
        this.fb = false;
    }
    ConfirmComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.fb = true;
        this.searchForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroup"]({
            'search': new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('')
        });
        this.admin = this.api.storage_get('logindata');
        this.api.fb_val('order', function (res) {
            if (_this.fb) {
                _this.load_bank(1);
            }
        });
        // this.load_score(1)
    };
    ConfirmComponent.prototype.ngOnDestroy = function () {
        this.fb = false;
    };
    ConfirmComponent.prototype.load_bank = function (page) {
        var _this = this;
        this.api.getData('Confirm/get_salebank/' + page + "?search=" + this.searchForm.value.search).then(function (res) {
            _this.order = res.order;
            console.log(res);
        });
    };
    ConfirmComponent.prototype.load_confirm = function (page) {
        var _this = this;
        this.api.getData('Confirm/load_confirm/' + page + "?search=" + this.searchForm.value.search).then(function (res) {
            console.log(res);
            _this.data = res.data;
            _this.all_page = res.count / 20 * 20;
        });
    };
    ConfirmComponent.prototype.search = function () {
        this.load_confirm(1);
    };
    ConfirmComponent.prototype.change_page = function ($event) {
        // console.log($event);
    };
    ConfirmComponent.prototype.test = function (t) {
        var _this = this;
        console.log(t);
        var modal = this.modalService.open(viewModalConfirm, { size: 'lg' });
        modal.componentInstance.payment = t;
        modal.result.then(function (result) {
            console.log(result);
        }, function (reason) {
        });
        modal.result.then(function (result) {
            _this.closeResult = "Closed with: " + result;
        }, function (reason) {
            _this.closeResult = "Dismissed " + _this.getDismissReason(reason);
            _this.load_bank(1);
            // this.load_score(1)
        });
    };
    ConfirmComponent.prototype.getDismissReason = function (reason) {
        // this.load_paymant(1)
        if (reason === _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__["ModalDismissReasons"].ESC) {
            return 'by pressing ESC';
        }
        else if (reason === _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__["ModalDismissReasons"].BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        }
        else {
            return "with: " + reason;
        }
    };
    ConfirmComponent.prototype.sbank = function (bb_id, order_id) {
        console.log(bb_id + '/' + order_id);
        this.buff.bb_id = bb_id;
        this.buff.order_id = order_id;
        // console.log('asasdasdasfsjdhgfskjdf');
    };
    ConfirmComponent.prototype.continue = function (data) {
        var _this = this;
        var modal = this.modalService.open(viewModalConfirm, { size: 'lg' });
        modal.componentInstance.data = data;
        modal.result.then(function (result) {
            console.log(result);
        }, function (reason) {
        });
        modal.result.then(function (result) {
            _this.closeResult = "Closed with: " + result;
        }, function (reason) {
            _this.closeResult = "Dismissed " + _this.getDismissReason(reason);
        });
    };
    ConfirmComponent.ctorParameters = function () { return [
        { type: app_webapi_service__WEBPACK_IMPORTED_MODULE_2__["WebapiService"] },
        { type: _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__["NgbModal"] },
        { type: _angular_fire_database__WEBPACK_IMPORTED_MODULE_5__["AngularFireDatabase"] },
        { type: ngx_lightbox__WEBPACK_IMPORTED_MODULE_7__["Lightbox"] }
    ]; };
    ConfirmComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-confirm',
            template: __webpack_require__(/*! raw-loader!./confirm.component.html */ "./node_modules/raw-loader/index.js!./src/app/confirm/confirm.component.html"),
            styles: [__webpack_require__(/*! ./confirm.component.scss */ "./src/app/confirm/confirm.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [app_webapi_service__WEBPACK_IMPORTED_MODULE_2__["WebapiService"],
            _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__["NgbModal"],
            _angular_fire_database__WEBPACK_IMPORTED_MODULE_5__["AngularFireDatabase"],
            ngx_lightbox__WEBPACK_IMPORTED_MODULE_7__["Lightbox"]])
    ], ConfirmComponent);
    return ConfirmComponent;
}());



/***/ }),

/***/ "./src/app/confirm/confirm_details.scss":
/*!**********************************************!*\
  !*** ./src/app/confirm/confirm_details.scss ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbmZpcm0vY29uZmlybV9kZXRhaWxzLnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/customer/add_customer.scss":
/*!********************************************!*\
  !*** ./src/app/customer/add_customer.scss ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "body.layout-dark .modal .modal-header {\n  background: #374fe6; }\n\n.avatar {\n  width: 80px;\n  height: 80px;\n  border: solid 3px #fff;\n  border-radius: 50%; }\n\noption {\n  background-color: #fff; }\n\nlabel {\n  color: #000;\n  display: inline-block;\n  padding-right: 7px;\n  font-size: 16px;\n  width: 130px; }\n\n.form-control {\n  width: calc(100% - 130px);\n  display: inline-block; }\n\n.change_img {\n  width: 26px;\n  height: 26px;\n  font-size: 13px;\n  text-align: center;\n  border-radius: 50%;\n  position: absolute;\n  left: calc(50% - 13px);\n  top: 82px;\n  padding-left: 1px;\n  padding-top: 3px;\n  background-color: black; }\n\ntd img {\n  width: 50px; }\n\n::-webkit-input-placeholder {\n  /* Edge */\n  color: red; }\n\n:-ms-input-placeholder {\n  /* Internet Explorer 10-11 */\n  color: red; }\n\n::-moz-placeholder {\n  color: red !important; }\n\n::-ms-input-placeholder {\n  color: red !important; }\n\n::placeholder {\n  color: red !important; }\n\nsmall {\n  margin-left: 130px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY3VzdG9tZXIvQzpcXGRldmVsb3BlclxcYmFja2VuZC9zcmNcXGFwcFxcY3VzdG9tZXJcXGFkZF9jdXN0b21lci5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNFO0VBQ0UsbUJBQW1CLEVBQUE7O0FBRXJCO0VBQ0UsV0FBVztFQUNYLFlBQVk7RUFDWixzQkFBc0I7RUFDdEIsa0JBQWtCLEVBQUE7O0FBRXBCO0VBQ0Usc0JBQXNCLEVBQUE7O0FBRXZCO0VBQ0MsV0FBVztFQUNYLHFCQUFxQjtFQUNyQixrQkFBa0I7RUFDbEIsZUFBZTtFQUNmLFlBQVksRUFBQTs7QUFFZjtFQUNHLHlCQUF5QjtFQUN6QixxQkFBcUIsRUFBQTs7QUFFdkI7RUFDRSxXQUFXO0VBQ1gsWUFBWTtFQUNaLGVBQWU7RUFDZixrQkFBa0I7RUFDbEIsa0JBQWtCO0VBQ2xCLGtCQUFrQjtFQUNsQixzQkFBc0I7RUFDdEIsU0FBUztFQUNULGlCQUFpQjtFQUNqQixnQkFBZ0I7RUFDaEIsdUJBQXVCLEVBQUE7O0FBRXpCO0VBRUksV0FBVyxFQUFBOztBQUlmO0VBQThCLFNBQUE7RUFDNUIsVUFBVSxFQUFBOztBQUdaO0VBQXlCLDRCQUFBO0VBQ3ZCLFVBQVUsRUFBQTs7QUFHWjtFQUNFLHFCQUFxQixFQUFBOztBQUR2QjtFQUNFLHFCQUFxQixFQUFBOztBQUR2QjtFQUNFLHFCQUFxQixFQUFBOztBQUV2QjtFQUNFLGtCQUFrQixFQUFBIiwiZmlsZSI6InNyYy9hcHAvY3VzdG9tZXIvYWRkX2N1c3RvbWVyLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcclxuICBib2R5LmxheW91dC1kYXJrIC5tb2RhbCAubW9kYWwtaGVhZGVyIHtcclxuICAgIGJhY2tncm91bmQ6ICMzNzRmZTY7XHJcbiAgfVxyXG4gIC5hdmF0YXIge1xyXG4gICAgd2lkdGg6IDgwcHg7XHJcbiAgICBoZWlnaHQ6IDgwcHg7XHJcbiAgICBib3JkZXI6IHNvbGlkIDNweCAjZmZmO1xyXG4gICAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG4gIH1cclxuICBvcHRpb24ge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcclxuICB9XHJcbiAgIGxhYmVsIHtcclxuICAgIGNvbG9yOiAjMDAwO1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgcGFkZGluZy1yaWdodDogN3B4O1xyXG4gICAgZm9udC1zaXplOiAxNnB4O1xyXG4gICAgd2lkdGg6IDEzMHB4O1xyXG4gIH1cclxuIC5mb3JtLWNvbnRyb2wge1xyXG4gICAgd2lkdGg6IGNhbGMoMTAwJSAtIDEzMHB4KTtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICB9XHJcbiAgLmNoYW5nZV9pbWcge1xyXG4gICAgd2lkdGg6IDI2cHg7XHJcbiAgICBoZWlnaHQ6IDI2cHg7XHJcbiAgICBmb250LXNpemU6IDEzcHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1MCU7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBsZWZ0OiBjYWxjKDUwJSAtIDEzcHgpO1xyXG4gICAgdG9wOiA4MnB4O1xyXG4gICAgcGFkZGluZy1sZWZ0OiAxcHg7XHJcbiAgICBwYWRkaW5nLXRvcDogM3B4O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogYmxhY2s7XHJcbiAgfVxyXG4gIHRkIHtcclxuICAgIGltZyB7XHJcbiAgICAgIHdpZHRoOiA1MHB4O1xyXG4gICAgfVxyXG4gIH1cclxuICBcclxuICA6Oi13ZWJraXQtaW5wdXQtcGxhY2Vob2xkZXIgeyAvKiBFZGdlICovXHJcbiAgICBjb2xvcjogcmVkO1xyXG4gIH1cclxuICBcclxuICA6LW1zLWlucHV0LXBsYWNlaG9sZGVyIHsgLyogSW50ZXJuZXQgRXhwbG9yZXIgMTAtMTEgKi9cclxuICAgIGNvbG9yOiByZWQ7XHJcbiAgfVxyXG4gIFxyXG4gIDo6cGxhY2Vob2xkZXIge1xyXG4gICAgY29sb3I6IHJlZCAhaW1wb3J0YW50O1xyXG4gIH1cclxuICBzbWFsbCAge1xyXG4gICAgbWFyZ2luLWxlZnQ6IDEzMHB4O1xyXG4gIH0iXX0= */"

/***/ }),

/***/ "./src/app/customer/customer.component.scss":
/*!**************************************************!*\
  !*** ./src/app/customer/customer.component.scss ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".btn[class*=\"btn-\"] {\n  margin-bottom: 0rem; }\n\n.justify-content-center {\n  display: flex !important;\n  justify-content: center !important; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY3VzdG9tZXIvQzpcXGRldmVsb3BlclxcYmFja2VuZC9zcmNcXGFwcFxcY3VzdG9tZXJcXGN1c3RvbWVyLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsbUJBQW1CLEVBQUE7O0FBRXJCO0VBQ0Usd0JBQXdCO0VBQ3hCLGtDQUFrQyxFQUFBIiwiZmlsZSI6InNyYy9hcHAvY3VzdG9tZXIvY3VzdG9tZXIuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuYnRuW2NsYXNzKj1cImJ0bi1cIl0ge1xyXG4gIG1hcmdpbi1ib3R0b206IDByZW07XHJcbn1cclxuLmp1c3RpZnktY29udGVudC1jZW50ZXIge1xyXG4gIGRpc3BsYXk6IGZsZXggIWltcG9ydGFudDtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlciAhaW1wb3J0YW50O1xyXG59Il19 */"

/***/ }),

/***/ "./src/app/customer/customer.component.ts":
/*!************************************************!*\
  !*** ./src/app/customer/customer.component.ts ***!
  \************************************************/
/*! exports provided: viewModalContentCustomer, NgbdModalContentCustomer, CustomerComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "viewModalContentCustomer", function() { return viewModalContentCustomer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NgbdModalContentCustomer", function() { return NgbdModalContentCustomer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomerComponent", function() { return CustomerComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var app_webapi_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! app/webapi.service */ "./src/app/webapi.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");








var viewModalContentCustomer = /** @class */ (function () {
    function viewModalContentCustomer(activeModal, api) {
        this.activeModal = activeModal;
        this.api = api;
    }
    viewModalContentCustomer.prototype.close = function () {
        this.activeModal.dismiss();
    };
    viewModalContentCustomer.prototype.ngOnInit = function () {
        console.log("asdasdasdadadasd" + this.para.member_id);
        // this.admin_id = this.api.storage_get('logindata').t_id
        console.log(this.api.storage_get('logindata'));
        // console.log(this.para);
        // this.customerForm = new FormGroup({
        //   'email': new FormControl(null, [Validators.required, Validators.email]),
        //   'username': new FormControl(null, [Validators.required, Validators.minLength(4), Validators.maxLength(24)]),
        //   'password': new FormControl(null, [Validators.required, Validators.minLength(4), Validators.maxLength(24)]),
        //   'name': new FormControl(null, [Validators.required, Validators.email, Validators.minLength(4)]),
        //   'phone': new FormControl(null, [Validators.required, Validators.email, Validators.minLength(9), Validators.minLength(10)]),
        // }, { updateOn: 'blur' });
        // this.customerForm.patchValue({username:'xxx'});
    };
    viewModalContentCustomer.prototype.c = function (val) {
        console.log(val);
    };
    viewModalContentCustomer.ctorParameters = function () { return [
        { type: _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbActiveModal"] },
        { type: app_webapi_service__WEBPACK_IMPORTED_MODULE_5__["WebapiService"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], viewModalContentCustomer.prototype, "para", void 0);
    viewModalContentCustomer = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'view-modal-content',
            template: __webpack_require__(/*! raw-loader!./view_customer.html */ "./node_modules/raw-loader/index.js!./src/app/customer/view_customer.html"),
            styles: [__webpack_require__(/*! ./view_customer.scss */ "./src/app/customer/view_customer.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbActiveModal"], app_webapi_service__WEBPACK_IMPORTED_MODULE_5__["WebapiService"]])
    ], viewModalContentCustomer);
    return viewModalContentCustomer;
}());

var NgbdModalContentCustomer = /** @class */ (function () {
    function NgbdModalContentCustomer(activeModal, fb, cd, toastr, http, api) {
        this.activeModal = activeModal;
        this.fb = fb;
        this.cd = cd;
        this.toastr = toastr;
        this.http = http;
        this.api = api;
    }
    NgbdModalContentCustomer.prototype.file_change = function (event) {
        var _this = this;
        var reader = new FileReader();
        if (event.target.files && event.target.files.length) {
            var file = event.target.files[0];
            reader.readAsDataURL(file);
            reader.onload = function () {
                console.log(reader.result);
                _this.para.picture = reader.result;
                var selectedFile = event.target.files[0];
                var uploadData = new FormData();
                uploadData.append('photo', selectedFile, selectedFile.name);
                _this.http.post(_this.api.base_url + 'customer/fileupload', uploadData, {
                    reportProgress: true,
                    observe: 'events'
                })
                    .subscribe(function (e) {
                    console.log(e);
                });
                //   this.formGroup.patchValue({
                //     file: reader.result
                //  });
                // need to run CD since file load runs outside of zone
                _this.cd.markForCheck();
            };
        }
    };
    NgbdModalContentCustomer.prototype.change_picture = function () {
        console.log('changepic');
        this.file.nativeElement.click();
    };
    NgbdModalContentCustomer.prototype.onReactiveFormSubmit = function () {
        var _this = this;
        // console.log(this.customerForm.value);
        var data = this.customerForm.value;
        data.t_id = this.para.t_id;
        this.api.postData('customer/add_customer', this.customerForm.value).then(function (res) {
            console.log(res);
            if (res == '1') {
                _this.toastr.success('บันทึกเรียบรอ้แล้ว');
                _this.activeModal.close();
            }
            else {
                _this.toastr.error('มีปัญหาในการบันทึกข้อมูล');
            }
        });
        console.log("summit");
    };
    NgbdModalContentCustomer.prototype.close = function () {
        this.activeModal.dismiss();
    };
    NgbdModalContentCustomer.prototype.ngOnInit = function () {
        console.log("xxxx");
        console.log(this.para);
        if (this.para.t_id == '0') {
            this.para = {
                t_id: "0", username: "", password: "", name: "", email: '', phone: "",
                gender: "m",
                picture: "https://image.flaticon.com/icons/svg/194/194938.svg",
                type: "1"
            };
            this.customerForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
                'username': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(6), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(24), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[a-zA-Z0-9._-]+$')]),
                'password': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(6), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(24)]),
                'name': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(6)]),
                'email': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].email]),
                'phone': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(9), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(10), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('[0-9]+')]),
                gender: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
                type: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required])
            }, { updateOn: 'blur' });
        }
        else {
            this.customerForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
                'username': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(6), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(24), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[a-zA-Z0-9._-]+$')]),
                'password': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(6), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(24)]),
                'name': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(6)]),
                'email': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].email]),
                'phone': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(9), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(10), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('[0-9]+')]),
                gender: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
                type: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required])
            }, { updateOn: 'blur' });
        }
        this.customerForm.patchValue(this.para);
        this.customerForm.patchValue({ password: '' });
    };
    NgbdModalContentCustomer.prototype.c = function (val) {
        console.log(val);
    };
    NgbdModalContentCustomer.ctorParameters = function () { return [
        { type: _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbActiveModal"] },
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["ChangeDetectorRef"] },
        { type: ngx_toastr__WEBPACK_IMPORTED_MODULE_7__["ToastrService"] },
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpClient"] },
        { type: app_webapi_service__WEBPACK_IMPORTED_MODULE_5__["WebapiService"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], NgbdModalContentCustomer.prototype, "para", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["ViewChild"])("file", { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_2__["ElementRef"])
    ], NgbdModalContentCustomer.prototype, "file", void 0);
    NgbdModalContentCustomer = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'ngbd-modal-content',
            template: __webpack_require__(/*! raw-loader!./add_customer.html */ "./node_modules/raw-loader/index.js!./src/app/customer/add_customer.html"),
            styles: [__webpack_require__(/*! ./add_customer.scss */ "./src/app/customer/add_customer.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbActiveModal"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"], _angular_core__WEBPACK_IMPORTED_MODULE_2__["ChangeDetectorRef"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_7__["ToastrService"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpClient"], app_webapi_service__WEBPACK_IMPORTED_MODULE_5__["WebapiService"]])
    ], NgbdModalContentCustomer);
    return NgbdModalContentCustomer;
}());

var CustomerComponent = /** @class */ (function () {
    function CustomerComponent(modalService, api) {
        this.modalService = modalService;
        this.api = api;
        this.customers = [];
        this.all_page = 0;
        this.page = 1;
        this.customers = this.api.storage_get('customers');
    }
    CustomerComponent.prototype.onReactiveFormSubmit = function () {
        var a = this.customerForm.value();
        console.log(a);
    };
    CustomerComponent.prototype.ngOnInit = function () {
        this.api.check_login();
        this.searchForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            'search': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('')
        });
        this.customerForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            'inputEmail': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].email]),
            'password': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(4), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(24)]),
            'textArea': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            'radioOption': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('Option one is this')
        }, { updateOn: 'blur' });
        this.load_data(1);
    };
    CustomerComponent.prototype.search = function () {
        console.log(this.searchForm.value);
        this.load_data(1);
    };
    CustomerComponent.prototype.load_data = function (page) {
        var _this = this;
        this.api.getData('customer/load_customer/' + page + "?search=" + this.searchForm.value.search).then(function (data) {
            _this.customers = data.data;
            _this.all_page = data.count / 20 * 20;
            _this.api.storage_set('customers', data.data);
        });
    };
    CustomerComponent.prototype.change_page = function (page) {
        this.load_data(page);
    };
    CustomerComponent.prototype.view = function (t) {
        var _this = this;
        console.log(t);
        var modal = this.modalService.open(viewModalContentCustomer);
        modal.componentInstance.para = t;
        modal.result.then(function (result) {
            console.log(result);
        }, function (reason) {
        });
        modal.result.then(function (result) {
            _this.closeResult = "Closed with: " + result;
        }, function (reason) {
            _this.closeResult = "Dismissed " + _this.getDismissReason(reason);
        });
    };
    CustomerComponent.prototype.view_profile = function (t) {
        this.view(t);
    };
    CustomerComponent.prototype.open = function (t) {
        var _this = this;
        console.log(t);
        var modal = this.modalService.open(NgbdModalContentCustomer);
        modal.componentInstance.para = t;
        modal.result.then(function (result) {
            _this.load_data(_this.page);
        }, function (reason) {
            console.log(reason);
        });
        modal.result.then(function (result) {
            _this.closeResult = "Closed with: " + result;
        }, function (reason) {
            _this.closeResult = "Dismissed " + _this.getDismissReason(reason);
        });
    };
    CustomerComponent.prototype.delete_member = function () {
        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
            title: 'คุณต้องการลบจริงหรือไม่',
            text: "รายการนี้จะไม่สามารถนำกลับมาได้อีก",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#0CC27E',
            cancelButtonColor: '#FF586B',
            confirmButtonText: 'ลบออก',
            cancelButtonText: 'ปิดออก',
            confirmButtonClass: 'btn btn-success btn-raised mr-2',
            cancelButtonClass: 'btn btn-default btn-raised',
            buttonsStyling: false
        }).then(function (result) {
            console.log(result);
            if (result.value) {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire('ลบเรียบร้อยแล้ว', '', 'success');
            }
        });
    };
    CustomerComponent.prototype.getDismissReason = function (reason) {
        if (reason === _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["ModalDismissReasons"].ESC) {
            return 'by pressing ESC';
        }
        else if (reason === _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["ModalDismissReasons"].BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        }
        else {
            return "with: " + reason;
        }
    };
    CustomerComponent.ctorParameters = function () { return [
        { type: _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbModal"] },
        { type: app_webapi_service__WEBPACK_IMPORTED_MODULE_5__["WebapiService"] }
    ]; };
    CustomerComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-customer',
            template: __webpack_require__(/*! raw-loader!./customer.component.html */ "./node_modules/raw-loader/index.js!./src/app/customer/customer.component.html"),
            providers: [_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbPaginationConfig"]],
            styles: [__webpack_require__(/*! ./customer.component.scss */ "./src/app/customer/customer.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbModal"], app_webapi_service__WEBPACK_IMPORTED_MODULE_5__["WebapiService"]])
    ], CustomerComponent);
    return CustomerComponent;
}());



/***/ }),

/***/ "./src/app/customer/view_customer.scss":
/*!*********************************************!*\
  !*** ./src/app/customer/view_customer.scss ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".content {\n  border: #fff solid 2px;\n  border-radius: 20px;\n  margin: 15px 5px;\n  padding: 20px; }\n\ndl {\n  padding-bottom: 0px; }\n\ndl dt {\n    font-weight: bold;\n    width: 120px;\n    text-align: right;\n    display: inline-block; }\n\ndl dt::after {\n    content: \":\"; }\n\ndl dd {\n    display: inline-block;\n    width: calc(100% - 120px);\n    color: #333333;\n    padding-left: 15px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY3VzdG9tZXIvQzpcXGRldmVsb3BlclxcYmFja2VuZC9zcmNcXGFwcFxcY3VzdG9tZXJcXHZpZXdfY3VzdG9tZXIuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLHNCQUFzQjtFQUN0QixtQkFBbUI7RUFDbkIsZ0JBQWdCO0VBQ2hCLGFBQWEsRUFBQTs7QUFHZjtFQWlCRSxtQkFBbUIsRUFBQTs7QUFqQnJCO0lBR0ksaUJBQWlCO0lBQ2pCLFlBQVk7SUFDWixpQkFBaUI7SUFDakIscUJBQXFCLEVBQUE7O0FBTnpCO0lBU0ksWUFBWSxFQUFBOztBQVRoQjtJQVlJLHFCQUFxQjtJQUNyQix5QkFBeUI7SUFDekIsY0FBYztJQUNkLGtCQUFrQixFQUFBIiwiZmlsZSI6InNyYy9hcHAvY3VzdG9tZXIvdmlld19jdXN0b21lci5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmNvbnRlbnQge1xyXG4gIGJvcmRlcjogI2ZmZiBzb2xpZCAycHg7XHJcbiAgYm9yZGVyLXJhZGl1czogMjBweDtcclxuICBtYXJnaW46IDE1cHggNXB4O1xyXG4gIHBhZGRpbmc6IDIwcHg7XHJcbn1cclxuXHJcbmRsIHtcclxuICBkdCB7XHJcbiAgICAgIFxyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICB3aWR0aDogMTIwcHg7XHJcbiAgICB0ZXh0LWFsaWduOiByaWdodDtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICB9XHJcbiAgZHQ6OmFmdGVyIHtcclxuICAgIGNvbnRlbnQ6IFwiOlwiO1xyXG4gIH1cclxuICBkZCB7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICB3aWR0aDogY2FsYygxMDAlIC0gMTIwcHgpO1xyXG4gICAgY29sb3I6ICMzMzMzMzM7XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDE1cHg7XHJcbiAgfVxyXG4gIHBhZGRpbmctYm90dG9tOiAwcHg7XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/layouts/content/content-layout.component.scss":
/*!***************************************************************!*\
  !*** ./src/app/layouts/content/content-layout.component.scss ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xheW91dHMvY29udGVudC9jb250ZW50LWxheW91dC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/layouts/content/content-layout.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/layouts/content/content-layout.component.ts ***!
  \*************************************************************/
/*! exports provided: ContentLayoutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContentLayoutComponent", function() { return ContentLayoutComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var app_shared_services_config_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! app/shared/services/config.service */ "./src/app/shared/services/config.service.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");




var ContentLayoutComponent = /** @class */ (function () {
    function ContentLayoutComponent(configService, document, renderer) {
        this.configService = configService;
        this.document = document;
        this.renderer = renderer;
        this.config = {};
    }
    ContentLayoutComponent.prototype.ngOnInit = function () {
        this.config = this.configService.templateConf;
    };
    ContentLayoutComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        setTimeout(function () {
            if (_this.config.layout.dir) {
                _this.direction = _this.config.layout.dir;
            }
            if (_this.config.layout.variant === "Dark") {
                _this.renderer.addClass(_this.document.body, 'layout-dark');
            }
            else if (_this.config.layout.variant === "Transparent") {
                _this.renderer.addClass(_this.document.body, 'layout-dark');
                _this.renderer.addClass(_this.document.body, 'layout-transparent');
                if (_this.config.layout.sidebar.backgroundColor) {
                    _this.renderer.addClass(_this.document.body, _this.config.layout.sidebar.backgroundColor);
                }
                else {
                    _this.renderer.addClass(_this.document.body, 'bg-glass-1');
                }
            }
        }, 0);
    };
    ContentLayoutComponent.ctorParameters = function () { return [
        { type: app_shared_services_config_service__WEBPACK_IMPORTED_MODULE_2__["ConfigService"] },
        { type: Document, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"], args: [_angular_common__WEBPACK_IMPORTED_MODULE_3__["DOCUMENT"],] }] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer2"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('content-wrapper', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], ContentLayoutComponent.prototype, "wrapper", void 0);
    ContentLayoutComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-content-layout',
            template: __webpack_require__(/*! raw-loader!./content-layout.component.html */ "./node_modules/raw-loader/index.js!./src/app/layouts/content/content-layout.component.html"),
            styles: [__webpack_require__(/*! ./content-layout.component.scss */ "./src/app/layouts/content/content-layout.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_angular_common__WEBPACK_IMPORTED_MODULE_3__["DOCUMENT"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [app_shared_services_config_service__WEBPACK_IMPORTED_MODULE_2__["ConfigService"],
            Document,
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer2"]])
    ], ContentLayoutComponent);
    return ContentLayoutComponent;
}());



/***/ }),

/***/ "./src/app/layouts/full/full-layout.component.scss":
/*!*********************************************************!*\
  !*** ./src/app/layouts/full/full-layout.component.scss ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xheW91dHMvZnVsbC9mdWxsLWxheW91dC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/layouts/full/full-layout.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/layouts/full/full-layout.component.ts ***!
  \*******************************************************/
/*! exports provided: FullLayoutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FullLayoutComponent", function() { return FullLayoutComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var app_shared_services_config_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! app/shared/services/config.service */ "./src/app/shared/services/config.service.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var app_shared_services_layout_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! app/shared/services/layout.service */ "./src/app/shared/services/layout.service.ts");





var fireRefreshEventOnWindow = function () {
    var evt = document.createEvent("HTMLEvents");
    evt.initEvent("resize", true, false);
    window.dispatchEvent(evt);
};
var FullLayoutComponent = /** @class */ (function () {
    function FullLayoutComponent(elementRef, layoutService, configService, document, renderer) {
        var _this = this;
        this.elementRef = elementRef;
        this.layoutService = layoutService;
        this.configService = configService;
        this.document = document;
        this.renderer = renderer;
        this.options = {
            direction: "ltr",
            bgColor: "black",
            bgImage: "assets/img/sidebar-bg/01.jpg"
        };
        this.iscollapsed = false;
        this.isSidebar_sm = false;
        this.isSidebar_lg = false;
        this.bgColor = "black";
        this.bgImage = "assets/img/sidebar-bg/01.jpg";
        this.config = {};
        console.log("full");
        //event emitter call from customizer
        this.layoutSub = layoutService.customizerChangeEmitted$.subscribe(function (options) {
            if (options) {
                if (options.bgColor) {
                    _this.bgColor = options.bgColor;
                }
                if (options.bgImage) {
                    _this.bgImage = options.bgImage;
                    _this.renderer.setAttribute(_this.sidebarBgImage.nativeElement, "style", 'background-image: url("' + _this.bgImage + '")');
                }
                if (options.bgImageDisplay === true) {
                    _this.bgImage = options.bgImage;
                    _this.renderer.setAttribute(_this.sidebarBgImage.nativeElement, "style", 'display: block; background-image: url("' + _this.bgImage + '")');
                }
                else if (options.bgImageDisplay === false) {
                    _this.bgImage = "";
                    // this.renderer.setAttribute(this.sidebarBgImage.nativeElement, 'style', 'display: none');
                    _this.renderer.setAttribute(_this.sidebarBgImage.nativeElement, "style", "background-image: none");
                }
                if (options.compactMenu === true) {
                    _this.renderer.addClass(_this.wrapper.nativeElement, "nav-collapsed");
                    _this.renderer.addClass(_this.wrapper.nativeElement, "menu-collapsed");
                }
                else if (options.compactMenu === false) {
                    if (_this.wrapper.nativeElement.classList.contains("nav-collapsed")) {
                        _this.renderer.removeClass(_this.wrapper.nativeElement, "nav-collapsed");
                        _this.renderer.removeClass(_this.wrapper.nativeElement, "menu-collapsed");
                    }
                }
                if (options.sidebarSize === "sidebar-lg") {
                    _this.isSidebar_sm = false;
                    _this.isSidebar_lg = true;
                }
                else if (options.sidebarSize === "sidebar-sm") {
                    _this.isSidebar_sm = true;
                    _this.isSidebar_lg = false;
                }
                else {
                    _this.isSidebar_sm = false;
                    _this.isSidebar_lg = false;
                }
                if (options.layout === "Light") {
                    _this.renderer.removeClass(_this.document.body, "layout-dark");
                    _this.renderer.removeClass(_this.document.body, "layout-transparent");
                    _this.renderer.removeClass(_this.document.body, "bg-hibiscus");
                    _this.renderer.removeClass(_this.document.body, "bg-purple-pizzazz");
                    _this.renderer.removeClass(_this.document.body, "bg-blue-lagoon");
                    _this.renderer.removeClass(_this.document.body, "bg-electric-violet");
                    _this.renderer.removeClass(_this.document.body, "bg-portage");
                    _this.renderer.removeClass(_this.document.body, "bg-tundora");
                    _this.renderer.removeClass(_this.document.body, "bg-glass-1");
                    _this.renderer.removeClass(_this.document.body, "bg-glass-2");
                    _this.renderer.removeClass(_this.document.body, "bg-glass-3");
                    _this.renderer.removeClass(_this.document.body, "bg-glass-4");
                }
                else if (options.layout === "Dark") {
                    if (_this.document.body.classList.contains("layout-transparent")) {
                        _this.renderer.removeClass(_this.document.body, "layout-transparent");
                        _this.renderer.removeClass(_this.document.body, "bg-hibiscus");
                        _this.renderer.removeClass(_this.document.body, "bg-purple-pizzazz");
                        _this.renderer.removeClass(_this.document.body, "bg-blue-lagoon");
                        _this.renderer.removeClass(_this.document.body, "bg-electric-violet");
                        _this.renderer.removeClass(_this.document.body, "bg-portage");
                        _this.renderer.removeClass(_this.document.body, "bg-tundora");
                        _this.renderer.removeClass(_this.document.body, "bg-glass-1");
                        _this.renderer.removeClass(_this.document.body, "bg-glass-2");
                        _this.renderer.removeClass(_this.document.body, "bg-glass-3");
                        _this.renderer.removeClass(_this.document.body, "bg-glass-4");
                        _this.renderer.addClass(_this.document.body, "layout-dark");
                    }
                    else {
                        _this.renderer.addClass(_this.document.body, "layout-dark");
                    }
                }
                else if (options.layout === "Transparent") {
                    _this.renderer.addClass(_this.document.body, "layout-transparent");
                    _this.renderer.addClass(_this.document.body, "layout-dark");
                    _this.renderer.addClass(_this.document.body, "bg-glass-1");
                }
                if (options.transparentColor) {
                    _this.renderer.removeClass(_this.document.body, "bg-hibiscus");
                    _this.renderer.removeClass(_this.document.body, "bg-purple-pizzazz");
                    _this.renderer.removeClass(_this.document.body, "bg-blue-lagoon");
                    _this.renderer.removeClass(_this.document.body, "bg-electric-violet");
                    _this.renderer.removeClass(_this.document.body, "bg-portage");
                    _this.renderer.removeClass(_this.document.body, "bg-tundora");
                    _this.renderer.removeClass(_this.document.body, "bg-glass-1");
                    _this.renderer.removeClass(_this.document.body, "bg-glass-2");
                    _this.renderer.removeClass(_this.document.body, "bg-glass-3");
                    _this.renderer.removeClass(_this.document.body, "bg-glass-4");
                    _this.renderer.addClass(_this.document.body, options.transparentColor);
                }
            }
        });
    }
    FullLayoutComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.config = this.configService.templateConf;
        this.bgColor = this.config.layout.sidebar.backgroundColor;
        if (!this.config.layout.sidebar.backgroundImage) {
            this.bgImage = "";
        }
        else {
            this.bgImage = this.config.layout.sidebar.backgroundImageURL;
        }
        if (this.config.layout.variant === "Transparent") {
            if (this.config.layout.sidebar.backgroundColor.toString().trim() === "") {
                this.bgColor = "bg-glass-1";
            }
        }
        else {
            if (this.config.layout.sidebar.backgroundColor.toString().trim() === "") {
                this.bgColor = "black";
            }
        }
        setTimeout(function () {
            if (_this.config.layout.sidebar.size === "sidebar-lg") {
                _this.isSidebar_sm = false;
                _this.isSidebar_lg = true;
            }
            else if (_this.config.layout.sidebar.size === "sidebar-sm") {
                _this.isSidebar_sm = true;
                _this.isSidebar_lg = false;
            }
            else {
                _this.isSidebar_sm = false;
                _this.isSidebar_lg = false;
            }
            _this.iscollapsed = _this.config.layout.sidebar.collapsed;
        }, 0);
        //emit event to customizer
        this.options.bgColor = this.bgColor;
        this.options.bgImage = this.bgImage;
        this.layoutService.emitCustomizerChange(this.options);
        //customizer events
        // this.elementRef.nativeElement
        //   .querySelector("#cz-compact-menu")
        //   .addEventListener("click", this.onClick.bind(this));
        // this.elementRef.nativeElement
        //   .querySelector("#cz-sidebar-width")
        //   .addEventListener("click", this.onClick.bind(this));
    };
    FullLayoutComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        setTimeout(function () {
            if (_this.config.layout.dir) {
                _this.options.direction = _this.config.layout.dir;
            }
            if (_this.config.layout.variant === "Dark") {
                _this.renderer.addClass(_this.document.body, "layout-dark");
            }
            else if (_this.config.layout.variant === "Transparent") {
                _this.renderer.addClass(_this.document.body, "layout-dark");
                _this.renderer.addClass(_this.document.body, "layout-transparent");
                if (_this.config.layout.sidebar.backgroundColor) {
                    _this.renderer.addClass(_this.document.body, _this.config.layout.sidebar.backgroundColor);
                }
                else {
                    _this.renderer.addClass(_this.document.body, "bg-glass-1");
                }
                _this.bgColor = "black";
                _this.options.bgColor = "black";
                _this.bgImage = "";
                _this.options.bgImage = "";
                _this.bgImage = "";
                _this.renderer.setAttribute(_this.sidebarBgImage.nativeElement, "style", "background-image: none");
            }
        }, 0);
    };
    FullLayoutComponent.prototype.ngOnDestroy = function () {
        if (this.layoutSub) {
            this.layoutSub.unsubscribe();
        }
    };
    FullLayoutComponent.prototype.onClick = function (event) {
        //initialize window resizer event on sidebar toggle click event
        setTimeout(function () {
            fireRefreshEventOnWindow();
        }, 300);
    };
    FullLayoutComponent.prototype.toggleHideSidebar = function ($event) {
        var _this = this;
        setTimeout(function () {
            _this.hideSidebar = $event;
        }, 0);
    };
    FullLayoutComponent.prototype.getOptions = function ($event) {
        this.options = $event;
    };
    FullLayoutComponent.ctorParameters = function () { return [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"] },
        { type: app_shared_services_layout_service__WEBPACK_IMPORTED_MODULE_4__["LayoutService"] },
        { type: app_shared_services_config_service__WEBPACK_IMPORTED_MODULE_2__["ConfigService"] },
        { type: Document, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"], args: [_angular_common__WEBPACK_IMPORTED_MODULE_3__["DOCUMENT"],] }] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer2"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])("sidebarBgImage", { static: true }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], FullLayoutComponent.prototype, "sidebarBgImage", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])("appSidebar", { static: true }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], FullLayoutComponent.prototype, "appSidebar", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])("wrapper", { static: true }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], FullLayoutComponent.prototype, "wrapper", void 0);
    FullLayoutComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "app-full-layout",
            template: __webpack_require__(/*! raw-loader!./full-layout.component.html */ "./node_modules/raw-loader/index.js!./src/app/layouts/full/full-layout.component.html"),
            styles: [__webpack_require__(/*! ./full-layout.component.scss */ "./src/app/layouts/full/full-layout.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](3, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_angular_common__WEBPACK_IMPORTED_MODULE_3__["DOCUMENT"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"],
            app_shared_services_layout_service__WEBPACK_IMPORTED_MODULE_4__["LayoutService"],
            app_shared_services_config_service__WEBPACK_IMPORTED_MODULE_2__["ConfigService"],
            Document,
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer2"]])
    ], FullLayoutComponent);
    return FullLayoutComponent;
}());



/***/ }),

/***/ "./src/app/news/add_news.scss":
/*!************************************!*\
  !*** ./src/app/news/add_news.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "body.layout-dark .modal .modal-header {\n  background: #374fe6; }\n\n.avatar {\n  width: 80px;\n  height: 80px;\n  border: solid 3px #fff;\n  border-radius: 50%; }\n\noption {\n  background-color: #fff; }\n\nlabel {\n  color: #000;\n  display: inline-block;\n  padding-right: 7px;\n  font-size: 16px;\n  width: 130px; }\n\n.form-control {\n  width: calc(100% - 130px);\n  display: inline-block; }\n\n.change_img {\n  width: 26px;\n  height: 26px;\n  font-size: 13px;\n  text-align: center;\n  border-radius: 50%;\n  position: absolute;\n  left: calc(50% - 13px);\n  top: 82px;\n  padding-left: 1px;\n  padding-top: 3px;\n  background-color: black; }\n\ntd img {\n  width: 50px; }\n\n::-webkit-input-placeholder {\n  /* Edge */\n  color: red; }\n\n:-ms-input-placeholder {\n  /* Internet Explorer 10-11 */\n  color: red; }\n\n::-moz-placeholder {\n  color: red !important; }\n\n::-ms-input-placeholder {\n  color: red !important; }\n\n::placeholder {\n  color: red !important; }\n\nsmall {\n  margin-left: 130px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbmV3cy9DOlxcZGV2ZWxvcGVyXFxiYWNrZW5kL3NyY1xcYXBwXFxuZXdzXFxhZGRfbmV3cy5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNFO0VBQ0UsbUJBQW1CLEVBQUE7O0FBRXJCO0VBQ0UsV0FBVztFQUNYLFlBQVk7RUFDWixzQkFBc0I7RUFDdEIsa0JBQWtCLEVBQUE7O0FBRXBCO0VBQ0Usc0JBQXNCLEVBQUE7O0FBRXZCO0VBQ0MsV0FBVztFQUNYLHFCQUFxQjtFQUNyQixrQkFBa0I7RUFDbEIsZUFBZTtFQUNmLFlBQVksRUFBQTs7QUFFZDtFQUNFLHlCQUF5QjtFQUN6QixxQkFBcUIsRUFBQTs7QUFFdkI7RUFDRSxXQUFXO0VBQ1gsWUFBWTtFQUNaLGVBQWU7RUFDZixrQkFBa0I7RUFDbEIsa0JBQWtCO0VBQ2xCLGtCQUFrQjtFQUNsQixzQkFBc0I7RUFDdEIsU0FBUztFQUNULGlCQUFpQjtFQUNqQixnQkFBZ0I7RUFDaEIsdUJBQXVCLEVBQUE7O0FBRXpCO0VBRUksV0FBVyxFQUFBOztBQUlmO0VBQThCLFNBQUE7RUFDNUIsVUFBVSxFQUFBOztBQUdaO0VBQXlCLDRCQUFBO0VBQ3ZCLFVBQVUsRUFBQTs7QUFHWjtFQUNFLHFCQUFxQixFQUFBOztBQUR2QjtFQUNFLHFCQUFxQixFQUFBOztBQUR2QjtFQUNFLHFCQUFxQixFQUFBOztBQUV2QjtFQUNFLGtCQUFrQixFQUFBIiwiZmlsZSI6InNyYy9hcHAvbmV3cy9hZGRfbmV3cy5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiXHJcbiAgYm9keS5sYXlvdXQtZGFyayAubW9kYWwgLm1vZGFsLWhlYWRlciB7XHJcbiAgICBiYWNrZ3JvdW5kOiAjMzc0ZmU2O1xyXG4gIH1cclxuICAuYXZhdGFyIHtcclxuICAgIHdpZHRoOiA4MHB4O1xyXG4gICAgaGVpZ2h0OiA4MHB4O1xyXG4gICAgYm9yZGVyOiBzb2xpZCAzcHggI2ZmZjtcclxuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcclxuICB9XHJcbiAgb3B0aW9uIHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XHJcbiAgfVxyXG4gICBsYWJlbCB7XHJcbiAgICBjb2xvcjogIzAwMDtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgIHBhZGRpbmctcmlnaHQ6IDdweDtcclxuICAgIGZvbnQtc2l6ZTogMTZweDtcclxuICAgIHdpZHRoOiAxMzBweDtcclxuICB9XHJcbiAgLmZvcm0tY29udHJvbCB7XHJcbiAgICB3aWR0aDogY2FsYygxMDAlIC0gMTMwcHgpO1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gIH1cclxuICAuY2hhbmdlX2ltZyB7XHJcbiAgICB3aWR0aDogMjZweDtcclxuICAgIGhlaWdodDogMjZweDtcclxuICAgIGZvbnQtc2l6ZTogMTNweDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGxlZnQ6IGNhbGMoNTAlIC0gMTNweCk7XHJcbiAgICB0b3A6IDgycHg7XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDFweDtcclxuICAgIHBhZGRpbmctdG9wOiAzcHg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiBibGFjaztcclxuICB9XHJcbiAgdGQge1xyXG4gICAgaW1nIHtcclxuICAgICAgd2lkdGg6IDUwcHg7XHJcbiAgICB9XHJcbiAgfVxyXG4gIFxyXG4gIDo6LXdlYmtpdC1pbnB1dC1wbGFjZWhvbGRlciB7IC8qIEVkZ2UgKi9cclxuICAgIGNvbG9yOiByZWQ7XHJcbiAgfVxyXG4gIFxyXG4gIDotbXMtaW5wdXQtcGxhY2Vob2xkZXIgeyAvKiBJbnRlcm5ldCBFeHBsb3JlciAxMC0xMSAqL1xyXG4gICAgY29sb3I6IHJlZDtcclxuICB9XHJcbiAgXHJcbiAgOjpwbGFjZWhvbGRlciB7XHJcbiAgICBjb2xvcjogcmVkICFpbXBvcnRhbnQ7XHJcbiAgfVxyXG4gIHNtYWxsICB7XHJcbiAgICBtYXJnaW4tbGVmdDogMTMwcHg7XHJcbiAgfSJdfQ== */"

/***/ }),

/***/ "./src/app/news/news.component.scss":
/*!******************************************!*\
  !*** ./src/app/news/news.component.scss ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".btn[class*=\"btn-\"] {\n  margin-bottom: 0rem; }\n\n.justify-content-center {\n  display: flex !important;\n  justify-content: center !important; }\n\n.searchform {\n  margin-right: 10px;\n  display: inline-block; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbmV3cy9DOlxcZGV2ZWxvcGVyXFxiYWNrZW5kL3NyY1xcYXBwXFxuZXdzXFxuZXdzLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsbUJBQW1CLEVBQUE7O0FBRXJCO0VBQ0Usd0JBQXdCO0VBQ3hCLGtDQUFrQyxFQUFBOztBQUVwQztFQUNFLGtCQUFrQjtFQUNsQixxQkFBcUIsRUFBQSIsImZpbGUiOiJzcmMvYXBwL25ld3MvbmV3cy5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5idG5bY2xhc3MqPVwiYnRuLVwiXSB7XHJcbiAgbWFyZ2luLWJvdHRvbTogMHJlbTtcclxufVxyXG4uanVzdGlmeS1jb250ZW50LWNlbnRlciB7XHJcbiAgZGlzcGxheTogZmxleCAhaW1wb3J0YW50O1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyICFpbXBvcnRhbnQ7XHJcbn1cclxuLnNlYXJjaGZvcm0ge1xyXG4gIG1hcmdpbi1yaWdodDogMTBweDtcclxuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/news/news.component.ts":
/*!****************************************!*\
  !*** ./src/app/news/news.component.ts ***!
  \****************************************/
/*! exports provided: viewModalNews, NgbdModalNews, NewsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "viewModalNews", function() { return viewModalNews; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NgbdModalNews", function() { return NgbdModalNews; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewsComponent", function() { return NewsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var app_webapi_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! app/webapi.service */ "./src/app/webapi.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ckeditor/ckeditor5-build-classic */ "./node_modules/@ckeditor/ckeditor5-build-classic/build/ckeditor.js");
/* harmony import */ var _ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(_ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_8__);









var viewModalNews = /** @class */ (function () {
    function viewModalNews(activeModal) {
        this.activeModal = activeModal;
        this.Editor = _ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_8__;
        this.editorConfig = {};
    }
    viewModalNews.prototype.close = function () {
        this.activeModal.dismiss();
    };
    viewModalNews.prototype.ngOnInit = function () {
        console.log("view");
        console.log(this.para);
        this.viewnews = this.para;
        // this.newsForm = new FormGroup({
        //   'email': new FormControl(null, [Validators.required, Validators.email]),
        //   'username': new FormControl(null, [Validators.required, Validators.minLength(4), Validators.maxLength(24)]),
        //   'password': new FormControl(null, [Validators.required, Validators.minLength(4), Validators.maxLength(24)]),
        //   'name': new FormControl(null, [Validators.required, Validators.email, Validators.minLength(4)]),
        //   'phone': new FormControl(null, [Validators.required, Validators.email, Validators.minLength(9), Validators.minLength(10)]),
        // }, { updateOn: 'blur' });
        // this.newsForm.patchValue({username:'xxx'});
    };
    viewModalNews.prototype.c = function (val) {
        console.log(val);
    };
    viewModalNews.ctorParameters = function () { return [
        { type: _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbActiveModal"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], viewModalNews.prototype, "para", void 0);
    viewModalNews = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'view-modal-news',
            template: __webpack_require__(/*! raw-loader!./view_news.html */ "./node_modules/raw-loader/index.js!./src/app/news/view_news.html"),
            styles: [__webpack_require__(/*! ./view_news.scss */ "./src/app/news/view_news.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbActiveModal"]])
    ], viewModalNews);
    return viewModalNews;
}());

var NgbdModalNews = /** @class */ (function () {
    function NgbdModalNews(activeModal, fb, cd, toastr, http, api) {
        this.activeModal = activeModal;
        this.fb = fb;
        this.cd = cd;
        this.toastr = toastr;
        this.http = http;
        this.api = api;
        this.Editor = _ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_8__;
        this.editorConfig = {};
    }
    NgbdModalNews.prototype.file_change = function (event) {
        var _this = this;
        var reader = new FileReader();
        if (event.target.files && event.target.files.length) {
            var file = event.target.files[0];
            console.log(event.target.files);
            if (event.target.files[0].size < 2000000) {
                if (event.target.files[0].type == 'image/jpeg' || event.target.files[0].type == 'image/png') {
                    reader.readAsDataURL(file);
                    reader.onload = function () {
                        _this.api.postData('news/upload', { data: reader.result, type: event.target.files[0].type }).then(function (res) {
                            console.log(res);
                            _this.para.picture = res.url;
                            _this.cd.detectChanges();
                        });
                        _this.cd.markForCheck();
                    };
                }
                else {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire('ต้องเป็นไฟล์ jpg หรือ png เท่านั้น', '', 'error');
                }
            }
            else {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire('ไฟล์ต้องมีขนาดน้อยกกว่า 2 Mb', '', 'error');
            }
        }
    };
    NgbdModalNews.prototype.change_picture = function () {
        console.log('changepic');
        this.file.nativeElement.click();
    };
    NgbdModalNews.prototype.onReactiveFormSubmit = function () {
        var _this = this;
        var data = this.newsForm.value;
        data.n_id = this.para.n_id;
        this.api.postData('news/add_news', data).then(function (res) {
            console.log(res);
            if (res == '1') {
                _this.toastr.success('บันทึกเรียบรอ้แล้ว');
                _this.activeModal.close();
            }
            else {
                _this.toastr.error('มีปัญหาในการบันทึกข้อมูล');
            }
        });
    };
    NgbdModalNews.prototype.close = function () {
        this.activeModal.dismiss();
    };
    NgbdModalNews.prototype.ngOnInit = function () {
        if (this.para.t_id == '0') {
            this.para = {
                n_id: "0",
                title: { th: '', en: '' },
                content: { th: '', en: '' }
            };
            this.newsForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
                'title': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
                    'th': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
                    'en': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required])
                }),
                'content': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
                    'th': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
                    'en': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required])
                }),
                'show': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
                    'show': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](true),
                })
            });
        }
        else {
            this.newsForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
                'title': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
                    'th': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
                    'en': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required])
                }),
                'content': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
                    'th': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
                    'en': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required])
                }),
                'show': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
                    'show': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.para.show),
                }),
            });
        }
        this.newsForm.patchValue(this.para);
    };
    NgbdModalNews.prototype.c = function (val) {
        console.log(val);
    };
    NgbdModalNews.ctorParameters = function () { return [
        { type: _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbActiveModal"] },
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["ChangeDetectorRef"] },
        { type: ngx_toastr__WEBPACK_IMPORTED_MODULE_7__["ToastrService"] },
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpClient"] },
        { type: app_webapi_service__WEBPACK_IMPORTED_MODULE_5__["WebapiService"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], NgbdModalNews.prototype, "para", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["ViewChild"])("file", { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_2__["ElementRef"])
    ], NgbdModalNews.prototype, "file", void 0);
    NgbdModalNews = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'ngbd-modal-news',
            template: __webpack_require__(/*! raw-loader!./add_news.html */ "./node_modules/raw-loader/index.js!./src/app/news/add_news.html"),
            styles: [__webpack_require__(/*! ./add_news.scss */ "./src/app/news/add_news.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbActiveModal"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"], _angular_core__WEBPACK_IMPORTED_MODULE_2__["ChangeDetectorRef"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_7__["ToastrService"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpClient"], app_webapi_service__WEBPACK_IMPORTED_MODULE_5__["WebapiService"]])
    ], NgbdModalNews);
    return NgbdModalNews;
}());

var NewsComponent = /** @class */ (function () {
    function NewsComponent(modalService, api) {
        this.modalService = modalService;
        this.api = api;
        this.news = [];
        this.all_page = 0;
        this.page = 1;
        this.news = this.api.storage_get('news') || [];
        this.searchForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            'search': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('')
        });
    }
    NewsComponent.prototype.onReactiveFormSubmit = function () {
        var a = this.newsForm.value();
        console.log(a);
    };
    NewsComponent.prototype.ngOnInit = function () {
        this.api.check_login();
        this.newsForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            'inputEmail': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].email]),
            'password': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(4), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(24)]),
            'textArea': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            'radioOption': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('Option one is this')
        }, { updateOn: 'blur' });
        this.load_data(1);
    };
    NewsComponent.prototype.view = function (t) {
        var _this = this;
        console.log(t);
        var modal = this.modalService.open(viewModalNews, { size: 'lg', backdrop: 'static' });
        modal.componentInstance.para = t;
        modal.result.then(function (result) {
            console.log(result);
        }, function (reason) {
        });
        modal.result.then(function (result) {
            _this.closeResult = "Closed with: " + result;
        }, function (reason) {
            _this.closeResult = "Dismissed " + _this.getDismissReason(reason);
        });
    };
    NewsComponent.prototype.view_profile = function (t) {
        this.view(t);
    };
    NewsComponent.prototype.load_data = function (page) {
        var _this = this;
        this.api.getData('news/load_news/' + page + "?search=" + this.searchForm.value.search).then(function (data) {
            console.log(data);
            _this.news = data.data;
            _this.all_page = data.count / 20 * 20;
            _this.api.storage_set('news', data.data);
        });
    };
    NewsComponent.prototype.search = function () {
        console.log(this.searchForm.value);
        this.load_data(1);
    };
    NewsComponent.prototype.open = function (t) {
        var _this = this;
        console.log(t);
        var modal = this.modalService.open(NgbdModalNews, { size: 'lg', backdrop: 'static' });
        modal.componentInstance.para = t;
        modal.result.then(function (result) {
            _this.load_data(_this.page);
        }, function (reason) {
            console.log(reason);
        });
        modal.result.then(function (result) {
            _this.closeResult = "Closed with: " + result;
        }, function (reason) {
            _this.closeResult = "Dismissed " + _this.getDismissReason(reason);
        });
    };
    NewsComponent.prototype.change_page = function (page) {
        this.load_data(page);
    };
    NewsComponent.prototype.delete_member = function (t) {
        var _this = this;
        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
            title: 'คุณต้องการลบจริงหรือไม่',
            text: "รายการนี้จะไม่สามารถนำกลับมาได้อีก",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#0CC27E',
            cancelButtonColor: '#FF586B',
            confirmButtonText: 'ลบออก',
            cancelButtonText: 'ปิดออก',
            confirmButtonClass: 'btn btn-success btn-raised mr-2',
            cancelButtonClass: 'btn btn-default btn-raised',
            buttonsStyling: false
        }).then(function (result) {
            console.log(t);
            if (result.value) {
                _this.api.postData('news/remove_news', { n_id: t.n_id }).then(function (res) {
                    console.log(res);
                    _this.load_data(_this.page);
                    sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire('ลบเรียบร้อยแล้ว', '', 'success');
                });
            }
        });
    };
    NewsComponent.prototype.getDismissReason = function (reason) {
        if (reason === _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["ModalDismissReasons"].ESC) {
            return 'by pressing ESC';
        }
        else if (reason === _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["ModalDismissReasons"].BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        }
        else {
            return "with: " + reason;
        }
    };
    NewsComponent.ctorParameters = function () { return [
        { type: _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbModal"] },
        { type: app_webapi_service__WEBPACK_IMPORTED_MODULE_5__["WebapiService"] }
    ]; };
    NewsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-news',
            template: __webpack_require__(/*! raw-loader!./news.component.html */ "./node_modules/raw-loader/index.js!./src/app/news/news.component.html"),
            styles: [__webpack_require__(/*! ./news.component.scss */ "./src/app/news/news.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbModal"], app_webapi_service__WEBPACK_IMPORTED_MODULE_5__["WebapiService"]])
    ], NewsComponent);
    return NewsComponent;
}());



/***/ }),

/***/ "./src/app/news/view_news.scss":
/*!*************************************!*\
  !*** ./src/app/news/view_news.scss ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".content {\n  border: #fff solid 2px;\n  border-radius: 20px;\n  margin: 15px 5px;\n  padding: 20px; }\n\ndl {\n  padding-bottom: 0px; }\n\ndl dt {\n    font-weight: bold;\n    width: 120px;\n    text-align: right;\n    display: inline-block; }\n\ndl dt::after {\n    content: \":\"; }\n\ndl dd {\n    display: inline-block;\n    width: calc(100% - 120px);\n    color: #ddd;\n    padding-left: 15px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbmV3cy9DOlxcZGV2ZWxvcGVyXFxiYWNrZW5kL3NyY1xcYXBwXFxuZXdzXFx2aWV3X25ld3Muc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLHNCQUFzQjtFQUN0QixtQkFBbUI7RUFDbkIsZ0JBQWdCO0VBQ2hCLGFBQWEsRUFBQTs7QUFHZjtFQWlCRSxtQkFBbUIsRUFBQTs7QUFqQnJCO0lBR0ksaUJBQWlCO0lBQ2pCLFlBQVk7SUFDWixpQkFBaUI7SUFDakIscUJBQXFCLEVBQUE7O0FBTnpCO0lBU0ksWUFBWSxFQUFBOztBQVRoQjtJQVlJLHFCQUFxQjtJQUNyQix5QkFBeUI7SUFDekIsV0FBVztJQUNYLGtCQUFrQixFQUFBIiwiZmlsZSI6InNyYy9hcHAvbmV3cy92aWV3X25ld3Muc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jb250ZW50IHtcclxuICBib3JkZXI6ICNmZmYgc29saWQgMnB4O1xyXG4gIGJvcmRlci1yYWRpdXM6IDIwcHg7XHJcbiAgbWFyZ2luOiAxNXB4IDVweDtcclxuICBwYWRkaW5nOiAyMHB4O1xyXG59XHJcblxyXG5kbCB7XHJcbiAgZHQge1xyXG4gICAgICBcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgd2lkdGg6IDEyMHB4O1xyXG4gICAgdGV4dC1hbGlnbjogcmlnaHQ7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgfVxyXG4gIGR0OjphZnRlciB7XHJcbiAgICBjb250ZW50OiBcIjpcIjtcclxuICB9XHJcbiAgZGQge1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgd2lkdGg6IGNhbGMoMTAwJSAtIDEyMHB4KTtcclxuICAgIGNvbG9yOiAjZGRkO1xyXG4gICAgcGFkZGluZy1sZWZ0OiAxNXB4O1xyXG4gIH1cclxuICBwYWRkaW5nLWJvdHRvbTogMHB4O1xyXG59XHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/order/add_order.scss":
/*!**************************************!*\
  !*** ./src/app/order/add_order.scss ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "body.layout-dark .modal .modal-header {\n  background: #374fe6; }\n\n.avatar {\n  width: 80px;\n  height: 80px;\n  border: solid 3px #fff;\n  border-radius: 50%; }\n\noption {\n  background-color: #000; }\n\nlabel {\n  color: #bdbdc7;\n  display: inline-block;\n  padding-right: 7px;\n  font-size: 16px;\n  width: 130px; }\n\n.form-control {\n  width: calc(100% - 130px);\n  display: inline-block; }\n\n.change_img {\n  width: 26px;\n  height: 26px;\n  font-size: 13px;\n  text-align: center;\n  border-radius: 50%;\n  position: absolute;\n  left: calc(50% - 13px);\n  top: 82px;\n  padding-left: 1px;\n  padding-top: 3px;\n  background-color: black; }\n\ntd img {\n  width: 50px; }\n\n::-webkit-input-placeholder {\n  /* Edge */\n  color: red; }\n\n:-ms-input-placeholder {\n  /* Internet Explorer 10-11 */\n  color: red; }\n\n::-moz-placeholder {\n  color: red !important; }\n\n::-ms-input-placeholder {\n  color: red !important; }\n\n::placeholder {\n  color: red !important; }\n\nsmall {\n  margin-left: 130px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvb3JkZXIvQzpcXGRldmVsb3BlclxcYmFja2VuZC9zcmNcXGFwcFxcb3JkZXJcXGFkZF9vcmRlci5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNFO0VBQ0UsbUJBQW1CLEVBQUE7O0FBRXJCO0VBQ0UsV0FBVztFQUNYLFlBQVk7RUFDWixzQkFBc0I7RUFDdEIsa0JBQWtCLEVBQUE7O0FBRXBCO0VBQ0Usc0JBQXNCLEVBQUE7O0FBRXZCO0VBQ0MsY0FBYztFQUNkLHFCQUFxQjtFQUNyQixrQkFBa0I7RUFDbEIsZUFBZTtFQUNmLFlBQVksRUFBQTs7QUFFZDtFQUNFLHlCQUF5QjtFQUN6QixxQkFBcUIsRUFBQTs7QUFFdkI7RUFDRSxXQUFXO0VBQ1gsWUFBWTtFQUNaLGVBQWU7RUFDZixrQkFBa0I7RUFDbEIsa0JBQWtCO0VBQ2xCLGtCQUFrQjtFQUNsQixzQkFBc0I7RUFDdEIsU0FBUztFQUNULGlCQUFpQjtFQUNqQixnQkFBZ0I7RUFDaEIsdUJBQXVCLEVBQUE7O0FBRXpCO0VBRUksV0FBVyxFQUFBOztBQUlmO0VBQThCLFNBQUE7RUFDNUIsVUFBVSxFQUFBOztBQUdaO0VBQXlCLDRCQUFBO0VBQ3ZCLFVBQVUsRUFBQTs7QUFHWjtFQUNFLHFCQUFxQixFQUFBOztBQUR2QjtFQUNFLHFCQUFxQixFQUFBOztBQUR2QjtFQUNFLHFCQUFxQixFQUFBOztBQUV2QjtFQUNFLGtCQUFrQixFQUFBIiwiZmlsZSI6InNyYy9hcHAvb3JkZXIvYWRkX29yZGVyLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcclxuICBib2R5LmxheW91dC1kYXJrIC5tb2RhbCAubW9kYWwtaGVhZGVyIHtcclxuICAgIGJhY2tncm91bmQ6ICMzNzRmZTY7XHJcbiAgfVxyXG4gIC5hdmF0YXIge1xyXG4gICAgd2lkdGg6IDgwcHg7XHJcbiAgICBoZWlnaHQ6IDgwcHg7XHJcbiAgICBib3JkZXI6IHNvbGlkIDNweCAjZmZmO1xyXG4gICAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG4gIH1cclxuICBvcHRpb24ge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzAwMDtcclxuICB9XHJcbiAgIGxhYmVsIHtcclxuICAgIGNvbG9yOiAjYmRiZGM3O1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgcGFkZGluZy1yaWdodDogN3B4O1xyXG4gICAgZm9udC1zaXplOiAxNnB4O1xyXG4gICAgd2lkdGg6IDEzMHB4O1xyXG4gIH1cclxuICAuZm9ybS1jb250cm9sIHtcclxuICAgIHdpZHRoOiBjYWxjKDEwMCUgLSAxMzBweCk7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgfVxyXG4gIC5jaGFuZ2VfaW1nIHtcclxuICAgIHdpZHRoOiAyNnB4O1xyXG4gICAgaGVpZ2h0OiAyNnB4O1xyXG4gICAgZm9udC1zaXplOiAxM3B4O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgbGVmdDogY2FsYyg1MCUgLSAxM3B4KTtcclxuICAgIHRvcDogODJweDtcclxuICAgIHBhZGRpbmctbGVmdDogMXB4O1xyXG4gICAgcGFkZGluZy10b3A6IDNweDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IGJsYWNrO1xyXG4gIH1cclxuICB0ZCB7XHJcbiAgICBpbWcge1xyXG4gICAgICB3aWR0aDogNTBweDtcclxuICAgIH1cclxuICB9XHJcbiAgXHJcbiAgOjotd2Via2l0LWlucHV0LXBsYWNlaG9sZGVyIHsgLyogRWRnZSAqL1xyXG4gICAgY29sb3I6IHJlZDtcclxuICB9XHJcbiAgXHJcbiAgOi1tcy1pbnB1dC1wbGFjZWhvbGRlciB7IC8qIEludGVybmV0IEV4cGxvcmVyIDEwLTExICovXHJcbiAgICBjb2xvcjogcmVkO1xyXG4gIH1cclxuICBcclxuICA6OnBsYWNlaG9sZGVyIHtcclxuICAgIGNvbG9yOiByZWQgIWltcG9ydGFudDtcclxuICB9XHJcbiAgc21hbGwgIHtcclxuICAgIG1hcmdpbi1sZWZ0OiAxMzBweDtcclxuICB9Il19 */"

/***/ }),

/***/ "./src/app/order/order.component.scss":
/*!********************************************!*\
  !*** ./src/app/order/order.component.scss ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".btn[class*=\"btn-\"] {\n  margin-bottom: 0rem; }\n\n.justify-content-center {\n  display: flex !important;\n  justify-content: center !important; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvb3JkZXIvQzpcXGRldmVsb3BlclxcYmFja2VuZC9zcmNcXGFwcFxcb3JkZXJcXG9yZGVyLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsbUJBQW1CLEVBQUE7O0FBRXJCO0VBQ0Usd0JBQXdCO0VBQ3hCLGtDQUFrQyxFQUFBIiwiZmlsZSI6InNyYy9hcHAvb3JkZXIvb3JkZXIuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuYnRuW2NsYXNzKj1cImJ0bi1cIl0ge1xyXG4gIG1hcmdpbi1ib3R0b206IDByZW07XHJcbn1cclxuLmp1c3RpZnktY29udGVudC1jZW50ZXIge1xyXG4gIGRpc3BsYXk6IGZsZXggIWltcG9ydGFudDtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlciAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/order/order.component.ts":
/*!******************************************!*\
  !*** ./src/app/order/order.component.ts ***!
  \******************************************/
/*! exports provided: viewModalOrder, NgbdModalOrder, OrderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "viewModalOrder", function() { return viewModalOrder; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NgbdModalOrder", function() { return NgbdModalOrder; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderComponent", function() { return OrderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _angular_fire_database__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/fire/database */ "./node_modules/@angular/fire/database/index.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var app_webapi_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! app/webapi.service */ "./src/app/webapi.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");









var viewModalOrder = /** @class */ (function () {
    function viewModalOrder(ref, activeModal, firebase, api) {
        this.ref = ref;
        this.activeModal = activeModal;
        this.firebase = firebase;
        this.api = api;
        // teamForm: FormGroup;
        this.details = { order_data: { order_id: '', p_name: { th: '', en: '' } } };
        this.action = [];
        this.fbstatus = false;
    }
    viewModalOrder.prototype.close = function () {
        this.activeModal.dismiss();
    };
    viewModalOrder.prototype.ngOnInit = function () {
        var _this = this;
        console.log("view");
        // console.log(this.para);
        this.fbstatus = true;
        this.api.fb_val('order', function (res) {
            console.log(res.node_.value_);
            console.log('new auction!!');
            if (_this.fbstatus) {
                _this.load_auc();
            }
        });
    };
    viewModalOrder.prototype.ngOnDestroy = function () {
        this.fbstatus = false;
    };
    viewModalOrder.prototype.load_auc = function () {
        var _this = this;
        this.api.getData("order/get_vs_auction/" + this.para).then(function (res) {
            // console.log(res);
            _this.details = res;
            _this.action = res.auction_rec;
            console.log(_this.action);
            _this.ref.detectChanges();
        });
    };
    viewModalOrder.prototype.c = function (val) {
        console.log(val);
    };
    viewModalOrder.ctorParameters = function () { return [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["ChangeDetectorRef"] },
        { type: _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbActiveModal"] },
        { type: _angular_fire_database__WEBPACK_IMPORTED_MODULE_4__["AngularFireDatabase"] },
        { type: app_webapi_service__WEBPACK_IMPORTED_MODULE_6__["WebapiService"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], viewModalOrder.prototype, "para", void 0);
    viewModalOrder = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'view-modal-content',
            template: __webpack_require__(/*! raw-loader!./view_order.html */ "./node_modules/raw-loader/index.js!./src/app/order/view_order.html"),
            styles: [__webpack_require__(/*! ./view_order.scss */ "./src/app/order/view_order.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_2__["ChangeDetectorRef"],
            _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbActiveModal"],
            _angular_fire_database__WEBPACK_IMPORTED_MODULE_4__["AngularFireDatabase"],
            app_webapi_service__WEBPACK_IMPORTED_MODULE_6__["WebapiService"]])
    ], viewModalOrder);
    return viewModalOrder;
}());

var NgbdModalOrder = /** @class */ (function () {
    function NgbdModalOrder(activeModal, firebase, fb, cd, toastr, http, api) {
        this.activeModal = activeModal;
        this.firebase = firebase;
        this.fb = fb;
        this.cd = cd;
        this.toastr = toastr;
        this.http = http;
        this.api = api;
    }
    NgbdModalOrder.prototype.file_change = function (event) {
        var _this = this;
        var reader = new FileReader();
        if (event.target.files && event.target.files.length) {
            var file = event.target.files[0];
            console.log(event.target.files);
            if (event.target.files[0].size < 2000000) {
                if (event.target.files[0].type == 'image/jpeg' || event.target.files[0].type == 'image/png') {
                    reader.readAsDataURL(file);
                    reader.onload = function () {
                        // console.log(reader.result);
                        // this.para.picture = reader.result;
                        // this.http.post(this.api.base_url + 'team/fileupload', { picture: reader.result }, {
                        //   reportProgress: true,
                        //   observe: 'events'
                        // })
                        _this.api.postData('team/upload', { data: reader.result, type: event.target.files[0].type }).then(function (res) {
                            console.log(res);
                            _this.para.picture = res.url;
                            _this.cd.detectChanges();
                        });
                        // let selectedFile = event.target.files[0];
                        // const uploadData = new FormData();
                        // uploadData.append('photo', selectedFile, selectedFile.name);
                        // this.http.post(this.api.base_url + 'team/fileupload', uploadData, {
                        //   reportProgress: true,
                        //   observe: 'events'
                        // })
                        //   .subscribe((e) => {
                        //     console.log(e);
                        //   })
                        // this.formGroup.patchValue({
                        //   file: reader.result
                        // });
                        // need to run CD since file load runs outside of zone
                        _this.cd.markForCheck();
                    };
                }
                else {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire('ต้องเป็นไฟล์ jpg หรือ png เท่านั้น', '', 'error');
                }
            }
            else {
                sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire('ไฟล์ต้องมีขนาดน้อยกกว่า 2 Mb', '', 'error');
            }
        }
    };
    NgbdModalOrder.prototype.change_picture = function () {
        console.log('changepic');
        this.file.nativeElement.click();
    };
    NgbdModalOrder.prototype.onReactiveFormSubmit = function () {
        var _this = this;
        // console.log(this.teamForm.value);
        var data = this.teamForm.value;
        data.t_id = this.para.t_id;
        data.picture = this.para.picture;
        this.api.postData('team/add_team', this.teamForm.value).then(function (res) {
            console.log(res);
            if (res == '1') {
                _this.api.fb_set('backend');
                _this.toastr.success('บันทึกเรียบรอ้แล้ว');
                _this.activeModal.close();
            }
            else {
                _this.toastr.error('มีปัญหาในการบันทึกข้อมูล');
            }
        });
        console.log("summit");
    };
    NgbdModalOrder.prototype.close = function () {
        this.activeModal.dismiss();
    };
    NgbdModalOrder.prototype.ngOnInit = function () {
        console.log("xxxx");
        console.log(this.para);
        if (this.para.t_id == '0') {
            this.para = {
                t_id: "0", username: "", password: "", name: "", email: '', phone: "",
                gender: "m",
                picture: "https://image.flaticon.com/icons/svg/194/194938.svg",
                type: "1"
            };
            this.teamForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
                'username': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(6), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(24), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[a-zA-Z0-9._-]+$')]),
                'password': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(6), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(24)]),
                'name': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(6)]),
                'email': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].email]),
                'phone': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(9), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(10), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('[0-9]+')]),
                gender: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
                type: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required])
            }, { updateOn: 'blur' });
        }
        else {
            this.teamForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
                'username': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(6), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(24), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[a-zA-Z0-9._-]+$')]),
                'password': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(6), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(24)]),
                'name': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(6)]),
                'email': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].email]),
                'phone': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(9), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(10), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('[0-9]+')]),
                gender: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
                type: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required])
            }, { updateOn: 'blur' });
        }
        this.teamForm.patchValue(this.para);
        this.teamForm.patchValue({ password: '' });
    };
    NgbdModalOrder.prototype.c = function (val) {
        console.log(val);
    };
    NgbdModalOrder.ctorParameters = function () { return [
        { type: _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbActiveModal"] },
        { type: _angular_fire_database__WEBPACK_IMPORTED_MODULE_4__["AngularFireDatabase"] },
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["ChangeDetectorRef"] },
        { type: ngx_toastr__WEBPACK_IMPORTED_MODULE_8__["ToastrService"] },
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_7__["HttpClient"] },
        { type: app_webapi_service__WEBPACK_IMPORTED_MODULE_6__["WebapiService"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], NgbdModalOrder.prototype, "para", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["ViewChild"])("file", { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_2__["ElementRef"])
    ], NgbdModalOrder.prototype, "file", void 0);
    NgbdModalOrder = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'ngbd-modal-content',
            template: __webpack_require__(/*! raw-loader!./add_order.html */ "./node_modules/raw-loader/index.js!./src/app/order/add_order.html"),
            styles: [__webpack_require__(/*! ./add_order.scss */ "./src/app/order/add_order.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbActiveModal"],
            _angular_fire_database__WEBPACK_IMPORTED_MODULE_4__["AngularFireDatabase"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"], _angular_core__WEBPACK_IMPORTED_MODULE_2__["ChangeDetectorRef"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_8__["ToastrService"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_7__["HttpClient"], app_webapi_service__WEBPACK_IMPORTED_MODULE_6__["WebapiService"]])
    ], NgbdModalOrder);
    return NgbdModalOrder;
}());

var OrderComponent = /** @class */ (function () {
    function OrderComponent(modalService, firebase, api) {
        this.modalService = modalService;
        this.firebase = firebase;
        this.api = api;
        this.orders = [];
        this.admin_id = 0;
        this.all_page = 0;
        this.page = 1;
        this.orders = this.api.storage_get('orders') || [];
    }
    OrderComponent.prototype.onReactiveFormSubmit = function () {
        var a = this.teamForm.value();
        console.log(a);
    };
    OrderComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.api.check_login();
        this.searchForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            'search': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('')
        });
        this.teamForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            'inputEmail': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].email]),
            'password': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(4), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(24)]),
            'textArea': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            'radioOption': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('Option one is this')
        }, { updateOn: 'blur' });
        this.load_data(1);
        this.admin_id = this.api.storage_get('logindata').t_id;
        this.api.fb_val('backend', function (res) {
            console.log(res.node_.value_);
            console.log('new order');
            _this.load_data(1);
            // this.menuItems[1].badge = res.node_.value_ ;
        });
    };
    OrderComponent.prototype.load_data = function (page) {
        var _this = this;
        this.api.getData('order/load_orders/' + page + "?search=" + this.searchForm.value.search).then(function (data) {
            console.log(data);
            _this.orders = data.data;
            _this.all_page = data.count / 20 * 20;
            _this.api.storage_set('orders', data.data);
        });
    };
    OrderComponent.prototype.change_page = function (page) {
        this.load_data(page);
    };
    OrderComponent.prototype.view = function (t) {
        var _this = this;
        console.log(t);
        var modal = this.modalService.open(viewModalOrder, { size: 'lg' });
        modal.componentInstance.para = t;
        modal.result.then(function (result) {
            console.log(result);
        }, function (reason) {
        });
        modal.result.then(function (result) {
            _this.closeResult = "Closed with: " + result;
        }, function (reason) {
            _this.closeResult = "Dismissed " + _this.getDismissReason(reason);
        });
    };
    OrderComponent.prototype.search = function () {
        this.load_data(1);
    };
    OrderComponent.prototype.view_profile = function (t) {
        this.view(t);
    };
    OrderComponent.prototype.open = function (t) {
        var _this = this;
        console.log(t);
        var modal = this.modalService.open(NgbdModalOrder);
        modal.componentInstance.para = t;
        modal.result.then(function (result) {
            _this.load_data(_this.page);
        }, function (reason) {
            console.log(reason);
        });
        modal.result.then(function (result) {
            _this.closeResult = "Closed with: " + result;
        }, function (reason) {
            _this.closeResult = "Dismissed " + _this.getDismissReason(reason);
        });
    };
    OrderComponent.prototype.cancel_order = function (t) {
        var _this = this;
        sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire({
            title: 'คุณต้องการยกเลิกรายการนี้จริงหรือไม่',
            text: "รายการนี้จะไม่สามารถนำกลับมาได้อีก",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#0CC27E',
            cancelButtonColor: '#FF586B',
            confirmButtonText: 'ยืนยัน',
            cancelButtonText: 'ปิดออก',
            confirmButtonClass: 'btn btn-success btn-raised mr-2',
            cancelButtonClass: 'btn btn-default btn-raised',
            buttonsStyling: false
        }).then(function (result) {
            console.log(t);
            if (result.value) {
                _this.api.postData('Order/cancel_order', { order_id: t, admin: _this.admin_id }).then(function (res) {
                    _this.api.fb_set('order');
                    console.log(res);
                    _this.load_data(_this.page);
                    sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire('ยกเลิกเรียบร้อยแล้ว', '', 'success');
                });
            }
        });
    };
    OrderComponent.prototype.getDismissReason = function (reason) {
        if (reason === _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["ModalDismissReasons"].ESC) {
            return 'by pressing ESC';
        }
        else if (reason === _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["ModalDismissReasons"].BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        }
        else {
            return "with: " + reason;
        }
    };
    OrderComponent.ctorParameters = function () { return [
        { type: _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbModal"] },
        { type: _angular_fire_database__WEBPACK_IMPORTED_MODULE_4__["AngularFireDatabase"] },
        { type: app_webapi_service__WEBPACK_IMPORTED_MODULE_6__["WebapiService"] }
    ]; };
    OrderComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-team',
            template: __webpack_require__(/*! raw-loader!./order.component.html */ "./node_modules/raw-loader/index.js!./src/app/order/order.component.html"),
            styles: [__webpack_require__(/*! ./order.component.scss */ "./src/app/order/order.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbModal"],
            _angular_fire_database__WEBPACK_IMPORTED_MODULE_4__["AngularFireDatabase"],
            app_webapi_service__WEBPACK_IMPORTED_MODULE_6__["WebapiService"]])
    ], OrderComponent);
    return OrderComponent;
}());



/***/ }),

/***/ "./src/app/order/view_order.scss":
/*!***************************************!*\
  !*** ./src/app/order/view_order.scss ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".content {\n  border: #fff solid 2px;\n  border-radius: 20px;\n  margin: 15px 5px;\n  padding: 20px; }\n\ndl {\n  padding-bottom: 0px; }\n\ndl dt {\n    font-weight: bold;\n    width: 120px;\n    text-align: right;\n    display: inline-block; }\n\ndl dt::after {\n    content: \":\"; }\n\ndl dd {\n    display: inline-block;\n    width: calc(100% - 120px);\n    color: #000;\n    padding-left: 15px; }\n\ntable tbody .win {\n  background-color: #ffeedf;\n  color: #f57F17; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvb3JkZXIvQzpcXGRldmVsb3BlclxcYmFja2VuZC9zcmNcXGFwcFxcb3JkZXJcXHZpZXdfb3JkZXIuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLHNCQUFzQjtFQUN0QixtQkFBbUI7RUFDbkIsZ0JBQWdCO0VBQ2hCLGFBQWEsRUFBQTs7QUFHZjtFQWlCRSxtQkFBbUIsRUFBQTs7QUFqQnJCO0lBR0ksaUJBQWlCO0lBQ2pCLFlBQVk7SUFDWixpQkFBaUI7SUFDakIscUJBQXFCLEVBQUE7O0FBTnpCO0lBU0ksWUFBWSxFQUFBOztBQVRoQjtJQVlJLHFCQUFxQjtJQUNyQix5QkFBeUI7SUFDekIsV0FBVztJQUNYLGtCQUFrQixFQUFBOztBQUt0QjtFQUdNLHlCQUF5QjtFQUN6QixjQUFjLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9vcmRlci92aWV3X29yZGVyLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuY29udGVudCB7XHJcbiAgYm9yZGVyOiAjZmZmIHNvbGlkIDJweDtcclxuICBib3JkZXItcmFkaXVzOiAyMHB4O1xyXG4gIG1hcmdpbjogMTVweCA1cHg7XHJcbiAgcGFkZGluZzogMjBweDtcclxufVxyXG5cclxuZGwge1xyXG4gIGR0IHtcclxuICAgICAgXHJcbiAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgIHdpZHRoOiAxMjBweDtcclxuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gIH1cclxuICBkdDo6YWZ0ZXIge1xyXG4gICAgY29udGVudDogXCI6XCI7XHJcbiAgfVxyXG4gIGRkIHtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgIHdpZHRoOiBjYWxjKDEwMCUgLSAxMjBweCk7XHJcbiAgICBjb2xvcjogIzAwMDtcclxuICAgIHBhZGRpbmctbGVmdDogMTVweDtcclxuICB9XHJcbiAgcGFkZGluZy1ib3R0b206IDBweDtcclxufVxyXG5cclxudGFibGUge1xyXG4gIHRib2R5e1xyXG4gICAgLndpbntcclxuICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZWVkZjtcclxuICAgICAgY29sb3I6ICNmNTdGMTc7XHJcbiAgICB9XHJcbiAgfVxyXG59Il19 */"

/***/ }),

/***/ "./src/app/payment/payment.component.scss":
/*!************************************************!*\
  !*** ./src/app/payment/payment.component.scss ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".btn[class*=\"btn-\"] {\n  margin-bottom: 0rem; }\n\n.justify-content-center {\n  display: flex !important;\n  justify-content: center !important; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGF5bWVudC9DOlxcZGV2ZWxvcGVyXFxiYWNrZW5kL3NyY1xcYXBwXFxwYXltZW50XFxwYXltZW50LmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksbUJBQW1CLEVBQUE7O0FBRXJCO0VBQ0Usd0JBQXdCO0VBQ3hCLGtDQUFrQyxFQUFBIiwiZmlsZSI6InNyYy9hcHAvcGF5bWVudC9wYXltZW50LmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmJ0bltjbGFzcyo9XCJidG4tXCJdIHtcclxuICAgIG1hcmdpbi1ib3R0b206IDByZW07XHJcbiAgfVxyXG4gIC5qdXN0aWZ5LWNvbnRlbnQtY2VudGVyIHtcclxuICAgIGRpc3BsYXk6IGZsZXggIWltcG9ydGFudDtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyICFpbXBvcnRhbnQ7XHJcbiAgfSJdfQ== */"

/***/ }),

/***/ "./src/app/payment/payment.component.ts":
/*!**********************************************!*\
  !*** ./src/app/payment/payment.component.ts ***!
  \**********************************************/
/*! exports provided: viewModalPayment, PaymentComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "viewModalPayment", function() { return viewModalPayment; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaymentComponent", function() { return PaymentComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var app_webapi_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! app/webapi.service */ "./src/app/webapi.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _angular_fire_database__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/fire/database */ "./node_modules/@angular/fire/database/index.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_6__);







var viewModalPayment = /** @class */ (function () {
    function viewModalPayment(activeModal, api, firebase) {
        this.activeModal = activeModal;
        this.api = api;
        this.firebase = firebase;
        this.admin_id = 0;
        this.admin_type = 0;
    }
    viewModalPayment.prototype.close = function () {
        this.activeModal.dismiss();
    };
    viewModalPayment.prototype.ngOnInit = function () {
        console.log("view");
        console.log(this.payment);
        this.admin_id = this.api.storage_get('logindata').t_id;
        this.admin_type = this.api.storage_get('logindata').type;
    };
    viewModalPayment.prototype.confirm = function (pm_id) {
        var _this = this;
        console.log('Confirm by admin ID =', this.admin_id, ' pm_id =', pm_id);
        this.api.postData('Payment/confirm_payment/', { pm_id: pm_id, id: this.admin_id, confirm: 1 }).then(function (res) {
            _this.api.fb_set('order');
            _this.close();
        });
    };
    viewModalPayment.prototype.cancel = function (pm_id) {
        var _this = this;
        console.log('Cancel by admin ID =', this.admin_id, ' pm_id =', pm_id);
        this.api.postData('Payment/confirm_payment/', { pm_id: pm_id, id: this.admin_id, confirm: -1 }).then(function (res) {
            _this.api.fb_set('order');
            _this.close();
        });
    };
    viewModalPayment.ctorParameters = function () { return [
        { type: _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__["NgbActiveModal"] },
        { type: app_webapi_service__WEBPACK_IMPORTED_MODULE_2__["WebapiService"] },
        { type: _angular_fire_database__WEBPACK_IMPORTED_MODULE_5__["AngularFireDatabase"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], viewModalPayment.prototype, "payment", void 0);
    viewModalPayment = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'view-modal-content',
            template: __webpack_require__(/*! raw-loader!./payment_details.html */ "./node_modules/raw-loader/index.js!./src/app/payment/payment_details.html"),
            styles: [__webpack_require__(/*! ./payment_details.scss */ "./src/app/payment/payment_details.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__["NgbActiveModal"], app_webapi_service__WEBPACK_IMPORTED_MODULE_2__["WebapiService"], _angular_fire_database__WEBPACK_IMPORTED_MODULE_5__["AngularFireDatabase"]])
    ], viewModalPayment);
    return viewModalPayment;
}());

var PaymentComponent = /** @class */ (function () {
    function PaymentComponent(api, ref, modalService, firebase) {
        this.api = api;
        this.ref = ref;
        this.modalService = modalService;
        this.firebase = firebase;
        this.status = 0;
        this.all_page = 0;
        this.page = 1;
        this.admin_id = 0;
        this.admin_type = 0;
        this.payments = { name: '' };
        this.pm_detail = {
            pm_id: '1',
            sum_price: '50000',
            img: '',
            sale_name: 'ตู่ รัฐบาป',
            cus_name: 'ป้อม รัฐบาป',
            product: 'รถถัง',
            count: '50',
            date_send: '22-6-2020',
            time_send: '17:00',
            address: 'ทำเนียบ',
            status: '0'
        };
        this.fb = false;
    }
    PaymentComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.fb = true;
        this.searchForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroup"]({
            'search': new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('')
        });
        this.admin_id = this.api.storage_get('logindata').t_id;
        this.admin_type = this.api.storage_get('logindata').type;
        this.api.fb_val('order', function (res) {
            if (_this.fb) {
                _this.load_paymant(1);
            }
        });
    };
    PaymentComponent.prototype.load_paymant = function (page) {
        var _this = this;
        console.log('OK pm');
        this.api.getData('Payment/load_payment/' + page + "?search=" + this.searchForm.value.search).then(function (res) {
            console.log(res);
            _this.payments = res;
            _this.all_page = res.count / 20 * 20;
            _this.ref.detectChanges();
        });
    };
    PaymentComponent.prototype.confirm = function (pm_id) {
        var _this = this;
        sweetalert2__WEBPACK_IMPORTED_MODULE_6___default.a.fire({
            title: 'คุณต้องการยืนยันการชำระเงินนี้',
            text: "รายการนี้จะถูกบันทึกว่าได้รับชำระเงินแล้ว",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#0CC27E',
            cancelButtonColor: '#FF586B',
            confirmButtonText: 'ยืนยัน',
            cancelButtonText: 'ปิดออก',
            confirmButtonClass: 'btn btn-success btn-raised mr-2',
            cancelButtonClass: 'btn btn-default btn-raised',
            buttonsStyling: false
        }).then(function (result) {
            if (result.value) {
                _this.api.postData('Payment/confirm_payment/', { pm_id: pm_id, id: _this.admin_id, confirm: 1 }).then(function (res) {
                    if (res == 'success') {
                        _this.api.fb_set('order');
                        sweetalert2__WEBPACK_IMPORTED_MODULE_6___default.a.fire('เรียบร้อยแล้ว', '', 'success');
                    }
                });
            }
        });
    };
    PaymentComponent.prototype.cancel = function (pm_id) {
        var _this = this;
        console.log('canceled order id =' + pm_id);
        this.api.postData('Payment/confirm_payment/', { pm_id: pm_id, id: this.admin_id, confirm: -2 }).then(function (res) {
            _this.api.fb_set('order');
            _this.load_paymant(1);
        });
    };
    PaymentComponent.prototype.search = function () {
        this.load_paymant(1);
    };
    PaymentComponent.prototype.change_page = function (page) {
        console.log('Change' + page);
        this.load_paymant(page);
    };
    PaymentComponent.prototype.test = function (t) {
        var _this = this;
        console.log('clicked');
        console.log(t);
        var modal = this.modalService.open(viewModalPayment, { size: 'lg' });
        modal.componentInstance.payment = t;
        modal.result.then(function (result) {
            console.log(result);
        }, function (reason) {
        });
        modal.result.then(function (result) {
            _this.closeResult = "Closed with: " + result;
        }, function (reason) {
            _this.closeResult = "Dismissed " + _this.getDismissReason(reason);
        });
    };
    PaymentComponent.prototype.getDismissReason = function (reason) {
        this.load_paymant(1);
        if (reason === _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__["ModalDismissReasons"].ESC) {
            return 'by pressing ESC';
        }
        else if (reason === _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__["ModalDismissReasons"].BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        }
        else {
            return "with: " + reason;
        }
    };
    PaymentComponent.prototype.ngOnDestroy = function () {
        this.fb = false;
    };
    PaymentComponent.ctorParameters = function () { return [
        { type: app_webapi_service__WEBPACK_IMPORTED_MODULE_2__["WebapiService"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"] },
        { type: _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__["NgbModal"] },
        { type: _angular_fire_database__WEBPACK_IMPORTED_MODULE_5__["AngularFireDatabase"] }
    ]; };
    PaymentComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-payment',
            template: __webpack_require__(/*! raw-loader!./payment.component.html */ "./node_modules/raw-loader/index.js!./src/app/payment/payment.component.html"),
            styles: [__webpack_require__(/*! ./payment.component.scss */ "./src/app/payment/payment.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [app_webapi_service__WEBPACK_IMPORTED_MODULE_2__["WebapiService"],
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"],
            _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__["NgbModal"],
            _angular_fire_database__WEBPACK_IMPORTED_MODULE_5__["AngularFireDatabase"]])
    ], PaymentComponent);
    return PaymentComponent;
}());



/***/ }),

/***/ "./src/app/payment/payment_details.scss":
/*!**********************************************!*\
  !*** ./src/app/payment/payment_details.scss ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".content {\n  border: #fff solid 2px;\n  border-radius: 20px;\n  margin: 15px 5px;\n  padding: 20px; }\n\ndl {\n  padding-bottom: 0px; }\n\ndl dt {\n    font-weight: bold;\n    width: 120px;\n    text-align: right;\n    display: inline-block; }\n\ndl dt::after {\n    content: \":\"; }\n\ndl dd {\n    display: inline-block;\n    width: calc(100% - 120px);\n    color: #000;\n    padding-left: 15px; }\n\ntable tbody .win {\n  background-color: #ffeedf;\n  color: #f57F17; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGF5bWVudC9DOlxcZGV2ZWxvcGVyXFxiYWNrZW5kL3NyY1xcYXBwXFxwYXltZW50XFxwYXltZW50X2RldGFpbHMuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLHNCQUFzQjtFQUN0QixtQkFBbUI7RUFDbkIsZ0JBQWdCO0VBQ2hCLGFBQWEsRUFBQTs7QUFHZjtFQWlCRSxtQkFBbUIsRUFBQTs7QUFqQnJCO0lBR0ksaUJBQWlCO0lBQ2pCLFlBQVk7SUFDWixpQkFBaUI7SUFDakIscUJBQXFCLEVBQUE7O0FBTnpCO0lBU0ksWUFBWSxFQUFBOztBQVRoQjtJQVlJLHFCQUFxQjtJQUNyQix5QkFBeUI7SUFDekIsV0FBVztJQUNYLGtCQUFrQixFQUFBOztBQUt0QjtFQUdNLHlCQUF5QjtFQUN6QixjQUFjLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9wYXltZW50L3BheW1lbnRfZGV0YWlscy5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmNvbnRlbnQge1xyXG4gICAgYm9yZGVyOiAjZmZmIHNvbGlkIDJweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDIwcHg7XHJcbiAgICBtYXJnaW46IDE1cHggNXB4O1xyXG4gICAgcGFkZGluZzogMjBweDtcclxuICB9XHJcbiAgXHJcbiAgZGwge1xyXG4gICAgZHQge1xyXG4gICAgICAgIFxyXG4gICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgd2lkdGg6IDEyMHB4O1xyXG4gICAgICB0ZXh0LWFsaWduOiByaWdodDtcclxuICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgfVxyXG4gICAgZHQ6OmFmdGVyIHtcclxuICAgICAgY29udGVudDogXCI6XCI7XHJcbiAgICB9XHJcbiAgICBkZCB7XHJcbiAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgICAgd2lkdGg6IGNhbGMoMTAwJSAtIDEyMHB4KTtcclxuICAgICAgY29sb3I6ICMwMDA7XHJcbiAgICAgIHBhZGRpbmctbGVmdDogMTVweDtcclxuICAgIH1cclxuICAgIHBhZGRpbmctYm90dG9tOiAwcHg7XHJcbiAgfVxyXG4gIFxyXG4gIHRhYmxlIHtcclxuICAgIHRib2R5e1xyXG4gICAgICAud2lue1xyXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmVlZGY7XHJcbiAgICAgICAgY29sb3I6ICNmNTdGMTc7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9Il19 */"

/***/ }),

/***/ "./src/app/product/add_product.scss":
/*!******************************************!*\
  !*** ./src/app/product/add_product.scss ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "body.layout-dark .modal .modal-header {\n  background: #374fe6; }\n\n.avatar {\n  width: 300px;\n  border: solid 3px #fff;\n  border-radius: 20px; }\n\noption {\n  background-color: #fff; }\n\nlabel {\n  color: #000;\n  display: inline-block;\n  padding-right: 7px;\n  font-size: 16px;\n  width: 130px; }\n\n.form-control {\n  width: calc(100% - 130px);\n  display: inline-block; }\n\n.newF {\n  width: calc(100% - 180px);\n  display: inline-block; }\n\n.newT {\n  width: calc(65% - 130px);\n  display: inline-block; }\n\n.change_img {\n  width: 26px;\n  height: 26px;\n  font-size: 13px;\n  text-align: center;\n  border-radius: 50%;\n  position: absolute;\n  left: calc(50% - 13px);\n  top: 82px;\n  padding-left: 1px;\n  padding-top: 3px;\n  background-color: black; }\n\ntd img {\n  width: 50px; }\n\n::-webkit-input-placeholder {\n  /* Edge */\n  color: red; }\n\n:-ms-input-placeholder {\n  /* Internet Explorer 10-11 */\n  color: red; }\n\n::-moz-placeholder {\n  color: red !important; }\n\n::-ms-input-placeholder {\n  color: red !important; }\n\n::placeholder {\n  color: red !important; }\n\nsmall {\n  margin-left: 130px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcHJvZHVjdC9DOlxcZGV2ZWxvcGVyXFxiYWNrZW5kL3NyY1xcYXBwXFxwcm9kdWN0XFxhZGRfcHJvZHVjdC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNFO0VBQ0UsbUJBQW1CLEVBQUE7O0FBRXJCO0VBQ0UsWUFBWTtFQUNaLHNCQUFzQjtFQUN0QixtQkFBbUIsRUFBQTs7QUFFckI7RUFDRSxzQkFBc0IsRUFBQTs7QUFFdkI7RUFDQyxXQUFXO0VBQ1gscUJBQXFCO0VBQ3JCLGtCQUFrQjtFQUNsQixlQUFlO0VBQ2YsWUFBWSxFQUFBOztBQUVkO0VBQ0UseUJBQXlCO0VBQ3pCLHFCQUFxQixFQUFBOztBQUd2QjtFQUNFLHlCQUF5QjtFQUN6QixxQkFBcUIsRUFBQTs7QUFFdkI7RUFDRSx3QkFBd0I7RUFDeEIscUJBQXFCLEVBQUE7O0FBRXZCO0VBQ0UsV0FBVztFQUNYLFlBQVk7RUFDWixlQUFlO0VBQ2Ysa0JBQWtCO0VBQ2xCLGtCQUFrQjtFQUNsQixrQkFBa0I7RUFDbEIsc0JBQXNCO0VBQ3RCLFNBQVM7RUFDVCxpQkFBaUI7RUFDakIsZ0JBQWdCO0VBQ2hCLHVCQUF1QixFQUFBOztBQUV6QjtFQUVJLFdBQVcsRUFBQTs7QUFJZjtFQUE4QixTQUFBO0VBQzVCLFVBQVUsRUFBQTs7QUFHWjtFQUF5Qiw0QkFBQTtFQUN2QixVQUFVLEVBQUE7O0FBR1o7RUFDRSxxQkFBcUIsRUFBQTs7QUFEdkI7RUFDRSxxQkFBcUIsRUFBQTs7QUFEdkI7RUFDRSxxQkFBcUIsRUFBQTs7QUFFdkI7RUFDRSxrQkFBa0IsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3Byb2R1Y3QvYWRkX3Byb2R1Y3Quc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIlxyXG4gIGJvZHkubGF5b3V0LWRhcmsgLm1vZGFsIC5tb2RhbC1oZWFkZXIge1xyXG4gICAgYmFja2dyb3VuZDogIzM3NGZlNjtcclxuICB9XHJcbiAgLmF2YXRhciB7XHJcbiAgICB3aWR0aDogMzAwcHg7XHJcbiAgICBib3JkZXI6IHNvbGlkIDNweCAjZmZmO1xyXG4gICAgYm9yZGVyLXJhZGl1czogMjBweDtcclxuICB9XHJcbiAgb3B0aW9uIHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XHJcbiAgfVxyXG4gICBsYWJlbCB7XHJcbiAgICBjb2xvcjogIzAwMDtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgIHBhZGRpbmctcmlnaHQ6IDdweDtcclxuICAgIGZvbnQtc2l6ZTogMTZweDtcclxuICAgIHdpZHRoOiAxMzBweDtcclxuICB9XHJcbiAgLmZvcm0tY29udHJvbCB7XHJcbiAgICB3aWR0aDogY2FsYygxMDAlIC0gMTMwcHgpO1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gIH1cclxuXHJcbiAgLm5ld0Yge1xyXG4gICAgd2lkdGg6IGNhbGMoMTAwJSAtIDE4MHB4KTtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICB9XHJcbiAgLm5ld1Qge1xyXG4gICAgd2lkdGg6IGNhbGMoNjUlIC0gMTMwcHgpO1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gIH1cclxuICAuY2hhbmdlX2ltZyB7XHJcbiAgICB3aWR0aDogMjZweDtcclxuICAgIGhlaWdodDogMjZweDtcclxuICAgIGZvbnQtc2l6ZTogMTNweDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGxlZnQ6IGNhbGMoNTAlIC0gMTNweCk7XHJcbiAgICB0b3A6IDgycHg7XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDFweDtcclxuICAgIHBhZGRpbmctdG9wOiAzcHg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiBibGFjaztcclxuICB9XHJcbiAgdGQge1xyXG4gICAgaW1nIHtcclxuICAgICAgd2lkdGg6IDUwcHg7XHJcbiAgICB9XHJcbiAgfVxyXG4gIFxyXG4gIDo6LXdlYmtpdC1pbnB1dC1wbGFjZWhvbGRlciB7IC8qIEVkZ2UgKi9cclxuICAgIGNvbG9yOiByZWQ7XHJcbiAgfVxyXG4gIFxyXG4gIDotbXMtaW5wdXQtcGxhY2Vob2xkZXIgeyAvKiBJbnRlcm5ldCBFeHBsb3JlciAxMC0xMSAqL1xyXG4gICAgY29sb3I6IHJlZDtcclxuICB9XHJcbiAgXHJcbiAgOjpwbGFjZWhvbGRlciB7XHJcbiAgICBjb2xvcjogcmVkICFpbXBvcnRhbnQ7XHJcbiAgfVxyXG4gIHNtYWxsICB7XHJcbiAgICBtYXJnaW4tbGVmdDogMTMwcHg7XHJcbiAgfSJdfQ== */"

/***/ }),

/***/ "./src/app/product/product.component.scss":
/*!************************************************!*\
  !*** ./src/app/product/product.component.scss ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".btn[class*=\"btn-\"] {\n  margin-bottom: 0rem; }\n\n.searchform {\n  margin-right: 10px;\n  display: inline-block; }\n\n.justify-content-center {\n  display: flex !important;\n  justify-content: center !important; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcHJvZHVjdC9DOlxcZGV2ZWxvcGVyXFxiYWNrZW5kL3NyY1xcYXBwXFxwcm9kdWN0XFxwcm9kdWN0LmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsbUJBQW1CLEVBQUE7O0FBRXJCO0VBQ0Usa0JBQWtCO0VBQ2xCLHFCQUFxQixFQUFBOztBQUV2QjtFQUNFLHdCQUF3QjtFQUN4QixrQ0FBa0MsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3Byb2R1Y3QvcHJvZHVjdC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5idG5bY2xhc3MqPVwiYnRuLVwiXSB7XHJcbiAgbWFyZ2luLWJvdHRvbTogMHJlbTtcclxufVxyXG4uc2VhcmNoZm9ybSB7XHJcbiAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xyXG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxufVxyXG4uanVzdGlmeS1jb250ZW50LWNlbnRlciB7XHJcbiAgZGlzcGxheTogZmxleCAhaW1wb3J0YW50O1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyICFpbXBvcnRhbnQ7XHJcbn0iXX0= */"

/***/ }),

/***/ "./src/app/product/product.component.ts":
/*!**********************************************!*\
  !*** ./src/app/product/product.component.ts ***!
  \**********************************************/
/*! exports provided: viewModalProduct, NgbdModalProduct, ProductComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "viewModalProduct", function() { return viewModalProduct; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NgbdModalProduct", function() { return NgbdModalProduct; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductComponent", function() { return ProductComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var app_webapi_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! app/webapi.service */ "./src/app/webapi.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ckeditor/ckeditor5-build-classic */ "./node_modules/@ckeditor/ckeditor5-build-classic/build/ckeditor.js");
/* harmony import */ var _ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(_ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_8__);









var viewModalProduct = /** @class */ (function () {
    // productForm: FormGroup;
    function viewModalProduct(activeModal) {
        this.activeModal = activeModal;
    }
    viewModalProduct.prototype.close = function () {
        this.activeModal.dismiss();
    };
    viewModalProduct.prototype.ngOnInit = function () {
        console.log("view");
        console.log(this.para);
        // this.productForm = new FormGroup({
        //   'email': new FormControl(null, [Validators.required, Validators.email]),
        //   'username': new FormControl(null, [Validators.required, Validators.minLength(4), Validators.maxLength(24)]),
        //   'password': new FormControl(null, [Validators.required, Validators.minLength(4), Validators.maxLength(24)]),
        //   'name': new FormControl(null, [Validators.required, Validators.email, Validators.minLength(4)]),
        //   'phone': new FormControl(null, [Validators.required, Validators.email, Validators.minLength(9), Validators.minLength(10)]),
        // }, { updateOn: 'blur' });
        // this.productForm.patchValue({username:'xxx'});
    };
    viewModalProduct.prototype.c = function (val) {
        console.log(val);
    };
    viewModalProduct.ctorParameters = function () { return [
        { type: _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbActiveModal"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], viewModalProduct.prototype, "para", void 0);
    viewModalProduct = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'view-modal-content',
            template: __webpack_require__(/*! raw-loader!./view_product.html */ "./node_modules/raw-loader/index.js!./src/app/product/view_product.html"),
            styles: [__webpack_require__(/*! ./view_product.scss */ "./src/app/product/view_product.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbActiveModal"]])
    ], viewModalProduct);
    return viewModalProduct;
}());

var NgbdModalProduct = /** @class */ (function () {
    function NgbdModalProduct(activeModal, fb, cd, toastr, http, api) {
        this.activeModal = activeModal;
        this.fb = fb;
        this.cd = cd;
        this.toastr = toastr;
        this.http = http;
        this.api = api;
        this.Editor = _ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_8__;
        this.editorConfig = {};
    }
    NgbdModalProduct.prototype.ngOnInit = function () {
        console.log(this.para);
        if (this.para.p_id == '0') {
            this.para = {
                p_id: "0",
                p_name: { th: '', en: '' },
                how_to_use: { th: '', en: '' },
                p_img: "https://api.concretedeliveryeasy.com/uploads/p_df.jpg",
                show: '',
                sort: 1
            };
            this.productForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
                'p_name': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
                    'th': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
                    'en': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required])
                }),
                'how_to_use': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
                    'th': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
                    'en': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required])
                }),
                'default_price': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
                    'price': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
                    'diff_sm': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
                    'diff_lg': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required])
                }),
                'show': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
                    'show': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](true),
                    'sort': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](1),
                }),
            });
        }
        else {
            this.productForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
                'p_name': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
                    'th': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
                    'en': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required])
                }),
                'how_to_use': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
                    'th': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
                    'en': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required])
                }),
                'default_price': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
                    'price': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
                    'diff_sm': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
                    'diff_lg': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required])
                }),
                'show': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
                    'show': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.para.show),
                    'sort': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.para.sort),
                }),
            });
        }
        this.productForm.patchValue(this.para);
    };
    NgbdModalProduct.prototype.file_change = function (event) {
        var _this = this;
        var reader = new FileReader();
        if (event.target.files && event.target.files.length) {
            var file = event.target.files[0];
            console.log(event.target.files);
            if (event.target.files[0].size < 2000000) {
                if (event.target.files[0].type == 'image/jpeg' || event.target.files[0].type == 'image/png') {
                    reader.readAsDataURL(file);
                    reader.onload = function () {
                        _this.api.postData('team/upload', { data: reader.result, type: event.target.files[0].type }).then(function (res) {
                            console.log(res);
                            _this.para.p_img = res.url;
                            _this.cd.detectChanges();
                        });
                        _this.cd.markForCheck();
                    };
                }
                else {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire('ต้องเป็นไฟล์ jpg หรือ png เท่านั้น', '', 'error');
                }
            }
            else {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire('ไฟล์ต้องมีขนาดน้อยกกว่า 2 Mb', '', 'error');
            }
        }
    };
    NgbdModalProduct.prototype.search = function () {
    };
    NgbdModalProduct.prototype.change_picture = function () {
        console.log('changepic');
        this.file.nativeElement.click();
    };
    NgbdModalProduct.prototype.onReactiveFormSubmit = function () {
        var _this = this;
        // console.log(this.productForm.value);
        var data = this.productForm.value;
        data.p_id = this.para.p_id;
        data.p_img = this.para.p_img;
        console.log(data);
        this.api.postData('product/add_product', data).then(function (res) {
            console.log(res);
            if (res == '1') {
                _this.toastr.success('บันทึกเรียบร้อยแล้ว');
                _this.activeModal.close();
            }
            else {
                _this.toastr.error('มีปัญหาในการบันทึกข้อมูล');
            }
        });
        console.log("summit");
    };
    NgbdModalProduct.prototype.close = function () {
        this.activeModal.dismiss();
    };
    NgbdModalProduct.prototype.c = function (val) {
        console.log(val);
    };
    NgbdModalProduct.ctorParameters = function () { return [
        { type: _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbActiveModal"] },
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["ChangeDetectorRef"] },
        { type: ngx_toastr__WEBPACK_IMPORTED_MODULE_7__["ToastrService"] },
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpClient"] },
        { type: app_webapi_service__WEBPACK_IMPORTED_MODULE_5__["WebapiService"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], NgbdModalProduct.prototype, "para", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["ViewChild"])("file", { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_2__["ElementRef"])
    ], NgbdModalProduct.prototype, "file", void 0);
    NgbdModalProduct = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'ngbd-modal-content',
            template: __webpack_require__(/*! raw-loader!./add_product.html */ "./node_modules/raw-loader/index.js!./src/app/product/add_product.html"),
            styles: [__webpack_require__(/*! ./add_product.scss */ "./src/app/product/add_product.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbActiveModal"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"], _angular_core__WEBPACK_IMPORTED_MODULE_2__["ChangeDetectorRef"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_7__["ToastrService"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpClient"], app_webapi_service__WEBPACK_IMPORTED_MODULE_5__["WebapiService"]])
    ], NgbdModalProduct);
    return NgbdModalProduct;
}());

var ProductComponent = /** @class */ (function () {
    function ProductComponent(modalService, api) {
        this.modalService = modalService;
        this.api = api;
        this.products = [];
        this.all_page = 0;
        this.page = 1;
        this.products = this.api.storage_get('products') || [];
    }
    ProductComponent.prototype.onReactiveFormSubmit = function () {
        var a = this.productForm.value();
        console.log(a);
    };
    ProductComponent.prototype.ngOnInit = function () {
        this.api.check_login();
        this.searchForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            'search': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('')
        });
        this.productForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            'inputEmail': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].email]),
            'password': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(4), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(24)]),
            'textArea': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            'radioOption': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('Option one is this')
        }, { updateOn: 'blur' });
        // this.api.getData('team/load_team').then((data: any) => {
        //   console.log(data);
        //   this.products = data;
        //   this.api.storage_set('teams', data);
        // })
        this.load_data(1);
    };
    ProductComponent.prototype.view = function (t) {
        var _this = this;
        console.log(t);
        var modal = this.modalService.open(viewModalProduct, { size: 'lg', backdrop: 'static' });
        modal.componentInstance.para = t;
        modal.result.then(function (result) {
            _this.closeResult = "Closed with: " + result;
        }, function (reason) {
            _this.closeResult = "Dismissed " + _this.getDismissReason(reason);
        });
    };
    ProductComponent.prototype.view_profile = function (t) {
        this.view(t);
    };
    ProductComponent.prototype.search = function () {
        this.load_data(1);
    };
    ProductComponent.prototype.load_data = function (page) {
        var _this = this;
        this.api.getData('product/load_product/' + page + "?search=" + this.searchForm.value.search).then(function (data) {
            _this.products = data.data;
            _this.all_page = data.count / 20 * 20;
            console.log(_this.all_page);
            _this.api.storage_set('products', data.data);
            console.log(data);
        });
    };
    ProductComponent.prototype.change_page = function (page) {
        console.log('Change' + page);
        this.load_data(page);
    };
    ProductComponent.prototype.open = function (t) {
        var _this = this;
        console.log(t);
        var modal = this.modalService.open(NgbdModalProduct, { size: 'lg', backdrop: 'static' });
        modal.componentInstance.para = t;
        modal.result.then(function (result) {
            _this.load_data(_this.page);
        }, function (reason) {
            console.log(reason);
        });
    };
    ProductComponent.prototype.delete_product = function (t) {
        var _this = this;
        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
            title: 'คุณต้องการลบจริงหรือไม่',
            text: "รายการนี้จะไม่สามารถนำกลับมาได้อีก",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#0CC27E',
            cancelButtonColor: '#FF586B',
            confirmButtonText: 'ลบออก',
            cancelButtonText: 'ปิดออก',
            confirmButtonClass: 'btn btn-success btn-raised mr-2',
            cancelButtonClass: 'btn btn-default btn-raised',
            buttonsStyling: false
        }).then(function (result) {
            console.log(t);
            console.log(t.p_id);
            if (result.value) {
                _this.api.postData('product/remove_product', { p_id: t.p_id }).then(function (res) {
                    console.log(res);
                    _this.load_data(_this.page);
                    sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire('ลบเรียบร้อยแล้ว', '', 'success');
                });
            }
        });
    };
    ProductComponent.prototype.getDismissReason = function (reason) {
        if (reason === _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["ModalDismissReasons"].ESC) {
            return 'by pressing ESC';
        }
        else if (reason === _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["ModalDismissReasons"].BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        }
        else {
            return "with: " + reason;
        }
    };
    ProductComponent.ctorParameters = function () { return [
        { type: _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbModal"] },
        { type: app_webapi_service__WEBPACK_IMPORTED_MODULE_5__["WebapiService"] }
    ]; };
    ProductComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-team',
            template: __webpack_require__(/*! raw-loader!./product.component.html */ "./node_modules/raw-loader/index.js!./src/app/product/product.component.html"),
            styles: [__webpack_require__(/*! ./product.component.scss */ "./src/app/product/product.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbModal"], app_webapi_service__WEBPACK_IMPORTED_MODULE_5__["WebapiService"]])
    ], ProductComponent);
    return ProductComponent;
}());



/***/ }),

/***/ "./src/app/product/view_product.scss":
/*!*******************************************!*\
  !*** ./src/app/product/view_product.scss ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".content {\n  border: #fff solid 2px;\n  border-radius: 20px;\n  margin: 15px 5px;\n  padding: 20px; }\n\ndl {\n  padding-bottom: 0px; }\n\ndl dt {\n    font-weight: bold;\n    width: 120px;\n    text-align: right;\n    display: inline-block; }\n\ndl dt::after {\n    content: \":\"; }\n\ndl dd {\n    display: inline-block;\n    width: calc(100% - 120px);\n    color: #ddd;\n    padding-left: 15px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcHJvZHVjdC9DOlxcZGV2ZWxvcGVyXFxiYWNrZW5kL3NyY1xcYXBwXFxwcm9kdWN0XFx2aWV3X3Byb2R1Y3Quc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLHNCQUFzQjtFQUN0QixtQkFBbUI7RUFDbkIsZ0JBQWdCO0VBQ2hCLGFBQWEsRUFBQTs7QUFHZjtFQWlCRSxtQkFBbUIsRUFBQTs7QUFqQnJCO0lBR0ksaUJBQWlCO0lBQ2pCLFlBQVk7SUFDWixpQkFBaUI7SUFDakIscUJBQXFCLEVBQUE7O0FBTnpCO0lBU0ksWUFBWSxFQUFBOztBQVRoQjtJQVlJLHFCQUFxQjtJQUNyQix5QkFBeUI7SUFDekIsV0FBVztJQUNYLGtCQUFrQixFQUFBIiwiZmlsZSI6InNyYy9hcHAvcHJvZHVjdC92aWV3X3Byb2R1Y3Quc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jb250ZW50IHtcclxuICBib3JkZXI6ICNmZmYgc29saWQgMnB4O1xyXG4gIGJvcmRlci1yYWRpdXM6IDIwcHg7XHJcbiAgbWFyZ2luOiAxNXB4IDVweDtcclxuICBwYWRkaW5nOiAyMHB4O1xyXG59XHJcblxyXG5kbCB7XHJcbiAgZHQge1xyXG4gICAgICBcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgd2lkdGg6IDEyMHB4O1xyXG4gICAgdGV4dC1hbGlnbjogcmlnaHQ7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgfVxyXG4gIGR0OjphZnRlciB7XHJcbiAgICBjb250ZW50OiBcIjpcIjtcclxuICB9XHJcbiAgZGQge1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgd2lkdGg6IGNhbGMoMTAwJSAtIDEyMHB4KTtcclxuICAgIGNvbG9yOiAjZGRkO1xyXG4gICAgcGFkZGluZy1sZWZ0OiAxNXB4O1xyXG4gIH1cclxuICBwYWRkaW5nLWJvdHRvbTogMHB4O1xyXG59XHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/saller/add_saller.scss":
/*!****************************************!*\
  !*** ./src/app/saller/add_saller.scss ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "body.layout-dark .modal .modal-header {\n  background: #374fe6; }\n\n.avatar {\n  width: 80px;\n  height: 80px;\n  border: solid 3px #fff;\n  border-radius: 50%; }\n\noption {\n  background-color: #fff; }\n\nlabel {\n  color: #000;\n  display: inline-block;\n  padding-right: 7px;\n  font-size: 16px;\n  width: 130px; }\n\n.form-control {\n  width: calc(100% - 130px);\n  display: inline-block; }\n\n.change_img {\n  width: 26px;\n  height: 26px;\n  font-size: 13px;\n  text-align: center;\n  border-radius: 50%;\n  position: absolute;\n  left: calc(50% - 13px);\n  top: 82px;\n  padding-left: 1px;\n  padding-top: 3px;\n  background-color: black; }\n\ntd img {\n  width: 50px; }\n\n::-webkit-input-placeholder {\n  /* Edge */\n  color: red; }\n\n:-ms-input-placeholder {\n  /* Internet Explorer 10-11 */\n  color: red; }\n\n::-moz-placeholder {\n  color: red !important; }\n\n::-ms-input-placeholder {\n  color: red !important; }\n\n::placeholder {\n  color: red !important; }\n\nsmall {\n  margin-left: 130px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2FsbGVyL0M6XFxkZXZlbG9wZXJcXGJhY2tlbmQvc3JjXFxhcHBcXHNhbGxlclxcYWRkX3NhbGxlci5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNFO0VBQ0UsbUJBQW1CLEVBQUE7O0FBRXJCO0VBQ0UsV0FBVztFQUNYLFlBQVk7RUFDWixzQkFBc0I7RUFDdEIsa0JBQWtCLEVBQUE7O0FBRXBCO0VBQ0Usc0JBQXNCLEVBQUE7O0FBRXZCO0VBQ0MsV0FBVztFQUNYLHFCQUFxQjtFQUNyQixrQkFBa0I7RUFDbEIsZUFBZTtFQUNmLFlBQVksRUFBQTs7QUFFZDtFQUNFLHlCQUF5QjtFQUN6QixxQkFBcUIsRUFBQTs7QUFFdkI7RUFDRSxXQUFXO0VBQ1gsWUFBWTtFQUNaLGVBQWU7RUFDZixrQkFBa0I7RUFDbEIsa0JBQWtCO0VBQ2xCLGtCQUFrQjtFQUNsQixzQkFBc0I7RUFDdEIsU0FBUztFQUNULGlCQUFpQjtFQUNqQixnQkFBZ0I7RUFDaEIsdUJBQXVCLEVBQUE7O0FBRXpCO0VBRUksV0FBVyxFQUFBOztBQUlmO0VBQThCLFNBQUE7RUFDNUIsVUFBVSxFQUFBOztBQUdaO0VBQXlCLDRCQUFBO0VBQ3ZCLFVBQVUsRUFBQTs7QUFHWjtFQUNFLHFCQUFxQixFQUFBOztBQUR2QjtFQUNFLHFCQUFxQixFQUFBOztBQUR2QjtFQUNFLHFCQUFxQixFQUFBOztBQUV2QjtFQUNFLGtCQUFrQixFQUFBIiwiZmlsZSI6InNyYy9hcHAvc2FsbGVyL2FkZF9zYWxsZXIuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIlxyXG4gIGJvZHkubGF5b3V0LWRhcmsgLm1vZGFsIC5tb2RhbC1oZWFkZXIge1xyXG4gICAgYmFja2dyb3VuZDogIzM3NGZlNjtcclxuICB9XHJcbiAgLmF2YXRhciB7XHJcbiAgICB3aWR0aDogODBweDtcclxuICAgIGhlaWdodDogODBweDtcclxuICAgIGJvcmRlcjogc29saWQgM3B4ICNmZmY7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1MCU7XHJcbiAgfVxyXG4gIG9wdGlvbiB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xyXG4gIH1cclxuICAgbGFiZWwge1xyXG4gICAgY29sb3I6ICMwMDA7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICBwYWRkaW5nLXJpZ2h0OiA3cHg7XHJcbiAgICBmb250LXNpemU6IDE2cHg7XHJcbiAgICB3aWR0aDogMTMwcHg7XHJcbiAgfVxyXG4gIC5mb3JtLWNvbnRyb2wge1xyXG4gICAgd2lkdGg6IGNhbGMoMTAwJSAtIDEzMHB4KTtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICB9XHJcbiAgLmNoYW5nZV9pbWcge1xyXG4gICAgd2lkdGg6IDI2cHg7XHJcbiAgICBoZWlnaHQ6IDI2cHg7XHJcbiAgICBmb250LXNpemU6IDEzcHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1MCU7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBsZWZ0OiBjYWxjKDUwJSAtIDEzcHgpO1xyXG4gICAgdG9wOiA4MnB4O1xyXG4gICAgcGFkZGluZy1sZWZ0OiAxcHg7XHJcbiAgICBwYWRkaW5nLXRvcDogM3B4O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogYmxhY2s7XHJcbiAgfVxyXG4gIHRkIHtcclxuICAgIGltZyB7XHJcbiAgICAgIHdpZHRoOiA1MHB4O1xyXG4gICAgfVxyXG4gIH1cclxuICBcclxuICA6Oi13ZWJraXQtaW5wdXQtcGxhY2Vob2xkZXIgeyAvKiBFZGdlICovXHJcbiAgICBjb2xvcjogcmVkO1xyXG4gIH1cclxuICBcclxuICA6LW1zLWlucHV0LXBsYWNlaG9sZGVyIHsgLyogSW50ZXJuZXQgRXhwbG9yZXIgMTAtMTEgKi9cclxuICAgIGNvbG9yOiByZWQ7XHJcbiAgfVxyXG4gIFxyXG4gIDo6cGxhY2Vob2xkZXIge1xyXG4gICAgY29sb3I6IHJlZCAhaW1wb3J0YW50O1xyXG4gIH1cclxuICBzbWFsbCAge1xyXG4gICAgbWFyZ2luLWxlZnQ6IDEzMHB4O1xyXG4gIH0iXX0= */"

/***/ }),

/***/ "./src/app/saller/saller.component.scss":
/*!**********************************************!*\
  !*** ./src/app/saller/saller.component.scss ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".btn[class*=\"btn-\"] {\n  margin-bottom: 0rem; }\n\n.justify-content-center {\n  display: flex !important;\n  justify-content: center !important; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2FsbGVyL0M6XFxkZXZlbG9wZXJcXGJhY2tlbmQvc3JjXFxhcHBcXHNhbGxlclxcc2FsbGVyLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsbUJBQW1CLEVBQUE7O0FBRXJCO0VBQ0Usd0JBQXdCO0VBQ3hCLGtDQUFrQyxFQUFBIiwiZmlsZSI6InNyYy9hcHAvc2FsbGVyL3NhbGxlci5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5idG5bY2xhc3MqPVwiYnRuLVwiXSB7XHJcbiAgbWFyZ2luLWJvdHRvbTogMHJlbTtcclxufVxyXG4uanVzdGlmeS1jb250ZW50LWNlbnRlciB7XHJcbiAgZGlzcGxheTogZmxleCAhaW1wb3J0YW50O1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyICFpbXBvcnRhbnQ7XHJcbn0iXX0= */"

/***/ }),

/***/ "./src/app/saller/saller.component.ts":
/*!********************************************!*\
  !*** ./src/app/saller/saller.component.ts ***!
  \********************************************/
/*! exports provided: viewModalContentSaller, NgbdModalContentSaller, SallerComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "viewModalContentSaller", function() { return viewModalContentSaller; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NgbdModalContentSaller", function() { return NgbdModalContentSaller; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SallerComponent", function() { return SallerComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var app_webapi_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! app/webapi.service */ "./src/app/webapi.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _angular_fire_database__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/fire/database */ "./node_modules/@angular/fire/database/index.js");










// import { race } from 'core-js/fn/promise';
var viewModalContentSaller = /** @class */ (function () {
    // sallerForm: FormGroup;
    function viewModalContentSaller(activeModal, api, firebase) {
        this.activeModal = activeModal;
        this.api = api;
        this.firebase = firebase;
        this.verify = {
            type1: [],
            type2: [],
            type3: []
        };
    }
    viewModalContentSaller.prototype.close = function () {
        this.activeModal.dismiss();
    };
    viewModalContentSaller.prototype.approve = function (member_id) {
        var _this = this;
        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
            title: 'คุณต้องการอนุมัติผู้ใช้นี้จริงหรือไม่',
            text: "กรุณายืนยันเมื่อทำการตรวจสอบสำเร็จแล้วเท่านั้น",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#0CC27E',
            cancelButtonColor: '#FF586B',
            confirmButtonText: 'อนุมัติ',
            cancelButtonText: 'ปิดออก',
            confirmButtonClass: 'btn btn-success btn-raised mr-2',
            cancelButtonClass: 'btn btn-default btn-raised',
            buttonsStyling: false,
        })
            .then(function (result) {
            if (result.value) {
                _this.api.getData('saller/approve/' + member_id).then(function (res) {
                    _this.api.fb_set('member');
                    _this.get_verify(_this.para.member_id);
                    _this.activeModal.dismiss();
                    sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire('อนุมัติแล้ว', '', 'success');
                });
            }
        });
        // this.activeModal.dismiss();
    };
    viewModalContentSaller.prototype.get_verify = function (sale_id) {
        var _this = this;
        this.api.getData('saller/get_verify/' + sale_id).then(function (res) {
            _this.verify = res;
            console.log(res);
        });
    };
    viewModalContentSaller.prototype.pass = function (mv_id) {
        var _this = this;
        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
            title: 'คุณต้องการยืนยันรายการนี้จริงหรือไม่',
            text: "กรุณายืนยันเมื่อทำการตรวจสอบสำเร็จแล้วเท่านั้น",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#0CC27E',
            cancelButtonColor: '#FF586B',
            confirmButtonText: 'ยืนยัน',
            cancelButtonText: 'ปิดออก',
            confirmButtonClass: 'btn btn-success btn-raised mr-2',
            cancelButtonClass: 'btn btn-default btn-raised',
            buttonsStyling: false,
        })
            .then(function (result) {
            if (result.value) {
                _this.api.getData('saller/verify/' + mv_id).then(function (res) {
                    console.log('OK');
                    // this.firebase.database.ref('order').set(Math.round(Math.random() * 100));
                    _this.get_verify(_this.para.member_id);
                    sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire('ยืนยันแล้ว', '', 'success');
                });
            }
        });
    };
    viewModalContentSaller.prototype.eject = function (mv_id) {
        var _this = this;
        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
            title: 'โปรดระบุเหตุผลในการยกเลิกเอกสารนี้',
            input: 'text',
            inputAttributes: {
                autocapitalize: 'off'
            },
            showCancelButton: true,
            confirmButtonText: 'ยืนยัน',
            cancelButtonText: 'ปิดออก',
            showLoaderOnConfirm: false,
            preConfirm: function (login) {
                console.log(login);
                if (!login) {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.showValidationMessage("Request failed");
                }
                // return fetch(`//api.github.com/users/${login}`)
                //   .then(response => {
                //     if (!response.ok) {
                //       throw new Error(response.statusText)
                //     }
                //     return response.json()
                //   })
                //   .catch(error => {
                //     Swal.showValidationMessage(
                //       `Request failed: ${error}`
                //     )
                //   })
            },
            allowOutsideClick: function () { return !sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.isLoading(); }
        }).then(function (result) {
            if (result.value) {
                console.log(result.value);
                _this.api.postData('saller/eject', { mv_id: mv_id, text: result.value }).then(function (res) {
                    // return fetch(`//api.github.com/users/${login}`)
                    //   .then(response => {
                    // if (!response.ok) {
                    //   throw new Error(response.statusText)
                    // }
                    // return response.json()
                    _this.get_verify(_this.para.member_id);
                    sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire('สำเร็จแล้ว', '', 'success');
                });
            }
        });
    };
    viewModalContentSaller.prototype.ngOnInit = function () {
        this.get_verify(this.para.member_id);
        // console.log(this.para);
        // this.sallerForm = new FormGroup({
        //   'email': new FormControl(null, [Validators.required, Validators.email]),
        //   'username': new FormControl(null, [Validators.required, Validators.minLength(4), Validators.maxLength(24)]),
        //   'password': new FormControl(null, [Validators.required, Validators.minLength(4), Validators.maxLength(24)]),
        //   'name': new FormControl(null, [Validators.required, Validators.email, Validators.minLength(4)]),
        //   'phone': new FormControl(null, [Validators.required, Validators.email, Validators.minLength(9), Validators.minLength(10)]),
        // }, { updateOn: 'blur' });
        // this.sallerForm.patchValue({username:'xxx'});
    };
    viewModalContentSaller.prototype.c = function (val) {
        console.log(val);
    };
    viewModalContentSaller.ctorParameters = function () { return [
        { type: _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbActiveModal"] },
        { type: app_webapi_service__WEBPACK_IMPORTED_MODULE_5__["WebapiService"] },
        { type: _angular_fire_database__WEBPACK_IMPORTED_MODULE_8__["AngularFireDatabase"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], viewModalContentSaller.prototype, "para", void 0);
    viewModalContentSaller = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'view-modal-content',
            template: __webpack_require__(/*! raw-loader!./view_saller.html */ "./node_modules/raw-loader/index.js!./src/app/saller/view_saller.html"),
            styles: [__webpack_require__(/*! ./view_saller.scss */ "./src/app/saller/view_saller.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbActiveModal"],
            app_webapi_service__WEBPACK_IMPORTED_MODULE_5__["WebapiService"],
            _angular_fire_database__WEBPACK_IMPORTED_MODULE_8__["AngularFireDatabase"]])
    ], viewModalContentSaller);
    return viewModalContentSaller;
}());

var NgbdModalContentSaller = /** @class */ (function () {
    function NgbdModalContentSaller(activeModal, fb, cd, toastr, http, api) {
        this.activeModal = activeModal;
        this.fb = fb;
        this.cd = cd;
        this.toastr = toastr;
        this.http = http;
        this.api = api;
    }
    NgbdModalContentSaller.prototype.file_change = function (event) {
        var _this = this;
        var reader = new FileReader();
        if (event.target.files && event.target.files.length) {
            var file = event.target.files[0];
            reader.readAsDataURL(file);
            reader.onload = function () {
                console.log(reader.result);
                _this.para.picture = reader.result;
                var selectedFile = event.target.files[0];
                var uploadData = new FormData();
                uploadData.append('photo', selectedFile, selectedFile.name);
                _this.http.post(_this.api.base_url + 'saller/fileupload', uploadData, {
                    reportProgress: true,
                    observe: 'events'
                })
                    .subscribe(function (e) {
                    console.log(e);
                });
                //   this.formGroup.patchValue({
                //     file: reader.result
                //  });
                // need to run CD since file load runs outside of zone
                _this.cd.markForCheck();
            };
        }
    };
    NgbdModalContentSaller.prototype.change_picture = function () {
        console.log('changepic');
        this.file.nativeElement.click();
    };
    NgbdModalContentSaller.prototype.onReactiveFormSubmit = function () {
        var _this = this;
        // console.log(this.sallerForm.value);
        var data = this.sallerForm.value;
        data.t_id = this.para.t_id;
        this.api.postData('saller/add_saller', this.sallerForm.value).then(function (res) {
            console.log(res);
            if (res == '1') {
                _this.toastr.success('บันทึกเรียบรอ้แล้ว');
                _this.activeModal.close();
            }
            else {
                _this.toastr.error('มีปัญหาในการบันทึกข้อมูล');
            }
        });
        console.log("summit");
    };
    NgbdModalContentSaller.prototype.close = function () {
        this.activeModal.dismiss();
    };
    NgbdModalContentSaller.prototype.ngOnInit = function () {
        console.log("xxxx");
        console.log(this.para);
        if (this.para.t_id == '0') {
            this.para = {
                t_id: "0", username: "", password: "", name: "", email: '', phone: "",
                gender: "m",
                picture: "https://image.flaticon.com/icons/svg/194/194938.svg",
                type: "1"
            };
            this.sallerForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
                'username': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(6), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(24), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[a-zA-Z0-9._-]+$')]),
                'password': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(6), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(24)]),
                'name': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(6)]),
                'email': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].email]),
                'phone': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(9), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(10), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('[0-9]+')]),
                gender: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
                type: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required])
            }, { updateOn: 'blur' });
        }
        else {
            this.sallerForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
                'username': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(6), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(24), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[a-zA-Z0-9._-]+$')]),
                'password': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(6), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(24)]),
                'name': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(6)]),
                'email': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].email]),
                'phone': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(9), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(10), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('[0-9]+')]),
                gender: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
                type: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required])
            }, { updateOn: 'blur' });
        }
        this.sallerForm.patchValue(this.para);
        this.sallerForm.patchValue({ password: '' });
    };
    NgbdModalContentSaller.prototype.c = function (val) {
        console.log(val);
    };
    NgbdModalContentSaller.ctorParameters = function () { return [
        { type: _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbActiveModal"] },
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["ChangeDetectorRef"] },
        { type: ngx_toastr__WEBPACK_IMPORTED_MODULE_7__["ToastrService"] },
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpClient"] },
        { type: app_webapi_service__WEBPACK_IMPORTED_MODULE_5__["WebapiService"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], NgbdModalContentSaller.prototype, "para", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["ViewChild"])("file", { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_2__["ElementRef"])
    ], NgbdModalContentSaller.prototype, "file", void 0);
    NgbdModalContentSaller = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'ngbd-modal-content',
            template: __webpack_require__(/*! raw-loader!./add_saller.html */ "./node_modules/raw-loader/index.js!./src/app/saller/add_saller.html"),
            styles: [__webpack_require__(/*! ./add_saller.scss */ "./src/app/saller/add_saller.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbActiveModal"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"], _angular_core__WEBPACK_IMPORTED_MODULE_2__["ChangeDetectorRef"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_7__["ToastrService"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpClient"], app_webapi_service__WEBPACK_IMPORTED_MODULE_5__["WebapiService"]])
    ], NgbdModalContentSaller);
    return NgbdModalContentSaller;
}());

var SallerComponent = /** @class */ (function () {
    function SallerComponent(modalService, api, firebase) {
        this.modalService = modalService;
        this.api = api;
        this.firebase = firebase;
        this.sallers = [];
        this.all_page = 0;
        this.page = 1;
        this.fb = false;
        this.sallers = this.api.storage_get("storage") || [];
    }
    SallerComponent.prototype.onReactiveFormSubmit = function () {
        var a = this.sallerForm.value();
        console.log(a);
    };
    SallerComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.fb = true;
        this.api.check_login();
        this.searchForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            'search': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('')
        });
        this.sallerForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            'inputEmail': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].email]),
            'password': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(4), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(24)]),
            'textArea': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            'radioOption': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('Option one is this')
        }, { updateOn: 'blur' });
        this.load_data(1);
        this.api.fb_val('member', function (res) {
            if (_this.fb) {
                _this.load_data(1);
            }
        });
    };
    SallerComponent.prototype.load_data = function (page) {
        var _this = this;
        this.api.getData('saller/load_saller/' + page + "?search=" + this.searchForm.value.search).then(function (data) {
            console.log(data);
            _this.sallers = data.data;
            _this.all_page = data.count / 20 * 20;
            _this.api.storage_set('saller', data.data);
        });
    };
    SallerComponent.prototype.view = function (t) {
        var _this = this;
        console.log(t);
        var modal = this.modalService.open(viewModalContentSaller, { size: 'lg' });
        modal.componentInstance.para = t;
        modal.result.then(function (result) {
            console.log(result);
        }, function (reason) {
        });
        modal.result.then(function (result) {
            _this.closeResult = "Closed with: " + result;
        }, function (reason) {
            _this.closeResult = "Dismissed " + _this.getDismissReason(reason);
        });
    };
    SallerComponent.prototype.search = function () {
        console.log(this.searchForm.value);
        this.load_data(1);
    };
    SallerComponent.prototype.change_page = function (page) {
        this.load_data(page);
    };
    SallerComponent.prototype.view_profile = function (t) {
        this.view(t);
    };
    SallerComponent.prototype.open = function (t) {
        var _this = this;
        console.log(t);
        var modal = this.modalService.open(NgbdModalContentSaller);
        modal.componentInstance.para = t;
        modal.result.then(function (result) {
            _this.load_data(_this.page);
        }, function (reason) {
            console.log(reason);
        });
        modal.result.then(function (result) {
            _this.closeResult = "Closed with: " + result;
        }, function (reason) {
            _this.closeResult = "Dismissed " + _this.getDismissReason(reason);
        });
    };
    SallerComponent.prototype.delete_member = function () {
        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
            title: 'คุณต้องการลบจริงหรือไม่',
            text: "รายการนี้จะไม่สามารถนำกลับมาได้อีก",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#0CC27E',
            cancelButtonColor: '#FF586B',
            confirmButtonText: 'ลบออก',
            cancelButtonText: 'ปิดออก',
            confirmButtonClass: 'btn btn-success btn-raised mr-2',
            cancelButtonClass: 'btn btn-default btn-raised',
            buttonsStyling: false
        }).then(function (result) {
            console.log(result);
            if (result.value) {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire('ลบเรียบร้อยแล้ว', '', 'success');
            }
        });
    };
    SallerComponent.prototype.getDismissReason = function (reason) {
        if (reason === _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["ModalDismissReasons"].ESC) {
            return 'by pressing ESC';
        }
        else if (reason === _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["ModalDismissReasons"].BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        }
        else {
            return "with: " + reason;
        }
    };
    SallerComponent.prototype.ngOnDestroy = function () {
        this.fb = false;
    };
    SallerComponent.ctorParameters = function () { return [
        { type: _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbModal"] },
        { type: app_webapi_service__WEBPACK_IMPORTED_MODULE_5__["WebapiService"] },
        { type: _angular_fire_database__WEBPACK_IMPORTED_MODULE_8__["AngularFireDatabase"] }
    ]; };
    SallerComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-saller',
            template: __webpack_require__(/*! raw-loader!./saller.component.html */ "./node_modules/raw-loader/index.js!./src/app/saller/saller.component.html"),
            styles: [__webpack_require__(/*! ./saller.component.scss */ "./src/app/saller/saller.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbModal"],
            app_webapi_service__WEBPACK_IMPORTED_MODULE_5__["WebapiService"],
            _angular_fire_database__WEBPACK_IMPORTED_MODULE_8__["AngularFireDatabase"]])
    ], SallerComponent);
    return SallerComponent;
}());



/***/ }),

/***/ "./src/app/saller/view_saller.scss":
/*!*****************************************!*\
  !*** ./src/app/saller/view_saller.scss ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".content {\n  border: #fff solid 2px;\n  border-radius: 20px;\n  margin: 15px 5px;\n  padding: 20px; }\n\ndl {\n  padding-bottom: 0px; }\n\ndl dt {\n    font-weight: bold;\n    width: 120px;\n    text-align: right;\n    display: inline-block; }\n\ndl dt::after {\n    content: \":\"; }\n\ndl dd {\n    display: inline-block;\n    width: calc(100% - 120px);\n    color: #333333;\n    padding-left: 15px; }\n\n.eject {\n  color: gray; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2FsbGVyL0M6XFxkZXZlbG9wZXJcXGJhY2tlbmQvc3JjXFxhcHBcXHNhbGxlclxcdmlld19zYWxsZXIuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLHNCQUFzQjtFQUN0QixtQkFBbUI7RUFDbkIsZ0JBQWdCO0VBQ2hCLGFBQWEsRUFBQTs7QUFHZjtFQWlCRSxtQkFBbUIsRUFBQTs7QUFqQnJCO0lBR0ksaUJBQWlCO0lBQ2pCLFlBQVk7SUFDWixpQkFBaUI7SUFDakIscUJBQXFCLEVBQUE7O0FBTnpCO0lBU0ksWUFBWSxFQUFBOztBQVRoQjtJQVlJLHFCQUFxQjtJQUNyQix5QkFBeUI7SUFDekIsY0FBYztJQUNkLGtCQUFrQixFQUFBOztBQUl0QjtFQUNFLFdBQVUsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3NhbGxlci92aWV3X3NhbGxlci5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmNvbnRlbnQge1xyXG4gIGJvcmRlcjogI2ZmZiBzb2xpZCAycHg7XHJcbiAgYm9yZGVyLXJhZGl1czogMjBweDtcclxuICBtYXJnaW46IDE1cHggNXB4O1xyXG4gIHBhZGRpbmc6IDIwcHg7XHJcbn1cclxuXHJcbmRsIHtcclxuICBkdCB7XHJcbiAgICAgIFxyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICB3aWR0aDogMTIwcHg7XHJcbiAgICB0ZXh0LWFsaWduOiByaWdodDtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICB9XHJcbiAgZHQ6OmFmdGVyIHtcclxuICAgIGNvbnRlbnQ6IFwiOlwiO1xyXG4gIH1cclxuICBkZCB7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICB3aWR0aDogY2FsYygxMDAlIC0gMTIwcHgpO1xyXG4gICAgY29sb3I6ICMzMzMzMzM7XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDE1cHg7XHJcbiAgfVxyXG4gIHBhZGRpbmctYm90dG9tOiAwcHg7XHJcbn1cclxuLmVqZWN0e1xyXG4gIGNvbG9yOmdyYXk7XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/score/score.component.scss":
/*!********************************************!*\
  !*** ./src/app/score/score.component.scss ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".btn[class*=\"btn-\"] {\n  margin-bottom: 0rem; }\n\n.justify-content-center {\n  display: flex !important;\n  justify-content: center !important; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2NvcmUvQzpcXGRldmVsb3BlclxcYmFja2VuZC9zcmNcXGFwcFxcc2NvcmVcXHNjb3JlLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksbUJBQW1CLEVBQUE7O0FBRXJCO0VBQ0Usd0JBQXdCO0VBQ3hCLGtDQUFrQyxFQUFBIiwiZmlsZSI6InNyYy9hcHAvc2NvcmUvc2NvcmUuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuYnRuW2NsYXNzKj1cImJ0bi1cIl0ge1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMHJlbTtcclxuICB9XHJcbiAgLmp1c3RpZnktY29udGVudC1jZW50ZXIge1xyXG4gICAgZGlzcGxheTogZmxleCAhaW1wb3J0YW50O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXIgIWltcG9ydGFudDtcclxuICB9Il19 */"

/***/ }),

/***/ "./src/app/score/score.component.ts":
/*!******************************************!*\
  !*** ./src/app/score/score.component.ts ***!
  \******************************************/
/*! exports provided: ScoreComponent, viewModalScore */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ScoreComponent", function() { return ScoreComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "viewModalScore", function() { return viewModalScore; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var app_webapi_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! app/webapi.service */ "./src/app/webapi.service.ts");
/* harmony import */ var _angular_fire_database__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/fire/database */ "./node_modules/@angular/fire/database/index.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var app_confirm_confirm_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! app/confirm/confirm.component */ "./src/app/confirm/confirm.component.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_7__);








var ScoreComponent = /** @class */ (function () {
    function ScoreComponent(api, modalService, firebase) {
        this.api = api;
        this.modalService = modalService;
        this.firebase = firebase;
        this.all_page = 0;
        this.fb = false;
        this.buff = {
            bb_id: '',
            order_id: ''
        };
        this.admin = { name: '' };
        this.score = { member_id: '' };
        this.imagePath = '';
        this.url = '';
        this.formData = new FormData();
    }
    ScoreComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.searchForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormGroup"]({
            'search': new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"]('')
        });
        this.admin = this.api.storage_get('logindata');
        this.fb = true;
        this.api.fb_val('order', function (res) {
            if (_this.fb) {
                _this.load_bank(1);
                _this.load_score(1);
            }
        });
    };
    ScoreComponent.prototype.load_bank = function (page) {
        var _this = this;
        this.api.getData('Confirm/get_salebank/' + page + "?search=" + this.searchForm.value.search).then(function (res) {
            _this.order = res.order;
            console.log(res);
        });
    };
    ScoreComponent.prototype.load_score = function (page) {
        var _this = this;
        this.api.getData('Score/expScore/' + page + "?search=" + this.searchForm.value.search).then(function (res) {
            _this.score = res;
            console.log(res);
        });
    };
    ScoreComponent.prototype.load_confirm = function (page) {
        var _this = this;
        this.api.getData('Confirm/load_confirm/' + page + "?search=" + this.searchForm.value.search).then(function (res) {
            console.log(res);
            _this.data = res.data;
            _this.all_page = res.count / 20 * 20;
        });
    };
    ScoreComponent.prototype.change_page = function ($event) {
        // console.log($event);
    };
    ScoreComponent.prototype.search = function () {
        this.load_confirm(1);
    };
    ScoreComponent.prototype.test = function (t) {
        var _this = this;
        console.log(t);
        var modal = this.modalService.open(app_confirm_confirm_component__WEBPACK_IMPORTED_MODULE_6__["viewModalConfirm"], { size: 'lg' });
        modal.componentInstance.payment = t;
        modal.result.then(function (result) {
            console.log(result);
        }, function (reason) {
        });
        modal.result.then(function (result) {
            _this.closeResult = "Closed with: " + result;
        }, function (reason) {
            _this.closeResult = "Dismissed " + _this.getDismissReason(reason);
            _this.load_bank(1);
            _this.load_score(1);
        });
    };
    ScoreComponent.prototype.getDismissReason = function (reason) {
        // this.load_paymant(1)
        if (reason === _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__["ModalDismissReasons"].ESC) {
            return 'by pressing ESC';
        }
        else if (reason === _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__["ModalDismissReasons"].BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        }
        else {
            return "with: " + reason;
        }
    };
    ScoreComponent.prototype.sbank = function (bb_id, order_id) {
        console.log(bb_id + '/' + order_id);
        this.buff.bb_id = bb_id;
        this.buff.order_id = order_id;
        // console.log('asasdasdasfsjdhgfskjdf');
    };
    ScoreComponent.prototype.confirm = function (order) {
        var _this = this;
        if (this.buff.order_id == order.order_id) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_7___default.a.fire({
                title: 'คุณต้องการยืนยันรายการนี้จริงหรือไม่',
                text: "กรุณายืนยันเมื่อทำการโอนเงินสำเร็จแล้วเท่านั้น",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#0CC27E',
                cancelButtonColor: '#FF586B',
                confirmButtonText: 'ยืนยัน',
                cancelButtonText: 'ปิดออก',
                confirmButtonClass: 'btn btn-success btn-raised mr-2',
                cancelButtonClass: 'btn btn-default btn-raised',
                buttonsStyling: false
            }).then(function (result) {
                if (result.value) {
                    _this.api.postData('Confirm/confirm_sale', { order_id: order.order_id, bb_id: _this.buff.bb_id, admin: _this.admin.t_id, price: order.com_price }).then(function (res) {
                        console.log('OK');
                        _this.api.fb_set('order');
                        _this.load_bank(1);
                        sweetalert2__WEBPACK_IMPORTED_MODULE_7___default.a.fire('ยืนยันแล้ว', '', 'success');
                    });
                }
            });
        }
        else {
            console.log('Select bank again');
            sweetalert2__WEBPACK_IMPORTED_MODULE_7___default.a.fire('กรุณาเลือกบัญชีธนาคารก่อน', '', 'warning');
        }
    };
    ScoreComponent.prototype.changeImg = function (event, member_id) {
        var _this = this;
        var data = {
            member_id: '',
            message: ''
        };
        console.log(event);
        this.formData.append('avatar', event.target.files[0]);
        this.reader = new FileReader();
        this.reader.onload = function (event) {
            _this.url = event.target.result;
        };
        this.reader.readAsDataURL(event.target.files[0]);
        this.type = event.target.files[0].type;
        console.log(this.reader.result);
        setTimeout(function () {
            console.log(member_id);
            // this.api.postData("Confirm/upload", { img: this.reader.result, type: this.type, member_id:member_id }).then((res: any) => {
            // })
        }, 1000);
    };
    ScoreComponent.prototype.continue = function (data) {
        var _this = this;
        var modal = this.modalService.open(viewModalScore, { size: 'lg' });
        modal.componentInstance.data = data;
        modal.result.then(function (result) {
            console.log(result);
        }, function (reason) {
        });
        modal.result.then(function (result) {
            _this.closeResult = "Closed with: " + result;
        }, function (reason) {
            _this.closeResult = "Dismissed " + _this.getDismissReason(reason);
        });
    };
    ScoreComponent.prototype.ngOnDestroy = function () {
        this.fb = false;
    };
    ScoreComponent.ctorParameters = function () { return [
        { type: app_webapi_service__WEBPACK_IMPORTED_MODULE_2__["WebapiService"] },
        { type: _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__["NgbModal"] },
        { type: _angular_fire_database__WEBPACK_IMPORTED_MODULE_3__["AngularFireDatabase"] }
    ]; };
    ScoreComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-score',
            template: __webpack_require__(/*! raw-loader!./score.component.html */ "./node_modules/raw-loader/index.js!./src/app/score/score.component.html"),
            styles: [__webpack_require__(/*! ./score.component.scss */ "./src/app/score/score.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [app_webapi_service__WEBPACK_IMPORTED_MODULE_2__["WebapiService"], _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__["NgbModal"], _angular_fire_database__WEBPACK_IMPORTED_MODULE_3__["AngularFireDatabase"]])
    ], ScoreComponent);
    return ScoreComponent;
}());

var viewModalScore = /** @class */ (function () {
    function viewModalScore(activeModal, api, firebase, ref) {
        this.activeModal = activeModal;
        this.api = api;
        this.firebase = firebase;
        this.ref = ref;
        this.imagePath = '';
        this.url = '';
        this.formData = new FormData();
        this.bb_id = '';
    }
    viewModalScore.prototype.close = function () {
        this.activeModal.dismiss(this.data);
    };
    viewModalScore.prototype.ngOnInit = function () {
    };
    viewModalScore.prototype.changeImg = function (event) {
        var _this = this;
        var data = {
            member_id: '',
            message: ''
        };
        console.log(event);
        this.formData.append('avatar', event.target.files[0]);
        this.reader = new FileReader();
        this.reader.onload = function (event) {
            _this.url = event.target.result;
            _this.ref.detectChanges();
        };
        this.reader.readAsDataURL(event.target.files[0]);
        this.type = event.target.files[0].type;
        console.log(this.reader.result);
        setTimeout(function () {
            // this.api.postData("Confirm/upload", { img: this.reader.result, type: this.type, member_id:member_id }).then((res: any) => {
            // })
        }, 1000);
    };
    viewModalScore.prototype.sbank = function (bb_id) {
        this.bb_id = bb_id;
        console.log(bb_id);
    };
    viewModalScore.prototype.confirm = function () {
        var _this = this;
        if (this.bb_id != '') {
            this.api.postData("Score/upload", { img: this.reader.result, type: this.type, bb_id: this.bb_id, price: this.data.exp, score_id: this.data.score_id }).then(function (res) {
                _this.api.fb_set('order');
                sweetalert2__WEBPACK_IMPORTED_MODULE_7___default.a.fire('สำเร็จ!', '', 'success');
                setTimeout(function () {
                    _this.close();
                }, 1000);
            });
        }
        else {
            sweetalert2__WEBPACK_IMPORTED_MODULE_7___default.a.fire('กรุณาเลือกบัญชีธนาคารก่อน', '', 'warning');
        }
    };
    viewModalScore.ctorParameters = function () { return [
        { type: _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__["NgbActiveModal"] },
        { type: app_webapi_service__WEBPACK_IMPORTED_MODULE_2__["WebapiService"] },
        { type: _angular_fire_database__WEBPACK_IMPORTED_MODULE_3__["AngularFireDatabase"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], viewModalScore.prototype, "data", void 0);
    viewModalScore = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'view-modal-content',
            template: __webpack_require__(/*! raw-loader!./score_details.html */ "./node_modules/raw-loader/index.js!./src/app/score/score_details.html"),
            styles: [__webpack_require__(/*! ./score_details.scss */ "./src/app/score/score_details.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__["NgbActiveModal"],
            app_webapi_service__WEBPACK_IMPORTED_MODULE_2__["WebapiService"],
            _angular_fire_database__WEBPACK_IMPORTED_MODULE_3__["AngularFireDatabase"],
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"]])
    ], viewModalScore);
    return viewModalScore;
}());



/***/ }),

/***/ "./src/app/score/score_details.scss":
/*!******************************************!*\
  !*** ./src/app/score/score_details.scss ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3Njb3JlL3Njb3JlX2RldGFpbHMuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/settings/add_bank.scss":
/*!****************************************!*\
  !*** ./src/app/settings/add_bank.scss ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "body.layout-dark .modal .modal-header {\n  background: #374fe6; }\n\n.avatar {\n  width: 300px;\n  border: solid 3px #fff;\n  border-radius: 20px; }\n\noption {\n  background-color: #fff; }\n\nlabel {\n  color: #000;\n  display: inline-block;\n  padding-right: 7px;\n  font-size: 16px;\n  width: 130px; }\n\n.form-control {\n  width: calc(100% - 130px);\n  display: inline-block; }\n\n.newF {\n  width: calc(100% - 180px);\n  display: inline-block; }\n\n.newT {\n  width: calc(65% - 130px);\n  display: inline-block; }\n\n.change_img {\n  width: 26px;\n  height: 26px;\n  font-size: 13px;\n  text-align: center;\n  border-radius: 50%;\n  position: absolute;\n  left: calc(50% - 13px);\n  top: 82px;\n  padding-left: 1px;\n  padding-top: 3px;\n  background-color: black; }\n\ntd img {\n  width: 50px; }\n\n::-webkit-input-placeholder {\n  /* Edge */\n  color: red; }\n\n:-ms-input-placeholder {\n  /* Internet Explorer 10-11 */\n  color: red; }\n\n::-moz-placeholder {\n  color: red !important; }\n\n::-ms-input-placeholder {\n  color: red !important; }\n\n::placeholder {\n  color: red !important; }\n\nsmall {\n  margin-left: 130px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2V0dGluZ3MvQzpcXGRldmVsb3BlclxcYmFja2VuZC9zcmNcXGFwcFxcc2V0dGluZ3NcXGFkZF9iYW5rLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0U7RUFDRSxtQkFBbUIsRUFBQTs7QUFFckI7RUFDRSxZQUFZO0VBQ1osc0JBQXNCO0VBQ3RCLG1CQUFtQixFQUFBOztBQUVyQjtFQUNFLHNCQUFzQixFQUFBOztBQUV2QjtFQUNDLFdBQVc7RUFDWCxxQkFBcUI7RUFDckIsa0JBQWtCO0VBQ2xCLGVBQWU7RUFDZixZQUFZLEVBQUE7O0FBRWQ7RUFDRSx5QkFBeUI7RUFDekIscUJBQXFCLEVBQUE7O0FBR3ZCO0VBQ0UseUJBQXlCO0VBQ3pCLHFCQUFxQixFQUFBOztBQUV2QjtFQUNFLHdCQUF3QjtFQUN4QixxQkFBcUIsRUFBQTs7QUFFdkI7RUFDRSxXQUFXO0VBQ1gsWUFBWTtFQUNaLGVBQWU7RUFDZixrQkFBa0I7RUFDbEIsa0JBQWtCO0VBQ2xCLGtCQUFrQjtFQUNsQixzQkFBc0I7RUFDdEIsU0FBUztFQUNULGlCQUFpQjtFQUNqQixnQkFBZ0I7RUFDaEIsdUJBQXVCLEVBQUE7O0FBRXpCO0VBRUksV0FBVyxFQUFBOztBQUlmO0VBQThCLFNBQUE7RUFDNUIsVUFBVSxFQUFBOztBQUdaO0VBQXlCLDRCQUFBO0VBQ3ZCLFVBQVUsRUFBQTs7QUFHWjtFQUNFLHFCQUFxQixFQUFBOztBQUR2QjtFQUNFLHFCQUFxQixFQUFBOztBQUR2QjtFQUNFLHFCQUFxQixFQUFBOztBQUV2QjtFQUNFLGtCQUFrQixFQUFBIiwiZmlsZSI6InNyYy9hcHAvc2V0dGluZ3MvYWRkX2Jhbmsuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIlxyXG4gIGJvZHkubGF5b3V0LWRhcmsgLm1vZGFsIC5tb2RhbC1oZWFkZXIge1xyXG4gICAgYmFja2dyb3VuZDogIzM3NGZlNjtcclxuICB9XHJcbiAgLmF2YXRhciB7XHJcbiAgICB3aWR0aDogMzAwcHg7XHJcbiAgICBib3JkZXI6IHNvbGlkIDNweCAjZmZmO1xyXG4gICAgYm9yZGVyLXJhZGl1czogMjBweDtcclxuICB9XHJcbiAgb3B0aW9uIHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XHJcbiAgfVxyXG4gICBsYWJlbCB7XHJcbiAgICBjb2xvcjogIzAwMDtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgIHBhZGRpbmctcmlnaHQ6IDdweDtcclxuICAgIGZvbnQtc2l6ZTogMTZweDtcclxuICAgIHdpZHRoOiAxMzBweDtcclxuICB9XHJcbiAgLmZvcm0tY29udHJvbCB7XHJcbiAgICB3aWR0aDogY2FsYygxMDAlIC0gMTMwcHgpO1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gIH1cclxuXHJcbiAgLm5ld0Yge1xyXG4gICAgd2lkdGg6IGNhbGMoMTAwJSAtIDE4MHB4KTtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICB9XHJcbiAgLm5ld1Qge1xyXG4gICAgd2lkdGg6IGNhbGMoNjUlIC0gMTMwcHgpO1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gIH1cclxuICAuY2hhbmdlX2ltZyB7XHJcbiAgICB3aWR0aDogMjZweDtcclxuICAgIGhlaWdodDogMjZweDtcclxuICAgIGZvbnQtc2l6ZTogMTNweDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGxlZnQ6IGNhbGMoNTAlIC0gMTNweCk7XHJcbiAgICB0b3A6IDgycHg7XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDFweDtcclxuICAgIHBhZGRpbmctdG9wOiAzcHg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiBibGFjaztcclxuICB9XHJcbiAgdGQge1xyXG4gICAgaW1nIHtcclxuICAgICAgd2lkdGg6IDUwcHg7XHJcbiAgICB9XHJcbiAgfVxyXG4gIFxyXG4gIDo6LXdlYmtpdC1pbnB1dC1wbGFjZWhvbGRlciB7IC8qIEVkZ2UgKi9cclxuICAgIGNvbG9yOiByZWQ7XHJcbiAgfVxyXG4gIFxyXG4gIDotbXMtaW5wdXQtcGxhY2Vob2xkZXIgeyAvKiBJbnRlcm5ldCBFeHBsb3JlciAxMC0xMSAqL1xyXG4gICAgY29sb3I6IHJlZDtcclxuICB9XHJcbiAgXHJcbiAgOjpwbGFjZWhvbGRlciB7XHJcbiAgICBjb2xvcjogcmVkICFpbXBvcnRhbnQ7XHJcbiAgfVxyXG4gIHNtYWxsICB7XHJcbiAgICBtYXJnaW4tbGVmdDogMTMwcHg7XHJcbiAgfSJdfQ== */"

/***/ }),

/***/ "./src/app/settings/settings.component.scss":
/*!**************************************************!*\
  !*** ./src/app/settings/settings.component.scss ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".btn[class*=\"btn-\"] {\n  margin-bottom: 0rem; }\n\n.go-right {\n  position: absolute;\n  right: 30px; }\n\n.under-line {\n  border-bottom: 2px solid;\n  padding-top: 10px; }\n\n.butt-right {\n  float: right; }\n\n.justify-content-center {\n  display: flex !important;\n  justify-content: center !important; }\n\n.searchform {\n  margin-right: 10px;\n  display: inline-block; }\n\n.form-control {\n  display: block;\n  width: 70%;\n  display: inline-block; }\n\nlabel {\n  color: #000;\n  display: inline-block; }\n\n.right {\n  width: 30%; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2V0dGluZ3MvQzpcXGRldmVsb3BlclxcYmFja2VuZC9zcmNcXGFwcFxcc2V0dGluZ3NcXHNldHRpbmdzLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsbUJBQW1CLEVBQUE7O0FBR3JCO0VBQ0ksa0JBQWtCO0VBQ2xCLFdBQVcsRUFBQTs7QUFFZjtFQUNFLHdCQUF3QjtFQUN4QixpQkFBaUIsRUFBQTs7QUFFbkI7RUFDRSxZQUFZLEVBQUE7O0FBRWQ7RUFDRSx3QkFBd0I7RUFDeEIsa0NBQWtDLEVBQUE7O0FBRXBDO0VBQ0Usa0JBQWtCO0VBQ2xCLHFCQUFxQixFQUFBOztBQUV2QjtFQUNFLGNBQWM7RUFDZCxVQUFVO0VBQ1YscUJBQXFCLEVBQUE7O0FBRXZCO0VBQ0UsV0FBVztFQUNYLHFCQUFxQixFQUFBOztBQUd2QjtFQUNFLFVBQVUsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3NldHRpbmdzL3NldHRpbmdzLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmJ0bltjbGFzcyo9XCJidG4tXCJdIHtcclxuICBtYXJnaW4tYm90dG9tOiAwcmVtO1xyXG5cclxufVxyXG4uZ28tcmlnaHQge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgcmlnaHQ6IDMwcHg7XHJcbn1cclxuLnVuZGVyLWxpbmV7XHJcbiAgYm9yZGVyLWJvdHRvbTogMnB4IHNvbGlkO1xyXG4gIHBhZGRpbmctdG9wOiAxMHB4O1xyXG59XHJcbi5idXR0LXJpZ2h0e1xyXG4gIGZsb2F0OiByaWdodDtcclxufVxyXG4uanVzdGlmeS1jb250ZW50LWNlbnRlciB7XHJcbiAgZGlzcGxheTogZmxleCAhaW1wb3J0YW50O1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyICFpbXBvcnRhbnQ7XHJcbn1cclxuLnNlYXJjaGZvcm0ge1xyXG4gIG1hcmdpbi1yaWdodDogMTBweDtcclxuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbn1cclxuLmZvcm0tY29udHJvbCB7XHJcbiAgZGlzcGxheTogYmxvY2s7XHJcbiAgd2lkdGg6IDcwJTtcclxuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbn1cclxubGFiZWwge1xyXG4gIGNvbG9yOiAjMDAwO1xyXG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuXHJcbn1cclxuLnJpZ2h0e1xyXG4gIHdpZHRoOiAzMCU7XHJcbn0iXX0= */"

/***/ }),

/***/ "./src/app/settings/settings.component.ts":
/*!************************************************!*\
  !*** ./src/app/settings/settings.component.ts ***!
  \************************************************/
/*! exports provided: NgbdModalBank, settingsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NgbdModalBank", function() { return NgbdModalBank; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "settingsComponent", function() { return settingsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var app_webapi_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! app/webapi.service */ "./src/app/webapi.service.ts");
/* harmony import */ var _ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ckeditor/ckeditor5-build-classic */ "./node_modules/@ckeditor/ckeditor5-build-classic/build/ckeditor.js");
/* harmony import */ var _ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _angular_fire_database__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/fire/database */ "./node_modules/@angular/fire/database/index.js");










var NgbdModalBank = /** @class */ (function () {
    function NgbdModalBank(activeModal, fb, cd, toastr, firebase, http, api) {
        this.activeModal = activeModal;
        this.fb = fb;
        this.cd = cd;
        this.toastr = toastr;
        this.firebase = firebase;
        this.http = http;
        this.api = api;
        this.admin = {
            t_id: '0'
        };
    }
    NgbdModalBank.prototype.ngOnInit = function () {
        console.log('add bank');
        this.admin = this.api.storage_get('logindata');
        console.log(this.b);
        if (this.b == '') {
            this.b = {
                bb_id: "0"
            };
            console.log(this.b);
            this.bankForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
                'no': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
                'name': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
                'bank': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
                'show': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](true),
            });
        }
        else {
            console.log(this.b);
            this.bankForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
                'no': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
                'name': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
                'bank': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
                'show': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.b.show),
            });
        }
        this.bankForm.patchValue(this.b);
    };
    NgbdModalBank.prototype.close = function () {
        this.activeModal.dismiss();
    };
    NgbdModalBank.prototype.onBankSubmit = function () {
        var _this = this;
        // console.log(this.productForm.value);
        var data = this.bankForm.value;
        data.bb_id = this.b.bb_id;
        data.admin = this.admin.t_id;
        console.log(data);
        this.api.postData('Settings/add_bank', data).then(function (res) {
            if (res == '1') {
                _this.toastr.success('บันทึกเรียบร้อยแล้ว');
                _this.activeModal.close();
            }
            else {
                _this.toastr.error('มีปัญหาในการบันทึกข้อมูล');
            }
        });
        // this.api.postData('product/add_product', data).then((res: any) => {
        //   console.log(res);
        //   if (res == '1') {
        //     this.toastr.success('บันทึกเรียบรอ้แล้ว');
        //     this.activeModal.close();
        //   } else {
        //     this.toastr.error('มีปัญหาในการบันทึกข้อมูล');
        //   }
        // });
        // console.log("summit");
    };
    NgbdModalBank.ctorParameters = function () { return [
        { type: _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbActiveModal"] },
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["ChangeDetectorRef"] },
        { type: ngx_toastr__WEBPACK_IMPORTED_MODULE_6__["ToastrService"] },
        { type: _angular_fire_database__WEBPACK_IMPORTED_MODULE_9__["AngularFireDatabase"] },
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_7__["HttpClient"] },
        { type: app_webapi_service__WEBPACK_IMPORTED_MODULE_4__["WebapiService"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], NgbdModalBank.prototype, "b", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["ViewChild"])("file", { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_2__["ElementRef"])
    ], NgbdModalBank.prototype, "file", void 0);
    NgbdModalBank = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'ngbd-modal-content',
            template: __webpack_require__(/*! raw-loader!./add_bank.html */ "./node_modules/raw-loader/index.js!./src/app/settings/add_bank.html"),
            styles: [__webpack_require__(/*! ./add_bank.scss */ "./src/app/settings/add_bank.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbActiveModal"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"], _angular_core__WEBPACK_IMPORTED_MODULE_2__["ChangeDetectorRef"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_6__["ToastrService"],
            _angular_fire_database__WEBPACK_IMPORTED_MODULE_9__["AngularFireDatabase"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_7__["HttpClient"], app_webapi_service__WEBPACK_IMPORTED_MODULE_4__["WebapiService"]])
    ], NgbdModalBank);
    return NgbdModalBank;
}());

var settingsComponent = /** @class */ (function () {
    function settingsComponent(modalService, firebase, api, ref) {
        this.modalService = modalService;
        this.firebase = firebase;
        this.api = api;
        this.ref = ref;
        this.setting_genaral = false;
        this.setting_rate = true;
        this.setting_account = true;
        this.setting_location = true;
        this.settingAll = {
            policy: { th: "", en: "" },
            service: { th: "", en: "" },
            up_date: '2020-01-01 00:00:00'
        };
        this.Editor = _ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_5__;
        this.editorConfig = {};
        this.settings = [];
        this.now_rate = { up_time: '2020-01-01 00:00:00' };
        this.admin_id = 0;
        this.all_page = 0;
        this.page = 1;
        this.fb = false;
        this.settings = this.api.storage_get('settings') || [];
    }
    settingsComponent.prototype.onReactiveFormSubmit = function () {
        var _this = this;
        var data = this.rate.value;
        console.log(data);
        this.api.postData('Settings/set_rate', { admin: this.admin_id, data: data }).then(function (res) {
            _this.get_rate();
        });
    };
    settingsComponent.prototype.onBankSubmit = function () {
        var data = this.bank.value;
        console.log(data);
    };
    settingsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.fb = true;
        this.api.check_login();
        this.api.fb_val('backend', function (val) {
            if (_this.fb) {
                _this.get_aboutus();
                _this.get_rate();
                _this.load_bank();
                _this.load_province();
            }
        });
        this.rate = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            'atOrder': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            'cus_rate': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            'sale_rate': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('')
        });
        this.admin_id = this.api.storage_get('logindata').t_id;
        setTimeout(function () { _this.rate.patchValue(_this.now_rate); }, 2000);
    };
    settingsComponent.prototype.view = function (t) {
    };
    settingsComponent.prototype.view_profile = function (t) {
        this.view(t);
    };
    settingsComponent.prototype.load_province = function () {
        var _this = this;
        this.api.getData('Settings/get_province').then(function (res) {
            _this.provinces = res;
            _this.ref.detectChanges();
        });
    };
    settingsComponent.prototype.pOpen = function (p) {
        var _this = this;
        sweetalert2__WEBPACK_IMPORTED_MODULE_8___default.a.fire({
            title: 'คุณต้องการเปิดพื้นที่บริการจริงหรือไม่',
            text: "พื้นที่ " + p.name_th + " จะสามารถใช้บริการได้ทันที",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#0CC27E',
            cancelButtonColor: '#FF586B',
            confirmButtonText: 'ยืนยัน',
            cancelButtonText: 'ปิดออก',
            confirmButtonClass: 'btn btn-success btn-raised mr-2',
            cancelButtonClass: 'btn btn-default btn-raised',
            buttonsStyling: false
        }).then(function (result) {
            if (result.value) {
                _this.api.getData('Settings/set_province/' + p.id + '/0').then(function (res) {
                    console.log(res);
                    _this.api.fb_set('backend');
                    _this.load_province();
                    sweetalert2__WEBPACK_IMPORTED_MODULE_8___default.a.fire('เปิดพื้นที่เรียบร้อยแล้ว', '', 'success');
                });
            }
        });
    };
    settingsComponent.prototype.pClose = function (p) {
        var _this = this;
        sweetalert2__WEBPACK_IMPORTED_MODULE_8___default.a.fire({
            title: 'คุณต้องการปิดพื้นที่บริการจริงหรือไม่',
            text: "พื้นที่ " + p.name_th + " จะไม่สามารถใช้บริการได้",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#0CC27E',
            cancelButtonColor: '#FF586B',
            confirmButtonText: 'ยืนยัน',
            cancelButtonText: 'ปิดออก',
            confirmButtonClass: 'btn btn-success btn-raised mr-2',
            cancelButtonClass: 'btn btn-default btn-raised',
            buttonsStyling: false
        }).then(function (result) {
            if (result.value) {
                _this.api.getData('Settings/set_province/' + p.id + '/1').then(function (res) {
                    console.log(res);
                    _this.api.fb_set('backend');
                    _this.load_province();
                    sweetalert2__WEBPACK_IMPORTED_MODULE_8___default.a.fire('ปิดพื้นที่เรียบร้อยแล้ว', '', 'success');
                });
            }
        });
    };
    settingsComponent.prototype.open = function (b) {
        var _this = this;
        console.log(b);
        var modal = this.modalService.open(NgbdModalBank);
        modal.componentInstance.b = b;
        modal.result.then(function (result) {
            // this.load_data(this.page);
            _this.load_bank();
        }, function (reason) {
            console.log(reason);
        });
    };
    settingsComponent.prototype.load_bank = function () {
        var _this = this;
        this.api.getData("Settings/load_bank").then(function (res) {
            // console.log(res);
            _this.bank = res;
            console.log(_this.bank);
        });
    };
    settingsComponent.prototype.autoGrowTextZone = function (e) {
        e.target.style.height = "0px";
        e.target.style.height = (e.target.scrollHeight + 25) + "px";
    };
    settingsComponent.prototype.show_setting = function (s) {
        if (s == 1) {
            this.setting_genaral = !this.setting_genaral;
        }
        else if (s == 2) {
            this.setting_rate = !this.setting_rate;
        }
        else if (s == 3) {
            this.setting_account = !this.setting_account;
        }
        else if (s == 4) {
            this.setting_location = !this.setting_location;
        }
    };
    settingsComponent.prototype.save = function () {
        this.api.postData('Settings/set_aboutus', this.settingAll).then(function (res) {
            console.log(res);
        });
        console.log(this.settingAll);
    };
    settingsComponent.prototype.onChangePolicyTH = function (_a) {
        var editor = _a.editor;
        // const data = editor.getData();
        this.settingAll.policy.th = editor.getData();
        // console.log(data);
    };
    settingsComponent.prototype.onChangeServiceTH = function (_a) {
        var editor = _a.editor;
        // const data = editor.getData();
        this.settingAll.service.th = editor.getData();
        // console.log(data);
    };
    settingsComponent.prototype.onChangePolicyEN = function (_a) {
        var editor = _a.editor;
        // const data = editor.getData();
        this.settingAll.policy.en = editor.getData();
        // console.log(data);
    };
    settingsComponent.prototype.onChangeServiceEN = function (_a) {
        var editor = _a.editor;
        // const data = editor.getData();
        this.settingAll.service.en = editor.getData();
        // console.log(data);
    };
    settingsComponent.prototype.ngOnChanges = function () {
        this.fb = true;
    };
    settingsComponent.prototype.get_aboutus = function () {
        var _this = this;
        this.api.getData("Settings/get_aboutus").then(function (res) {
            // console.log(res);
            _this.settingAll = res;
            console.log(_this.settingAll);
        });
    };
    settingsComponent.prototype.get_rate = function () {
        var _this = this;
        this.api.getData("Settings/get_rate").then(function (res) {
            _this.now_rate = res;
            console.log(_this.now_rate);
        });
    };
    settingsComponent.prototype.delete = function (b) {
        var _this = this;
        sweetalert2__WEBPACK_IMPORTED_MODULE_8___default.a.fire({
            title: 'คุณต้องการลบจริงหรือไม่',
            text: "รายการนี้จะไม่สามารถนำกลับมาได้อีก",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#0CC27E',
            cancelButtonColor: '#FF586B',
            confirmButtonText: 'ลบออก',
            cancelButtonText: 'ปิดออก',
            confirmButtonClass: 'btn btn-success btn-raised mr-2',
            cancelButtonClass: 'btn btn-default btn-raised',
            buttonsStyling: false
        }).then(function (result) {
            console.log(b);
            console.log(b.bb_id);
            if (result.value) {
                _this.api.postData('Settings/remove_bank', { bb_id: b.bb_id, admin: _this.admin_id }).then(function (res) {
                    console.log(res);
                    _this.load_bank();
                    sweetalert2__WEBPACK_IMPORTED_MODULE_8___default.a.fire('ลบเรียบร้อยแล้ว', '', 'success');
                });
            }
        });
    };
    settingsComponent.prototype.ngOnDestroy = function () {
        this.fb = false;
    };
    settingsComponent.ctorParameters = function () { return [
        { type: _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbModal"] },
        { type: _angular_fire_database__WEBPACK_IMPORTED_MODULE_9__["AngularFireDatabase"] },
        { type: app_webapi_service__WEBPACK_IMPORTED_MODULE_4__["WebapiService"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["ChangeDetectorRef"] }
    ]; };
    settingsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-settings',
            template: __webpack_require__(/*! raw-loader!./settings.component.html */ "./node_modules/raw-loader/index.js!./src/app/settings/settings.component.html"),
            styles: [__webpack_require__(/*! ./settings.component.scss */ "./src/app/settings/settings.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbModal"],
            _angular_fire_database__WEBPACK_IMPORTED_MODULE_9__["AngularFireDatabase"],
            app_webapi_service__WEBPACK_IMPORTED_MODULE_4__["WebapiService"],
            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ChangeDetectorRef"]])
    ], settingsComponent);
    return settingsComponent;
}());



/***/ }),

/***/ "./src/app/shared/animations/custom-animations.ts":
/*!********************************************************!*\
  !*** ./src/app/shared/animations/custom-animations.ts ***!
  \********************************************************/
/*! exports provided: customAnimations */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "customAnimations", function() { return customAnimations; });
/* harmony import */ var _angular_animations__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/animations */ "./node_modules/@angular/animations/fesm5/animations.js");

var customAnimations = [
    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["trigger"])('slideInOut', [
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["state"])('1', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ height: '*' })),
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["state"])('0', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ height: '0px' })),
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["transition"])('1 <=> 0', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["animate"])(200))
    ])
];


/***/ }),

/***/ "./src/app/shared/auth/auth-guard.service.ts":
/*!***************************************************!*\
  !*** ./src/app/shared/auth/auth-guard.service.ts ***!
  \***************************************************/
/*! exports provided: AuthGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthGuard", function() { return AuthGuard; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./auth.service */ "./src/app/shared/auth/auth.service.ts");



var AuthGuard = /** @class */ (function () {
    function AuthGuard(authService) {
        this.authService = authService;
    }
    AuthGuard.prototype.canActivate = function (route, state) {
        return this.authService.isAuthenticated();
    };
    AuthGuard.ctorParameters = function () { return [
        { type: _auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"] }
    ]; };
    AuthGuard = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"]])
    ], AuthGuard);
    return AuthGuard;
}());



/***/ }),

/***/ "./src/app/shared/auth/auth.service.ts":
/*!*********************************************!*\
  !*** ./src/app/shared/auth/auth.service.ts ***!
  \*********************************************/
/*! exports provided: AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return AuthService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var AuthService = /** @class */ (function () {
    function AuthService() {
    }
    AuthService.prototype.signupUser = function (email, password) {
        //your code for signing up the new user
    };
    AuthService.prototype.signinUser = function (email, password) {
        //your code for checking credentials and getting tokens for for signing in user
    };
    AuthService.prototype.logout = function () {
        this.token = null;
    };
    AuthService.prototype.getToken = function () {
        return this.token;
    };
    AuthService.prototype.isAuthenticated = function () {
        // here you can check if user is authenticated or not through his token 
        return true;
    };
    AuthService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], AuthService);
    return AuthService;
}());



/***/ }),

/***/ "./src/app/shared/customizer/customizer.component.scss":
/*!*************************************************************!*\
  !*** ./src/app/shared/customizer/customizer.component.scss ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".customizer {\n  width: 400px;\n  right: -400px;\n  padding: 0;\n  background-color: #FFF;\n  z-index: 1051;\n  position: fixed;\n  top: 0;\n  bottom: 0;\n  height: 100vh;\n  transition: right 0.4s cubic-bezier(0.05, 0.74, 0.2, 0.99);\n  -webkit-backface-visibility: hidden;\n          backface-visibility: hidden;\n  border-left: 1px solid rgba(0, 0, 0, 0.05);\n  box-shadow: 0 0 8px rgba(0, 0, 0, 0.1); }\n  .customizer.open {\n    right: 0; }\n  .customizer .customizer-content {\n    position: relative;\n    height: 100%; }\n  .customizer a.customizer-toggle {\n    background: #FFF;\n    color: theme-color(\"primary\");\n    display: block;\n    box-shadow: -3px 0px 8px rgba(0, 0, 0, 0.1); }\n  .customizer a.customizer-close {\n    color: #000; }\n  .customizer .customizer-close {\n    position: absolute;\n    right: 10px;\n    top: 10px;\n    padding: 7px;\n    width: auto;\n    z-index: 10; }\n  .customizer #rtl-icon {\n    position: absolute;\n    right: -1px;\n    top: 35%;\n    width: 54px;\n    height: 50px;\n    text-align: center;\n    cursor: pointer;\n    line-height: 50px;\n    margin-top: 50px; }\n  .customizer .customizer-toggle {\n    position: absolute;\n    top: 35%;\n    width: 54px;\n    height: 50px;\n    left: -54px;\n    text-align: center;\n    line-height: 50px;\n    cursor: pointer; }\n  .customizer .color-options a {\n    white-space: pre; }\n  .customizer .cz-bg-color {\n    margin: 0 auto; }\n  .customizer .cz-bg-color span:hover {\n      cursor: pointer; }\n  .customizer .cz-bg-color span.white {\n      color: #ddd !important; }\n  .customizer .cz-bg-color .selected,\n  .customizer .cz-tl-bg-color .selected {\n    box-shadow: 0 0 10px 3px #009da0;\n    border: 3px solid #fff; }\n  .customizer .cz-bg-image:hover {\n    cursor: pointer; }\n  .customizer .cz-bg-image img.rounded {\n    border-radius: 1rem !important;\n    border: 2px solid #e6e6e6;\n    height: 100px;\n    width: 50px; }\n  .customizer .cz-bg-image img.rounded.selected {\n      border: 2px solid #FF586B; }\n  .customizer .tl-color-options {\n    display: none; }\n  .customizer .cz-tl-bg-image img.rounded {\n    border-radius: 1rem !important;\n    border: 2px solid #e6e6e6;\n    height: 100px;\n    width: 70px; }\n  .customizer .cz-tl-bg-image img.rounded.selected {\n      border: 2px solid #FF586B; }\n  .customizer .cz-tl-bg-image img.rounded:hover {\n      cursor: pointer; }\n  .customizer .bg-hibiscus {\n    background-image: linear-gradient(to right bottom, #f05f57, #c83d5c, #99245a, #671351, #360940);\n    background-size: cover;\n    background-size: 100% 100%;\n    background-attachment: fixed;\n    background-position: center;\n    background-repeat: no-repeat;\n    transition: background .3s; }\n  .customizer .bg-purple-pizzazz {\n    background-image: linear-gradient(to right bottom, #662d86, #8b2a8a, #ae2389, #cf1d83, #ed1e79);\n    background-size: cover;\n    background-size: 100% 100%;\n    background-attachment: fixed;\n    background-position: center;\n    background-repeat: no-repeat;\n    transition: background .3s; }\n  .customizer .bg-blue-lagoon {\n    background-image: linear-gradient(to right bottom, #144e68, #006d83, #008d92, #00ad91, #57ca85);\n    background-size: cover;\n    background-size: 100% 100%;\n    background-attachment: fixed;\n    background-position: center;\n    background-repeat: no-repeat;\n    transition: background .3s; }\n  .customizer .bg-electric-violet {\n    background-image: linear-gradient(to left top, #4a00e0, #600de0, #7119e1, #8023e1, #8e2de2);\n    background-size: cover;\n    background-size: 100% 100%;\n    background-attachment: fixed;\n    background-position: center;\n    background-repeat: no-repeat;\n    transition: background .3s; }\n  .customizer .bg-portage {\n    background-image: linear-gradient(to left top, #97abff, #798ce5, #5b6ecb, #3b51b1, #123597);\n    background-size: cover;\n    background-size: 100% 100%;\n    background-attachment: fixed;\n    background-position: center;\n    background-repeat: no-repeat;\n    transition: background .3s; }\n  .customizer .bg-tundora {\n    background-image: linear-gradient(to left top, #474747, #4a4a4a, #4c4d4d, #4f5050, #525352);\n    background-size: cover;\n    background-size: 100% 100%;\n    background-attachment: fixed;\n    background-position: center;\n    background-repeat: no-repeat;\n    transition: background .3s; }\n  .customizer .cz-bg-color .col span.rounded-circle:hover,\n  .customizer .cz-tl-bg-color .col span.rounded-circle:hover {\n    cursor: pointer; }\n  [dir=rtl] :host ::ng-deep .customizer {\n  left: -400px;\n  right: auto;\n  border-right: 1px solid rgba(0, 0, 0, 0.05);\n  border-left: 0px; }\n  [dir=rtl] :host ::ng-deep .customizer.open {\n    left: 0;\n    right: auto; }\n  [dir=rtl] :host ::ng-deep .customizer .customizer-close {\n    left: 10px;\n    right: auto; }\n  [dir=rtl] :host ::ng-deep .customizer .customizer-toggle {\n    right: -54px;\n    left: auto; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hhcmVkL2N1c3RvbWl6ZXIvQzpcXGRldmVsb3BlclxcYmFja2VuZC9zcmNcXGFwcFxcc2hhcmVkXFxjdXN0b21pemVyXFxjdXN0b21pemVyLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9zaGFyZWQvY3VzdG9taXplci9jdXN0b21pemVyLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQVFBO0VBQ0UsWUFBWTtFQUNaLGFBQWE7RUFDYixVQUFVO0VBQ1Ysc0JBQXNCO0VBQ3RCLGFBQWE7RUFDYixlQUFlO0VBQ2YsTUFBTTtFQUNOLFNBQVM7RUFDVCxhQUFhO0VBQ2IsMERBQTBEO0VBQzFELG1DQUEyQjtVQUEzQiwyQkFBMkI7RUFDM0IsMENBQTBDO0VBQzFDLHNDQUFzQyxFQUFBO0VBYnhDO0lBZ0JJLFFBQVEsRUFBQTtFQWhCWjtJQW9CSSxrQkFBa0I7SUFDbEIsWUFBWSxFQUFBO0VBckJoQjtJQXlCSSxnQkFBZ0I7SUFDaEIsNkJBQTZCO0lBQzdCLGNBQWM7SUFDZCwyQ0FBMkMsRUFBQTtFQTVCL0M7SUFnQ0ksV0FBVyxFQUFBO0VBaENmO0lBb0NJLGtCQUFrQjtJQUNsQixXQUFXO0lBQ1gsU0FBUztJQUNULFlBQVk7SUFDWixXQUFXO0lBQ1gsV0FBVyxFQUFBO0VBekNmO0lBNkNJLGtCQUFrQjtJQUNsQixXQUFXO0lBQ1gsUUFBUTtJQUNSLFdBQVc7SUFDWCxZQUFZO0lBQ1osa0JBQWtCO0lBQ2xCLGVBQWU7SUFDZixpQkFBaUI7SUFDakIsZ0JBQWdCLEVBQUE7RUFyRHBCO0lBeURJLGtCQUFrQjtJQUNsQixRQUFRO0lBQ1IsV0FBVztJQUNYLFlBQVk7SUFDWixXQUFXO0lBQ1gsa0JBQWtCO0lBQ2xCLGlCQUFpQjtJQUNqQixlQUFlLEVBQUE7RUFoRW5CO0lBcUVNLGdCQUFnQixFQUFBO0VBckV0QjtJQTBFSSxjQUFjLEVBQUE7RUExRWxCO01BOEVRLGVBQWUsRUFBQTtFQTlFdkI7TUFrRlEsc0JBQXNCLEVBQUE7RUFsRjlCOztJQTBGTSxnQ0FBZ0M7SUFDaEMsc0JBQXNCLEVBQUE7RUEzRjVCO0lBaUdNLGVBQWUsRUFBQTtFQWpHckI7SUFxR00sOEJBQThCO0lBQzlCLHlCQUF5QjtJQUN6QixhQUFhO0lBQ2IsV0FBVyxFQUFBO0VBeEdqQjtNQTJHUSx5QkFBeUIsRUFBQTtFQTNHakM7SUFpSEksYUFBYSxFQUFBO0VBakhqQjtJQXNITSw4QkFBOEI7SUFDOUIseUJBQXlCO0lBQ3pCLGFBQWE7SUFDYixXQUFXLEVBQUE7RUF6SGpCO01BNEhRLHlCQUF5QixFQUFBO0VBNUhqQztNQWdJUSxlQUFlLEVBQUE7RUFoSXZCO0lBc0lJLCtGQTdJd0Y7SUE4SXhGLHNCQUFzQjtJQUN0QiwwQkFBMEI7SUFDMUIsNEJBQTRCO0lBQzVCLDJCQUEyQjtJQUMzQiw0QkFBNEI7SUFDNUIsMEJBQTBCLEVBQUE7RUE1STlCO0lBZ0pJLCtGQXRKOEY7SUF1SjlGLHNCQUFzQjtJQUN0QiwwQkFBMEI7SUFDMUIsNEJBQTRCO0lBQzVCLDJCQUEyQjtJQUMzQiw0QkFBNEI7SUFDNUIsMEJBQTBCLEVBQUE7RUF0SjlCO0lBMEpJLCtGQS9KMEY7SUFnSzFGLHNCQUFzQjtJQUN0QiwwQkFBMEI7SUFDMUIsNEJBQTRCO0lBQzVCLDJCQUEyQjtJQUMzQiw0QkFBNEI7SUFDNUIsMEJBQTBCLEVBQUE7RUFoSzlCO0lBb0tJLDJGQXhLeUY7SUF5S3pGLHNCQUFzQjtJQUN0QiwwQkFBMEI7SUFDMUIsNEJBQTRCO0lBQzVCLDJCQUEyQjtJQUMzQiw0QkFBNEI7SUFDNUIsMEJBQTBCLEVBQUE7RUExSzlCO0lBOEtJLDJGQWpMaUY7SUFrTGpGLHNCQUFzQjtJQUN0QiwwQkFBMEI7SUFDMUIsNEJBQTRCO0lBQzVCLDJCQUEyQjtJQUMzQiw0QkFBNEI7SUFDNUIsMEJBQTBCLEVBQUE7RUFwTDlCO0lBeUxJLDJGQTNMaUY7SUE0TGpGLHNCQUFzQjtJQUN0QiwwQkFBMEI7SUFDMUIsNEJBQTRCO0lBQzVCLDJCQUEyQjtJQUMzQiw0QkFBNEI7SUFDNUIsMEJBQTBCLEVBQUE7RUEvTDlCOztJQXVNVSxlQUFlLEVBQUE7RUN2RXpCO0VEa0ZJLFlBQVk7RUFDWixXQUFXO0VBQ1gsMkNBQTJDO0VBQzNDLGdCQUFnQixFQUFBO0VDaEZsQjtJRG1GSSxPQUFPO0lBQ1AsV0FBVyxFQUFBO0VDakZmO0lEcUZJLFVBQVU7SUFDVixXQUFXLEVBQUE7RUNuRmY7SUR3RkksWUFBWTtJQUNaLFVBQVUsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9jdXN0b21pemVyL2N1c3RvbWl6ZXIuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvLyBWYXJpYWJsZXMgRm9yIFRyYW5zcGFyZW50IExheW91dFxuJGJnLWhpYmlzY3VzIDogbGluZWFyLWdyYWRpZW50KHRvIHJpZ2h0IGJvdHRvbSwgI2YwNWY1NywgI2M4M2Q1YywgIzk5MjQ1YSwgIzY3MTM1MSwgIzM2MDk0MCk7XG4kYmctcHVycGxlLXBpenphenogOiBsaW5lYXItZ3JhZGllbnQodG8gcmlnaHQgYm90dG9tLCAjNjYyZDg2LCAjOGIyYThhLCAjYWUyMzg5LCAjY2YxZDgzLCAjZWQxZTc5KTtcbiRiZy1ibHVlLWxhZ29vbjogbGluZWFyLWdyYWRpZW50KHRvIHJpZ2h0IGJvdHRvbSwgIzE0NGU2OCwgIzAwNmQ4MywgIzAwOGQ5MiwgIzAwYWQ5MSwgIzU3Y2E4NSk7XG4kYmctZWxlY3RyaWMtdmlvbGV0OmxpbmVhci1ncmFkaWVudCh0byBsZWZ0IHRvcCwgIzRhMDBlMCwgIzYwMGRlMCwgIzcxMTllMSwgIzgwMjNlMSwgIzhlMmRlMik7XG4kYmctcG9ydGFnZTpsaW5lYXItZ3JhZGllbnQodG8gbGVmdCB0b3AsICM5N2FiZmYsICM3OThjZTUsICM1YjZlY2IsICMzYjUxYjEsICMxMjM1OTcpO1xuJGJnLXR1bmRvcmE6bGluZWFyLWdyYWRpZW50KHRvIGxlZnQgdG9wLCAjNDc0NzQ3LCAjNGE0YTRhLCAjNGM0ZDRkLCAjNGY1MDUwLCAjNTI1MzUyKTtcblxuLmN1c3RvbWl6ZXIge1xuICB3aWR0aDogNDAwcHg7XG4gIHJpZ2h0OiAtNDAwcHg7XG4gIHBhZGRpbmc6IDA7XG4gIGJhY2tncm91bmQtY29sb3I6ICNGRkY7XG4gIHotaW5kZXg6IDEwNTE7XG4gIHBvc2l0aW9uOiBmaXhlZDtcbiAgdG9wOiAwO1xuICBib3R0b206IDA7XG4gIGhlaWdodDogMTAwdmg7XG4gIHRyYW5zaXRpb246IHJpZ2h0IDAuNHMgY3ViaWMtYmV6aWVyKDAuMDUsIDAuNzQsIDAuMiwgMC45OSk7XG4gIGJhY2tmYWNlLXZpc2liaWxpdHk6IGhpZGRlbjtcbiAgYm9yZGVyLWxlZnQ6IDFweCBzb2xpZCByZ2JhKDAsIDAsIDAsIDAuMDUpO1xuICBib3gtc2hhZG93OiAwIDAgOHB4IHJnYmEoMCwgMCwgMCwgMC4xKTtcblxuICAmLm9wZW4ge1xuICAgIHJpZ2h0OiAwO1xuICB9XG5cbiAgLmN1c3RvbWl6ZXItY29udGVudCB7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIGhlaWdodDogMTAwJTtcbiAgfVxuXG4gIGEuY3VzdG9taXplci10b2dnbGUge1xuICAgIGJhY2tncm91bmQ6ICNGRkY7XG4gICAgY29sb3I6IHRoZW1lLWNvbG9yKCdwcmltYXJ5Jyk7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gICAgYm94LXNoYWRvdzogLTNweCAwcHggOHB4IHJnYmEoMCwgMCwgMCwgMC4xKTtcbiAgfVxuXG4gIGEuY3VzdG9taXplci1jbG9zZSB7XG4gICAgY29sb3I6ICMwMDA7XG4gIH1cblxuICAuY3VzdG9taXplci1jbG9zZSB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHJpZ2h0OiAxMHB4O1xuICAgIHRvcDogMTBweDtcbiAgICBwYWRkaW5nOiA3cHg7XG4gICAgd2lkdGg6IGF1dG87XG4gICAgei1pbmRleDogMTA7XG4gIH1cblxuICAjcnRsLWljb24ge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICByaWdodDogLTFweDtcbiAgICB0b3A6IDM1JTtcbiAgICB3aWR0aDogNTRweDtcbiAgICBoZWlnaHQ6IDUwcHg7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICBsaW5lLWhlaWdodDogNTBweDtcbiAgICBtYXJnaW4tdG9wOiA1MHB4O1xuICB9XG5cbiAgLmN1c3RvbWl6ZXItdG9nZ2xlIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgdG9wOiAzNSU7XG4gICAgd2lkdGg6IDU0cHg7XG4gICAgaGVpZ2h0OiA1MHB4O1xuICAgIGxlZnQ6IC01NHB4O1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBsaW5lLWhlaWdodDogNTBweDtcbiAgICBjdXJzb3I6IHBvaW50ZXI7XG4gIH1cblxuICAuY29sb3Itb3B0aW9ucyB7XG4gICAgYSB7XG4gICAgICB3aGl0ZS1zcGFjZTogcHJlO1xuICAgIH1cbiAgfVxuXG4gIC5jei1iZy1jb2xvciB7XG4gICAgbWFyZ2luOiAwIGF1dG87XG5cbiAgICBzcGFuIHtcbiAgICAgICY6aG92ZXIge1xuICAgICAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgICB9XG5cbiAgICAgICYud2hpdGUge1xuICAgICAgICBjb2xvcjogI2RkZCAhaW1wb3J0YW50O1xuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIC5jei1iZy1jb2xvcixcbiAgLmN6LXRsLWJnLWNvbG9yIHtcbiAgICAuc2VsZWN0ZWQge1xuICAgICAgYm94LXNoYWRvdzogMCAwIDEwcHggM3B4ICMwMDlkYTA7XG4gICAgICBib3JkZXI6IDNweCBzb2xpZCAjZmZmO1xuICAgIH1cbiAgfVxuXG4gIC5jei1iZy1pbWFnZSB7XG4gICAgJjpob3ZlciB7XG4gICAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgfVxuXG4gICAgaW1nLnJvdW5kZWQge1xuICAgICAgYm9yZGVyLXJhZGl1czogMXJlbSAhaW1wb3J0YW50O1xuICAgICAgYm9yZGVyOiAycHggc29saWQgI2U2ZTZlNjtcbiAgICAgIGhlaWdodDogMTAwcHg7XG4gICAgICB3aWR0aDogNTBweDtcblxuICAgICAgJi5zZWxlY3RlZCB7XG4gICAgICAgIGJvcmRlcjogMnB4IHNvbGlkICNGRjU4NkI7XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgLnRsLWNvbG9yLW9wdGlvbnMge1xuICAgIGRpc3BsYXk6IG5vbmU7XG4gIH1cblxuICAuY3otdGwtYmctaW1hZ2Uge1xuICAgIGltZy5yb3VuZGVkIHtcbiAgICAgIGJvcmRlci1yYWRpdXM6IDFyZW0gIWltcG9ydGFudDtcbiAgICAgIGJvcmRlcjogMnB4IHNvbGlkICNlNmU2ZTY7XG4gICAgICBoZWlnaHQ6IDEwMHB4O1xuICAgICAgd2lkdGg6IDcwcHg7XG5cbiAgICAgICYuc2VsZWN0ZWQge1xuICAgICAgICBib3JkZXI6IDJweCBzb2xpZCAjRkY1ODZCO1xuICAgICAgfVxuXG4gICAgICAmOmhvdmVyIHtcbiAgICAgICAgY3Vyc29yOiBwb2ludGVyO1xuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIC5iZy1oaWJpc2N1cyB7XG4gICAgYmFja2dyb3VuZC1pbWFnZTogJGJnLWhpYmlzY3VzO1xuICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG4gICAgYmFja2dyb3VuZC1hdHRhY2htZW50OiBmaXhlZDtcbiAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgICB0cmFuc2l0aW9uOiBiYWNrZ3JvdW5kIC4zcztcbiAgfVxuXG4gIC5iZy1wdXJwbGUtcGl6emF6eiB7XG4gICAgYmFja2dyb3VuZC1pbWFnZTogJGJnLXB1cnBsZS1waXp6YXp6O1xuICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG4gICAgYmFja2dyb3VuZC1hdHRhY2htZW50OiBmaXhlZDtcbiAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgICB0cmFuc2l0aW9uOiBiYWNrZ3JvdW5kIC4zcztcbiAgfVxuXG4gIC5iZy1ibHVlLWxhZ29vbiB7XG4gICAgYmFja2dyb3VuZC1pbWFnZTogJGJnLWJsdWUtbGFnb29uO1xuICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG4gICAgYmFja2dyb3VuZC1hdHRhY2htZW50OiBmaXhlZDtcbiAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgICB0cmFuc2l0aW9uOiBiYWNrZ3JvdW5kIC4zcztcbiAgfVxuXG4gIC5iZy1lbGVjdHJpYy12aW9sZXQge1xuICAgIGJhY2tncm91bmQtaW1hZ2U6ICRiZy1lbGVjdHJpYy12aW9sZXQ7XG4gICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbiAgICBiYWNrZ3JvdW5kLWF0dGFjaG1lbnQ6IGZpeGVkO1xuICAgIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcbiAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICAgIHRyYW5zaXRpb246IGJhY2tncm91bmQgLjNzO1xuICB9XG5cbiAgLmJnLXBvcnRhZ2Uge1xuICAgIGJhY2tncm91bmQtaW1hZ2U6ICRiZy1wb3J0YWdlO1xuICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG4gICAgYmFja2dyb3VuZC1hdHRhY2htZW50OiBmaXhlZDtcbiAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgICB0cmFuc2l0aW9uOiBiYWNrZ3JvdW5kIC4zcztcbiAgfVxuXG5cbiAgLmJnLXR1bmRvcmEge1xuICAgIGJhY2tncm91bmQtaW1hZ2U6ICRiZy10dW5kb3JhO1xuICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG4gICAgYmFja2dyb3VuZC1hdHRhY2htZW50OiBmaXhlZDtcbiAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgICB0cmFuc2l0aW9uOiBiYWNrZ3JvdW5kIC4zcztcbiAgfVxuXG4gIC5jei1iZy1jb2xvcixcbiAgLmN6LXRsLWJnLWNvbG9yIHtcbiAgICAuY29sIHtcbiAgICAgIHNwYW4ucm91bmRlZC1jaXJjbGUge1xuICAgICAgICAmOmhvdmVyIHtcbiAgICAgICAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gIH1cblxufVxuXG5cbltkaXI9cnRsXSA6aG9zdCA6Om5nLWRlZXAge1xuICAuY3VzdG9taXplciB7XG4gICAgbGVmdDogLTQwMHB4O1xuICAgIHJpZ2h0OiBhdXRvO1xuICAgIGJvcmRlci1yaWdodDogMXB4IHNvbGlkIHJnYmEoMCwgMCwgMCwgMC4wNSk7XG4gICAgYm9yZGVyLWxlZnQ6IDBweDtcblxuICAgICYub3BlbiB7XG4gICAgICBsZWZ0OiAwO1xuICAgICAgcmlnaHQ6IGF1dG87XG4gICAgfVxuXG4gICAgLmN1c3RvbWl6ZXItY2xvc2Uge1xuICAgICAgbGVmdDogMTBweDtcbiAgICAgIHJpZ2h0OiBhdXRvO1xuXG4gICAgfVxuXG4gICAgLmN1c3RvbWl6ZXItdG9nZ2xlIHtcbiAgICAgIHJpZ2h0OiAtNTRweDtcbiAgICAgIGxlZnQ6IGF1dG87XG4gICAgfVxuXG4gIH1cbn1cbiIsIi5jdXN0b21pemVyIHtcbiAgd2lkdGg6IDQwMHB4O1xuICByaWdodDogLTQwMHB4O1xuICBwYWRkaW5nOiAwO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjRkZGO1xuICB6LWluZGV4OiAxMDUxO1xuICBwb3NpdGlvbjogZml4ZWQ7XG4gIHRvcDogMDtcbiAgYm90dG9tOiAwO1xuICBoZWlnaHQ6IDEwMHZoO1xuICB0cmFuc2l0aW9uOiByaWdodCAwLjRzIGN1YmljLWJlemllcigwLjA1LCAwLjc0LCAwLjIsIDAuOTkpO1xuICBiYWNrZmFjZS12aXNpYmlsaXR5OiBoaWRkZW47XG4gIGJvcmRlci1sZWZ0OiAxcHggc29saWQgcmdiYSgwLCAwLCAwLCAwLjA1KTtcbiAgYm94LXNoYWRvdzogMCAwIDhweCByZ2JhKDAsIDAsIDAsIDAuMSk7IH1cbiAgLmN1c3RvbWl6ZXIub3BlbiB7XG4gICAgcmlnaHQ6IDA7IH1cbiAgLmN1c3RvbWl6ZXIgLmN1c3RvbWl6ZXItY29udGVudCB7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIGhlaWdodDogMTAwJTsgfVxuICAuY3VzdG9taXplciBhLmN1c3RvbWl6ZXItdG9nZ2xlIHtcbiAgICBiYWNrZ3JvdW5kOiAjRkZGO1xuICAgIGNvbG9yOiB0aGVtZS1jb2xvcihcInByaW1hcnlcIik7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gICAgYm94LXNoYWRvdzogLTNweCAwcHggOHB4IHJnYmEoMCwgMCwgMCwgMC4xKTsgfVxuICAuY3VzdG9taXplciBhLmN1c3RvbWl6ZXItY2xvc2Uge1xuICAgIGNvbG9yOiAjMDAwOyB9XG4gIC5jdXN0b21pemVyIC5jdXN0b21pemVyLWNsb3NlIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgcmlnaHQ6IDEwcHg7XG4gICAgdG9wOiAxMHB4O1xuICAgIHBhZGRpbmc6IDdweDtcbiAgICB3aWR0aDogYXV0bztcbiAgICB6LWluZGV4OiAxMDsgfVxuICAuY3VzdG9taXplciAjcnRsLWljb24ge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICByaWdodDogLTFweDtcbiAgICB0b3A6IDM1JTtcbiAgICB3aWR0aDogNTRweDtcbiAgICBoZWlnaHQ6IDUwcHg7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICBsaW5lLWhlaWdodDogNTBweDtcbiAgICBtYXJnaW4tdG9wOiA1MHB4OyB9XG4gIC5jdXN0b21pemVyIC5jdXN0b21pemVyLXRvZ2dsZSB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHRvcDogMzUlO1xuICAgIHdpZHRoOiA1NHB4O1xuICAgIGhlaWdodDogNTBweDtcbiAgICBsZWZ0OiAtNTRweDtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgbGluZS1oZWlnaHQ6IDUwcHg7XG4gICAgY3Vyc29yOiBwb2ludGVyOyB9XG4gIC5jdXN0b21pemVyIC5jb2xvci1vcHRpb25zIGEge1xuICAgIHdoaXRlLXNwYWNlOiBwcmU7IH1cbiAgLmN1c3RvbWl6ZXIgLmN6LWJnLWNvbG9yIHtcbiAgICBtYXJnaW46IDAgYXV0bzsgfVxuICAgIC5jdXN0b21pemVyIC5jei1iZy1jb2xvciBzcGFuOmhvdmVyIHtcbiAgICAgIGN1cnNvcjogcG9pbnRlcjsgfVxuICAgIC5jdXN0b21pemVyIC5jei1iZy1jb2xvciBzcGFuLndoaXRlIHtcbiAgICAgIGNvbG9yOiAjZGRkICFpbXBvcnRhbnQ7IH1cbiAgLmN1c3RvbWl6ZXIgLmN6LWJnLWNvbG9yIC5zZWxlY3RlZCxcbiAgLmN1c3RvbWl6ZXIgLmN6LXRsLWJnLWNvbG9yIC5zZWxlY3RlZCB7XG4gICAgYm94LXNoYWRvdzogMCAwIDEwcHggM3B4ICMwMDlkYTA7XG4gICAgYm9yZGVyOiAzcHggc29saWQgI2ZmZjsgfVxuICAuY3VzdG9taXplciAuY3otYmctaW1hZ2U6aG92ZXIge1xuICAgIGN1cnNvcjogcG9pbnRlcjsgfVxuICAuY3VzdG9taXplciAuY3otYmctaW1hZ2UgaW1nLnJvdW5kZWQge1xuICAgIGJvcmRlci1yYWRpdXM6IDFyZW0gIWltcG9ydGFudDtcbiAgICBib3JkZXI6IDJweCBzb2xpZCAjZTZlNmU2O1xuICAgIGhlaWdodDogMTAwcHg7XG4gICAgd2lkdGg6IDUwcHg7IH1cbiAgICAuY3VzdG9taXplciAuY3otYmctaW1hZ2UgaW1nLnJvdW5kZWQuc2VsZWN0ZWQge1xuICAgICAgYm9yZGVyOiAycHggc29saWQgI0ZGNTg2QjsgfVxuICAuY3VzdG9taXplciAudGwtY29sb3Itb3B0aW9ucyB7XG4gICAgZGlzcGxheTogbm9uZTsgfVxuICAuY3VzdG9taXplciAuY3otdGwtYmctaW1hZ2UgaW1nLnJvdW5kZWQge1xuICAgIGJvcmRlci1yYWRpdXM6IDFyZW0gIWltcG9ydGFudDtcbiAgICBib3JkZXI6IDJweCBzb2xpZCAjZTZlNmU2O1xuICAgIGhlaWdodDogMTAwcHg7XG4gICAgd2lkdGg6IDcwcHg7IH1cbiAgICAuY3VzdG9taXplciAuY3otdGwtYmctaW1hZ2UgaW1nLnJvdW5kZWQuc2VsZWN0ZWQge1xuICAgICAgYm9yZGVyOiAycHggc29saWQgI0ZGNTg2QjsgfVxuICAgIC5jdXN0b21pemVyIC5jei10bC1iZy1pbWFnZSBpbWcucm91bmRlZDpob3ZlciB7XG4gICAgICBjdXJzb3I6IHBvaW50ZXI7IH1cbiAgLmN1c3RvbWl6ZXIgLmJnLWhpYmlzY3VzIHtcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQodG8gcmlnaHQgYm90dG9tLCAjZjA1ZjU3LCAjYzgzZDVjLCAjOTkyNDVhLCAjNjcxMzUxLCAjMzYwOTQwKTtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICAgIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xuICAgIGJhY2tncm91bmQtYXR0YWNobWVudDogZml4ZWQ7XG4gICAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xuICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gICAgdHJhbnNpdGlvbjogYmFja2dyb3VuZCAuM3M7IH1cbiAgLmN1c3RvbWl6ZXIgLmJnLXB1cnBsZS1waXp6YXp6IHtcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQodG8gcmlnaHQgYm90dG9tLCAjNjYyZDg2LCAjOGIyYThhLCAjYWUyMzg5LCAjY2YxZDgzLCAjZWQxZTc5KTtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICAgIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xuICAgIGJhY2tncm91bmQtYXR0YWNobWVudDogZml4ZWQ7XG4gICAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xuICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gICAgdHJhbnNpdGlvbjogYmFja2dyb3VuZCAuM3M7IH1cbiAgLmN1c3RvbWl6ZXIgLmJnLWJsdWUtbGFnb29uIHtcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQodG8gcmlnaHQgYm90dG9tLCAjMTQ0ZTY4LCAjMDA2ZDgzLCAjMDA4ZDkyLCAjMDBhZDkxLCAjNTdjYTg1KTtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICAgIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xuICAgIGJhY2tncm91bmQtYXR0YWNobWVudDogZml4ZWQ7XG4gICAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xuICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gICAgdHJhbnNpdGlvbjogYmFja2dyb3VuZCAuM3M7IH1cbiAgLmN1c3RvbWl6ZXIgLmJnLWVsZWN0cmljLXZpb2xldCB7XG4gICAgYmFja2dyb3VuZC1pbWFnZTogbGluZWFyLWdyYWRpZW50KHRvIGxlZnQgdG9wLCAjNGEwMGUwLCAjNjAwZGUwLCAjNzExOWUxLCAjODAyM2UxLCAjOGUyZGUyKTtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICAgIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xuICAgIGJhY2tncm91bmQtYXR0YWNobWVudDogZml4ZWQ7XG4gICAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xuICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gICAgdHJhbnNpdGlvbjogYmFja2dyb3VuZCAuM3M7IH1cbiAgLmN1c3RvbWl6ZXIgLmJnLXBvcnRhZ2Uge1xuICAgIGJhY2tncm91bmQtaW1hZ2U6IGxpbmVhci1ncmFkaWVudCh0byBsZWZ0IHRvcCwgIzk3YWJmZiwgIzc5OGNlNSwgIzViNmVjYiwgIzNiNTFiMSwgIzEyMzU5Nyk7XG4gICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbiAgICBiYWNrZ3JvdW5kLWF0dGFjaG1lbnQ6IGZpeGVkO1xuICAgIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcbiAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICAgIHRyYW5zaXRpb246IGJhY2tncm91bmQgLjNzOyB9XG4gIC5jdXN0b21pemVyIC5iZy10dW5kb3JhIHtcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQodG8gbGVmdCB0b3AsICM0NzQ3NDcsICM0YTRhNGEsICM0YzRkNGQsICM0ZjUwNTAsICM1MjUzNTIpO1xuICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG4gICAgYmFja2dyb3VuZC1hdHRhY2htZW50OiBmaXhlZDtcbiAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgICB0cmFuc2l0aW9uOiBiYWNrZ3JvdW5kIC4zczsgfVxuICAuY3VzdG9taXplciAuY3otYmctY29sb3IgLmNvbCBzcGFuLnJvdW5kZWQtY2lyY2xlOmhvdmVyLFxuICAuY3VzdG9taXplciAuY3otdGwtYmctY29sb3IgLmNvbCBzcGFuLnJvdW5kZWQtY2lyY2xlOmhvdmVyIHtcbiAgICBjdXJzb3I6IHBvaW50ZXI7IH1cblxuW2Rpcj1ydGxdIDpob3N0IDo6bmctZGVlcCAuY3VzdG9taXplciB7XG4gIGxlZnQ6IC00MDBweDtcbiAgcmlnaHQ6IGF1dG87XG4gIGJvcmRlci1yaWdodDogMXB4IHNvbGlkIHJnYmEoMCwgMCwgMCwgMC4wNSk7XG4gIGJvcmRlci1sZWZ0OiAwcHg7IH1cbiAgW2Rpcj1ydGxdIDpob3N0IDo6bmctZGVlcCAuY3VzdG9taXplci5vcGVuIHtcbiAgICBsZWZ0OiAwO1xuICAgIHJpZ2h0OiBhdXRvOyB9XG4gIFtkaXI9cnRsXSA6aG9zdCA6Om5nLWRlZXAgLmN1c3RvbWl6ZXIgLmN1c3RvbWl6ZXItY2xvc2Uge1xuICAgIGxlZnQ6IDEwcHg7XG4gICAgcmlnaHQ6IGF1dG87IH1cbiAgW2Rpcj1ydGxdIDpob3N0IDo6bmctZGVlcCAuY3VzdG9taXplciAuY3VzdG9taXplci10b2dnbGUge1xuICAgIHJpZ2h0OiAtNTRweDtcbiAgICBsZWZ0OiBhdXRvOyB9XG4iXX0= */"

/***/ }),

/***/ "./src/app/shared/customizer/customizer.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/shared/customizer/customizer.component.ts ***!
  \***********************************************************/
/*! exports provided: CustomizerComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomizerComponent", function() { return CustomizerComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_layout_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/layout.service */ "./src/app/shared/services/layout.service.ts");
/* harmony import */ var _services_config_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/config.service */ "./src/app/shared/services/config.service.ts");




var CustomizerComponent = /** @class */ (function () {
    function CustomizerComponent(elRef, renderer, layoutService, configService) {
        var _this = this;
        this.elRef = elRef;
        this.renderer = renderer;
        this.layoutService = layoutService;
        this.configService = configService;
        this.options = {
            direction: "ltr",
            bgColor: "black",
            transparentColor: "",
            bgImage: "assets/img/sidebar-bg/01.jpg",
            bgImageDisplay: true,
            compactMenu: false,
            sidebarSize: "sidebar-md",
            layout: "Light"
        };
        this.size = "sidebar-md";
        this.isOpen = true;
        this.config = {};
        this.isBgImageDisplay = true;
        this.selectedBgColor = "black";
        this.selectedBgImage = "assets/img/sidebar-bg/01.jpg";
        this.selectedTLBgColor = "";
        this.selectedTLBgImage = "";
        this.directionEvent = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.layoutSub = layoutService.customizerChangeEmitted$.subscribe(function (options) {
            if (options) {
                if (options.bgColor) {
                    _this.selectedBgColor = options.bgColor;
                    _this.selectedBgImage = options.bgImage;
                }
            }
        });
    }
    CustomizerComponent.prototype.ngOnInit = function () {
        this.config = this.configService.templateConf;
        this.isOpen = !this.config.layout.customizer.hidden;
        if (this.config.layout.sidebar.size) {
            this.options.sidebarSize = this.config.layout.sidebar.size;
            this.size = this.config.layout.sidebar.size;
        }
    };
    CustomizerComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        setTimeout(function () {
            if (_this.config.layout.dir) {
                _this.options.direction = _this.config.layout.dir;
            }
            if (_this.config.layout.variant) {
                _this.options.layout = _this.config.layout.variant;
            }
            if (_this.config.layout.sidebar.collapsed != undefined) {
                _this.options.compactMenu = _this.config.layout.sidebar.collapsed;
            }
            if (_this.config.layout.sidebar.backgroundColor &&
                _this.config.layout.sidebar.backgroundColor != "") {
                _this.options.bgColor = _this.config.layout.sidebar.backgroundColor;
                _this.selectedBgColor = _this.config.layout.sidebar.backgroundColor;
            }
            else {
                _this.options.bgColor = "black";
                _this.selectedBgColor = "black";
            }
            if (_this.config.layout.sidebar.backgroundImage != undefined) {
                _this.options.bgImageDisplay = _this.config.layout.sidebar.backgroundImage;
                _this.isBgImageDisplay = _this.config.layout.sidebar.backgroundImage;
            }
            if (_this.config.layout.sidebar.backgroundImageURL) {
                _this.options.bgImage = _this.config.layout.sidebar.backgroundImageURL;
                _this.selectedBgImage = _this.config.layout.sidebar.backgroundImageURL;
            }
            if (_this.options.layout === "Transparent") {
                _this.options.bgColor = "black";
                _this.selectedBgColor = "black";
                _this.options.bgImageDisplay = false;
                _this.selectedTLBgColor = "";
                _this.selectedBgImage = "";
                _this.options.bgImage = "";
                _this.isBgImageDisplay = false;
                if (_this.config.layout.sidebar.backgroundColor) {
                    if (_this.config.layout.sidebar.backgroundColor === "bg-glass-1") {
                        _this.selectedTLBgImage = "assets/img/gallery/bg-glass-1.jpg";
                        _this.options.transparentColor = "bg-glass-1";
                    }
                    else if (_this.config.layout.sidebar.backgroundColor === "bg-glass-2") {
                        _this.selectedTLBgImage = "assets/img/gallery/bg-glass-2.jpg";
                        _this.options.transparentColor = "bg-glass-2";
                    }
                    else if (_this.config.layout.sidebar.backgroundColor === "bg-glass-3") {
                        _this.selectedTLBgImage = "assets/img/gallery/bg-glass-3.jpg";
                        _this.options.transparentColor = "bg-glass-3";
                    }
                    else if (_this.config.layout.sidebar.backgroundColor === "bg-glass-4") {
                        _this.selectedTLBgImage = "assets/img/gallery/bg-glass-4.jpg";
                        _this.options.transparentColor = "bg-glass-4";
                    }
                    else {
                        _this.options.transparentColor = _this.config.layout.sidebar.backgroundColor;
                        _this.selectedTLBgColor = _this.config.layout.sidebar.backgroundColor;
                    }
                }
                else {
                    _this.options.bgColor = "black";
                    _this.selectedBgColor = "black";
                    _this.options.bgImageDisplay = false;
                    _this.selectedBgColor = "";
                    _this.selectedTLBgColor = "";
                    _this.selectedTLBgImage = "assets/img/gallery/bg-glass-1.jpg";
                    _this.options.transparentColor = "bg-glass-1";
                }
            }
        }, 0);
    };
    CustomizerComponent.prototype.ngOnDestroy = function () {
        if (this.layoutSub) {
            this.layoutSub.unsubscribe();
        }
    };
    CustomizerComponent.prototype.sendOptions = function () {
        this.directionEvent.emit(this.options);
        this.layoutService.emitChange(this.options);
    };
    CustomizerComponent.prototype.bgImageDisplay = function (e) {
        if (e.target.checked) {
            this.options.bgImageDisplay = true;
            this.isBgImageDisplay = true;
        }
        else {
            this.options.bgImageDisplay = false;
            this.isBgImageDisplay = false;
        }
        //emit event to FUll Layout
        this.layoutService.emitCustomizerChange(this.options);
    };
    CustomizerComponent.prototype.toggleCompactMenu = function (e) {
        if (e.target.checked) {
            this.options.compactMenu = true;
        }
        else {
            this.options.compactMenu = false;
        }
        //emit event to FUll Layout
        this.layoutService.emitCustomizerChange(this.options);
    };
    CustomizerComponent.prototype.changeSidebarWidth = function (value) {
        this.options.sidebarSize = value;
        this.size = value;
        //emit event to FUll Layout
        this.layoutService.emitCustomizerChange(this.options);
    };
    CustomizerComponent.prototype.onLightLayout = function () {
        this.options.layout = "Light";
        this.options.bgColor = "man-of-steel";
        this.selectedBgColor = "man-of-steel";
        if (this.isBgImageDisplay) {
            this.options.bgImageDisplay = true;
        }
        //emit event to FUll Layout
        this.layoutService.emitCustomizerChange(this.options);
    };
    CustomizerComponent.prototype.onDarkLayout = function () {
        this.options.layout = "Dark";
        this.options.bgColor = "black";
        this.selectedBgColor = "black";
        if (this.isBgImageDisplay) {
            this.options.bgImageDisplay = true;
        }
        //emit event to FUll Layout
        this.layoutService.emitCustomizerChange(this.options);
    };
    CustomizerComponent.prototype.onTransparentLayout = function () {
        this.options.layout = "Transparent";
        this.options.bgColor = "black";
        this.selectedBgColor = "black";
        this.options.bgImageDisplay = false;
        this.selectedBgColor = "";
        this.selectedTLBgColor = "";
        this.selectedTLBgImage = "assets/img/gallery/bg-glass-1.jpg";
        this.options.transparentColor = "bg-glass-1";
        //emit event to FUll Layout
        this.layoutService.emitCustomizerChange(this.options);
    };
    CustomizerComponent.prototype.toggleCustomizer = function () {
        if (this.isOpen) {
            this.renderer.removeClass(this.customizer.nativeElement, "open");
            this.isOpen = false;
        }
        else {
            this.renderer.addClass(this.customizer.nativeElement, "open");
            this.isOpen = true;
        }
    };
    CustomizerComponent.prototype.closeCustomizer = function () {
        this.renderer.removeClass(this.customizer.nativeElement, "open");
        this.isOpen = false;
    };
    CustomizerComponent.prototype.changeSidebarBgColor = function (color) {
        this.selectedBgColor = color;
        this.selectedTLBgColor = "";
        this.options.bgColor = color;
        if (this.isBgImageDisplay) {
            this.options.bgImageDisplay = true;
        }
        //emit event to FUll Layout
        this.layoutService.emitCustomizerChange(this.options);
    };
    CustomizerComponent.prototype.changeSidebarBgImage = function (url) {
        this.selectedBgImage = url;
        this.selectedTLBgImage = "";
        //emit event to FUll Layout
        this.options.bgImage = url;
        if (this.isBgImageDisplay) {
            this.options.bgImageDisplay = true;
        }
        //emit event to FUll Layout
        this.layoutService.emitCustomizerChange(this.options);
    };
    CustomizerComponent.prototype.changeSidebarTLBgColor = function (color) {
        this.selectedBgColor = "";
        this.selectedTLBgColor = color;
        this.selectedTLBgImage = "";
        this.options.transparentColor = color;
        this.options.bgImageDisplay = false;
        //emit event to FUll Layout
        this.layoutService.emitCustomizerChange(this.options);
    };
    CustomizerComponent.prototype.changeSidebarTLBgImage = function (url, color) {
        this.selectedTLBgColor = "";
        this.selectedTLBgImage = url;
        this.options.transparentColor = color;
        this.options.bgImageDisplay = false;
        //emit event to FUll Layout
        this.layoutService.emitCustomizerChange(this.options);
    };
    CustomizerComponent.ctorParameters = function () { return [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer2"] },
        { type: _services_layout_service__WEBPACK_IMPORTED_MODULE_2__["LayoutService"] },
        { type: _services_config_service__WEBPACK_IMPORTED_MODULE_3__["ConfigService"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])("customizer", { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], CustomizerComponent.prototype, "customizer", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], CustomizerComponent.prototype, "directionEvent", void 0);
    CustomizerComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "app-customizer",
            template: __webpack_require__(/*! raw-loader!./customizer.component.html */ "./node_modules/raw-loader/index.js!./src/app/shared/customizer/customizer.component.html"),
            styles: [__webpack_require__(/*! ./customizer.component.scss */ "./src/app/shared/customizer/customizer.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"],
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer2"],
            _services_layout_service__WEBPACK_IMPORTED_MODULE_2__["LayoutService"],
            _services_config_service__WEBPACK_IMPORTED_MODULE_3__["ConfigService"]])
    ], CustomizerComponent);
    return CustomizerComponent;
}());



/***/ }),

/***/ "./src/app/shared/directives/sidebar.directive.ts":
/*!********************************************************!*\
  !*** ./src/app/shared/directives/sidebar.directive.ts ***!
  \********************************************************/
/*! exports provided: SidebarDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SidebarDirective", function() { return SidebarDirective; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");



var SidebarDirective = /** @class */ (function () {
    function SidebarDirective(el, renderer, router, cd) {
        this.el = el;
        this.renderer = renderer;
        this.router = router;
        this.cd = cd;
        this.navlinks = [];
        this.activeLinks = [];
        this.toggleHideSidebar = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
    }
    SidebarDirective.prototype.ngOnInit = function () {
        this.activeRoute = this.router.url;
        this.innerWidth = window.innerWidth;
    };
    SidebarDirective.prototype.ngAfterViewInit = function () {
        var element = this.el.nativeElement;
        this.$wrapper = this.renderer.parentNode(this.el.nativeElement); // document.getElementsByClassName("wrapper")[0];
        var $sidebar_img_container = this.el.nativeElement.querySelector('.sidebar-background');
        var $sidebar_img = element.getAttribute("data-image");
        if ($sidebar_img_container.length !== 0 && $sidebar_img !== undefined) {
            this.renderer.setAttribute($sidebar_img_container, 'style', 'background-image: url("' + $sidebar_img + '")');
        }
        if (!this.$wrapper.classList.contains("nav-collapsed")) {
            this.expandActive();
        }
        if (this.innerWidth < 992) {
            this.renderer.removeClass(this.$wrapper, 'nav-collapsed');
            this.renderer.removeClass(this.$wrapper, 'menu-collapsed');
            this.toggleHideSidebar.emit(true);
        }
        this.cd.detectChanges();
    };
    SidebarDirective.prototype.onMouseOver = function (e) {
        if (this.$wrapper.classList.contains("nav-collapsed")) {
            this.renderer.removeClass(this.$wrapper, 'menu-collapsed');
            this.navlinks
                .filter(function (_) { return _.navCollapsedOpen === true; })
                .forEach(function (link) {
                link.open = true;
                link.navCollapsedOpen = false;
            });
        }
    };
    SidebarDirective.prototype.onMouseOut = function (e) {
        if (this.$wrapper.classList.contains("nav-collapsed")) {
            this.renderer.addClass(this.$wrapper, 'menu-collapsed');
            this.navlinks
                .filter(function (_) { return _.open === true; })
                .forEach(function (link) {
                link.open = false;
                link.navCollapsedOpen = true;
            });
        }
    };
    SidebarDirective.prototype.onClick = function (e) {
        if (e.target.parentElement.classList.contains("logo-text") ||
            e.target.parentElement.classList.contains("logo-img")) {
            this.activeLinks = [];
            this.activeRoute = this.router.url;
            this.expandActive();
            this.hideSidebar();
        }
        else if (e.target.parentElement.classList.contains("nav-close") ||
            e.target.classList.contains("nav-close")) {
            this.toggleHideSidebar.emit(true);
        }
    };
    SidebarDirective.prototype.onResize = function (event) {
        this.innerWidth = window.innerWidth;
        if (event.target.innerWidth < 992) {
            this.renderer.removeClass(this.$wrapper, 'nav-collapsed');
            this.renderer.removeClass(this.$wrapper, 'menu-collapsed');
            this.toggleHideSidebar.emit(true);
        }
        if (event.target.innerWidth > 992) {
            var toggleStatus = this.el.nativeElement;
            this.el.nativeElement.querySelector('.toggle-icon')
                .getAttribute("data-toggle");
            if (toggleStatus === "collapsed" &&
                this.$wrapper.classList.contains("nav-collapsed") &&
                this.$wrapper.classList.contains("menu-collapsed")) {
                this.$wrapper.classList.add("nav-collapsed");
                this.$wrapper.classList.add("menu-collapsed");
            }
            this.toggleHideSidebar.emit(false);
        }
    };
    // check outside click and close sidebar for smaller screen <992
    SidebarDirective.prototype.onOutsideClick = function (event) {
        if (this.innerWidth < 992) {
            var clickedComponent = event.target;
            var inside = false;
            do {
                if (clickedComponent === this.el.nativeElement) {
                    inside = true;
                }
                clickedComponent = clickedComponent.parentNode;
            } while (clickedComponent);
            if (!this.el.nativeElement.classList.contains("hide-sidebar") &&
                !inside &&
                !event.target.classList.contains("navbar-toggle")) {
                this.toggleHideSidebar.emit(true);
            }
        }
    };
    SidebarDirective.prototype.addLink = function (link) {
        this.navlinks.push(link);
    };
    SidebarDirective.prototype.getLinks = function () {
        return this.navlinks;
    };
    SidebarDirective.prototype.hideSidebar = function () {
        if (this.innerWidth < 992) {
            this.toggleHideSidebar.emit(true);
        }
    };
    SidebarDirective.prototype.expandActive = function () {
        var _this = this;
        this.navlinks
            .filter(function (_) { return _.routePath === _this.activeRoute; })
            .forEach(function (link) {
            link.isShown = true;
            if (link.level.toString().trim() === "3") {
                _this.navlinks
                    .filter(function (_) { return _.level.toString().trim() === "2" && _.title === link.parent; })
                    .forEach(function (parentLink) {
                    parentLink.open = true;
                    _this.activeLinks.push(parentLink.title);
                    _this.navlinks
                        .filter(function (_) {
                        return _.level.toString().trim() === "1" &&
                            _.title === parentLink.parent;
                    })
                        .forEach(function (superParentLink) {
                        superParentLink.open = true;
                        _this.activeLinks.push(superParentLink.title);
                        superParentLink.toggleEmit.emit(_this.activeLinks);
                    });
                });
            }
            else if (link.level.toString().trim() === "2") {
                _this.navlinks
                    .filter(function (_) { return _.level.toString().trim() === "1" && _.title === link.parent; })
                    .forEach(function (parentLink) {
                    parentLink.open = true;
                    _this.activeLinks.push(parentLink.title);
                    parentLink.toggleEmit.emit(_this.activeLinks);
                });
            }
        });
    };
    SidebarDirective.prototype.toggleActiveList = function () {
        var _this = this;
        this.navlinks
            .filter(function (_) {
            return _.level.toString().trim() === "3" && _.routePath !== _this.activeRoute;
        })
            .forEach(function (link) {
            link.active = false;
        });
    };
    SidebarDirective.prototype.setIsShown = function (parentLink) {
        var childLevel = Number(parentLink.level) + 1;
        this.navlinks
            .filter(function (_) {
            return _.level.toString().trim() === childLevel.toString().trim() &&
                _.parent === parentLink.title;
        })
            .forEach(function (link) {
            link.isShown = true;
            link.isHidden = false;
        });
    };
    SidebarDirective.prototype.setIsHidden = function (parentLink) {
        var childLevel = Number(parentLink.level) + 1;
        this.navlinks
            .filter(function (_) {
            return _.level.toString().trim() === childLevel.toString().trim() &&
                _.parent === parentLink.title;
        })
            .forEach(function (link) {
            link.isShown = false;
            link.isHidden = true;
        });
    };
    SidebarDirective.ctorParameters = function () { return [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer2"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], SidebarDirective.prototype, "toggleHideSidebar", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])("mouseenter", ["$event"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
    ], SidebarDirective.prototype, "onMouseOver", null);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])("mouseleave", ["$event"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
    ], SidebarDirective.prototype, "onMouseOut", null);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])("click", ["$event"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
    ], SidebarDirective.prototype, "onClick", null);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])("window:resize", ["$event"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
    ], SidebarDirective.prototype, "onResize", null);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])("document:click", ["$event"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
    ], SidebarDirective.prototype, "onOutsideClick", null);
    SidebarDirective = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Directive"])({ selector: "[appSidebar]" }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"],
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer2"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"]])
    ], SidebarDirective);
    return SidebarDirective;
}());



/***/ }),

/***/ "./src/app/shared/directives/sidebaranchortoggle.directive.ts":
/*!********************************************************************!*\
  !*** ./src/app/shared/directives/sidebaranchortoggle.directive.ts ***!
  \********************************************************************/
/*! exports provided: SidebarAnchorToggleDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SidebarAnchorToggleDirective", function() { return SidebarAnchorToggleDirective; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _sidebarlink_directive__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./sidebarlink.directive */ "./src/app/shared/directives/sidebarlink.directive.ts");



var SidebarAnchorToggleDirective = /** @class */ (function () {
    function SidebarAnchorToggleDirective(navlink) {
        this.navlink = navlink;
    }
    SidebarAnchorToggleDirective.prototype.onClick = function () {
        this.navlink.toggle();
    };
    SidebarAnchorToggleDirective.ctorParameters = function () { return [
        { type: _sidebarlink_directive__WEBPACK_IMPORTED_MODULE_2__["SidebarLinkDirective"], decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"], args: [_sidebarlink_directive__WEBPACK_IMPORTED_MODULE_2__["SidebarLinkDirective"],] }] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])("click", ["$event"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", []),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
    ], SidebarAnchorToggleDirective.prototype, "onClick", null);
    SidebarAnchorToggleDirective = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Directive"])({
            selector: "[appSidebarAnchorToggle]"
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_sidebarlink_directive__WEBPACK_IMPORTED_MODULE_2__["SidebarLinkDirective"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_sidebarlink_directive__WEBPACK_IMPORTED_MODULE_2__["SidebarLinkDirective"]])
    ], SidebarAnchorToggleDirective);
    return SidebarAnchorToggleDirective;
}());



/***/ }),

/***/ "./src/app/shared/directives/sidebarlink.directive.ts":
/*!************************************************************!*\
  !*** ./src/app/shared/directives/sidebarlink.directive.ts ***!
  \************************************************************/
/*! exports provided: SidebarLinkDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SidebarLinkDirective", function() { return SidebarLinkDirective; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _sidebarlist_directive__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./sidebarlist.directive */ "./src/app/shared/directives/sidebarlist.directive.ts");
/* harmony import */ var _sidebar_directive__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./sidebar.directive */ "./src/app/shared/directives/sidebar.directive.ts");




var SidebarLinkDirective = /** @class */ (function () {
    function SidebarLinkDirective(sidebarList, sidebar, el) {
        this.el = el;
        this.toggleEmit = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.sidebarList = sidebarList;
        this.sidebar = sidebar;
    }
    Object.defineProperty(SidebarLinkDirective.prototype, "open", {
        get: function () {
            return this._open;
        },
        set: function (value) {
            this._open = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SidebarLinkDirective.prototype, "navCollapsedOpen", {
        get: function () {
            return this._navCollapsedOpen;
        },
        set: function (value) {
            this._navCollapsedOpen = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SidebarLinkDirective.prototype, "active", {
        get: function () {
            return this._active;
        },
        set: function (value) {
            this._active = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SidebarLinkDirective.prototype, "isShown", {
        get: function () {
            return this._isShown;
        },
        set: function (value) {
            this._isShown = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SidebarLinkDirective.prototype, "isHidden", {
        get: function () {
            return this._isHidden;
        },
        set: function (value) {
            this._isHidden = value;
        },
        enumerable: true,
        configurable: true
    });
    SidebarLinkDirective.prototype.ngOnInit = function () {
        this.sidebar.addLink(this);
    };
    SidebarLinkDirective.prototype.toggle = function () {
        this.sidebarList.activeLinks = [];
        this.sidebarList.setList(this.sidebar.getLinks());
        var classList = this.el.nativeElement.classList;
        if (this.level.toString().trim() === "3") {
            this.active = true;
            this.sidebarList.toggleActiveList(this);
            this.sidebar.hideSidebar();
        }
        if (this.level.toString().trim() === "1" && !classList.contains("has-sub")) {
            this.sidebarList.collapseOtherLinks(this);
        }
        if (classList.contains("has-sub") && classList.contains("open")) {
            this.sidebarList.collapse(this);
        }
        else {
            if (classList.contains("has-sub")) {
                this.sidebarList.expand(this);
            }
        }
    };
    SidebarLinkDirective.ctorParameters = function () { return [
        { type: _sidebarlist_directive__WEBPACK_IMPORTED_MODULE_2__["SidebarListDirective"], decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"], args: [_sidebarlist_directive__WEBPACK_IMPORTED_MODULE_2__["SidebarListDirective"],] }] },
        { type: _sidebar_directive__WEBPACK_IMPORTED_MODULE_3__["SidebarDirective"], decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"], args: [_sidebar_directive__WEBPACK_IMPORTED_MODULE_3__["SidebarDirective"],] }] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Number)
    ], SidebarLinkDirective.prototype, "level", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], SidebarLinkDirective.prototype, "classes", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], SidebarLinkDirective.prototype, "parent", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], SidebarLinkDirective.prototype, "title", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], SidebarLinkDirective.prototype, "routePath", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], SidebarLinkDirective.prototype, "toggleEmit", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostBinding"])("class.open"),
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Boolean),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Boolean])
    ], SidebarLinkDirective.prototype, "open", null);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostBinding"])("class.nav-collapsed-open"),
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Boolean),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Boolean])
    ], SidebarLinkDirective.prototype, "navCollapsedOpen", null);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostBinding"])("class.active"),
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Boolean),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Boolean])
    ], SidebarLinkDirective.prototype, "active", null);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostBinding"])("class.is-shown"),
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Boolean),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Boolean])
    ], SidebarLinkDirective.prototype, "isShown", null);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostBinding"])("class.is-hidden"),
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Boolean),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Boolean])
    ], SidebarLinkDirective.prototype, "isHidden", null);
    SidebarLinkDirective = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Directive"])({ selector: "[appSidebarlink]" }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_sidebarlist_directive__WEBPACK_IMPORTED_MODULE_2__["SidebarListDirective"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_sidebar_directive__WEBPACK_IMPORTED_MODULE_3__["SidebarDirective"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_sidebarlist_directive__WEBPACK_IMPORTED_MODULE_2__["SidebarListDirective"],
            _sidebar_directive__WEBPACK_IMPORTED_MODULE_3__["SidebarDirective"],
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"]])
    ], SidebarLinkDirective);
    return SidebarLinkDirective;
}());



/***/ }),

/***/ "./src/app/shared/directives/sidebarlist.directive.ts":
/*!************************************************************!*\
  !*** ./src/app/shared/directives/sidebarlist.directive.ts ***!
  \************************************************************/
/*! exports provided: SidebarListDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SidebarListDirective", function() { return SidebarListDirective; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var SidebarListDirective = /** @class */ (function () {
    function SidebarListDirective() {
        this.navlinks = [];
        this.activeLinks = [];
    }
    SidebarListDirective.prototype.setList = function (list) {
        this.navlinks = list;
    };
    SidebarListDirective.prototype.collapse = function (link) {
        link.open = false;
        this.setIsHidden(link);
        if (link.level.toString().trim() === "2") {
            this.activeLinks.push(this.navlinks
                .filter(function (_) { return _.level.toString().trim() === "1" && _.open === true; })[0].title);
        }
        link.toggleEmit.emit(this.activeLinks);
    };
    SidebarListDirective.prototype.expand = function (link) {
        link.open = true;
        this.activeLinks.push(link.title);
        this.collapseOtherLinks(link);
        this.setIsShown(link);
        link.toggleEmit.emit(this.activeLinks);
    };
    SidebarListDirective.prototype.toggleActiveList = function (openLink) {
        this.navlinks
            .filter(function (_) { return _.level.toString().trim() === "3"; })
            .forEach(function (link) {
            if (link !== openLink) {
                link.active = false;
            }
        });
    };
    SidebarListDirective.prototype.collapseOtherLinks = function (openLink) {
        var _this = this;
        if (openLink.level.toString().trim() === "1") {
            this.navlinks
                .filter(function (_) {
                return (_.level.toString().trim() === openLink.level.toString().trim() ||
                    _.level.toString().trim() === "2") &&
                    _.open === true;
            })
                .forEach(function (link) {
                if (link !== openLink) {
                    link.open = false;
                    _this.setIsHidden(link);
                }
            });
        }
        else if (openLink.level.toString().trim() === "2") {
            this.activeLinks.push(this.navlinks
                .filter(function (_) { return _.level.toString().trim() === "1" && _.open === true; })[0].title);
            this.navlinks
                .filter(function (_) {
                return _.level.toString().trim() === openLink.level.toString().trim() &&
                    _.parent === openLink.parent &&
                    _.open === true;
            })
                .forEach(function (link) {
                if (link !== openLink) {
                    link.open = false;
                    _this.setIsHidden(link);
                }
            });
        }
    };
    SidebarListDirective.prototype.setIsShown = function (parentLink) {
        var childLevel = Number(parentLink.level) + 1;
        this.navlinks
            .filter(function (_) {
            return _.level.toString().trim() === childLevel.toString().trim() &&
                _.parent === parentLink.title;
        })
            .forEach(function (link) {
            link.isShown = true;
            link.isHidden = false;
        });
    };
    SidebarListDirective.prototype.setIsHidden = function (parentLink) {
        var childLevel = Number(parentLink.level) + 1;
        this.navlinks
            .filter(function (_) {
            return _.level.toString().trim() === childLevel.toString().trim() &&
                _.parent === parentLink.title;
        })
            .forEach(function (link) {
            link.isShown = false;
            link.isHidden = true;
        });
    };
    SidebarListDirective = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Directive"])({ selector: "[appSidebarList]" })
    ], SidebarListDirective);
    return SidebarListDirective;
}());



/***/ }),

/***/ "./src/app/shared/directives/sidebartoggle.directive.ts":
/*!**************************************************************!*\
  !*** ./src/app/shared/directives/sidebartoggle.directive.ts ***!
  \**************************************************************/
/*! exports provided: SidebarToggleDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SidebarToggleDirective", function() { return SidebarToggleDirective; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var SidebarToggleDirective = /** @class */ (function () {
    function SidebarToggleDirective(el, renderer) {
        this.el = el;
        this.renderer = renderer;
        this._dataToggle = "expanded";
        this._toggleRight = true;
        this._toggleLeft = false;
        this.fireRefreshEventOnWindow = function () {
            var evt = document.createEvent("HTMLEvents");
            evt.initEvent("resize", true, false);
            window.dispatchEvent(evt);
        };
    }
    Object.defineProperty(SidebarToggleDirective.prototype, "dataToggle", {
        get: function () {
            return this._dataToggle;
        },
        set: function (value) {
            this._dataToggle = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SidebarToggleDirective.prototype, "toggleRight", {
        get: function () {
            return this._toggleRight;
        },
        set: function (value) {
            this._toggleRight = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SidebarToggleDirective.prototype, "toggleLeft", {
        get: function () {
            return this._toggleLeft;
        },
        set: function (value) {
            this._toggleLeft = value;
        },
        enumerable: true,
        configurable: true
    });
    SidebarToggleDirective.prototype.ngAfterViewInit = function () {
        this.$wrapper = document.getElementsByClassName("wrapper")[0];
        this.compact_menu_checkbox = this.$wrapper.querySelector('.cz-compact-menu');
    };
    SidebarToggleDirective.prototype.onClick = function (e) {
        var _this = this;
        if (this.dataToggle === "expanded") {
            this.dataToggle = "collapsed";
            this.renderer.addClass(this.$wrapper, 'nav-collapsed');
            if (this.compact_menu_checkbox) {
                this.compact_menu_checkbox.checked = true;
            }
        }
        else {
            this.dataToggle = "expanded";
            this.renderer.removeClass(this.$wrapper, 'nav-collapsed');
            this.renderer.removeClass(this.$wrapper, 'menu-collapsed');
            if (this.compact_menu_checkbox) {
                this.compact_menu_checkbox.checked = false;
            }
        }
        this.toggleLeft = !this.toggleLeft;
        this.toggleRight = !this.toggleRight;
        setTimeout(function () {
            _this.fireRefreshEventOnWindow();
        }, 300);
    };
    SidebarToggleDirective.ctorParameters = function () { return [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer2"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostBinding"])("attr.data-toggle"),
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [String])
    ], SidebarToggleDirective.prototype, "dataToggle", null);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostBinding"])("class.ft-toggle-right"),
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Boolean),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Boolean])
    ], SidebarToggleDirective.prototype, "toggleRight", null);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostBinding"])("class.ft-toggle-left"),
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Boolean),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Boolean])
    ], SidebarToggleDirective.prototype, "toggleLeft", null);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])("click", ["$event"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
    ], SidebarToggleDirective.prototype, "onClick", null);
    SidebarToggleDirective = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Directive"])({ selector: "[appSidebarToggle]" }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"],
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer2"]])
    ], SidebarToggleDirective);
    return SidebarToggleDirective;
}());



/***/ }),

/***/ "./src/app/shared/directives/toggle-fullscreen.directive.ts":
/*!******************************************************************!*\
  !*** ./src/app/shared/directives/toggle-fullscreen.directive.ts ***!
  \******************************************************************/
/*! exports provided: ToggleFullscreenDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ToggleFullscreenDirective", function() { return ToggleFullscreenDirective; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var screenfull__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! screenfull */ "./node_modules/screenfull/dist/screenfull.js");
/* harmony import */ var screenfull__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(screenfull__WEBPACK_IMPORTED_MODULE_2__);



var ToggleFullscreenDirective = /** @class */ (function () {
    function ToggleFullscreenDirective() {
    }
    ToggleFullscreenDirective.prototype.onClick = function () {
        if (screenfull__WEBPACK_IMPORTED_MODULE_2__["enabled"]) {
            screenfull__WEBPACK_IMPORTED_MODULE_2__["toggle"]();
        }
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])('click'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", []),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
    ], ToggleFullscreenDirective.prototype, "onClick", null);
    ToggleFullscreenDirective = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Directive"])({
            selector: '[appToggleFullscreen]'
        })
    ], ToggleFullscreenDirective);
    return ToggleFullscreenDirective;
}());



/***/ }),

/***/ "./src/app/shared/footer/footer.component.scss":
/*!*****************************************************!*\
  !*** ./src/app/shared/footer/footer.component.scss ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9mb290ZXIvZm9vdGVyLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/shared/footer/footer.component.ts":
/*!***************************************************!*\
  !*** ./src/app/shared/footer/footer.component.ts ***!
  \***************************************************/
/*! exports provided: FooterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FooterComponent", function() { return FooterComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var FooterComponent = /** @class */ (function () {
    function FooterComponent() {
        //Variables
        this.currentDate = new Date();
    }
    FooterComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-footer',
            template: __webpack_require__(/*! raw-loader!./footer.component.html */ "./node_modules/raw-loader/index.js!./src/app/shared/footer/footer.component.html"),
            styles: [__webpack_require__(/*! ./footer.component.scss */ "./src/app/shared/footer/footer.component.scss")]
        })
    ], FooterComponent);
    return FooterComponent;
}());



/***/ }),

/***/ "./src/app/shared/navbar/navbar.component.scss":
/*!*****************************************************!*\
  !*** ./src/app/shared/navbar/navbar.component.scss ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "a:not([href]):not([tabindex]) {\n  color: white;\n  text-decoration: none; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hhcmVkL25hdmJhci9DOlxcZGV2ZWxvcGVyXFxiYWNrZW5kL3NyY1xcYXBwXFxzaGFyZWRcXG5hdmJhclxcbmF2YmFyLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksWUFBWTtFQUNaLHFCQUFxQixFQUFBIiwiZmlsZSI6InNyYy9hcHAvc2hhcmVkL25hdmJhci9uYXZiYXIuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJhOm5vdChbaHJlZl0pOm5vdChbdGFiaW5kZXhdKSB7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbn0iXX0= */"

/***/ }),

/***/ "./src/app/shared/navbar/navbar.component.ts":
/*!***************************************************!*\
  !*** ./src/app/shared/navbar/navbar.component.ts ***!
  \***************************************************/
/*! exports provided: NavbarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavbarComponent", function() { return NavbarComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var _services_layout_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/layout.service */ "./src/app/shared/services/layout.service.ts");
/* harmony import */ var _services_config_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../services/config.service */ "./src/app/shared/services/config.service.ts");
/* harmony import */ var app_webapi_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! app/webapi.service */ "./src/app/webapi.service.ts");







var NavbarComponent = /** @class */ (function () {
    function NavbarComponent(translate, api, router, layoutService, configService) {
        var _this = this;
        this.translate = translate;
        this.api = api;
        this.router = router;
        this.layoutService = layoutService;
        this.configService = configService;
        this.currentLang = "en";
        this.toggleClass = "ft-maximize";
        this.placement = "bottom-right";
        this.isCollapsed = true;
        this.toggleHideSidebar = new _angular_core__WEBPACK_IMPORTED_MODULE_2__["EventEmitter"]();
        this.config = {};
        this.admin = { name: '' };
        var browserLang = translate.getBrowserLang();
        translate.use(browserLang.match(/en|es|pt|de/) ? browserLang : "en");
        this.layoutSub = layoutService.changeEmitted$.subscribe(function (direction) {
            var dir = direction.direction;
            if (dir === "rtl") {
                _this.placement = "bottom-left";
            }
            else if (dir === "ltr") {
                _this.placement = "bottom-right";
            }
        });
    }
    NavbarComponent.prototype.ngOnInit = function () {
        this.config = this.configService.templateConf;
        this.admin = this.api.storage_get('logindata');
    };
    NavbarComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        if (this.config.layout.dir) {
            setTimeout(function () {
                var dir = _this.config.layout.dir;
                if (dir === "rtl") {
                    _this.placement = "bottom-left";
                }
                else if (dir === "ltr") {
                    _this.placement = "bottom-right";
                }
            }, 0);
        }
    };
    NavbarComponent.prototype.ngOnDestroy = function () {
        if (this.layoutSub) {
            this.layoutSub.unsubscribe();
        }
    };
    NavbarComponent.prototype.ChangeLanguage = function (language) {
        this.translate.use(language);
    };
    NavbarComponent.prototype.ToggleClass = function () {
        if (this.toggleClass === "ft-maximize") {
            this.toggleClass = "ft-minimize";
        }
        else {
            this.toggleClass = "ft-maximize";
        }
    };
    NavbarComponent.prototype.toggleNotificationSidebar = function () {
        this.layoutService.emitNotiSidebarChange(true);
    };
    NavbarComponent.prototype.toggleSidebar = function () {
        var appSidebar = document.getElementsByClassName("app-sidebar")[0];
        if (appSidebar.classList.contains("hide-sidebar")) {
            this.toggleHideSidebar.emit(false);
        }
        else {
            this.toggleHideSidebar.emit(true);
        }
    };
    NavbarComponent.prototype.logout = function () {
        this.api.storage_set("logindata", null);
        this.router.navigate(['/pages/login']);
    };
    NavbarComponent.prototype.goSetting = function () {
        this.router.navigate(['/pages/setting']);
    };
    NavbarComponent.ctorParameters = function () { return [
        { type: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_3__["TranslateService"] },
        { type: app_webapi_service__WEBPACK_IMPORTED_MODULE_6__["WebapiService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"] },
        { type: _services_layout_service__WEBPACK_IMPORTED_MODULE_4__["LayoutService"] },
        { type: _services_config_service__WEBPACK_IMPORTED_MODULE_5__["ConfigService"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], NavbarComponent.prototype, "toggleHideSidebar", void 0);
    NavbarComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: "app-navbar",
            template: __webpack_require__(/*! raw-loader!./navbar.component.html */ "./node_modules/raw-loader/index.js!./src/app/shared/navbar/navbar.component.html"),
            styles: [__webpack_require__(/*! ./navbar.component.scss */ "./src/app/shared/navbar/navbar.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ngx_translate_core__WEBPACK_IMPORTED_MODULE_3__["TranslateService"],
            app_webapi_service__WEBPACK_IMPORTED_MODULE_6__["WebapiService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _services_layout_service__WEBPACK_IMPORTED_MODULE_4__["LayoutService"],
            _services_config_service__WEBPACK_IMPORTED_MODULE_5__["ConfigService"]])
    ], NavbarComponent);
    return NavbarComponent;
}());



/***/ }),

/***/ "./src/app/shared/notification-sidebar/notification-sidebar.component.scss":
/*!*********************************************************************************!*\
  !*** ./src/app/shared/notification-sidebar/notification-sidebar.component.scss ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#notification-sidebar {\n  width: 400px;\n  right: -400px;\n  padding: 0;\n  background-color: #FFF;\n  z-index: 1051;\n  position: fixed;\n  top: 0;\n  bottom: 0;\n  height: 100vh;\n  transition: right 0.4s cubic-bezier(0.05, 0.74, 0.2, 0.99);\n  -webkit-backface-visibility: hidden;\n          backface-visibility: hidden;\n  border-left: 1px solid rgba(0, 0, 0, 0.05);\n  box-shadow: 0 0 8px rgba(0, 0, 0, 0.1); }\n  #notification-sidebar.open {\n    right: 0; }\n  #notification-sidebar .notification-sidebar-content {\n    position: relative;\n    height: 100%;\n    padding: 10px; }\n  #notification-sidebar .notification-sidebar-content #timeline.timeline-left .timeline-item:before {\n      border: none; }\n  #notification-sidebar .notification-sidebar-content #timeline.timeline-left .timeline-item:after {\n      border: none; }\n  #notification-sidebar .notification-sidebar-content #settings .switch {\n      border: none; }\n  #notification-sidebar a.notification-sidebar-toggle {\n    background: #FFF;\n    color: theme-color(\"primary\");\n    display: block;\n    box-shadow: -3px 0px 8px rgba(0, 0, 0, 0.1); }\n  #notification-sidebar a.notification-sidebar-close {\n    color: #000; }\n  #notification-sidebar .notification-sidebar-close {\n    position: absolute;\n    right: 10px;\n    top: 10px;\n    padding: 7px;\n    width: auto;\n    z-index: 10; }\n  #notification-sidebar .notification-sidebar-toggle {\n    position: absolute;\n    top: 35%;\n    width: 54px;\n    height: 50px;\n    left: -54px;\n    text-align: center;\n    line-height: 50px;\n    cursor: pointer; }\n  [dir=\"rtl\"] :host ::ng-deep #notification-sidebar {\n  left: -400px;\n  right: auto; }\n  [dir=\"rtl\"] :host ::ng-deep #notification-sidebar.open {\n    left: 0;\n    right: auto; }\n  [dir=\"rtl\"] :host ::ng-deep #notification-sidebar .notification-sidebar-close {\n    left: 10px;\n    right: auto; }\n  [dir=\"rtl\"] :host ::ng-deep #notification-sidebar .notification-sidebar-toggle {\n    right: -54px;\n    left: auto; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hhcmVkL25vdGlmaWNhdGlvbi1zaWRlYmFyL0M6XFxkZXZlbG9wZXJcXGJhY2tlbmQvc3JjXFxhcHBcXHNoYXJlZFxcbm90aWZpY2F0aW9uLXNpZGViYXJcXG5vdGlmaWNhdGlvbi1zaWRlYmFyLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9zaGFyZWQvbm90aWZpY2F0aW9uLXNpZGViYXIvbm90aWZpY2F0aW9uLXNpZGViYXIuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDQyxZQUFZO0VBQ1QsYUFBYTtFQUNoQixVQUFVO0VBQ1Asc0JBQXNCO0VBQ3pCLGFBQWE7RUFDVixlQUFlO0VBQ2YsTUFBTTtFQUNOLFNBQVM7RUFDVCxhQUFhO0VBQ2IsMERBQTBEO0VBQzFELG1DQUEyQjtVQUEzQiwyQkFBMkI7RUFDM0IsMENBQTBDO0VBQzFDLHNDQUFzQyxFQUFBO0VBYjFDO0lBZ0JFLFFBQVEsRUFBQTtFQWhCVjtJQXNCRSxrQkFBa0I7SUFDZixZQUFZO0lBQ1osYUFBYSxFQUFBO0VBeEJsQjtNQThCTSxZQUFXLEVBQUE7RUE5QmpCO01BaUNNLFlBQVcsRUFBQTtFQWpDakI7TUF5Q0ksWUFBWSxFQUFBO0VBekNoQjtJQWdERSxnQkFBZ0I7SUFDaEIsNkJBQTRCO0lBQzVCLGNBQWM7SUFDWCwyQ0FBMkMsRUFBQTtFQW5EaEQ7SUFzREssV0FBVyxFQUFBO0VBdERoQjtJQXlERSxrQkFBa0I7SUFDZixXQUFXO0lBQ1gsU0FBUztJQUNULFlBQVk7SUFDWixXQUFXO0lBQ1gsV0FBVyxFQUFBO0VBOURoQjtJQWlFRSxrQkFBa0I7SUFDZixRQUFRO0lBQ1IsV0FBVztJQUNYLFlBQVk7SUFDWixXQUFXO0lBQ1gsa0JBQWtCO0lBQ2xCLGlCQUFpQjtJQUNqQixlQUFlLEVBQUE7RUN0QnBCO0VEZ0NFLFlBQVk7RUFDWixXQUFXLEVBQUE7RUM5Qlg7SURnQ0MsT0FBTztJQUNQLFdBQVcsRUFBQTtFQzlCWjtJRGlDQyxVQUFVO0lBQ1YsV0FBVyxFQUFBO0VDL0JaO0lEa0NDLFlBQVk7SUFDWixVQUFVLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9zaGFyZWQvbm90aWZpY2F0aW9uLXNpZGViYXIvbm90aWZpY2F0aW9uLXNpZGViYXIuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIjbm90aWZpY2F0aW9uLXNpZGViYXJ7XG5cdHdpZHRoOiA0MDBweDtcbiAgICByaWdodDogLTQwMHB4O1xuXHRwYWRkaW5nOiAwO1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNGRkY7XG5cdHotaW5kZXg6IDEwNTE7XG4gICAgcG9zaXRpb246IGZpeGVkO1xuICAgIHRvcDogMDtcbiAgICBib3R0b206IDA7XG4gICAgaGVpZ2h0OiAxMDB2aDtcbiAgICB0cmFuc2l0aW9uOiByaWdodCAwLjRzIGN1YmljLWJlemllcigwLjA1LCAwLjc0LCAwLjIsIDAuOTkpO1xuICAgIGJhY2tmYWNlLXZpc2liaWxpdHk6IGhpZGRlbjtcbiAgICBib3JkZXItbGVmdDogMXB4IHNvbGlkIHJnYmEoMCwgMCwgMCwgMC4wNSk7XG4gICAgYm94LXNoYWRvdzogMCAwIDhweCByZ2JhKDAsIDAsIDAsIDAuMSk7XG5cblx0Ji5vcGVue1xuXHRcdHJpZ2h0OiAwO1xuXHR9XG5cblx0XG5cblx0Lm5vdGlmaWNhdGlvbi1zaWRlYmFyLWNvbnRlbnR7XG5cdFx0cG9zaXRpb246IHJlbGF0aXZlO1xuICAgIFx0aGVpZ2h0OiAxMDAlO1xuICAgIFx0cGFkZGluZzogMTBweDtcblxuICAgIFx0I3RpbWVsaW5le1xuICAgIFx0XHQmLnRpbWVsaW5lLWxlZnR7XG4gICAgXHRcdFx0LnRpbWVsaW5lLWl0ZW17XG4gICAgXHRcdFx0XHQmOmJlZm9yZXtcblx0XHRcdFx0XHRcdGJvcmRlcjpub25lO1xuICAgIFx0XHRcdFx0fVxuICAgIFx0XHRcdFx0JjphZnRlcntcblx0XHRcdFx0XHRcdGJvcmRlcjpub25lO1xuICAgIFx0XHRcdFx0fVxuICAgIFx0XHRcdH1cbiAgICBcdFx0fVxuXHRcdH1cblx0XHRcblx0XHQjc2V0dGluZ3N7XG5cdFx0XHQuc3dpdGNoe1xuXHRcdFx0XHRib3JkZXI6IG5vbmU7XG5cdFx0XHR9XG5cdFx0fVxuXG5cdH1cblxuXHRhLm5vdGlmaWNhdGlvbi1zaWRlYmFyLXRvZ2dsZXtcblx0XHRiYWNrZ3JvdW5kOiAjRkZGO1xuXHRcdGNvbG9yOnRoZW1lLWNvbG9yKCdwcmltYXJ5Jyk7XG5cdFx0ZGlzcGxheTogYmxvY2s7XG4gICAgXHRib3gtc2hhZG93OiAtM3B4IDBweCA4cHggcmdiYSgwLCAwLCAwLCAwLjEpO1xuXHR9XG5cdGEubm90aWZpY2F0aW9uLXNpZGViYXItY2xvc2Uge1xuICAgIFx0Y29sb3I6ICMwMDA7XG5cdH1cblx0Lm5vdGlmaWNhdGlvbi1zaWRlYmFyLWNsb3Nle1xuXHRcdHBvc2l0aW9uOiBhYnNvbHV0ZTtcblx0ICAgIHJpZ2h0OiAxMHB4O1xuXHQgICAgdG9wOiAxMHB4O1xuXHQgICAgcGFkZGluZzogN3B4O1xuXHQgICAgd2lkdGg6IGF1dG87XG5cdCAgICB6LWluZGV4OiAxMDtcblx0fVxuXHQubm90aWZpY2F0aW9uLXNpZGViYXItdG9nZ2xle1xuXHRcdHBvc2l0aW9uOiBhYnNvbHV0ZTtcblx0ICAgIHRvcDogMzUlO1xuXHQgICAgd2lkdGg6IDU0cHg7XG5cdCAgICBoZWlnaHQ6IDUwcHg7XG5cdCAgICBsZWZ0OiAtNTRweDtcblx0ICAgIHRleHQtYWxpZ246IGNlbnRlcjtcblx0ICAgIGxpbmUtaGVpZ2h0OiA1MHB4O1xuXHQgICAgY3Vyc29yOiBwb2ludGVyO1xuXHR9XG5cdFxuXG5cdFxuXG59XG5cbltkaXI9XCJydGxcIl0gOmhvc3QgOjpuZy1kZWVwe1xuXHQjbm90aWZpY2F0aW9uLXNpZGViYXJ7XHRcblx0XHRsZWZ0OiAtNDAwcHg7XHRcblx0XHRyaWdodDogYXV0bztcdCAgIFxuXHRcdCYub3Blbntcblx0XHRcdGxlZnQ6IDA7XG5cdFx0XHRyaWdodDogYXV0bztcblx0XHR9XG5cdFx0Lm5vdGlmaWNhdGlvbi1zaWRlYmFyLWNsb3Nle1xuXHRcdFx0bGVmdDogMTBweDtcblx0XHRcdHJpZ2h0OiBhdXRvO1xuXHRcdH1cblx0XHQubm90aWZpY2F0aW9uLXNpZGViYXItdG9nZ2xle1x0XG5cdFx0XHRyaWdodDogLTU0cHg7XG5cdFx0XHRsZWZ0OiBhdXRvO1xuXHRcdH1cdFxuXHR9XG59IiwiI25vdGlmaWNhdGlvbi1zaWRlYmFyIHtcbiAgd2lkdGg6IDQwMHB4O1xuICByaWdodDogLTQwMHB4O1xuICBwYWRkaW5nOiAwO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjRkZGO1xuICB6LWluZGV4OiAxMDUxO1xuICBwb3NpdGlvbjogZml4ZWQ7XG4gIHRvcDogMDtcbiAgYm90dG9tOiAwO1xuICBoZWlnaHQ6IDEwMHZoO1xuICB0cmFuc2l0aW9uOiByaWdodCAwLjRzIGN1YmljLWJlemllcigwLjA1LCAwLjc0LCAwLjIsIDAuOTkpO1xuICBiYWNrZmFjZS12aXNpYmlsaXR5OiBoaWRkZW47XG4gIGJvcmRlci1sZWZ0OiAxcHggc29saWQgcmdiYSgwLCAwLCAwLCAwLjA1KTtcbiAgYm94LXNoYWRvdzogMCAwIDhweCByZ2JhKDAsIDAsIDAsIDAuMSk7IH1cbiAgI25vdGlmaWNhdGlvbi1zaWRlYmFyLm9wZW4ge1xuICAgIHJpZ2h0OiAwOyB9XG4gICNub3RpZmljYXRpb24tc2lkZWJhciAubm90aWZpY2F0aW9uLXNpZGViYXItY29udGVudCB7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIGhlaWdodDogMTAwJTtcbiAgICBwYWRkaW5nOiAxMHB4OyB9XG4gICAgI25vdGlmaWNhdGlvbi1zaWRlYmFyIC5ub3RpZmljYXRpb24tc2lkZWJhci1jb250ZW50ICN0aW1lbGluZS50aW1lbGluZS1sZWZ0IC50aW1lbGluZS1pdGVtOmJlZm9yZSB7XG4gICAgICBib3JkZXI6IG5vbmU7IH1cbiAgICAjbm90aWZpY2F0aW9uLXNpZGViYXIgLm5vdGlmaWNhdGlvbi1zaWRlYmFyLWNvbnRlbnQgI3RpbWVsaW5lLnRpbWVsaW5lLWxlZnQgLnRpbWVsaW5lLWl0ZW06YWZ0ZXIge1xuICAgICAgYm9yZGVyOiBub25lOyB9XG4gICAgI25vdGlmaWNhdGlvbi1zaWRlYmFyIC5ub3RpZmljYXRpb24tc2lkZWJhci1jb250ZW50ICNzZXR0aW5ncyAuc3dpdGNoIHtcbiAgICAgIGJvcmRlcjogbm9uZTsgfVxuICAjbm90aWZpY2F0aW9uLXNpZGViYXIgYS5ub3RpZmljYXRpb24tc2lkZWJhci10b2dnbGUge1xuICAgIGJhY2tncm91bmQ6ICNGRkY7XG4gICAgY29sb3I6IHRoZW1lLWNvbG9yKFwicHJpbWFyeVwiKTtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgICBib3gtc2hhZG93OiAtM3B4IDBweCA4cHggcmdiYSgwLCAwLCAwLCAwLjEpOyB9XG4gICNub3RpZmljYXRpb24tc2lkZWJhciBhLm5vdGlmaWNhdGlvbi1zaWRlYmFyLWNsb3NlIHtcbiAgICBjb2xvcjogIzAwMDsgfVxuICAjbm90aWZpY2F0aW9uLXNpZGViYXIgLm5vdGlmaWNhdGlvbi1zaWRlYmFyLWNsb3NlIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgcmlnaHQ6IDEwcHg7XG4gICAgdG9wOiAxMHB4O1xuICAgIHBhZGRpbmc6IDdweDtcbiAgICB3aWR0aDogYXV0bztcbiAgICB6LWluZGV4OiAxMDsgfVxuICAjbm90aWZpY2F0aW9uLXNpZGViYXIgLm5vdGlmaWNhdGlvbi1zaWRlYmFyLXRvZ2dsZSB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHRvcDogMzUlO1xuICAgIHdpZHRoOiA1NHB4O1xuICAgIGhlaWdodDogNTBweDtcbiAgICBsZWZ0OiAtNTRweDtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgbGluZS1oZWlnaHQ6IDUwcHg7XG4gICAgY3Vyc29yOiBwb2ludGVyOyB9XG5cbltkaXI9XCJydGxcIl0gOmhvc3QgOjpuZy1kZWVwICNub3RpZmljYXRpb24tc2lkZWJhciB7XG4gIGxlZnQ6IC00MDBweDtcbiAgcmlnaHQ6IGF1dG87IH1cbiAgW2Rpcj1cInJ0bFwiXSA6aG9zdCA6Om5nLWRlZXAgI25vdGlmaWNhdGlvbi1zaWRlYmFyLm9wZW4ge1xuICAgIGxlZnQ6IDA7XG4gICAgcmlnaHQ6IGF1dG87IH1cbiAgW2Rpcj1cInJ0bFwiXSA6aG9zdCA6Om5nLWRlZXAgI25vdGlmaWNhdGlvbi1zaWRlYmFyIC5ub3RpZmljYXRpb24tc2lkZWJhci1jbG9zZSB7XG4gICAgbGVmdDogMTBweDtcbiAgICByaWdodDogYXV0bzsgfVxuICBbZGlyPVwicnRsXCJdIDpob3N0IDo6bmctZGVlcCAjbm90aWZpY2F0aW9uLXNpZGViYXIgLm5vdGlmaWNhdGlvbi1zaWRlYmFyLXRvZ2dsZSB7XG4gICAgcmlnaHQ6IC01NHB4O1xuICAgIGxlZnQ6IGF1dG87IH1cbiJdfQ== */"

/***/ }),

/***/ "./src/app/shared/notification-sidebar/notification-sidebar.component.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/shared/notification-sidebar/notification-sidebar.component.ts ***!
  \*******************************************************************************/
/*! exports provided: NotificationSidebarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotificationSidebarComponent", function() { return NotificationSidebarComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_layout_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/layout.service */ "./src/app/shared/services/layout.service.ts");



var NotificationSidebarComponent = /** @class */ (function () {
    function NotificationSidebarComponent(elRef, renderer, layoutService) {
        var _this = this;
        this.elRef = elRef;
        this.renderer = renderer;
        this.layoutService = layoutService;
        this.isOpen = false;
        this.layoutSub = layoutService.notiSidebarChangeEmitted$.subscribe(function (value) {
            if (_this.isOpen) {
                _this.renderer.removeClass(_this.sidebar.nativeElement, 'open');
                _this.isOpen = false;
            }
            else {
                _this.renderer.addClass(_this.sidebar.nativeElement, 'open');
                _this.isOpen = true;
            }
        });
    }
    NotificationSidebarComponent.prototype.ngOnInit = function () {
    };
    NotificationSidebarComponent.prototype.ngOnDestroy = function () {
        if (this.layoutSub) {
            this.layoutSub.unsubscribe();
        }
    };
    NotificationSidebarComponent.prototype.onClose = function () {
        this.renderer.removeClass(this.sidebar.nativeElement, 'open');
        this.isOpen = false;
    };
    NotificationSidebarComponent.ctorParameters = function () { return [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer2"] },
        { type: _services_layout_service__WEBPACK_IMPORTED_MODULE_2__["LayoutService"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('sidebar', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], NotificationSidebarComponent.prototype, "sidebar", void 0);
    NotificationSidebarComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-notification-sidebar',
            template: __webpack_require__(/*! raw-loader!./notification-sidebar.component.html */ "./node_modules/raw-loader/index.js!./src/app/shared/notification-sidebar/notification-sidebar.component.html"),
            styles: [__webpack_require__(/*! ./notification-sidebar.component.scss */ "./src/app/shared/notification-sidebar/notification-sidebar.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"],
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer2"],
            _services_layout_service__WEBPACK_IMPORTED_MODULE_2__["LayoutService"]])
    ], NotificationSidebarComponent);
    return NotificationSidebarComponent;
}());



/***/ }),

/***/ "./src/app/shared/routes/content-layout.routes.ts":
/*!********************************************************!*\
  !*** ./src/app/shared/routes/content-layout.routes.ts ***!
  \********************************************************/
/*! exports provided: CONTENT_ROUTES */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CONTENT_ROUTES", function() { return CONTENT_ROUTES; });
//Route for content layout without sidebar, navbar and footer for pages like Login, Registration etc...
var CONTENT_ROUTES = [
    {
        path: 'pages',
        loadChildren: function () { return Promise.all(/*! import() | pages-content-pages-content-pages-module */[__webpack_require__.e("default~charts-charts-module~pages-content-pages-content-pages-module"), __webpack_require__.e("pages-content-pages-content-pages-module")]).then(__webpack_require__.bind(null, /*! ../../pages/content-pages/content-pages.module */ "./src/app/pages/content-pages/content-pages.module.ts")).then(function (m) { return m.ContentPagesModule; }); }
    }
];


/***/ }),

/***/ "./src/app/shared/routes/full-layout.routes.ts":
/*!*****************************************************!*\
  !*** ./src/app/shared/routes/full-layout.routes.ts ***!
  \*****************************************************/
/*! exports provided: Full_ROUTES */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Full_ROUTES", function() { return Full_ROUTES; });
/* harmony import */ var app_team_team_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! app/team/team.component */ "./src/app/team/team.component.ts");
/* harmony import */ var app_saller_saller_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! app/saller/saller.component */ "./src/app/saller/saller.component.ts");
/* harmony import */ var app_customer_customer_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! app/customer/customer.component */ "./src/app/customer/customer.component.ts");
/* harmony import */ var app_product_product_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! app/product/product.component */ "./src/app/product/product.component.ts");
/* harmony import */ var app_order_order_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! app/order/order.component */ "./src/app/order/order.component.ts");
/* harmony import */ var app_news_news_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! app/news/news.component */ "./src/app/news/news.component.ts");
/* harmony import */ var app_settings_settings_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! app/settings/settings.component */ "./src/app/settings/settings.component.ts");
/* harmony import */ var app_payment_payment_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! app/payment/payment.component */ "./src/app/payment/payment.component.ts");
/* harmony import */ var app_confirm_confirm_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! app/confirm/confirm.component */ "./src/app/confirm/confirm.component.ts");
/* harmony import */ var app_score_score_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! app/score/score.component */ "./src/app/score/score.component.ts");










//Route for content layout with sidebar, navbar and footer.
var Full_ROUTES = [
    {
        path: 'dashboard',
        loadChildren: function () { return Promise.all(/*! import() | dashboard-dashboard-module */[__webpack_require__.e("default~charts-charts-module~dashboard-dashboard-module~pages-full-pages-full-pages-module"), __webpack_require__.e("dashboard-dashboard-module")]).then(__webpack_require__.bind(null, /*! ../../dashboard/dashboard.module */ "./src/app/dashboard/dashboard.module.ts")).then(function (m) { return m.DashboardModule; }); }
    },
    {
        path: 'order',
        component: app_order_order_component__WEBPACK_IMPORTED_MODULE_4__["OrderComponent"]
    },
    {
        path: 'team',
        component: app_team_team_component__WEBPACK_IMPORTED_MODULE_0__["TeamComponent"]
    },
    {
        path: 'saller',
        component: app_saller_saller_component__WEBPACK_IMPORTED_MODULE_1__["SallerComponent"]
    },
    {
        path: 'customer',
        component: app_customer_customer_component__WEBPACK_IMPORTED_MODULE_2__["CustomerComponent"]
    },
    {
        path: 'product',
        component: app_product_product_component__WEBPACK_IMPORTED_MODULE_3__["ProductComponent"]
    },
    {
        path: 'news',
        component: app_news_news_component__WEBPACK_IMPORTED_MODULE_5__["NewsComponent"]
    },
    {
        path: 'settings',
        component: app_settings_settings_component__WEBPACK_IMPORTED_MODULE_6__["settingsComponent"]
    },
    {
        path: 'payment',
        component: app_payment_payment_component__WEBPACK_IMPORTED_MODULE_7__["PaymentComponent"]
    },
    {
        path: 'confirm',
        component: app_confirm_confirm_component__WEBPACK_IMPORTED_MODULE_8__["ConfirmComponent"]
    },
    {
        path: 'score',
        component: app_score_score_component__WEBPACK_IMPORTED_MODULE_9__["ScoreComponent"]
    },
    {
        path: 'chat_sale',
        loadChildren: function () { return Promise.all(/*! import() | chat_sale-chat-module */[__webpack_require__.e("default~chat-chat-module~chat-ngrx-chat-ngrx-module~chat_sale-chat-module~player-player-module"), __webpack_require__.e("chat_sale-chat-module")]).then(__webpack_require__.bind(null, /*! ../../chat_sale/chat.module */ "./src/app/chat_sale/chat.module.ts")).then(function (m) { return m.ChatModule; }); }
    },
    {
        path: 'chat_customer',
        loadChildren: function () { return Promise.all(/*! import() | chat-chat-module */[__webpack_require__.e("default~chat-chat-module~chat-ngrx-chat-ngrx-module~chat_sale-chat-module~player-player-module"), __webpack_require__.e("chat-chat-module")]).then(__webpack_require__.bind(null, /*! ../../chat/chat.module */ "./src/app/chat/chat.module.ts")).then(function (m) { return m.ChatModule; }); }
    },
    {
        path: 'calendar',
        loadChildren: function () { return __webpack_require__.e(/*! import() | calendar-calendar-module */ "calendar-calendar-module").then(__webpack_require__.bind(null, /*! ../../calendar/calendar.module */ "./src/app/calendar/calendar.module.ts")).then(function (m) { return m.CalendarsModule; }); }
    },
    {
        path: 'charts',
        loadChildren: function () { return Promise.all(/*! import() | charts-charts-module */[__webpack_require__.e("default~charts-charts-module~dashboard-dashboard-module~pages-full-pages-full-pages-module"), __webpack_require__.e("default~cards-cards-module~charts-charts-module"), __webpack_require__.e("default~charts-charts-module~pages-content-pages-content-pages-module"), __webpack_require__.e("default~charts-charts-module~tables-tables-module"), __webpack_require__.e("charts-charts-module")]).then(__webpack_require__.bind(null, /*! ../../charts/charts.module */ "./src/app/charts/charts.module.ts")).then(function (m) { return m.ChartsNg2Module; }); }
    },
    {
        path: 'forms',
        loadChildren: function () { return __webpack_require__.e(/*! import() | forms-forms-module */ "forms-forms-module").then(__webpack_require__.bind(null, /*! ../../forms/forms.module */ "./src/app/forms/forms.module.ts")).then(function (m) { return m.FormModule; }); }
    },
    {
        path: 'maps',
        loadChildren: function () { return __webpack_require__.e(/*! import() | maps-maps-module */ "maps-maps-module").then(__webpack_require__.bind(null, /*! ../../maps/maps.module */ "./src/app/maps/maps.module.ts")).then(function (m) { return m.MapsModule; }); }
    },
    {
        path: 'tables',
        loadChildren: function () { return Promise.all(/*! import() | tables-tables-module */[__webpack_require__.e("default~charts-charts-module~tables-tables-module"), __webpack_require__.e("tables-tables-module")]).then(__webpack_require__.bind(null, /*! ../../tables/tables.module */ "./src/app/tables/tables.module.ts")).then(function (m) { return m.TablesModule; }); }
    },
    {
        path: 'datatables',
        loadChildren: function () { return __webpack_require__.e(/*! import() | data-tables-data-tables-module */ "data-tables-data-tables-module").then(__webpack_require__.bind(null, /*! ../../data-tables/data-tables.module */ "./src/app/data-tables/data-tables.module.ts")).then(function (m) { return m.DataTablesModule; }); }
    },
    {
        path: 'uikit',
        loadChildren: function () { return Promise.all(/*! import() | ui-kit-ui-kit-module */[__webpack_require__.e("default~components-ui-components-module~ui-kit-ui-kit-module"), __webpack_require__.e("ui-kit-ui-kit-module")]).then(__webpack_require__.bind(null, /*! ../../ui-kit/ui-kit.module */ "./src/app/ui-kit/ui-kit.module.ts")).then(function (m) { return m.UIKitModule; }); }
    },
    {
        path: 'components',
        loadChildren: function () { return Promise.all(/*! import() | components-ui-components-module */[__webpack_require__.e("default~components-ui-components-module~inbox-inbox-module"), __webpack_require__.e("default~components-ui-components-module~ui-kit-ui-kit-module"), __webpack_require__.e("components-ui-components-module")]).then(__webpack_require__.bind(null, /*! ../../components/ui-components.module */ "./src/app/components/ui-components.module.ts")).then(function (m) { return m.UIComponentsModule; }); }
    },
    {
        path: 'pages',
        loadChildren: function () { return Promise.all(/*! import() | pages-full-pages-full-pages-module */[__webpack_require__.e("default~charts-charts-module~dashboard-dashboard-module~pages-full-pages-full-pages-module"), __webpack_require__.e("pages-full-pages-full-pages-module")]).then(__webpack_require__.bind(null, /*! ../../pages/full-pages/full-pages.module */ "./src/app/pages/full-pages/full-pages.module.ts")).then(function (m) { return m.FullPagesModule; }); }
    },
    {
        path: 'cards',
        loadChildren: function () { return Promise.all(/*! import() | cards-cards-module */[__webpack_require__.e("default~cards-cards-module~charts-charts-module"), __webpack_require__.e("cards-cards-module")]).then(__webpack_require__.bind(null, /*! ../../cards/cards.module */ "./src/app/cards/cards.module.ts")).then(function (m) { return m.CardsModule; }); }
    },
    {
        path: 'colorpalettes',
        loadChildren: function () { return __webpack_require__.e(/*! import() | color-palette-color-palette-module */ "color-palette-color-palette-module").then(__webpack_require__.bind(null, /*! ../../color-palette/color-palette.module */ "./src/app/color-palette/color-palette.module.ts")).then(function (m) { return m.ColorPaletteModule; }); }
    },
    {
        path: 'chat-ngrx',
        loadChildren: function () { return Promise.all(/*! import() | chat-ngrx-chat-ngrx-module */[__webpack_require__.e("default~chat-chat-module~chat-ngrx-chat-ngrx-module~chat_sale-chat-module~player-player-module"), __webpack_require__.e("chat-ngrx-chat-ngrx-module")]).then(__webpack_require__.bind(null, /*! ../../chat-ngrx/chat-ngrx.module */ "./src/app/chat-ngrx/chat-ngrx.module.ts")).then(function (m) { return m.ChatNGRXModule; }); }
    },
    {
        path: 'inbox',
        loadChildren: function () { return Promise.all(/*! import() | inbox-inbox-module */[__webpack_require__.e("default~components-ui-components-module~inbox-inbox-module"), __webpack_require__.e("inbox-inbox-module")]).then(__webpack_require__.bind(null, /*! ../../inbox/inbox.module */ "./src/app/inbox/inbox.module.ts")).then(function (m) { return m.InboxModule; }); }
    },
    {
        path: 'taskboard',
        loadChildren: function () { return __webpack_require__.e(/*! import() | taskboard-taskboard-module */ "taskboard-taskboard-module").then(__webpack_require__.bind(null, /*! ../../taskboard/taskboard.module */ "./src/app/taskboard/taskboard.module.ts")).then(function (m) { return m.TaskboardModule; }); }
    },
    {
        path: 'taskboard-ngrx',
        loadChildren: function () { return __webpack_require__.e(/*! import() | taskboard-ngrx-taskboard-ngrx-module */ "taskboard-ngrx-taskboard-ngrx-module").then(__webpack_require__.bind(null, /*! ../../taskboard-ngrx/taskboard-ngrx.module */ "./src/app/taskboard-ngrx/taskboard-ngrx.module.ts")).then(function (m) { return m.TaskboardNGRXModule; }); }
    },
    {
        path: 'player',
        loadChildren: function () { return Promise.all(/*! import() | player-player-module */[__webpack_require__.e("default~chat-chat-module~chat-ngrx-chat-ngrx-module~chat_sale-chat-module~player-player-module"), __webpack_require__.e("player-player-module")]).then(__webpack_require__.bind(null, /*! ../../player/player.module */ "./src/app/player/player.module.ts")).then(function (m) { return m.PlayerModule; }); }
    }
];


/***/ }),

/***/ "./src/app/shared/services/config.service.ts":
/*!***************************************************!*\
  !*** ./src/app/shared/services/config.service.ts ***!
  \***************************************************/
/*! exports provided: ConfigService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConfigService", function() { return ConfigService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var ConfigService = /** @class */ (function () {
    function ConfigService() {
        this.setConfigValue();
    }
    ConfigService.prototype.setConfigValue = function () {
        this.templateConf = {
            layout: {
                variant: "Light",
                dir: "ltr",
                customizer: {
                    hidden: true //options: true, false
                },
                sidebar: {
                    collapsed: false,
                    size: "sidebar-sm",
                    backgroundColor: "black",
                    /* If you want transparent layout add any of the following mentioned classes to backgroundColor of sidebar option :
                      bg-glass-1, bg-glass-2, bg-glass-3, bg-glass-4, bg-hibiscus, bg-purple-pizzaz, bg-blue-lagoon, bg-electric-viloet, bg-protage, bg-tundora
                    */
                    backgroundImage: false,
                    backgroundImageURL: "assets/img/sidebar-bg/04.jpg"
                }
            }
        };
    };
    ConfigService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: "root"
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], ConfigService);
    return ConfigService;
}());



/***/ }),

/***/ "./src/app/shared/services/layout.service.ts":
/*!***************************************************!*\
  !*** ./src/app/shared/services/layout.service.ts ***!
  \***************************************************/
/*! exports provided: LayoutService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LayoutService", function() { return LayoutService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");



var LayoutService = /** @class */ (function () {
    function LayoutService() {
        this.emitChangeSource = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.changeEmitted$ = this.emitChangeSource.asObservable();
        //Customizer
        this.emitCustomizerSource = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.customizerChangeEmitted$ = this.emitCustomizerSource.asObservable();
        //customizer - compact menu
        this.emitCustomizerCMSource = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.customizerCMChangeEmitted$ = this.emitCustomizerCMSource.asObservable();
        //customizer - compact menu
        this.emitNotiSidebarSource = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.notiSidebarChangeEmitted$ = this.emitNotiSidebarSource.asObservable();
    }
    LayoutService.prototype.emitChange = function (change) {
        this.emitChangeSource.next(change);
    };
    LayoutService.prototype.emitCustomizerChange = function (change) {
        this.emitCustomizerSource.next(change);
    };
    LayoutService.prototype.emitCustomizerCMChange = function (change) {
        this.emitCustomizerCMSource.next(change);
    };
    LayoutService.prototype.emitNotiSidebarChange = function (change) {
        this.emitNotiSidebarSource.next(change);
    };
    LayoutService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        })
    ], LayoutService);
    return LayoutService;
}());



/***/ }),

/***/ "./src/app/shared/shared.module.ts":
/*!*****************************************!*\
  !*** ./src/app/shared/shared.module.ts ***!
  \*****************************************/
/*! exports provided: SharedModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SharedModule", function() { return SharedModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var ngx_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-perfect-scrollbar */ "./node_modules/ngx-perfect-scrollbar/dist/ngx-perfect-scrollbar.es5.js");
/* harmony import */ var _footer_footer_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./footer/footer.component */ "./src/app/shared/footer/footer.component.ts");
/* harmony import */ var _navbar_navbar_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./navbar/navbar.component */ "./src/app/shared/navbar/navbar.component.ts");
/* harmony import */ var _sidebar_sidebar_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./sidebar/sidebar.component */ "./src/app/shared/sidebar/sidebar.component.ts");
/* harmony import */ var _customizer_customizer_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./customizer/customizer.component */ "./src/app/shared/customizer/customizer.component.ts");
/* harmony import */ var _notification_sidebar_notification_sidebar_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./notification-sidebar/notification-sidebar.component */ "./src/app/shared/notification-sidebar/notification-sidebar.component.ts");
/* harmony import */ var _directives_toggle_fullscreen_directive__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./directives/toggle-fullscreen.directive */ "./src/app/shared/directives/toggle-fullscreen.directive.ts");
/* harmony import */ var _directives_sidebar_directive__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./directives/sidebar.directive */ "./src/app/shared/directives/sidebar.directive.ts");
/* harmony import */ var _directives_sidebarlink_directive__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./directives/sidebarlink.directive */ "./src/app/shared/directives/sidebarlink.directive.ts");
/* harmony import */ var _directives_sidebarlist_directive__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./directives/sidebarlist.directive */ "./src/app/shared/directives/sidebarlist.directive.ts");
/* harmony import */ var _directives_sidebaranchortoggle_directive__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./directives/sidebaranchortoggle.directive */ "./src/app/shared/directives/sidebaranchortoggle.directive.ts");
/* harmony import */ var _directives_sidebartoggle_directive__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./directives/sidebartoggle.directive */ "./src/app/shared/directives/sidebartoggle.directive.ts");







//COMPONENTS





//DIRECTIVES






var SharedModule = /** @class */ (function () {
    function SharedModule() {
    }
    SharedModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            exports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _footer_footer_component__WEBPACK_IMPORTED_MODULE_7__["FooterComponent"],
                _navbar_navbar_component__WEBPACK_IMPORTED_MODULE_8__["NavbarComponent"],
                _sidebar_sidebar_component__WEBPACK_IMPORTED_MODULE_9__["SidebarComponent"],
                _customizer_customizer_component__WEBPACK_IMPORTED_MODULE_10__["CustomizerComponent"],
                _notification_sidebar_notification_sidebar_component__WEBPACK_IMPORTED_MODULE_11__["NotificationSidebarComponent"],
                _directives_toggle_fullscreen_directive__WEBPACK_IMPORTED_MODULE_12__["ToggleFullscreenDirective"],
                _directives_sidebar_directive__WEBPACK_IMPORTED_MODULE_13__["SidebarDirective"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__["NgbModule"],
                _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__["TranslateModule"]
            ],
            imports: [
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__["NgbModule"],
                _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__["TranslateModule"],
                ngx_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_6__["PerfectScrollbarModule"]
            ],
            declarations: [
                _footer_footer_component__WEBPACK_IMPORTED_MODULE_7__["FooterComponent"],
                _navbar_navbar_component__WEBPACK_IMPORTED_MODULE_8__["NavbarComponent"],
                _sidebar_sidebar_component__WEBPACK_IMPORTED_MODULE_9__["SidebarComponent"],
                _customizer_customizer_component__WEBPACK_IMPORTED_MODULE_10__["CustomizerComponent"],
                _notification_sidebar_notification_sidebar_component__WEBPACK_IMPORTED_MODULE_11__["NotificationSidebarComponent"],
                _directives_toggle_fullscreen_directive__WEBPACK_IMPORTED_MODULE_12__["ToggleFullscreenDirective"],
                _directives_sidebar_directive__WEBPACK_IMPORTED_MODULE_13__["SidebarDirective"],
                _directives_sidebarlink_directive__WEBPACK_IMPORTED_MODULE_14__["SidebarLinkDirective"],
                _directives_sidebarlist_directive__WEBPACK_IMPORTED_MODULE_15__["SidebarListDirective"],
                _directives_sidebaranchortoggle_directive__WEBPACK_IMPORTED_MODULE_16__["SidebarAnchorToggleDirective"],
                _directives_sidebartoggle_directive__WEBPACK_IMPORTED_MODULE_17__["SidebarToggleDirective"]
            ]
        })
    ], SharedModule);
    return SharedModule;
}());



/***/ }),

/***/ "./src/app/shared/sidebar/sidebar-routes.config.ts":
/*!*********************************************************!*\
  !*** ./src/app/shared/sidebar/sidebar-routes.config.ts ***!
  \*********************************************************/
/*! exports provided: ROUTES */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ROUTES", function() { return ROUTES; });
//Sidebar menu Routes and data
var ROUTES = [
    { path: '/dashboard', title: 'หน้าแรก', icon: 'ft-home', class: '', badge: '', badgeClass: 'badge badge-pill badge-danger float-right mr-1 mt-1', isExternalLink: false, submenu: [] },
    { path: '/order', title: 'ออเดอร์', icon: 'ft-clipboard', class: '', badge: '', badgeClass: 'badge badge-pill badge-danger float-right mr-1 mt-1', isExternalLink: false, submenu: [] },
    { path: '/team', title: 'ทีมงาน', icon: 'ft-users', class: '', badge: '', badgeClass: 'badge badge-pill badge-danger float-right mr-1 mt-1', isExternalLink: false, submenu: [] },
    { path: '/saller', title: 'ผู้ขาย', icon: 'icon-user-following', class: '', badge: '', badgeClass: 'badge badge-pill badge-danger float-right mr-1 mt-1', isExternalLink: false, submenu: [] },
    { path: '/customer', title: 'ลูกค้า', icon: 'icon-user-follow', class: '', badge: '', badgeClass: 'badge badge-pill badge-danger float-right mr-1 mt-1', isExternalLink: false, submenu: [] },
    { path: '/chat_sale', title: 'แชทผู้ขาย', icon: 'ft-message-square', class: '', badge: '', badgeClass: 'badge badge-pill badge-danger float-right mr-1 mt-1', isExternalLink: false, submenu: [] },
    { path: '/chat_customer', title: 'แชทลูกค้า', icon: 'ft-message-square', class: '', badge: '', badgeClass: 'badge badge-pill badge-danger float-right mr-1 mt-1', isExternalLink: false, submenu: [] },
    { path: '/product', title: 'สินค้า', icon: 'ft-grid', class: '', badge: '', badgeClass: 'badge badge-pill badge-danger float-right mr-1 mt-1', isExternalLink: false, submenu: [] },
    { path: '/payment', title: 'การชำระเงิน', icon: 'ft-check-square', class: '', badge: '', badgeClass: 'badge badge-pill badge-danger float-right mr-1 mt-1', isExternalLink: false, submenu: [] },
    { path: '/confirm', title: 'โอนเงินผู้ขาย', icon: 'ft-check-circle', class: '', badge: '', badgeClass: 'badge badge-pill badge-danger float-right mr-1 mt-1', isExternalLink: false, submenu: [] },
    { path: '/score', title: 'เบิกแต้มสะสม', icon: 'ft-check-circle', class: '', badge: '', badgeClass: 'badge badge-pill badge-danger float-right mr-1 mt-1', isExternalLink: false, submenu: [] },
    { path: '/news', title: 'ข่าวสาร', icon: 'ft-mail', class: '', badge: '', badgeClass: 'badge badge-pill badge-danger float-right mr-1 mt-1', isExternalLink: false, submenu: [] },
    { path: '/settings', title: 'ตั้งค่า', icon: 'ft-settings', class: '', badge: '', badgeClass: 'badge badge-pill badge-danger float-right mr-1 mt-1', isExternalLink: false, submenu: [] },
];


/***/ }),

/***/ "./src/app/shared/sidebar/sidebar.component.ts":
/*!*****************************************************!*\
  !*** ./src/app/shared/sidebar/sidebar.component.ts ***!
  \*****************************************************/
/*! exports provided: SidebarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SidebarComponent", function() { return SidebarComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_fire_database__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/database */ "./node_modules/@angular/fire/database/index.js");
/* harmony import */ var _sidebar_routes_config__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./sidebar-routes.config */ "./src/app/shared/sidebar/sidebar-routes.config.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var _animations_custom_animations__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../animations/custom-animations */ "./src/app/shared/animations/custom-animations.ts");
/* harmony import */ var _services_config_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../services/config.service */ "./src/app/shared/services/config.service.ts");
/* harmony import */ var _services_layout_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../services/layout.service */ "./src/app/shared/services/layout.service.ts");
/* harmony import */ var app_webapi_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! app/webapi.service */ "./src/app/webapi.service.ts");










var SidebarComponent = /** @class */ (function () {
    function SidebarComponent(ref, firebase, elementRef, renderer, router, route, translate, configService, layoutService, api) {
        var _this = this;
        this.ref = ref;
        this.firebase = firebase;
        this.elementRef = elementRef;
        this.renderer = renderer;
        this.router = router;
        this.route = route;
        this.translate = translate;
        this.configService = configService;
        this.layoutService = layoutService;
        this.api = api;
        this.activeTitles = [];
        this.nav_collapsed_open = false;
        this.logoUrl = 'assets/img/logo.png';
        this.config = {};
        if (this.depth === undefined) {
            this.depth = 0;
            this.expanded = true;
        }
        this.layoutSub = layoutService.customizerChangeEmitted$.subscribe(function (options) {
            if (options) {
                if (options.bgColor) {
                    if (options.bgColor === 'white') {
                        _this.logoUrl = 'assets/img/logo-dark.png';
                    }
                    else {
                        _this.logoUrl = 'assets/img/logo.png';
                    }
                }
                if (options.compactMenu === true) {
                    _this.expanded = false;
                    _this.renderer.addClass(_this.toggleIcon.nativeElement, 'ft-toggle-left');
                    _this.renderer.removeClass(_this.toggleIcon.nativeElement, 'ft-toggle-right');
                    _this.nav_collapsed_open = true;
                }
                else if (options.compactMenu === false) {
                    _this.expanded = true;
                    _this.renderer.removeClass(_this.toggleIcon.nativeElement, 'ft-toggle-left');
                    _this.renderer.addClass(_this.toggleIcon.nativeElement, 'ft-toggle-right');
                    _this.nav_collapsed_open = false;
                }
            }
        });
    }
    SidebarComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.config = this.configService.templateConf;
        this.menuItems = _sidebar_routes_config__WEBPACK_IMPORTED_MODULE_3__["ROUTES"];
        if (this.config.layout.sidebar.backgroundColor === 'white') {
            this.logoUrl = 'assets/img/logo-dark.png';
        }
        else {
            this.logoUrl = 'assets/img/logo.png';
        }
        console.log(this.menuItems);
        // this.menuItems[1].badge = '20'+' new'
        this.api.fb_val('order', function (res) {
            console.log(res.node_.value_);
            _this.menuItems[1].badge = res.node_.value_;
        });
    };
    SidebarComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        setTimeout(function () {
            if (_this.config.layout.sidebar.collapsed != undefined) {
                if (_this.config.layout.sidebar.collapsed === true) {
                    _this.expanded = false;
                    _this.renderer.addClass(_this.toggleIcon.nativeElement, 'ft-toggle-left');
                    _this.renderer.removeClass(_this.toggleIcon.nativeElement, 'ft-toggle-right');
                    _this.nav_collapsed_open = true;
                }
                else if (_this.config.layout.sidebar.collapsed === false) {
                    _this.expanded = true;
                    _this.renderer.removeClass(_this.toggleIcon.nativeElement, 'ft-toggle-left');
                    _this.renderer.addClass(_this.toggleIcon.nativeElement, 'ft-toggle-right');
                    _this.nav_collapsed_open = false;
                }
            }
        }, 0);
    };
    SidebarComponent.prototype.ngOnDestroy = function () {
        if (this.layoutSub) {
            this.layoutSub.unsubscribe();
        }
    };
    SidebarComponent.prototype.toggleSlideInOut = function () {
        this.expanded = !this.expanded;
    };
    SidebarComponent.prototype.handleToggle = function (titles) {
        this.activeTitles = titles;
    };
    // NGX Wizard - skip url change
    SidebarComponent.prototype.ngxWizardFunction = function (path) {
        if (path.indexOf("forms/ngx") !== -1)
            this.router.navigate(["forms/ngx/wizard"], { skipLocationChange: false });
    };
    SidebarComponent.ctorParameters = function () { return [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"] },
        { type: _angular_fire_database__WEBPACK_IMPORTED_MODULE_2__["AngularFireDatabase"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer2"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"] },
        { type: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__["TranslateService"] },
        { type: _services_config_service__WEBPACK_IMPORTED_MODULE_7__["ConfigService"] },
        { type: _services_layout_service__WEBPACK_IMPORTED_MODULE_8__["LayoutService"] },
        { type: app_webapi_service__WEBPACK_IMPORTED_MODULE_9__["WebapiService"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('toggleIcon', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], SidebarComponent.prototype, "toggleIcon", void 0);
    SidebarComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "app-sidebar",
            template: __webpack_require__(/*! raw-loader!./sidebar.component.html */ "./node_modules/raw-loader/index.js!./src/app/shared/sidebar/sidebar.component.html"),
            animations: _animations_custom_animations__WEBPACK_IMPORTED_MODULE_6__["customAnimations"],
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"],
            _angular_fire_database__WEBPACK_IMPORTED_MODULE_2__["AngularFireDatabase"],
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"],
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer2"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"],
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__["TranslateService"],
            _services_config_service__WEBPACK_IMPORTED_MODULE_7__["ConfigService"],
            _services_layout_service__WEBPACK_IMPORTED_MODULE_8__["LayoutService"],
            app_webapi_service__WEBPACK_IMPORTED_MODULE_9__["WebapiService"]])
    ], SidebarComponent);
    return SidebarComponent;
}());



/***/ }),

/***/ "./src/app/team/add_team.scss":
/*!************************************!*\
  !*** ./src/app/team/add_team.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "body.layout-dark .modal .modal-header {\n  background: #374fe6; }\n\n.avatar {\n  width: 80px;\n  height: 80px;\n  border: solid 3px #fff;\n  border-radius: 50%; }\n\noption {\n  background-color: #fff; }\n\nlabel {\n  color: #000;\n  display: inline-block;\n  padding-right: 7px;\n  font-size: 16px;\n  width: 130px; }\n\n.form-control {\n  width: calc(100% - 130px);\n  display: inline-block; }\n\n.change_img {\n  width: 26px;\n  height: 26px;\n  font-size: 13px;\n  text-align: center;\n  border-radius: 50%;\n  position: absolute;\n  left: calc(50% - 13px);\n  top: 82px;\n  padding-left: 1px;\n  padding-top: 3px;\n  background-color: #fff; }\n\ntd img {\n  width: 50px; }\n\n::-webkit-input-placeholder {\n  /* Edge */\n  color: red; }\n\n:-ms-input-placeholder {\n  /* Internet Explorer 10-11 */\n  color: red; }\n\n::-moz-placeholder {\n  color: red !important; }\n\n::-ms-input-placeholder {\n  color: red !important; }\n\n::placeholder {\n  color: red !important; }\n\nsmall {\n  margin-left: 130px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdGVhbS9DOlxcZGV2ZWxvcGVyXFxiYWNrZW5kL3NyY1xcYXBwXFx0ZWFtXFxhZGRfdGVhbS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNFO0VBQ0UsbUJBQW1CLEVBQUE7O0FBRXJCO0VBQ0UsV0FBVztFQUNYLFlBQVk7RUFDWixzQkFBc0I7RUFDdEIsa0JBQWtCLEVBQUE7O0FBRXBCO0VBQ0Usc0JBQXNCLEVBQUE7O0FBRXZCO0VBQ0MsV0FBVztFQUNYLHFCQUFxQjtFQUNyQixrQkFBa0I7RUFDbEIsZUFBZTtFQUNmLFlBQVksRUFBQTs7QUFFZDtFQUNFLHlCQUF5QjtFQUN6QixxQkFBcUIsRUFBQTs7QUFFdkI7RUFDRSxXQUFXO0VBQ1gsWUFBWTtFQUNaLGVBQWU7RUFDZixrQkFBa0I7RUFDbEIsa0JBQWtCO0VBQ2xCLGtCQUFrQjtFQUNsQixzQkFBc0I7RUFDdEIsU0FBUztFQUNULGlCQUFpQjtFQUNqQixnQkFBZ0I7RUFDaEIsc0JBQXNCLEVBQUE7O0FBRXhCO0VBRUksV0FBVyxFQUFBOztBQUlmO0VBQThCLFNBQUE7RUFDNUIsVUFBVSxFQUFBOztBQUdaO0VBQXlCLDRCQUFBO0VBQ3ZCLFVBQVUsRUFBQTs7QUFHWjtFQUNFLHFCQUFxQixFQUFBOztBQUR2QjtFQUNFLHFCQUFxQixFQUFBOztBQUR2QjtFQUNFLHFCQUFxQixFQUFBOztBQUV2QjtFQUNFLGtCQUFrQixFQUFBIiwiZmlsZSI6InNyYy9hcHAvdGVhbS9hZGRfdGVhbS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiXHJcbiAgYm9keS5sYXlvdXQtZGFyayAubW9kYWwgLm1vZGFsLWhlYWRlciB7XHJcbiAgICBiYWNrZ3JvdW5kOiAjMzc0ZmU2O1xyXG4gIH1cclxuICAuYXZhdGFyIHtcclxuICAgIHdpZHRoOiA4MHB4O1xyXG4gICAgaGVpZ2h0OiA4MHB4O1xyXG4gICAgYm9yZGVyOiBzb2xpZCAzcHggI2ZmZjtcclxuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcclxuICB9XHJcbiAgb3B0aW9uIHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XHJcbiAgfVxyXG4gICBsYWJlbCB7XHJcbiAgICBjb2xvcjogIzAwMDtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgIHBhZGRpbmctcmlnaHQ6IDdweDtcclxuICAgIGZvbnQtc2l6ZTogMTZweDtcclxuICAgIHdpZHRoOiAxMzBweDtcclxuICB9XHJcbiAgLmZvcm0tY29udHJvbCB7XHJcbiAgICB3aWR0aDogY2FsYygxMDAlIC0gMTMwcHgpO1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gIH1cclxuICAuY2hhbmdlX2ltZyB7XHJcbiAgICB3aWR0aDogMjZweDtcclxuICAgIGhlaWdodDogMjZweDtcclxuICAgIGZvbnQtc2l6ZTogMTNweDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGxlZnQ6IGNhbGMoNTAlIC0gMTNweCk7XHJcbiAgICB0b3A6IDgycHg7XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDFweDtcclxuICAgIHBhZGRpbmctdG9wOiAzcHg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xyXG4gIH1cclxuICB0ZCB7XHJcbiAgICBpbWcge1xyXG4gICAgICB3aWR0aDogNTBweDtcclxuICAgIH1cclxuICB9XHJcbiAgXHJcbiAgOjotd2Via2l0LWlucHV0LXBsYWNlaG9sZGVyIHsgLyogRWRnZSAqL1xyXG4gICAgY29sb3I6IHJlZDtcclxuICB9XHJcbiAgXHJcbiAgOi1tcy1pbnB1dC1wbGFjZWhvbGRlciB7IC8qIEludGVybmV0IEV4cGxvcmVyIDEwLTExICovXHJcbiAgICBjb2xvcjogcmVkO1xyXG4gIH1cclxuICBcclxuICA6OnBsYWNlaG9sZGVyIHtcclxuICAgIGNvbG9yOiByZWQgIWltcG9ydGFudDtcclxuICB9XHJcbiAgc21hbGwgIHtcclxuICAgIG1hcmdpbi1sZWZ0OiAxMzBweDtcclxuICB9Il19 */"

/***/ }),

/***/ "./src/app/team/team.component.scss":
/*!******************************************!*\
  !*** ./src/app/team/team.component.scss ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".btn[class*=\"btn-\"] {\n  margin-bottom: 0rem; }\n\n.justify-content-center {\n  display: flex !important;\n  justify-content: center !important; }\n\n.searchform {\n  margin-right: 10px;\n  display: inline-block; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdGVhbS9DOlxcZGV2ZWxvcGVyXFxiYWNrZW5kL3NyY1xcYXBwXFx0ZWFtXFx0ZWFtLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsbUJBQW1CLEVBQUE7O0FBRXJCO0VBQ0Usd0JBQXdCO0VBQ3hCLGtDQUFrQyxFQUFBOztBQUVwQztFQUNFLGtCQUFrQjtFQUNsQixxQkFBcUIsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3RlYW0vdGVhbS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5idG5bY2xhc3MqPVwiYnRuLVwiXSB7XHJcbiAgbWFyZ2luLWJvdHRvbTogMHJlbTtcclxufVxyXG4uanVzdGlmeS1jb250ZW50LWNlbnRlciB7XHJcbiAgZGlzcGxheTogZmxleCAhaW1wb3J0YW50O1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyICFpbXBvcnRhbnQ7XHJcbn1cclxuLnNlYXJjaGZvcm0ge1xyXG4gIG1hcmdpbi1yaWdodDogMTBweDtcclxuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/team/team.component.ts":
/*!****************************************!*\
  !*** ./src/app/team/team.component.ts ***!
  \****************************************/
/*! exports provided: viewModalContent, NgbdModalContent, TeamComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "viewModalContent", function() { return viewModalContent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NgbdModalContent", function() { return NgbdModalContent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TeamComponent", function() { return TeamComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var app_webapi_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! app/webapi.service */ "./src/app/webapi.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");








var viewModalContent = /** @class */ (function () {
    // teamForm: FormGroup;
    function viewModalContent(activeModal, api) {
        this.activeModal = activeModal;
        this.api = api;
        this.admin_type = 0;
    }
    viewModalContent.prototype.close = function () {
        this.activeModal.dismiss();
    };
    viewModalContent.prototype.ngOnInit = function () {
        console.log("view");
        console.log(this.para);
        this.admin_type = this.api.storage_get('logindata').type;
        // this.teamForm = new FormGroup({
        //   'email': new FormControl(null, [Validators.required, Validators.email]),
        //   'username': new FormControl(null, [Validators.required, Validators.minLength(4), Validators.maxLength(24)]),
        //   'password': new FormControl(null, [Validators.required, Validators.minLength(4), Validators.maxLength(24)]),
        //   'name': new FormControl(null, [Validators.required, Validators.email, Validators.minLength(4)]),
        //   'phone': new FormControl(null, [Validators.required, Validators.email, Validators.minLength(9), Validators.minLength(10)]),
        // }, { updateOn: 'blur' });
        // this.teamForm.patchValue({username:'xxx'});
    };
    viewModalContent.prototype.c = function (val) {
        console.log(val);
    };
    viewModalContent.ctorParameters = function () { return [
        { type: _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbActiveModal"] },
        { type: app_webapi_service__WEBPACK_IMPORTED_MODULE_5__["WebapiService"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], viewModalContent.prototype, "para", void 0);
    viewModalContent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'view-modal-content',
            template: __webpack_require__(/*! raw-loader!./view_team.html */ "./node_modules/raw-loader/index.js!./src/app/team/view_team.html"),
            styles: [__webpack_require__(/*! ./view_team.scss */ "./src/app/team/view_team.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbActiveModal"], app_webapi_service__WEBPACK_IMPORTED_MODULE_5__["WebapiService"]])
    ], viewModalContent);
    return viewModalContent;
}());

var NgbdModalContent = /** @class */ (function () {
    function NgbdModalContent(activeModal, fb, cd, toastr, http, api) {
        this.activeModal = activeModal;
        this.fb = fb;
        this.cd = cd;
        this.toastr = toastr;
        this.http = http;
        this.api = api;
        this.admin_type = 0;
    }
    NgbdModalContent.prototype.file_change = function (event) {
        var _this = this;
        var reader = new FileReader();
        if (event.target.files && event.target.files.length) {
            var file = event.target.files[0];
            console.log(event.target.files);
            if (event.target.files[0].size < 2000000) {
                if (event.target.files[0].type == 'image/jpeg' || event.target.files[0].type == 'image/png') {
                    reader.readAsDataURL(file);
                    reader.onload = function () {
                        // console.log(reader.result);
                        // this.para.picture = reader.result;
                        // this.http.post(this.api.base_url + 'team/fileupload', { picture: reader.result }, {
                        //   reportProgress: true,
                        //   observe: 'events'
                        // })
                        _this.api.postData('team/upload', { data: reader.result, type: event.target.files[0].type }).then(function (res) {
                            console.log(res);
                            _this.para.picture = res.url;
                            _this.cd.detectChanges();
                        });
                        // let selectedFile = event.target.files[0];
                        // const uploadData = new FormData();
                        // uploadData.append('photo', selectedFile, selectedFile.name);
                        // this.http.post(this.api.base_url + 'team/fileupload', uploadData, {
                        //   reportProgress: true,
                        //   observe: 'events'
                        // })
                        //   .subscribe((e) => {
                        //     console.log(e);
                        //   })
                        // this.formGroup.patchValue({
                        //   file: reader.result
                        // });
                        // need to run CD since file load runs outside of zone
                        _this.cd.markForCheck();
                    };
                }
                else {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire('ต้องเป็นไฟล์ jpg หรือ png เท่านั้น', '', 'error');
                }
            }
            else {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire('ไฟล์ต้องมีขนาดน้อยกกว่า 2 Mb', '', 'error');
            }
        }
    };
    NgbdModalContent.prototype.change_picture = function () {
        console.log('changepic');
        this.file.nativeElement.click();
    };
    NgbdModalContent.prototype.onReactiveFormSubmit = function () {
        var _this = this;
        // console.log(this.teamForm.value);
        var data = this.teamForm.value;
        data.t_id = this.para.t_id;
        data.picture = this.para.picture;
        this.api.postData('team/add_team', this.teamForm.value).then(function (res) {
            console.log(res);
            if (res == '1') {
                _this.toastr.success('บันทึกเรียบรอ้แล้ว');
                _this.activeModal.close();
            }
            else {
                _this.toastr.error('มีปัญหาในการบันทึกข้อมูล');
            }
        });
        console.log("summit");
    };
    NgbdModalContent.prototype.close = function () {
        this.activeModal.dismiss();
    };
    NgbdModalContent.prototype.ngOnInit = function () {
        this.admin_type = this.api.storage_get('logindata').type;
        console.log("xxxx");
        console.log(this.para);
        if (this.para.t_id == '0') {
            this.para = {
                t_id: "0", username: "", password: "", name: "", email: '', phone: "",
                gender: "m",
                picture: "https://image.flaticon.com/icons/svg/194/194938.svg",
                type: "2"
            };
            this.teamForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
                'username': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(6), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(24), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[a-zA-Z0-9._-]+$')]),
                'password': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(6), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(24)]),
                'name': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(6)]),
                'email': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].email]),
                'phone': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(9), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(10), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('[0-9]+')]),
                gender: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
                type: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required])
            }, { updateOn: 'blur' });
        }
        else {
            this.teamForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
                'username': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(6), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(24), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[a-zA-Z0-9._-]+$')]),
                'password': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(6), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(24)]),
                'name': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(6)]),
                'email': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].email]),
                'phone': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(9), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(10), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('[0-9]+')]),
                gender: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
                type: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required])
            }, { updateOn: 'blur' });
        }
        this.teamForm.patchValue(this.para);
        this.teamForm.patchValue({ password: '' });
    };
    NgbdModalContent.prototype.c = function (val) {
        console.log(val);
    };
    NgbdModalContent.ctorParameters = function () { return [
        { type: _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbActiveModal"] },
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["ChangeDetectorRef"] },
        { type: ngx_toastr__WEBPACK_IMPORTED_MODULE_7__["ToastrService"] },
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpClient"] },
        { type: app_webapi_service__WEBPACK_IMPORTED_MODULE_5__["WebapiService"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], NgbdModalContent.prototype, "para", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["ViewChild"])("file", { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_2__["ElementRef"])
    ], NgbdModalContent.prototype, "file", void 0);
    NgbdModalContent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'ngbd-modal-content',
            template: __webpack_require__(/*! raw-loader!./add_team.html */ "./node_modules/raw-loader/index.js!./src/app/team/add_team.html"),
            styles: [__webpack_require__(/*! ./add_team.scss */ "./src/app/team/add_team.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbActiveModal"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"], _angular_core__WEBPACK_IMPORTED_MODULE_2__["ChangeDetectorRef"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_7__["ToastrService"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpClient"], app_webapi_service__WEBPACK_IMPORTED_MODULE_5__["WebapiService"]])
    ], NgbdModalContent);
    return NgbdModalContent;
}());

var TeamComponent = /** @class */ (function () {
    function TeamComponent(modalService, api) {
        this.modalService = modalService;
        this.api = api;
        this.admin_type = 0;
        this.teams = [];
        this.all_page = 0;
        this.page = 1;
        this.teams = this.api.storage_get('teams') || [];
        this.searchForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            'search': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('')
        });
    }
    TeamComponent.prototype.onReactiveFormSubmit = function () {
        var a = this.teamForm.value();
        console.log(a);
    };
    TeamComponent.prototype.ngOnInit = function () {
        this.api.check_login();
        this.teamForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            'inputEmail': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].email]),
            'password': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(4), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(24)]),
            'textArea': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            'radioOption': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('Option one is this')
        }, { updateOn: 'blur' });
        this.load_data(1);
        console.log('admin ' + this.api.storage_get('logindata').type);
        this.admin_type = this.api.storage_get('logindata').type;
    };
    TeamComponent.prototype.view = function (t) {
        var _this = this;
        console.log(t);
        var modal = this.modalService.open(viewModalContent);
        modal.componentInstance.para = t;
        modal.result.then(function (result) {
            console.log(result);
        }, function (reason) {
        });
        modal.result.then(function (result) {
            _this.closeResult = "Closed with: " + result;
        }, function (reason) {
            _this.closeResult = "Dismissed " + _this.getDismissReason(reason);
        });
    };
    TeamComponent.prototype.view_profile = function (t) {
        this.view(t);
    };
    TeamComponent.prototype.load_data = function (page) {
        var _this = this;
        this.api.getData('team/load_team/' + page + "?search=" + this.searchForm.value.search).then(function (data) {
            console.log(data);
            _this.teams = data.data;
            _this.all_page = data.count / 20 * 20;
            _this.api.storage_set('team', data.data);
        });
    };
    TeamComponent.prototype.search = function () {
        console.log(this.searchForm.value);
        this.load_data(1);
    };
    TeamComponent.prototype.open = function (t) {
        var _this = this;
        console.log(t);
        var modal = this.modalService.open(NgbdModalContent);
        modal.componentInstance.para = t;
        modal.result.then(function (result) {
            _this.load_data(_this.page);
        }, function (reason) {
            console.log(reason);
        });
        modal.result.then(function (result) {
            _this.closeResult = "Closed with: " + result;
        }, function (reason) {
            _this.closeResult = "Dismissed " + _this.getDismissReason(reason);
        });
    };
    TeamComponent.prototype.change_page = function (page) {
        this.load_data(page);
    };
    TeamComponent.prototype.delete_member = function (t) {
        var _this = this;
        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
            title: 'คุณต้องการลบจริงหรือไม่',
            text: "รายการนี้จะไม่สามารถนำกลับมาได้อีก",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#0CC27E',
            cancelButtonColor: '#FF586B',
            confirmButtonText: 'ลบออก',
            cancelButtonText: 'ปิดออก',
            confirmButtonClass: 'btn btn-success btn-raised mr-2',
            cancelButtonClass: 'btn btn-default btn-raised',
            buttonsStyling: false
        }).then(function (result) {
            console.log(t);
            if (result.value) {
                _this.api.postData('team/remove_team', { t_id: t.t_id }).then(function (res) {
                    console.log(res);
                    _this.load_data(_this.page);
                    sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire('ลบเรียบร้อยแล้ว', '', 'success');
                });
            }
        });
    };
    TeamComponent.prototype.getDismissReason = function (reason) {
        if (reason === _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["ModalDismissReasons"].ESC) {
            return 'by pressing ESC';
        }
        else if (reason === _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["ModalDismissReasons"].BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        }
        else {
            return "with: " + reason;
        }
    };
    TeamComponent.ctorParameters = function () { return [
        { type: _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbModal"] },
        { type: app_webapi_service__WEBPACK_IMPORTED_MODULE_5__["WebapiService"] }
    ]; };
    TeamComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-team',
            template: __webpack_require__(/*! raw-loader!./team.component.html */ "./node_modules/raw-loader/index.js!./src/app/team/team.component.html"),
            styles: [__webpack_require__(/*! ./team.component.scss */ "./src/app/team/team.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbModal"], app_webapi_service__WEBPACK_IMPORTED_MODULE_5__["WebapiService"]])
    ], TeamComponent);
    return TeamComponent;
}());



/***/ }),

/***/ "./src/app/team/view_team.scss":
/*!*************************************!*\
  !*** ./src/app/team/view_team.scss ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".content {\n  border: #fff solid 2px;\n  border-radius: 20px;\n  margin: 15px 5px;\n  padding: 20px; }\n\ndl {\n  padding-bottom: 0px; }\n\ndl dt {\n    font-weight: bold;\n    width: 120px;\n    text-align: right;\n    display: inline-block; }\n\ndl dt::after {\n    content: \":\"; }\n\ndl dd {\n    display: inline-block;\n    width: calc(100% - 120px);\n    color: #333333;\n    padding-left: 15px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdGVhbS9DOlxcZGV2ZWxvcGVyXFxiYWNrZW5kL3NyY1xcYXBwXFx0ZWFtXFx2aWV3X3RlYW0uc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLHNCQUFzQjtFQUN0QixtQkFBbUI7RUFDbkIsZ0JBQWdCO0VBQ2hCLGFBQWEsRUFBQTs7QUFHZjtFQWlCRSxtQkFBbUIsRUFBQTs7QUFqQnJCO0lBR0ksaUJBQWlCO0lBQ2pCLFlBQVk7SUFDWixpQkFBaUI7SUFDakIscUJBQXFCLEVBQUE7O0FBTnpCO0lBU0ksWUFBWSxFQUFBOztBQVRoQjtJQVlJLHFCQUFxQjtJQUNyQix5QkFBeUI7SUFDekIsY0FBYztJQUNkLGtCQUFrQixFQUFBIiwiZmlsZSI6InNyYy9hcHAvdGVhbS92aWV3X3RlYW0uc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jb250ZW50IHtcclxuICBib3JkZXI6ICNmZmYgc29saWQgMnB4O1xyXG4gIGJvcmRlci1yYWRpdXM6IDIwcHg7XHJcbiAgbWFyZ2luOiAxNXB4IDVweDtcclxuICBwYWRkaW5nOiAyMHB4O1xyXG59XHJcblxyXG5kbCB7XHJcbiAgZHQge1xyXG4gICAgICBcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgd2lkdGg6IDEyMHB4O1xyXG4gICAgdGV4dC1hbGlnbjogcmlnaHQ7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgfVxyXG4gIGR0OjphZnRlciB7XHJcbiAgICBjb250ZW50OiBcIjpcIjtcclxuICB9XHJcbiAgZGQge1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgd2lkdGg6IGNhbGMoMTAwJSAtIDEyMHB4KTtcclxuICAgIGNvbG9yOiAjMzMzMzMzO1xyXG4gICAgcGFkZGluZy1sZWZ0OiAxNXB4O1xyXG4gIH1cclxuICBwYWRkaW5nLWJvdHRvbTogMHB4O1xyXG59XHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/webapi.service.ts":
/*!***********************************!*\
  !*** ./src/app/webapi.service.ts ***!
  \***********************************/
/*! exports provided: WebapiService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WebapiService", function() { return WebapiService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var angular_web_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! angular-web-storage */ "./node_modules/angular-web-storage/fesm5/angular-web-storage.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_fire_database__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/fire/database */ "./node_modules/@angular/fire/database/index.js");






var WebapiService = /** @class */ (function () {
    function WebapiService(http, router, local, firebase) {
        this.http = http;
        this.router = router;
        this.local = local;
        this.firebase = firebase;
        this.base_url = "https://api2.concretedeliveryeasy.com/index.php/";
        this.dev = 'dev/';
    }
    WebapiService.prototype.fb_set = function (child) {
        this.firebase.database.ref(this.dev + child).set(Math.ceil(Math.random() * 100));
    };
    WebapiService.prototype.fb_setZero = function (child) {
        this.firebase.database.ref(this.dev + child).set(0);
    };
    WebapiService.prototype.fb_val = function (child, func) {
        this.firebase.database.ref(this.dev + child).on("value", function (val) {
            func(val.node_.value_);
        });
    };
    WebapiService.prototype.presentToast = function () {
    };
    WebapiService.prototype.storage_set = function (key, val) {
        this.local.set(key, val);
    };
    WebapiService.prototype.storage_get = function (key) {
        return this.local.get(key);
    };
    WebapiService.prototype.check_login = function () {
        if (!this.storage_get('logindata')) {
            this.router.navigate(['/pages/login']);
        }
    };
    WebapiService.prototype.postData = function (segment, objdata) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var headers = new Headers();
            headers.append("Accept", 'application/json');
            headers.append('Content-Type', 'application/json');
            _this.http.post(_this.base_url + segment, JSON.stringify(objdata))
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                if (err.status == 0) {
                    _this.presentToast();
                    reject(err);
                }
            });
        });
    };
    WebapiService.prototype.getData = function (segment) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.get(_this.base_url + segment)
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                if (err.status == 0) {
                    _this.presentToast();
                    reject(err);
                }
                reject(err);
            });
        });
    };
    WebapiService.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
        { type: angular_web_storage__WEBPACK_IMPORTED_MODULE_3__["LocalStorageService"] },
        { type: _angular_fire_database__WEBPACK_IMPORTED_MODULE_5__["AngularFireDatabase"] }
    ]; };
    WebapiService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"],
            angular_web_storage__WEBPACK_IMPORTED_MODULE_3__["LocalStorageService"],
            _angular_fire_database__WEBPACK_IMPORTED_MODULE_5__["AngularFireDatabase"]])
    ], WebapiService);
    return WebapiService;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false,
    firebase: {
        apiKey: "AIzaSyAscilrqRznimU8mwoLrufXfvo8m2VnQDc",
        authDomain: "concretedeliveryeasy-f16ad.firebaseapp.com",
        databaseURL: "https://concretedeliveryeasy-f16ad.firebaseio.com",
        projectId: "concretedeliveryeasy-f16ad",
        storageBucket: "concretedeliveryeasy-f16ad.appspot.com",
        messagingSenderId: "472114082705",
        appId: "1:472114082705:web:ba657314a370fce3936030",
        measurementId: "G-GV7WW66GQD"
    }
};


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"]);


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\developer\backend\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map