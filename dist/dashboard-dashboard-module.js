(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["dashboard-dashboard-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/dashboard/dashboard1/dashboard1.component.html":
/*!******************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/dashboard/dashboard1/dashboard1.component.html ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--Statistics cards Starts-->\r\n<div class=\"row\">\r\n\t<div class=\"col-xl-3 col-lg-6 col-md-6 col-12\">\r\n\t\t<div class=\"card gradient-blackberry\">\r\n\t\t\t<div class=\"card-content\">\r\n\t\t\t\t<div class=\"card-body pt-2 pb-2\">\r\n\t\t\t\t\t<div class=\"media\">\r\n\t\t\t\t\t\t<div class=\"media-body white text-left text-bold-400\">\r\n\t\t\t\t\t\t\t<h5>ยอดขาย</h5>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<div class=\"media-body white text-right\">\r\n\t\t\t\t\t\t\t<h5 class=\"font-medium-4 mb-0\">{{total.inc|number}} ฿</h5>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t</div>\r\n\t<div class=\"col-xl-3 col-lg-6 col-md-6 col-12\">\r\n\t\t<div class=\"card gradient-ibiza-sunset\">\r\n\t\t\t<div class=\"card-content\">\r\n\t\t\t\t<div class=\"card-body pt-2 pb-2\">\r\n\t\t\t\t\t<div class=\"media\">\r\n\t\t\t\t\t\t<div class=\"media-body white text-left text-bold-400\">\r\n\t\t\t\t\t\t\t<h5>รายจ่าย</h5>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<div class=\"media-body white text-right\">\r\n\t\t\t\t\t\t\t<h5 class=\"font-medium-4 mb-0\">{{total.exp|number}} ฿</h5>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t</div>\r\n\r\n\t<div class=\"col-xl-3 col-lg-6 col-md-6 col-12\">\r\n\t\t<div class=\"card gradient-green-tea\">\r\n\t\t\t<div class=\"card-content\">\r\n\t\t\t\t<div class=\"card-body pt-2 pb-2\">\r\n\t\t\t\t\t<div class=\"media\">\r\n\t\t\t\t\t\t<div class=\"media-body white text-left text-bold-400\">\r\n\t\t\t\t\t\t\t<h5>กำไร</h5>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<div class=\"media-body white text-right\">\r\n\t\t\t\t\t\t\t<h5 class=\"font-medium-4 mb-0\">{{total.profit|number}} ฿</h5>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t</div>\r\n\t<div class=\"col-xl-3 col-lg-6 col-md-6 col-12\">\r\n\t\t<div class=\"card gradient-pomegranate\">\r\n\t\t\t<div class=\"card-content\">\r\n\t\t\t\t<div class=\"card-body pt-2 pb-2\">\r\n\t\t\t\t\t<div class=\"media\">\r\n\t\t\t\t\t\t<div class=\"media-body white text-left text-bold-400\">\r\n\t\t\t\t\t\t\t<h5>ภาษี</h5>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<div class=\"media-body white text-right\">\r\n\t\t\t\t\t\t\t<h5 class=\"font-medium-4 mb-0\">{{total.tax|number}} ฿</h5>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t</div>\r\n</div>\r\n<!--Statistics cards Ends-->\r\n<div class=\"row\" matchHeight=\"card\">\r\n\t<div class=\"col-xl-4 col-lg-12 col-12\">\r\n\t\t<div class=\"card\">\r\n\t\t\t<div class=\"card-header\">\r\n\t\t\t\t<h4 class=\"card-title mb-0\">สมาชิกใหม่</h4>\r\n\t\t\t</div>\r\n\t\t\t<div class=\"card-content\">\r\n\t\t\t\t<div class=\"card-body\">\r\n\t\t\t\t\t<div class=\"media mb-1 list-user\" *ngFor=\"let u of users\">\r\n\t\t\t\t\t\t<div class=\"avatar\" [ngStyle]=\"{'background-image': 'url('+u.picture+')'}\">\r\n\t\t\t\t\t\t\t<!-- this is img part form css -->\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<div class=\"media-body\">\r\n\t\t\t\t\t\t\t<h4 class=\"font-medium-1 mt-1 mb-0\">{{u.name}}</h4>\r\n\t\t\t\t\t\t\t<p class=\"text-muted font-small-3\">{{u.phone}}</p>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<div class=\"mt-1\">\r\n\t\t\t\t\t\t\t<div class=\"custom-control custom-checkbox mb-2 mr-sm-2 mb-sm-0\">\r\n\t\t\t\t\t\t\t\t<span class=\"badge badge-primary\" *ngIf=\"u.type=='2'\">ผู้ขาย</span>\r\n\t\t\t\t\t\t\t\t<span class=\"badge badge-success\" *ngIf=\"u.type=='1'\">ลูกค้า</span>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t\t\r\n\t\t\t\t\t<!-- <div class=\"action-buttons mt-2 text-center\">\r\n\t\t\t\t\t\t<a class=\"btn btn-raised gradient-blackberry py-2 px-4 white mr-2\">Add New</a>\r\n\t\t\t\t\t</div> -->\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t</div>\r\n\t<div class=\"col-xl-8 col-lg-12 col-12\">\r\n\t\t<div class=\"card\">\r\n\t\t\t<div class=\"card-header\">\r\n\t\t\t\t<h4 class=\"card-title\">รายการที่กำลังประมูล</h4>\r\n\t\t\t</div>\r\n\t\t\t<div class=\"card-content\">\r\n\t\t\t\t<table class=\"table table-responsive-sm text-center\">\r\n\t\t\t\t\t<thead>\r\n\t\t\t\t\t\t<tr>\r\n\t\t\t\t\t\t\t<th>รูปภาพ</th>\r\n\t\t\t\t\t\t\t<th>สินค้า</th>\r\n\t\t\t\t\t\t\t<th>จำนวน</th>\r\n\t\t\t\t\t\t\t<th>สถานที่</th>\r\n\t\t\t\t\t\t\t<th>ราคาต่ำสุด</th>\r\n\t\t\t\t\t\t</tr>\r\n\t\t\t\t\t</thead>\r\n\t\t\t\t\t<tbody>\r\n\t\t\t\t\t\t<tr *ngFor=\"let o of orders\">\r\n\t\t\t\t\t\t\t<td><img class=\"media-object round-media height-50\"\r\n\t\t\t\t\t\t\t\t\tsrc=\"{{o.p_img}}\" [src]=\"o.p_img\"\r\n\t\t\t\t\t\t\t\t\talt=\"{{o.p_name}}\" /></td>\r\n\t\t\t\t\t\t\t<td>{{o.p_name}}</td>\r\n\t\t\t\t\t\t\t<td>{{o.count}}</td>\r\n\t\t\t\t\t\t\t<td>\r\n\t\t\t\t\t\t\t\t{{o.address}}\r\n\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t<td>{{o.min_price|number}} บาท</td>\r\n\t\t\t\t\t\t\r\n\t\t\t\t\t\t</tr>\r\n\t\t\t\t\t</tbody>\r\n\t\t\t\t</table>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t</div>\r\n</div>\r\n\r\n<!--Line with Area Chart 1 Starts-->\r\n\r\n\r\n<div class=\"row\" matchHeight=\"card\">\r\n\t<div class=\"col-xl-12 col-lg-12 col-12\">\r\n\t\t<!-- <app-confirm></app-confirm> -->\r\n\t\t<!-- <div class=\"card\">\r\n\t\t\t<div class=\"card-header\">\r\n\t\t\t\t<h4 class=\"card-title mb-0\">Visit & Sales Statistics</h4>\r\n\t\t\t</div>\r\n\t\t\t<div class=\"card-content\">\r\n\t\t\t\t<div class=\"card-body\">\r\n\t\t\t\t\t<div class=\"chart-info mb-2\">\r\n\t\t\t\t\t\t<span class=\"text-uppercase mr-3\"><i class=\"fa fa-circle primary font-small-2 mr-1\"></i>\r\n\t\t\t\t\t\t\tSales</span>\r\n\t\t\t\t\t\t<span class=\"text-uppercase\"><i class=\"fa fa-circle deep-purple font-small-2 mr-1\"></i>\r\n\t\t\t\t\t\t\tVisits</span>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t\t<div id=\"line-area2\" class=\"height-400 lineArea2\">\r\n\t\t\t\t\t\t<x-chartist class=\"\" [data]=\"lineArea2.data\" [type]=\"lineArea2.type\"\r\n\t\t\t\t\t\t\t[options]=\"lineArea2.options\" [responsiveOptions]=\"lineArea2.responsiveOptions\"\r\n\t\t\t\t\t\t\t[events]=\"lineArea2.events\">\r\n\r\n\t\t\t\t\t\t</x-chartist>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div> -->\r\n\t</div>\r\n\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/dashboard/dashboard2/dashboard2.component.html":
/*!******************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/dashboard/dashboard2/dashboard2.component.html ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\n  <div class=\"col-xl-3 col-lg-6 col-md-6 col-12\">\n    <div class=\"card bg-primary\">\n      <div class=\"card-content\">\n        <div class=\"card-body pt-2 pb-0\">\n          <div class=\"media\">\n            <div class=\"media-body white text-left\">\n              <h3 class=\"font-large-1 mb-0\">$15,678</h3>\n              <span>Total Cost</span>\n            </div>\n            <div class=\"media-right white text-right\">\n              <i class=\"icon-bulb font-large-1\"></i>\n            </div>\n          </div>\n        </div>\n        <div id=\"Widget-line-chart\" class=\"height-75 WidgetlineChart WidgetlineChartShadow mb-3\">\n          <x-chartist class=\"\" [data]=\"WidgetlineChart.data\" [type]=\"WidgetlineChart.type\" [options]=\"WidgetlineChart.options\"\n            [responsiveOptions]=\"WidgetlineChart.responsiveOptions\" [events]=\"WidgetlineChart.events\">\n          </x-chartist>\n        </div>\n      </div>\n    </div>\n  </div>\n  <div class=\"col-xl-3 col-lg-6 col-md-6 col-12\">\n    <div class=\"card bg-warning\">\n      <div class=\"card-content\">\n        <div class=\"card-body pt-2 pb-0\">\n          <div class=\"media\">\n            <div class=\"media-body white text-left\">\n              <h3 class=\"font-large-1 mb-0\">$2156</h3>\n              <span>Total Tax</span>\n            </div>\n            <div class=\"media-right white text-right\">\n              <i class=\"icon-pie-chart font-large-1\"></i>\n            </div>\n          </div>\n        </div>\n        <div id=\"Widget-line-chart2\" class=\"height-75 WidgetlineChart WidgetlineChartShadow mb-3\">\n          <x-chartist class=\"\" [data]=\"WidgetlineChart.data\" [type]=\"WidgetlineChart.type\" [options]=\"WidgetlineChart.options\"\n            [responsiveOptions]=\"WidgetlineChart.responsiveOptions\" [events]=\"WidgetlineChart.events\">\n          </x-chartist>\n        </div>\n      </div>\n    </div>\n  </div>\n\n  <div class=\"col-xl-3 col-lg-6 col-md-6 col-12\">\n    <div class=\"card bg-success\">\n      <div class=\"card-content\">\n        <div class=\"card-body pt-2 pb-0\">\n          <div class=\"media\">\n            <div class=\"media-body white text-left\">\n              <h3 class=\"font-large-1 mb-0\">$45,668</h3>\n              <span>Total Sales</span>\n            </div>\n            <div class=\"media-right white text-right\">\n              <i class=\"icon-graph font-large-1\"></i>\n            </div>\n          </div>\n        </div>\n        <div id=\"Widget-line-chart2\" class=\"height-75 WidgetlineChart WidgetlineChartShadow mb-3\">\n          <x-chartist class=\"\" [data]=\"WidgetlineChart.data\" [type]=\"WidgetlineChart.type\" [options]=\"WidgetlineChart.options\"\n            [responsiveOptions]=\"WidgetlineChart.responsiveOptions\" [events]=\"WidgetlineChart.events\">\n          </x-chartist>\n        </div>\n      </div>\n    </div>\n  </div>\n  <div class=\"col-xl-3 col-lg-6 col-md-6 col-12\">\n    <div class=\"card bg-danger\">\n      <div class=\"card-content\">\n        <div class=\"card-body pt-2 pb-0\">\n          <div class=\"media\">\n            <div class=\"media-body white text-left\">\n              <h3 class=\"font-large-1 mb-0\">$32,454</h3>\n              <span>Total Earning</span>\n            </div>\n            <div class=\"media-right white text-right\">\n              <i class=\"icon-wallet font-large-1\"></i>\n            </div>\n          </div>\n        </div>\n        <div id=\"Widget-line-chart2\" class=\"height-75 WidgetlineChart WidgetlineChartShadow mb-3\">\n          <x-chartist class=\"\" [data]=\"WidgetlineChart.data\" [type]=\"WidgetlineChart.type\" [options]=\"WidgetlineChart.options\"\n            [responsiveOptions]=\"WidgetlineChart.responsiveOptions\" [events]=\"WidgetlineChart.events\">\n          </x-chartist>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n<div class=\"row\" matchHeight=\"card\">\n  <div class=\"col-xl-4 col-lg-12\">\n    <div class=\"card\">\n      <div class=\"card-header\">\n        <h4 class=\"card-title mb-0\">Discover People</h4>\n      </div>\n      <div class=\"card-content\">\n        <div class=\"card-body\">\n          <div class=\"media mb-3\">\n            <img alt=\"96x96\" class=\"media-object d-flex mr-3 align-self-center bg-primary height-50 rounded-circle\" src=\"assets/img/portrait/small/avatar-s-12.png\">\n            <div class=\"media-body\">\n              <h4 class=\"font-medium-1 mt-2 mb-0\">Jessica Rice</h4>\n            </div>\n            <a class=\"d-flex ml-3 btn btn-raised btn-round gradient-blackberry py-2 width-150 justify-content-center white\">Following</a>\n          </div>\n          <div class=\"media mb-3\">\n            <img alt=\"96x96\" class=\"media-object d-flex mr-3 align-self-center bg-danger height-50 rounded-circle\" src=\"assets/img/portrait/small/avatar-s-11.png\">\n            <div class=\"media-body\">\n              <h4 class=\"font-medium-1 mt-2 mb-0\">Jacob Rios</h4>\n            </div>\n            <a class=\"d-flex ml-3 btn btn-raised btn-round btn-outline-grey py-2 width-150 justify-content-center\">Follow</a>\n          </div>\n          <div class=\"media mb-3\">\n            <img alt=\"96x96\" class=\"media-object d-flex mr-3 align-self-center bg-success height-50 rounded-circle\" src=\"assets/img/portrait/small/avatar-s-3.png\">\n            <div class=\"media-body\">\n              <h4 class=\"font-medium-1 mt-2 mb-0\">Russell Diaz</h4>\n            </div>\n            <a class=\"d-flex ml-3 btn btn-raised btn-round btn-outline-grey py-2 width-150 justify-content-center\">Follow</a>\n          </div>\n          <div class=\"media mb-3\">\n            <img alt=\"96x96\" class=\"media-object d-flex mr-3 align-self-center bg-warning height-50 rounded-circle\" src=\"assets/img/portrait/small/avatar-s-6.png\">\n            <div class=\"media-body\">\n              <h4 class=\"font-medium-1 mt-2 mb-0\">Sara Bell</h4>\n            </div>\n            <a class=\"d-flex ml-3 btn btn-raised btn-round gradient-blackberry py-2 width-150 justify-content-center white\">Following</a>\n          </div>\n          <div class=\"media mb-3\">\n            <img alt=\"96x96\" class=\"media-object d-flex mr-3 align-self-center bg-info height-50 rounded-circle\" src=\"assets/img/portrait/small/avatar-s-18.png\">\n            <div class=\"media-body\">\n              <h4 class=\"font-medium-1 mt-2 mb-0\">Janet Lucas</h4>\n            </div>\n            <a class=\"d-flex ml-3 btn btn-raised btn-round btn-outline-grey py-2 width-150 justify-content-center\">Follow</a>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n  <div class=\"col-xl-8 col-lg-12\">\n    <div class=\"card\">\n      <div class=\"card-header\">\n        <h4 class=\"card-title mb-0\">Sales Analysis</h4>\n      </div>\n      <div class=\"card-content\">\n        <div class=\"card-body\">\n          <div class=\"chart-info mb-3\">\n            <span class=\"text-uppercase mr-3\"><i class=\"fa fa-circle primary font-small-2 mr-1\"></i> Sales</span>\n            <span class=\"text-uppercase mr-3\"><i class=\"fa fa-circle warning font-small-2 mr-1\"></i> Visits</span>\n            <span class=\"text-uppercase\"><i class=\"fa fa-circle danger font-small-2 mr-1\"></i> clicks</span>\n          </div>\n          <div id=\"line-chart1\" class=\"height-350 lineChart1 lineChart1Shadow\">\n            <x-chartist class=\"\" [data]=\"lineChart1.data\" [type]=\"lineChart1.type\" [options]=\"lineChart1.options\"\n              [responsiveOptions]=\"lineChart1.responsiveOptions\" [events]=\"lineChart1.events\">\n            </x-chartist>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n<div class=\"row\" matchHeight=\"card\">\n  <div class=\"col-lg-4 col-md-12\">\n    <div class=\"card\">\n      <div class=\"card-header text-center pb-0\">\n        <span class=\"font-medium-2 primary\">Steps</span>\n        <h3 class=\"font-large-2 mt-1\">3261</h3>\n      </div>\n      <div class=\"card-content\">\n        <div id=\"donut-chart1\" class=\"height-250 donut1\">\n          <x-chartist [data]=\"DonutChart1.data\" [type]=\"DonutChart1.type\" [options]=\"DonutChart1.options\"\n            [responsiveOptions]=\"DonutChart1.responsiveOptions\" [events]=\"DonutChart1.events\">\n          </x-chartist>\n        </div>\n        <div class=\"card-body text-center\">\n          <span class=\"font-large-1 d-block mb-1\">5000</span>\n          <span class=\"primary font-medium-1\">Steps Today's Target</span>\n        </div>\n      </div>\n    </div>\n  </div>\n  <div class=\"col-lg-4 col-md-12\">\n    <div class=\"card\">\n      <div class=\"card-header text-center pb-0\">\n        <span class=\"font-medium-2 warning\">Distance</span>\n        <h3 class=\"font-large-2 mt-1\">7.6\n          <span class=\"font-medium-1 grey darken-1 text-bold-400\">miles</span>\n        </h3>\n      </div>\n      <div class=\"card-content\">\n        <div id=\"donut-chart2\" class=\"height-250 donut2\">\n          <x-chartist [data]=\"DonutChart2.data\" [type]=\"DonutChart2.type\" [options]=\"DonutChart2.options\"\n            [responsiveOptions]=\"DonutChart2.responsiveOptions\" [events]=\"DonutChart2.events\">\n          </x-chartist>\n        </div>\n        <div class=\"card-body text-center\">\n          <span class=\"font-large-1 d-block mb-1\">10</span>\n          <span class=\"warning font-medium-1\">Miles Today's Target</span>\n        </div>\n      </div>\n    </div>\n  </div>\n  <div class=\"col-lg-4 col-md-12\">\n    <div class=\"card\">\n      <div class=\"card-header text-center pb-0\">\n        <span class=\"font-medium-2 danger\">Calories</span>\n        <h3 class=\"font-large-2 mt-1\">4,025\n          <span class=\"font-medium-1 grey darken-1 text-bold-400\">kcal</span>\n        </h3>\n      </div>\n      <div class=\"card-content\">\n        <div id=\"donut-chart3\" class=\"height-250 donut3\">\n          <x-chartist [data]=\"DonutChart3.data\" [type]=\"DonutChart3.type\" [options]=\"DonutChart3.options\"\n            [responsiveOptions]=\"DonutChart3.responsiveOptions\" [events]=\"DonutChart3.events\">\n          </x-chartist>\n        </div>\n        <div class=\"card-body text-center\">\n          <span class=\"font-large-1 d-block mb-1\">5000</span>\n          <span class=\"danger font-medium-1\">kcla Today's Target</span>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n<div class=\"row\" matchHeight=\"card\">\n  <div class=\"col-xl-6 col-lg-12 col-sm-12\">\n    <div class=\"card\">\n      <div class=\"card-content\">\n        <div class=\"card-img\">\n          <img class=\"card-img-top img-fluid height-300\" src=\"assets/img/photos/weather-1.jpg\" alt=\"Card image cap\">\n          <h4 class=\"card-title\">Sunny</h4>\n          <a class=\"btn btn-floating halfway-fab bg-primary\"><i class=\"ft-plus\"></i></a>\n        </div>\n        <div class=\"card-body mt-2\">\n          <div class=\"row\">\n            <div class=\"col-2 text-center\">\n              <span class=\"d-block\">Mon</span>\n              <i class=\"wi wi-day-sunny d-block warning font-large-1 my-3\"></i>\n              <span class=\"d-block\">13&deg;</span>\n            </div>\n            <div class=\"col-2 text-center\">\n              <span class=\"d-block\">Tue</span>\n              <i class=\"wi wi-day-cloudy d-block warning font-large-1 my-3\"></i>\n              <span class=\"d-block\">12&deg;</span>\n            </div>\n            <div class=\"col-2 text-center\">\n              <span class=\"d-block\">Wed</span>\n              <i class=\"wi wi-day-cloudy-gusts d-block warning font-large-1 my-3\"></i>\n              <span class=\"d-block\">10&deg;</span>\n            </div>\n            <div class=\"col-2 text-center\">\n              <span class=\"d-block\">Thu</span>\n              <i class=\"wi wi-day-cloudy-windy d-block warning font-large-1 my-3\"></i>\n              <span class=\"d-block\">12&deg;</span>\n            </div>\n            <div class=\"col-2 text-center\">\n              <span class=\"d-block\">Fri</span>\n              <i class=\"wi wi-day-fog d-block warning font-large-1 my-3\"></i>\n              <span class=\"d-block\">9&deg;</span>\n            </div>\n            <div class=\"col-2 text-center\">\n              <span class=\"d-block\">Sat</span>\n              <i class=\"wi wi-day-lightning d-block warning font-large-1 my-3\"></i>\n              <span class=\"d-block\">6&deg;</span>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n\n  <div class=\"col-xl-6 col-lg-12\">\n    <div class=\"card\">\n      <div class=\"card-header pb-0\">\n        <h4 class=\"card-title\">Statistics</h4>\n      </div>\n      <div class=\"card-content\">\n        <div class=\"card-body\">\n          <div class=\"chart-info mb-2\">\n            <span class=\"text-uppercase mr-3\"><i class=\"fa fa-circle primary font-small-2 mr-1\"></i> Sales</span>\n            <span class=\"text-uppercase\"><i class=\"fa fa-circle warning font-small-2 mr-1\"></i> Visits</span>\n          </div>\n          <div id=\"line-chart2\" class=\"height-350 lineChart2 lineChart2Shadow\">\n            <x-chartist class=\"\" [data]=\"lineChart2.data\" [type]=\"lineChart2.type\" [options]=\"lineChart2.options\"\n              [responsiveOptions]=\"lineChart2.responsiveOptions\" [events]=\"lineChart2.events\">\n            </x-chartist>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n<div class=\"row mb-3\" matchHeight=\"card\">\n  <div class=\"col-xl-4 col-lg-12\">\n    <div class=\"card\">\n      <div class=\"card-content\">\n        <div class=\"card-body\">\n          <div class=\"row d-flex mb-3 py-2\">\n            <div class=\"col align-self-center text-center\"><i class=\"icon-graph font-large-2 blue-grey lighten-2\"></i></div>\n            <div class=\"col align-self-center\"><img alt=\"96x96\" class=\"bg-danger width-150 rounded-circle img-fluid\"\n                src=\"assets/img/portrait/small/avatar-s-11.png\"></div>\n            <div class=\"col align-self-center text-center\"><i class=\"icon-envelope font-large-2 blue-grey lighten-2\"></i></div>\n          </div>\n          <h3 class=\"font-large-1 text-center\">Kevin Sullivan</h3>\n          <span class=\"font-medium-1 grey d-block text-center\">UX Designer</span>\n          <div class=\"row mt-4 mb-3\">\n            <div class=\"col-xl-7 col-8 \">\n              <div id=\"Widget-line-chart1\" class=\"height-75 WidgetlineChart1 WidgetlineChart1Shadow px-2\">\n                <x-chartist class=\"\" [data]=\"WidgetlineChart1.data\" [type]=\"WidgetlineChart1.type\" [options]=\"WidgetlineChart1.options\"\n                  [responsiveOptions]=\"WidgetlineChart1.responsiveOptions\" [events]=\"WidgetlineChart1.events\">\n                </x-chartist>\n              </div>\n            </div>\n            <div class=\"col-xl-5 col-4\">\n              <span class=\"font-large-1\"><i class=\"fa fa-caret-up font-large-2 success\"></i> 27 %</span>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n  <div class=\"col-xl-4 col-lg-12\">\n    <div class=\"card\">\n      <div class=\"card-content\">\n        <div class=\"card-img\">\n          <ngb-carousel>\n            <ng-template ngbSlide>\n              <img src=\"assets/img/photos/17.jpg\" alt=\"Random first slide\">\n              <div class=\"carousel-caption\">\n                <h3>First slide label</h3>\n                <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>\n              </div>\n            </ng-template>\n            <ng-template ngbSlide>\n              <img src=\"assets/img/photos/13.jpg\" alt=\"Random second slide\">\n              <div class=\"carousel-caption\">\n                <h3>Second slide label</h3>\n                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>\n              </div>\n            </ng-template>\n            <ng-template ngbSlide>\n              <img src=\"assets/img/photos/12.jpg\" alt=\"Random third slide\">\n              <div class=\"carousel-caption\">\n                <h3>Third slide label</h3>\n                <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>\n              </div>\n            </ng-template>\n          </ngb-carousel>\n          <a class=\"btn btn-floating halfway-fab btn-large gradient-blackberry\"><i class=\"ft-plus\"></i></a>\n        </div>\n        <div class=\"card-body mt-3\">\n          <h4 class=\"card-title\">Card title</h4>\n          <p class=\"card-text\">Sweet halvah dragée jelly-o halvah carrot cake oat cake. Donut jujubes jelly chocolate\n            cake.</p>\n        </div>\n      </div>\n    </div>\n  </div>\n  <div class=\"col-xl-4 col-lg-12\">\n    <div class=\"card\">\n      <div class=\"card-header\">\n        <h4 class=\"card-title\">Earnings</h4>\n        <span class=\"grey\">Mon 18 - Sun 21</span>\n      </div>\n      <div class=\"card-content\">\n        <div class=\"card-body\">\n          <div class=\"earning-details mb-4\">\n            <h3 class=\"font-large-2 mb-1\">$4295.36 <i class=\"ft-arrow-up font-large-2 teal accent-3\"></i></h3>\n            <span class=\"font-medium-1 grey d-block\">Total Earnings</span>\n          </div>\n          <div id=\"Widget-line-chart2\" class=\"height-100 WidgetlineChart2 WidgetlineChart2Shadow\">\n            <x-chartist class=\"\" [data]=\"WidgetlineChart2.data\" [type]=\"WidgetlineChart2.type\" [options]=\"WidgetlineChart2.options\"\n              [responsiveOptions]=\"WidgetlineChart2.responsiveOptions\" [events]=\"WidgetlineChart2.events\">\n            </x-chartist>\n          </div>\n          <div class=\"action-buttons mt-4 mb-1 text-center\">\n            <a class=\"btn btn-raised gradient-blackberry py-2 px-4 white mr-2\">View Full</a>\n            <a class=\"btn btn-raised btn-outline-grey py-2 px-3\">Print</a>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n<div class=\"row\" matchHeight=\"card\">\n  <div class=\"col-lg-4 col-md-4 col-sm-12 mb-2\">\n    <div class=\"card card-inverse bg-primary text-center\">\n      <div class=\"card-content\">\n        <div class=\"card-img overlap\">\n          <img src=\"assets/img/elements/11.png\" alt=\"element 06\" width=\"190\" class=\"mb-1\">\n        </div>\n        <div class=\"card-body\">\n          <h4 class=\"card-title\">New Arrival</h4>\n          <p class=\"card-text\">Donut toffee candy brownie soufflé macaroon.</p>\n        </div>\n      </div>\n    </div>\n  </div>\n\n  <div class=\"col-lg-4 col-md-4 col-sm-12 mb-2\">\n    <div class=\"card card-inverse bg-danger text-center\">\n      <div class=\"card-content\">\n        <div class=\"card-img overlap\">\n          <img src=\"assets/img/elements/14.png\" alt=\"element 03\" width=\"170\">\n        </div>\n        <div class=\"card-body\">\n          <h4 class=\"card-title\">Brand Minute</h4>\n          <p class=\"card-text\">Donut toffee candy brownie soufflé macaroon.</p>\n        </div>\n      </div>\n    </div>\n  </div>\n\n  <div class=\"col-lg-4 col-md-4 col-sm-12 mb-2\">\n    <div class=\"card card-inverse bg-warning text-center\">\n      <div class=\"card-content\">\n        <div class=\"card-img overlap\">\n          <img src=\"assets/img/elements/07.png\" alt=\"element 07\" width=\"225\">\n        </div>\n        <div class=\"card-body\">\n          <h4 class=\"card-title\">Brand Minute</h4>\n          <p class=\"card-text\">Donut toffee candy brownie soufflé macaroon.</p>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n<div class=\"row\" matchHeight=\"card\">\n  <div class=\"col-xl-8 col-lg-12\">\n    <div class=\"card\">\n      <div class=\"card-header pb-0\">\n        <h4 class=\"card-title\">Sales Per Visit</h4>\n      </div>\n      <div class=\"card-content\">\n        <div class=\"card-body\">\n          <div class=\"chart-info mb-2\">\n            <span class=\"text-uppercase mr-3\"><i class=\"fa fa-circle primary font-small-2 mr-1\"></i> Sales</span>\n            <span class=\"text-uppercase\"><i class=\"fa fa-circle warning font-small-2 mr-1\"></i> Visits</span>\n          </div>\n          <div id=\"line-area-chart\" class=\"height-300 lineAreaChart mb-1\">\n            <x-chartist class=\"\" [data]=\"lineAreaChart.data\" [type]=\"lineAreaChart.type\" [options]=\"lineAreaChart.options\"\n              [responsiveOptions]=\"lineAreaChart.responsiveOptions\" [events]=\"lineAreaChart.events\">\n\n            </x-chartist>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n  <div class=\"col-xl-4 col-lg-12\">\n    <div class=\"card\">\n      <div class=\"card-content\">\n        <div class=\"card-body\">\n          <h4 class=\"card-title\">DAILY DIET</h4>\n          <p class=\"card-text\">Some quick example text to build on the card.</p>\n        </div>\n        <ul class=\"list-group\">\n          <li class=\"list-group-item text-left\">\n            <span class=\"badge bg-primary float-right text-white\">4</span> Protein Milk\n          </li>\n          <li class=\"list-group-item text-left\">\n            <span class=\"badge bg-info float-right text-white\">2</span> oz Water\n          </li>\n          <li class=\"list-group-item text-left\">\n            <span class=\"badge bg-warning float-right text-white\">6</span> Vegetable Juice\n          </li>\n          <li class=\"list-group-item text-left\">\n            <span class=\"badge bg-success float-right text-white\">1</span> Sugar Free Jello-O\n          </li>\n          <li class=\"list-group-item text-left\">\n            <span class=\"badge bg-danger float-right text-white\">3</span> Protein Meal\n          </li>\n        </ul>\n        <div class=\"card-body\">\n          <a class=\"card-link success\">Card link</a>\n          <a class=\"card-link success\">Another link</a>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/dashboard/dashboard-routing.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/dashboard/dashboard-routing.module.ts ***!
  \*******************************************************/
/*! exports provided: DashboardRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardRoutingModule", function() { return DashboardRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _dashboard1_dashboard1_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./dashboard1/dashboard1.component */ "./src/app/dashboard/dashboard1/dashboard1.component.ts");
/* harmony import */ var _dashboard2_dashboard2_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./dashboard2/dashboard2.component */ "./src/app/dashboard/dashboard2/dashboard2.component.ts");





var routes = [
    {
        path: '',
        children: [
            {
                path: '',
                component: _dashboard1_dashboard1_component__WEBPACK_IMPORTED_MODULE_3__["Dashboard1Component"],
                data: {
                    title: 'หน้าหลัก'
                }
            },
            {
                path: 'dashboard2',
                component: _dashboard2_dashboard2_component__WEBPACK_IMPORTED_MODULE_4__["Dashboard2Component"],
                data: {
                    title: 'Dashboard 2'
                }
            },
        ]
    }
];
var DashboardRoutingModule = /** @class */ (function () {
    function DashboardRoutingModule() {
    }
    DashboardRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
        })
    ], DashboardRoutingModule);
    return DashboardRoutingModule;
}());



/***/ }),

/***/ "./src/app/dashboard/dashboard.module.ts":
/*!***********************************************!*\
  !*** ./src/app/dashboard/dashboard.module.ts ***!
  \***********************************************/
/*! exports provided: DashboardModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardModule", function() { return DashboardModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _dashboard_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./dashboard-routing.module */ "./src/app/dashboard/dashboard-routing.module.ts");
/* harmony import */ var ng_chartist__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ng-chartist */ "./node_modules/ng-chartist/fesm5/ng-chartist.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _shared_directives_match_height_directive__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../shared/directives/match-height.directive */ "./src/app/shared/directives/match-height.directive.ts");
/* harmony import */ var _dashboard1_dashboard1_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./dashboard1/dashboard1.component */ "./src/app/dashboard/dashboard1/dashboard1.component.ts");
/* harmony import */ var _dashboard2_dashboard2_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./dashboard2/dashboard2.component */ "./src/app/dashboard/dashboard2/dashboard2.component.ts");









var DashboardModule = /** @class */ (function () {
    function DashboardModule() {
    }
    DashboardModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _dashboard_routing_module__WEBPACK_IMPORTED_MODULE_3__["DashboardRoutingModule"],
                ng_chartist__WEBPACK_IMPORTED_MODULE_4__["ChartistModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_5__["NgbModule"],
                _shared_directives_match_height_directive__WEBPACK_IMPORTED_MODULE_6__["MatchHeightModule"]
            ],
            exports: [],
            declarations: [
                _dashboard1_dashboard1_component__WEBPACK_IMPORTED_MODULE_7__["Dashboard1Component"],
                _dashboard2_dashboard2_component__WEBPACK_IMPORTED_MODULE_8__["Dashboard2Component"]
            ],
            providers: [],
        })
    ], DashboardModule);
    return DashboardModule;
}());



/***/ }),

/***/ "./src/app/dashboard/dashboard1/dashboard1.component.scss":
/*!****************************************************************!*\
  !*** ./src/app/dashboard/dashboard1/dashboard1.component.scss ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host /deep/ .ct-grid {\n  stroke-dasharray: 0px;\n  stroke: rgba(0, 0, 0, 0.1); }\n\n:host /deep/ .ct-label {\n  font-size: 0.9rem; }\n\n:host /deep/ .lineArea .ct-series-a .ct-area {\n  fill-opacity: 0.7;\n  fill: url(\"/dashboard/dashboard1#gradient1\") !important; }\n\n:host /deep/ .lineArea .ct-series-b .ct-area {\n  fill: url(\"/dashboard/dashboard1#gradient\") !important;\n  fill-opacity: 0.9; }\n\n:host /deep/ .lineArea .ct-line {\n  stroke-width: 0px; }\n\n:host /deep/ .lineArea .ct-point {\n  stroke-width: 0px; }\n\n:host /deep/ .Stackbarchart .ct-series-a .ct-bar {\n  stroke: url(\"/dashboard/dashboard1#linear\") !important; }\n\n:host /deep/ .Stackbarchart .ct-series-b .ct-bar {\n  stroke: #e9e9e9; }\n\n:host /deep/ .lineArea2 .ct-series-a .ct-area {\n  fill: url(\"/dashboard/dashboard1#gradient2\") !important; }\n\n:host /deep/ .lineArea2 .ct-series-b .ct-area {\n  fill: url(\"/dashboard/dashboard1#gradient3\") !important; }\n\n:host /deep/ .lineArea2 .ct-point-circle {\n  stroke-width: 2px;\n  fill: white; }\n\n:host /deep/ .lineArea2 .ct-series-b .ct-point-circle {\n  stroke: #843cf7; }\n\n:host /deep/ .lineArea2 .ct-series-b .ct-line {\n  stroke: #A675F4; }\n\n:host /deep/ .lineArea2 .ct-series-a .ct-point-circle {\n  stroke: #31afb2; }\n\n:host /deep/ .lineArea2 .ct-line {\n  fill: none;\n  stroke-width: 2px; }\n\n:host /deep/ .lineChart .ct-point-circle {\n  stroke-width: 2px;\n  fill: white; }\n\n:host /deep/ .lineChart .ct-series-a .ct-point-circle {\n  stroke: white; }\n\n:host /deep/ .lineChart .ct-line {\n  fill: none;\n  stroke: white;\n  stroke-width: 1px; }\n\n:host /deep/ .lineChart .ct-label {\n  color: #FFF; }\n\n:host /deep/ .lineChartShadow {\n  -webkit-filter: drop-shadow(0px 25px 8px rgba(0, 0, 0, 0.3));\n  filter: drop-shadow(0px 25px 8px rgba(0, 0, 0, 0.3));\n  /* Same syntax as box-shadow, except \r\n                                                       for the spread property */ }\n\n:host /deep/ .donut .ct-done .ct-slice-donut {\n  stroke: #0CC27E;\n  stroke-width: 24px !important; }\n\n:host /deep/ .donut .ct-progress .ct-slice-donut {\n  stroke: #FFC107;\n  stroke-width: 16px !important; }\n\n:host /deep/ .donut .ct-outstanding .ct-slice-donut {\n  stroke: #7E57C2;\n  stroke-width: 8px !important; }\n\n:host /deep/ .donut .ct-started .ct-slice-donut {\n  stroke: #2196F3;\n  stroke-width: 32px !important; }\n\n:host /deep/ .donut .ct-label {\n  text-anchor: middle;\n  alignment-baseline: middle;\n  font-size: 20px;\n  fill: #868e96; }\n\n:host /deep/ .BarChart .ct-series-a .ct-bar:nth-of-type(4n+1) {\n  stroke: url(\"/dashboard/dashboard1#gradient7\"); }\n\n:host /deep/ .BarChart .ct-series-a .ct-bar:nth-of-type(4n+2) {\n  stroke: url(\"/dashboard/dashboard1#gradient5\"); }\n\n:host /deep/ .BarChart .ct-series-a .ct-bar:nth-of-type(4n+3) {\n  stroke: url(\"/dashboard/dashboard1#gradient6\"); }\n\n:host /deep/ .BarChart .ct-series-a .ct-bar:nth-of-type(4n+4) {\n  stroke: url(\"/dashboard/dashboard1#gradient4\"); }\n\n:host /deep/ .BarChartShadow {\n  -webkit-filter: drop-shadow(0px 20px 8px rgba(0, 0, 0, 0.3));\n  filter: drop-shadow(0px 20px 8px rgba(0, 0, 0, 0.3));\n  /* Same syntax as box-shadow, except \r\n                                                       for the spread property */ }\n\n:host /deep/ .WidgetlineChart .ct-point {\n  stroke-width: 0px; }\n\n:host /deep/ .WidgetlineChart .ct-line {\n  stroke: #fff; }\n\n:host /deep/ .WidgetlineChart .ct-grid {\n  stroke-dasharray: 0px;\n  stroke: rgba(255, 255, 255, 0.2); }\n\n:host /deep/ .WidgetlineChartshadow {\n  -webkit-filter: drop-shadow(0px 15px 5px rgba(0, 0, 0, 0.8));\n  filter: drop-shadow(0px 15px 5px rgba(0, 0, 0, 0.8));\n  /* Same syntax as box-shadow, except \r\n                                                       for the spread property */ }\n\n.list-user .avatar {\n  border: solid 2px silver;\n  display: inline;\n  width: 50px;\n  height: 50px;\n  border-radius: 50px;\n  background-size: cover;\n  margin-left: 5px;\n  margin-right: 5px; }\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZGFzaGJvYXJkL2Rhc2hib2FyZDEvQzpcXGRldmVsb3BlclxcYmFja2VuZC9zcmNcXGFwcFxcZGFzaGJvYXJkXFxkYXNoYm9hcmQxXFxkYXNoYm9hcmQxLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9kYXNoYm9hcmQvZGFzaGJvYXJkMS9kYXNoYm9hcmQxLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBO0VBQ0kscUJBQXFCO0VBQ3JCLDBCQUEwQixFQUFBOztBQUc5QjtFQUNJLGlCQUFpQixFQUFBOztBQU1yQjtFQUNJLGlCQUFpQjtFQUNqQix1REFBNEQsRUFBQTs7QUFHaEU7RUFDSSxzREFBNEQ7RUFDNUQsaUJBQWlCLEVBQUE7O0FBRXJCO0VBQ0ksaUJBQWlCLEVBQUE7O0FBRXJCO0VBQ0ksaUJBQWlCLEVBQUE7O0FBT3JCO0VBR1ksc0RBQTRELEVBQUE7O0FBSHhFO0VBUVksZUFBZSxFQUFBOztBQVMzQjtFQUNJLHVEQUE2RCxFQUFBOztBQUdqRTtFQUNJLHVEQUE2RCxFQUFBOztBQUdqRTtFQUNJLGlCQUFpQjtFQUNqQixXQUFXLEVBQUE7O0FBR2Y7RUFDSSxlQUFlLEVBQUE7O0FBR25CO0VBQ0ksZUFBZSxFQUFBOztBQUduQjtFQUNJLGVBQWUsRUFBQTs7QUFHbkI7RUFDSSxVQUFVO0VBQ1YsaUJBQWlCLEVBQUE7O0FBT3JCO0VBQ0ksaUJBQWlCO0VBQ2pCLFdBQVcsRUFBQTs7QUFHZjtFQUNJLGFBQWEsRUFBQTs7QUFHakI7RUFDSSxVQUFVO0VBQ1YsYUFBYTtFQUNiLGlCQUFpQixFQUFBOztBQUdyQjtFQUNJLFdBQVcsRUFBQTs7QUFHZjtFQUNJLDREQUEyRDtFQUNuRCxvREFBbUQ7RUFBRTtnRkNyQ2UsRURzQ0M7O0FBTS9FO0VBQ0UsZUFBZTtFQUNmLDZCQUE2QixFQUFBOztBQUUvQjtFQUNFLGVBQWU7RUFDZiw2QkFBNkIsRUFBQTs7QUFFL0I7RUFDRSxlQUFlO0VBQ2YsNEJBQTRCLEVBQUE7O0FBRzlCO0VBQ0UsZUFBZTtFQUNmLDZCQUE2QixFQUFBOztBQUcvQjtFQUNFLG1CQUFtQjtFQUNuQiwwQkFBMEI7RUFDMUIsZUFBZTtFQUNmLGFBQWEsRUFBQTs7QUFPZjtFQUNFLDhDQUFvRCxFQUFBOztBQUV0RDtFQUNFLDhDQUFvRCxFQUFBOztBQUV0RDtFQUNFLDhDQUFvRCxFQUFBOztBQUV0RDtFQUNFLDhDQUFvRCxFQUFBOztBQUd0RDtFQUNFLDREQUEyRDtFQUNuRCxvREFBbUQ7RUFBRTtnRkNoRGUsRURpREM7O0FBT2pGO0VBQ0ksaUJBQWlCLEVBQUE7O0FBRXJCO0VBQ0ksWUFBWSxFQUFBOztBQUloQjtFQUNJLHFCQUFxQjtFQUNwQixnQ0FBZ0MsRUFBQTs7QUFHckM7RUFDSSw0REFBMkQ7RUFDbkQsb0RBQW1EO0VBQUU7Z0ZDdkRlLEVEd0RDOztBQUdqRjtFQUVRLHdCQUFvQztFQUNwQyxlQUFlO0VBQ2YsV0FBVztFQUNYLFlBQVk7RUFDWixtQkFBbUI7RUFDbkIsc0JBQXNCO0VBQ3RCLGdCQUFnQjtFQUNoQixpQkFBaUIsRUFBQSIsImZpbGUiOiJzcmMvYXBwL2Rhc2hib2FyZC9kYXNoYm9hcmQxL2Rhc2hib2FyZDEuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJAaW1wb3J0IFwiLi4vLi4vLi4vYXNzZXRzL3Nhc3Mvc2Nzcy9ncmFkaWVudC12YXJpYWJsZXNcIjtcclxuXHJcbjpob3N0IC9kZWVwLyAuY3QtZ3JpZHtcclxuICAgIHN0cm9rZS1kYXNoYXJyYXk6IDBweDtcclxuICAgIHN0cm9rZTogcmdiYSgwLCAwLCAwLCAwLjEpO1xyXG59XHJcblxyXG46aG9zdCAvZGVlcC8gLmN0LWxhYmVse1xyXG4gICAgZm9udC1zaXplOiAwLjlyZW07XHJcbn1cclxuXHJcblxyXG4vLyBMaW5lIHdpdGggQXJlYSBDaGFydCBDU1MgU3RhcnRzXHJcblxyXG46aG9zdCAvZGVlcC8gLmxpbmVBcmVhIC5jdC1zZXJpZXMtYSAuY3QtYXJlYSB7XHJcbiAgICBmaWxsLW9wYWNpdHk6IDAuNztcclxuICAgIGZpbGw6dXJsKCRkYXNoYm9hcmQxLWdyYWRpZW50LXBhdGggKyAgI2dyYWRpZW50MSkgIWltcG9ydGFudDtcclxufVxyXG5cclxuOmhvc3QgL2RlZXAvIC5saW5lQXJlYSAuY3Qtc2VyaWVzLWIgLmN0LWFyZWEge1xyXG4gICAgZmlsbDogdXJsKCRkYXNoYm9hcmQxLWdyYWRpZW50LXBhdGggKyAgI2dyYWRpZW50KSAhaW1wb3J0YW50O1xyXG4gICAgZmlsbC1vcGFjaXR5OiAwLjk7XHJcbn1cclxuOmhvc3QgL2RlZXAvIC5saW5lQXJlYSAuY3QtbGluZXtcclxuICAgIHN0cm9rZS13aWR0aDogMHB4O1xyXG59XHJcbjpob3N0IC9kZWVwLyAubGluZUFyZWEgLmN0LXBvaW50IHtcclxuICAgIHN0cm9rZS13aWR0aDogMHB4O1xyXG59XHJcblxyXG4vLyBMaW5lIHdpdGggQXJlYSBDaGFydCAxIENTUyBFbmRzXHJcblxyXG4vLyBTdGFjayBCYXIgQ2hhcnQgQ1NTIFN0YXJ0c1xyXG5cclxuOmhvc3QgL2RlZXAvIC5TdGFja2JhcmNoYXJ0e1xyXG4gICAgLmN0LXNlcmllcy1hIHtcclxuICAgICAgICAuY3QtYmFye1xyXG4gICAgICAgICAgICBzdHJva2U6IHVybCgkZGFzaGJvYXJkMS1ncmFkaWVudC1wYXRoICsgICNsaW5lYXIpICFpbXBvcnRhbnRcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICAuY3Qtc2VyaWVzLWIge1xyXG4gICAgICAgIC5jdC1iYXJ7XHJcbiAgICAgICAgICAgIHN0cm9rZTogI2U5ZTllOTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuXHJcbi8vIFN0YWNrIEJhciBDaGFydCBDU1MgRW5kc1xyXG5cclxuLy8gTGluZSB3aXRoIEFyZWEgQ2hhcnQgMiBDU1MgU3RhcnRzXHJcblxyXG46aG9zdCAvZGVlcC8gLmxpbmVBcmVhMiAuY3Qtc2VyaWVzLWEgLmN0LWFyZWEge1xyXG4gICAgZmlsbDogdXJsKCRkYXNoYm9hcmQxLWdyYWRpZW50LXBhdGggKyAgI2dyYWRpZW50MikgIWltcG9ydGFudDtcclxufVxyXG5cclxuOmhvc3QgL2RlZXAvIC5saW5lQXJlYTIgLmN0LXNlcmllcy1iIC5jdC1hcmVhIHtcclxuICAgIGZpbGw6IHVybCgkZGFzaGJvYXJkMS1ncmFkaWVudC1wYXRoICsgICNncmFkaWVudDMpICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbjpob3N0IC9kZWVwLyAubGluZUFyZWEyIC5jdC1wb2ludC1jaXJjbGUge1xyXG4gICAgc3Ryb2tlLXdpZHRoOiAycHg7XHJcbiAgICBmaWxsOiB3aGl0ZTtcclxufVxyXG5cclxuOmhvc3QgL2RlZXAvIC5saW5lQXJlYTIgLmN0LXNlcmllcy1iIC5jdC1wb2ludC1jaXJjbGV7XHJcbiAgICBzdHJva2U6ICM4NDNjZjc7XHJcbn1cclxuXHJcbjpob3N0IC9kZWVwLyAubGluZUFyZWEyIC5jdC1zZXJpZXMtYiAuY3QtbGluZXtcclxuICAgIHN0cm9rZTogI0E2NzVGNDtcclxufVxyXG5cclxuOmhvc3QgL2RlZXAvIC5saW5lQXJlYTIgLmN0LXNlcmllcy1hIC5jdC1wb2ludC1jaXJjbGUge1xyXG4gICAgc3Ryb2tlOiAjMzFhZmIyO1xyXG59XHJcblxyXG46aG9zdCAvZGVlcC8gLmxpbmVBcmVhMiAuY3QtbGluZSB7XHJcbiAgICBmaWxsOiBub25lO1xyXG4gICAgc3Ryb2tlLXdpZHRoOiAycHg7XHJcbn1cclxuXHJcbi8vIExpbmUgd2l0aCBBcmVhIENoYXJ0IDIgQ1NTIEVuZHNcclxuXHJcbi8vIExpbmUgQ2hhcnQgQ1NTIFN0YXJ0c1xyXG5cclxuOmhvc3QgL2RlZXAvIC5saW5lQ2hhcnQgLmN0LXBvaW50LWNpcmNsZSB7XHJcbiAgICBzdHJva2Utd2lkdGg6IDJweDtcclxuICAgIGZpbGw6IHdoaXRlO1xyXG59XHJcblxyXG46aG9zdCAvZGVlcC8gLmxpbmVDaGFydCAuY3Qtc2VyaWVzLWEgLmN0LXBvaW50LWNpcmNsZSB7XHJcbiAgICBzdHJva2U6IHdoaXRlO1xyXG59XHJcblxyXG46aG9zdCAvZGVlcC8gLmxpbmVDaGFydCAuY3QtbGluZSB7XHJcbiAgICBmaWxsOiBub25lO1xyXG4gICAgc3Ryb2tlOiB3aGl0ZTtcclxuICAgIHN0cm9rZS13aWR0aDogMXB4O1xyXG59XHJcblxyXG46aG9zdCAvZGVlcC8gLmxpbmVDaGFydCAuY3QtbGFiZWwge1xyXG4gICAgY29sb3I6ICNGRkY7XHJcbn1cclxuXHJcbjpob3N0IC9kZWVwLyAubGluZUNoYXJ0U2hhZG93IHtcclxuICAgIC13ZWJraXQtZmlsdGVyOiBkcm9wLXNoYWRvdyggMHB4IDI1cHggOHB4IHJnYmEoMCwwLDAsMC4zKSApO1xyXG4gICAgICAgICAgICBmaWx0ZXI6IGRyb3Atc2hhZG93KCAwcHggMjVweCA4cHggcmdiYSgwLDAsMCwwLjMpICk7IC8qIFNhbWUgc3ludGF4IGFzIGJveC1zaGFkb3csIGV4Y2VwdCBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZvciB0aGUgc3ByZWFkIHByb3BlcnR5ICovXHJcbn1cclxuXHJcbi8vIExpbmUgQ2hhcnQgIENTUyBFbmRzXHJcbiBcclxuICAvLyBEb251dCBDaGFydCAgQ1NTIEVuZHNcclxuICA6aG9zdCAvZGVlcC8gLmRvbnV0IC5jdC1kb25lIC5jdC1zbGljZS1kb251dCB7XHJcbiAgICBzdHJva2U6ICMwQ0MyN0U7XHJcbiAgICBzdHJva2Utd2lkdGg6IDI0cHggIWltcG9ydGFudDtcclxuICB9XHJcbiAgOmhvc3QgL2RlZXAvIC5kb251dCAuY3QtcHJvZ3Jlc3MgLmN0LXNsaWNlLWRvbnV0IHtcclxuICAgIHN0cm9rZTogI0ZGQzEwNztcclxuICAgIHN0cm9rZS13aWR0aDogMTZweCAhaW1wb3J0YW50O1xyXG4gIH1cclxuICA6aG9zdCAvZGVlcC8gLmRvbnV0IC5jdC1vdXRzdGFuZGluZyAuY3Qtc2xpY2UtZG9udXQge1xyXG4gICAgc3Ryb2tlOiAjN0U1N0MyO1xyXG4gICAgc3Ryb2tlLXdpZHRoOiA4cHggIWltcG9ydGFudDtcclxuICB9XHJcbiAgXHJcbiAgOmhvc3QgL2RlZXAvIC5kb251dCAuY3Qtc3RhcnRlZCAuY3Qtc2xpY2UtZG9udXQge1xyXG4gICAgc3Ryb2tlOiAjMjE5NkYzO1xyXG4gICAgc3Ryb2tlLXdpZHRoOiAzMnB4ICFpbXBvcnRhbnQ7XHJcbiAgfVxyXG5cclxuICA6aG9zdCAvZGVlcC8gLmRvbnV0IC5jdC1sYWJlbCB7XHJcbiAgICB0ZXh0LWFuY2hvcjogbWlkZGxlO1xyXG4gICAgYWxpZ25tZW50LWJhc2VsaW5lOiBtaWRkbGU7XHJcbiAgICBmb250LXNpemU6IDIwcHg7XHJcbiAgICBmaWxsOiAjODY4ZTk2O1xyXG4gIH1cclxuXHJcbiAgLy8gRG9udXQgQ2hhcnQgIENTUyBFbmRzXHJcblxyXG4gIC8vIEJhciBDaGFydCBDU1MgU3RhcnRzXHJcblxyXG4gIDpob3N0IC9kZWVwLyAuQmFyQ2hhcnQgLmN0LXNlcmllcy1hIC5jdC1iYXI6bnRoLW9mLXR5cGUoNG4rMSkge1xyXG4gICAgc3Ryb2tlOiB1cmwoJGRhc2hib2FyZDEtZ3JhZGllbnQtcGF0aCArICAjZ3JhZGllbnQ3KTtcclxuICB9XHJcbiAgOmhvc3QgL2RlZXAvIC5CYXJDaGFydCAuY3Qtc2VyaWVzLWEgLmN0LWJhcjpudGgtb2YtdHlwZSg0bisyKSB7XHJcbiAgICBzdHJva2U6IHVybCgkZGFzaGJvYXJkMS1ncmFkaWVudC1wYXRoICsgICNncmFkaWVudDUpO1xyXG4gIH1cclxuICA6aG9zdCAvZGVlcC8gLkJhckNoYXJ0IC5jdC1zZXJpZXMtYSAuY3QtYmFyOm50aC1vZi10eXBlKDRuKzMpIHtcclxuICAgIHN0cm9rZTogdXJsKCRkYXNoYm9hcmQxLWdyYWRpZW50LXBhdGggKyAgI2dyYWRpZW50Nik7XHJcbiAgfVxyXG4gIDpob3N0IC9kZWVwLyAuQmFyQ2hhcnQgLmN0LXNlcmllcy1hIC5jdC1iYXI6bnRoLW9mLXR5cGUoNG4rNCkge1xyXG4gICAgc3Ryb2tlOiB1cmwoJGRhc2hib2FyZDEtZ3JhZGllbnQtcGF0aCArICAjZ3JhZGllbnQ0KTtcclxuICB9XHJcblxyXG4gIDpob3N0IC9kZWVwLyAuQmFyQ2hhcnRTaGFkb3cge1xyXG4gICAgLXdlYmtpdC1maWx0ZXI6IGRyb3Atc2hhZG93KCAwcHggMjBweCA4cHggcmdiYSgwLDAsMCwwLjMpICk7XHJcbiAgICAgICAgICAgIGZpbHRlcjogZHJvcC1zaGFkb3coIDBweCAyMHB4IDhweCByZ2JhKDAsMCwwLDAuMykgKTsgLyogU2FtZSBzeW50YXggYXMgYm94LXNoYWRvdywgZXhjZXB0IFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZm9yIHRoZSBzcHJlYWQgcHJvcGVydHkgKi9cclxufVxyXG5cclxuLy8gQmFyIENoYXJ0IENTUyBFbmRzXHJcblxyXG4vLyBXaWRnZXQgbGluZSBDaGFydCBDU1MgU3RhcnRzXHJcblxyXG46aG9zdCAvZGVlcC8gLldpZGdldGxpbmVDaGFydCAuY3QtcG9pbnQge1xyXG4gICAgc3Ryb2tlLXdpZHRoOiAwcHg7XHJcbn1cclxuOmhvc3QgL2RlZXAvIC5XaWRnZXRsaW5lQ2hhcnQgLmN0LWxpbmV7XHJcbiAgICBzdHJva2U6ICNmZmY7XHJcbn1cclxuXHJcblxyXG46aG9zdCAvZGVlcC8gLldpZGdldGxpbmVDaGFydCAuY3QtZ3JpZCB7XHJcbiAgICBzdHJva2UtZGFzaGFycmF5OiAwcHg7ICAgIFxyXG4gICAgIHN0cm9rZTogcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjIpO1xyXG59XHJcblxyXG46aG9zdCAvZGVlcC8gLldpZGdldGxpbmVDaGFydHNoYWRvdyB7XHJcbiAgICAtd2Via2l0LWZpbHRlcjogZHJvcC1zaGFkb3coIDBweCAxNXB4IDVweCByZ2JhKDAsMCwwLDAuOCkgKTtcclxuICAgICAgICAgICAgZmlsdGVyOiBkcm9wLXNoYWRvdyggMHB4IDE1cHggNXB4IHJnYmEoMCwwLDAsMC44KSApOyAvKiBTYW1lIHN5bnRheCBhcyBib3gtc2hhZG93LCBleGNlcHQgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBmb3IgdGhlIHNwcmVhZCBwcm9wZXJ0eSAqL1xyXG59XHJcblxyXG4ubGlzdC11c2Vye1xyXG4gICAgLmF2YXRhcntcclxuICAgICAgICBib3JkZXI6IHNvbGlkIDJweCByZ2IoMTkyLCAxOTIsIDE5Mik7XHJcbiAgICAgICAgZGlzcGxheTogaW5saW5lOyBcclxuICAgICAgICB3aWR0aDogNTBweDtcclxuICAgICAgICBoZWlnaHQ6IDUwcHg7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogNTBweDtcclxuICAgICAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xyXG4gICAgICAgIG1hcmdpbi1sZWZ0OiA1cHg7XHJcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiA1cHg7XHJcbiAgICB9XHJcbn1cclxuXHJcbi8vIFdpZGdldCBsaW5lIENoYXJ0IENTUyBFbmRzIiwiOmhvc3QgL2RlZXAvIC5jdC1ncmlkIHtcbiAgc3Ryb2tlLWRhc2hhcnJheTogMHB4O1xuICBzdHJva2U6IHJnYmEoMCwgMCwgMCwgMC4xKTsgfVxuXG46aG9zdCAvZGVlcC8gLmN0LWxhYmVsIHtcbiAgZm9udC1zaXplOiAwLjlyZW07IH1cblxuOmhvc3QgL2RlZXAvIC5saW5lQXJlYSAuY3Qtc2VyaWVzLWEgLmN0LWFyZWEge1xuICBmaWxsLW9wYWNpdHk6IDAuNztcbiAgZmlsbDogdXJsKFwiL2Rhc2hib2FyZC9kYXNoYm9hcmQxI2dyYWRpZW50MVwiKSAhaW1wb3J0YW50OyB9XG5cbjpob3N0IC9kZWVwLyAubGluZUFyZWEgLmN0LXNlcmllcy1iIC5jdC1hcmVhIHtcbiAgZmlsbDogdXJsKFwiL2Rhc2hib2FyZC9kYXNoYm9hcmQxI2dyYWRpZW50XCIpICFpbXBvcnRhbnQ7XG4gIGZpbGwtb3BhY2l0eTogMC45OyB9XG5cbjpob3N0IC9kZWVwLyAubGluZUFyZWEgLmN0LWxpbmUge1xuICBzdHJva2Utd2lkdGg6IDBweDsgfVxuXG46aG9zdCAvZGVlcC8gLmxpbmVBcmVhIC5jdC1wb2ludCB7XG4gIHN0cm9rZS13aWR0aDogMHB4OyB9XG5cbjpob3N0IC9kZWVwLyAuU3RhY2tiYXJjaGFydCAuY3Qtc2VyaWVzLWEgLmN0LWJhciB7XG4gIHN0cm9rZTogdXJsKFwiL2Rhc2hib2FyZC9kYXNoYm9hcmQxI2xpbmVhclwiKSAhaW1wb3J0YW50OyB9XG5cbjpob3N0IC9kZWVwLyAuU3RhY2tiYXJjaGFydCAuY3Qtc2VyaWVzLWIgLmN0LWJhciB7XG4gIHN0cm9rZTogI2U5ZTllOTsgfVxuXG46aG9zdCAvZGVlcC8gLmxpbmVBcmVhMiAuY3Qtc2VyaWVzLWEgLmN0LWFyZWEge1xuICBmaWxsOiB1cmwoXCIvZGFzaGJvYXJkL2Rhc2hib2FyZDEjZ3JhZGllbnQyXCIpICFpbXBvcnRhbnQ7IH1cblxuOmhvc3QgL2RlZXAvIC5saW5lQXJlYTIgLmN0LXNlcmllcy1iIC5jdC1hcmVhIHtcbiAgZmlsbDogdXJsKFwiL2Rhc2hib2FyZC9kYXNoYm9hcmQxI2dyYWRpZW50M1wiKSAhaW1wb3J0YW50OyB9XG5cbjpob3N0IC9kZWVwLyAubGluZUFyZWEyIC5jdC1wb2ludC1jaXJjbGUge1xuICBzdHJva2Utd2lkdGg6IDJweDtcbiAgZmlsbDogd2hpdGU7IH1cblxuOmhvc3QgL2RlZXAvIC5saW5lQXJlYTIgLmN0LXNlcmllcy1iIC5jdC1wb2ludC1jaXJjbGUge1xuICBzdHJva2U6ICM4NDNjZjc7IH1cblxuOmhvc3QgL2RlZXAvIC5saW5lQXJlYTIgLmN0LXNlcmllcy1iIC5jdC1saW5lIHtcbiAgc3Ryb2tlOiAjQTY3NUY0OyB9XG5cbjpob3N0IC9kZWVwLyAubGluZUFyZWEyIC5jdC1zZXJpZXMtYSAuY3QtcG9pbnQtY2lyY2xlIHtcbiAgc3Ryb2tlOiAjMzFhZmIyOyB9XG5cbjpob3N0IC9kZWVwLyAubGluZUFyZWEyIC5jdC1saW5lIHtcbiAgZmlsbDogbm9uZTtcbiAgc3Ryb2tlLXdpZHRoOiAycHg7IH1cblxuOmhvc3QgL2RlZXAvIC5saW5lQ2hhcnQgLmN0LXBvaW50LWNpcmNsZSB7XG4gIHN0cm9rZS13aWR0aDogMnB4O1xuICBmaWxsOiB3aGl0ZTsgfVxuXG46aG9zdCAvZGVlcC8gLmxpbmVDaGFydCAuY3Qtc2VyaWVzLWEgLmN0LXBvaW50LWNpcmNsZSB7XG4gIHN0cm9rZTogd2hpdGU7IH1cblxuOmhvc3QgL2RlZXAvIC5saW5lQ2hhcnQgLmN0LWxpbmUge1xuICBmaWxsOiBub25lO1xuICBzdHJva2U6IHdoaXRlO1xuICBzdHJva2Utd2lkdGg6IDFweDsgfVxuXG46aG9zdCAvZGVlcC8gLmxpbmVDaGFydCAuY3QtbGFiZWwge1xuICBjb2xvcjogI0ZGRjsgfVxuXG46aG9zdCAvZGVlcC8gLmxpbmVDaGFydFNoYWRvdyB7XG4gIC13ZWJraXQtZmlsdGVyOiBkcm9wLXNoYWRvdygwcHggMjVweCA4cHggcmdiYSgwLCAwLCAwLCAwLjMpKTtcbiAgZmlsdGVyOiBkcm9wLXNoYWRvdygwcHggMjVweCA4cHggcmdiYSgwLCAwLCAwLCAwLjMpKTtcbiAgLyogU2FtZSBzeW50YXggYXMgYm94LXNoYWRvdywgZXhjZXB0IFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZm9yIHRoZSBzcHJlYWQgcHJvcGVydHkgKi8gfVxuXG46aG9zdCAvZGVlcC8gLmRvbnV0IC5jdC1kb25lIC5jdC1zbGljZS1kb251dCB7XG4gIHN0cm9rZTogIzBDQzI3RTtcbiAgc3Ryb2tlLXdpZHRoOiAyNHB4ICFpbXBvcnRhbnQ7IH1cblxuOmhvc3QgL2RlZXAvIC5kb251dCAuY3QtcHJvZ3Jlc3MgLmN0LXNsaWNlLWRvbnV0IHtcbiAgc3Ryb2tlOiAjRkZDMTA3O1xuICBzdHJva2Utd2lkdGg6IDE2cHggIWltcG9ydGFudDsgfVxuXG46aG9zdCAvZGVlcC8gLmRvbnV0IC5jdC1vdXRzdGFuZGluZyAuY3Qtc2xpY2UtZG9udXQge1xuICBzdHJva2U6ICM3RTU3QzI7XG4gIHN0cm9rZS13aWR0aDogOHB4ICFpbXBvcnRhbnQ7IH1cblxuOmhvc3QgL2RlZXAvIC5kb251dCAuY3Qtc3RhcnRlZCAuY3Qtc2xpY2UtZG9udXQge1xuICBzdHJva2U6ICMyMTk2RjM7XG4gIHN0cm9rZS13aWR0aDogMzJweCAhaW1wb3J0YW50OyB9XG5cbjpob3N0IC9kZWVwLyAuZG9udXQgLmN0LWxhYmVsIHtcbiAgdGV4dC1hbmNob3I6IG1pZGRsZTtcbiAgYWxpZ25tZW50LWJhc2VsaW5lOiBtaWRkbGU7XG4gIGZvbnQtc2l6ZTogMjBweDtcbiAgZmlsbDogIzg2OGU5NjsgfVxuXG46aG9zdCAvZGVlcC8gLkJhckNoYXJ0IC5jdC1zZXJpZXMtYSAuY3QtYmFyOm50aC1vZi10eXBlKDRuKzEpIHtcbiAgc3Ryb2tlOiB1cmwoXCIvZGFzaGJvYXJkL2Rhc2hib2FyZDEjZ3JhZGllbnQ3XCIpOyB9XG5cbjpob3N0IC9kZWVwLyAuQmFyQ2hhcnQgLmN0LXNlcmllcy1hIC5jdC1iYXI6bnRoLW9mLXR5cGUoNG4rMikge1xuICBzdHJva2U6IHVybChcIi9kYXNoYm9hcmQvZGFzaGJvYXJkMSNncmFkaWVudDVcIik7IH1cblxuOmhvc3QgL2RlZXAvIC5CYXJDaGFydCAuY3Qtc2VyaWVzLWEgLmN0LWJhcjpudGgtb2YtdHlwZSg0biszKSB7XG4gIHN0cm9rZTogdXJsKFwiL2Rhc2hib2FyZC9kYXNoYm9hcmQxI2dyYWRpZW50NlwiKTsgfVxuXG46aG9zdCAvZGVlcC8gLkJhckNoYXJ0IC5jdC1zZXJpZXMtYSAuY3QtYmFyOm50aC1vZi10eXBlKDRuKzQpIHtcbiAgc3Ryb2tlOiB1cmwoXCIvZGFzaGJvYXJkL2Rhc2hib2FyZDEjZ3JhZGllbnQ0XCIpOyB9XG5cbjpob3N0IC9kZWVwLyAuQmFyQ2hhcnRTaGFkb3cge1xuICAtd2Via2l0LWZpbHRlcjogZHJvcC1zaGFkb3coMHB4IDIwcHggOHB4IHJnYmEoMCwgMCwgMCwgMC4zKSk7XG4gIGZpbHRlcjogZHJvcC1zaGFkb3coMHB4IDIwcHggOHB4IHJnYmEoMCwgMCwgMCwgMC4zKSk7XG4gIC8qIFNhbWUgc3ludGF4IGFzIGJveC1zaGFkb3csIGV4Y2VwdCBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZvciB0aGUgc3ByZWFkIHByb3BlcnR5ICovIH1cblxuOmhvc3QgL2RlZXAvIC5XaWRnZXRsaW5lQ2hhcnQgLmN0LXBvaW50IHtcbiAgc3Ryb2tlLXdpZHRoOiAwcHg7IH1cblxuOmhvc3QgL2RlZXAvIC5XaWRnZXRsaW5lQ2hhcnQgLmN0LWxpbmUge1xuICBzdHJva2U6ICNmZmY7IH1cblxuOmhvc3QgL2RlZXAvIC5XaWRnZXRsaW5lQ2hhcnQgLmN0LWdyaWQge1xuICBzdHJva2UtZGFzaGFycmF5OiAwcHg7XG4gIHN0cm9rZTogcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjIpOyB9XG5cbjpob3N0IC9kZWVwLyAuV2lkZ2V0bGluZUNoYXJ0c2hhZG93IHtcbiAgLXdlYmtpdC1maWx0ZXI6IGRyb3Atc2hhZG93KDBweCAxNXB4IDVweCByZ2JhKDAsIDAsIDAsIDAuOCkpO1xuICBmaWx0ZXI6IGRyb3Atc2hhZG93KDBweCAxNXB4IDVweCByZ2JhKDAsIDAsIDAsIDAuOCkpO1xuICAvKiBTYW1lIHN5bnRheCBhcyBib3gtc2hhZG93LCBleGNlcHQgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBmb3IgdGhlIHNwcmVhZCBwcm9wZXJ0eSAqLyB9XG5cbi5saXN0LXVzZXIgLmF2YXRhciB7XG4gIGJvcmRlcjogc29saWQgMnB4IHNpbHZlcjtcbiAgZGlzcGxheTogaW5saW5lO1xuICB3aWR0aDogNTBweDtcbiAgaGVpZ2h0OiA1MHB4O1xuICBib3JkZXItcmFkaXVzOiA1MHB4O1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICBtYXJnaW4tbGVmdDogNXB4O1xuICBtYXJnaW4tcmlnaHQ6IDVweDsgfVxuIl19 */"

/***/ }),

/***/ "./src/app/dashboard/dashboard1/dashboard1.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/dashboard/dashboard1/dashboard1.component.ts ***!
  \**************************************************************/
/*! exports provided: Dashboard1Component */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Dashboard1Component", function() { return Dashboard1Component; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var chartist__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! chartist */ "./node_modules/chartist/dist/chartist.js");
/* harmony import */ var chartist__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(chartist__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var app_webapi_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! app/webapi.service */ "./src/app/webapi.service.ts");
/* harmony import */ var _angular_fire_database__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/fire/database */ "./node_modules/@angular/fire/database/index.js");





var data = __webpack_require__(/*! ../../shared/data/chartist.json */ "./src/app/shared/data/chartist.json");
var Dashboard1Component = /** @class */ (function () {
    function Dashboard1Component(api, ref, firebase) {
        this.api = api;
        this.ref = ref;
        this.firebase = firebase;
        this.fbstatus = false;
        // Line area chart configuration Starts
        this.lineArea = {
            type: 'Line',
            data: data['lineAreaDashboard'],
            options: {
                low: 0,
                showArea: true,
                fullWidth: true,
                onlyInteger: true,
                axisY: {
                    low: 0,
                    scaleMinSpace: 50,
                },
                axisX: {
                    showGrid: false
                }
            },
            events: {
                created: function (data) {
                    var defs = data.svg.elem('defs');
                    defs.elem('linearGradient', {
                        id: 'gradient',
                        x1: 0,
                        y1: 1,
                        x2: 1,
                        y2: 0
                    }).elem('stop', {
                        offset: 0,
                        'stop-color': 'rgba(0, 201, 255, 1)'
                    }).parent().elem('stop', {
                        offset: 1,
                        'stop-color': 'rgba(146, 254, 157, 1)'
                    });
                    defs.elem('linearGradient', {
                        id: 'gradient1',
                        x1: 0,
                        y1: 1,
                        x2: 1,
                        y2: 0
                    }).elem('stop', {
                        offset: 0,
                        'stop-color': 'rgba(132, 60, 247, 1)'
                    }).parent().elem('stop', {
                        offset: 1,
                        'stop-color': 'rgba(56, 184, 242, 1)'
                    });
                },
            },
        };
        // Line area chart configuration Ends
        // Stacked Bar chart configuration Starts
        this.Stackbarchart = {
            type: 'Bar',
            data: data['Stackbarchart'],
            options: {
                stackBars: true,
                fullWidth: true,
                axisX: {
                    showGrid: false,
                },
                axisY: {
                    showGrid: false,
                    showLabel: false,
                    offset: 0
                },
                chartPadding: 30
            },
            events: {
                created: function (data) {
                    var defs = data.svg.elem('defs');
                    defs.elem('linearGradient', {
                        id: 'linear',
                        x1: 0,
                        y1: 1,
                        x2: 0,
                        y2: 0
                    }).elem('stop', {
                        offset: 0,
                        'stop-color': 'rgba(0, 201, 255,1)'
                    }).parent().elem('stop', {
                        offset: 1,
                        'stop-color': 'rgba(17,228,183, 1)'
                    });
                },
                draw: function (data) {
                    if (data.type === 'bar') {
                        data.element.attr({
                            style: 'stroke-width: 5px',
                            x1: data.x1 + 0.001
                        });
                    }
                    else if (data.type === 'label') {
                        data.element.attr({
                            y: 270
                        });
                    }
                }
            },
        };
        // Stacked Bar chart configuration Ends
        // Line area chart 2 configuration Starts
        this.lineArea2 = {
            type: 'Line',
            data: data['lineArea2'],
            options: {
                showArea: true,
                fullWidth: true,
                lineSmooth: chartist__WEBPACK_IMPORTED_MODULE_2__["Interpolation"].none(),
                axisX: {
                    showGrid: false,
                },
                axisY: {
                    low: 0,
                    scaleMinSpace: 50,
                }
            },
            responsiveOptions: [
                ['screen and (max-width: 640px) and (min-width: 381px)', {
                        axisX: {
                            labelInterpolationFnc: function (value, index) {
                                return index % 2 === 0 ? value : null;
                            }
                        }
                    }],
                ['screen and (max-width: 380px)', {
                        axisX: {
                            labelInterpolationFnc: function (value, index) {
                                return index % 3 === 0 ? value : null;
                            }
                        }
                    }]
            ],
            events: {
                created: function (data) {
                    var defs = data.svg.elem('defs');
                    defs.elem('linearGradient', {
                        id: 'gradient2',
                        x1: 0,
                        y1: 1,
                        x2: 0,
                        y2: 0
                    }).elem('stop', {
                        offset: 0,
                        'stop-opacity': '0.2',
                        'stop-color': 'rgba(255, 255, 255, 1)'
                    }).parent().elem('stop', {
                        offset: 1,
                        'stop-opacity': '0.2',
                        'stop-color': 'rgba(0, 201, 255, 1)'
                    });
                    defs.elem('linearGradient', {
                        id: 'gradient3',
                        x1: 0,
                        y1: 1,
                        x2: 0,
                        y2: 0
                    }).elem('stop', {
                        offset: 0.3,
                        'stop-opacity': '0.2',
                        'stop-color': 'rgba(255, 255, 255, 1)'
                    }).parent().elem('stop', {
                        offset: 1,
                        'stop-opacity': '0.2',
                        'stop-color': 'rgba(132, 60, 247, 1)'
                    });
                },
                draw: function (data) {
                    var circleRadius = 4;
                    if (data.type === 'point') {
                        var circle = new chartist__WEBPACK_IMPORTED_MODULE_2__["Svg"]('circle', {
                            cx: data.x,
                            cy: data.y,
                            r: circleRadius,
                            class: 'ct-point-circle'
                        });
                        data.element.replace(circle);
                    }
                    else if (data.type === 'label') {
                        // adjust label position for rotation
                        var dX = data.width / 2 + (30 - data.width);
                        data.element.attr({ x: data.element.attr('x') - dX });
                    }
                }
            },
        };
        // Line area chart 2 configuration Ends
        // Line chart configuration Starts
        this.lineChart = {
            type: 'Line', data: data['LineDashboard'],
            options: {
                axisX: {
                    showGrid: false
                },
                axisY: {
                    showGrid: false,
                    showLabel: false,
                    low: 0,
                    high: 100,
                    offset: 0,
                },
                fullWidth: true,
                offset: 0,
            },
            events: {
                draw: function (data) {
                    var circleRadius = 4;
                    if (data.type === 'point') {
                        var circle = new chartist__WEBPACK_IMPORTED_MODULE_2__["Svg"]('circle', {
                            cx: data.x,
                            cy: data.y,
                            r: circleRadius,
                            class: 'ct-point-circle'
                        });
                        data.element.replace(circle);
                    }
                    else if (data.type === 'label') {
                        // adjust label position for rotation
                        var dX = data.width / 2 + (30 - data.width);
                        data.element.attr({ x: data.element.attr('x') - dX });
                    }
                }
            },
        };
        // Line chart configuration Ends
        // Donut chart configuration Starts
        this.DonutChart = {
            type: 'Pie',
            data: data['donutDashboard'],
            options: {
                donut: true,
                startAngle: 0,
                labelInterpolationFnc: function (value) {
                    var total = data['donutDashboard'].series.reduce(function (prev, series) {
                        return prev + series.value;
                    }, 0);
                    return total + '%';
                }
            },
            events: {
                draw: function (data) {
                    if (data.type === 'label') {
                        if (data.index === 0) {
                            data.element.attr({
                                dx: data.element.root().width() / 2,
                                dy: data.element.root().height() / 2
                            });
                        }
                        else {
                            data.element.remove();
                        }
                    }
                }
            }
        };
        // Donut chart configuration Ends
        //  Bar chart configuration Starts
        this.BarChart = {
            type: 'Bar', data: data['DashboardBar'], options: {
                axisX: {
                    showGrid: false,
                },
                axisY: {
                    showGrid: false,
                    showLabel: false,
                    offset: 0
                },
                low: 0,
                high: 60,
            },
            responsiveOptions: [
                ['screen and (max-width: 640px)', {
                        seriesBarDistance: 5,
                        axisX: {
                            labelInterpolationFnc: function (value) {
                                return value[0];
                            }
                        }
                    }]
            ],
            events: {
                created: function (data) {
                    var defs = data.svg.elem('defs');
                    defs.elem('linearGradient', {
                        id: 'gradient4',
                        x1: 0,
                        y1: 1,
                        x2: 0,
                        y2: 0
                    }).elem('stop', {
                        offset: 0,
                        'stop-color': 'rgba(238, 9, 121,1)'
                    }).parent().elem('stop', {
                        offset: 1,
                        'stop-color': 'rgba(255, 106, 0, 1)'
                    });
                    defs.elem('linearGradient', {
                        id: 'gradient5',
                        x1: 0,
                        y1: 1,
                        x2: 0,
                        y2: 0
                    }).elem('stop', {
                        offset: 0,
                        'stop-color': 'rgba(0, 75, 145,1)'
                    }).parent().elem('stop', {
                        offset: 1,
                        'stop-color': 'rgba(120, 204, 55, 1)'
                    });
                    defs.elem('linearGradient', {
                        id: 'gradient6',
                        x1: 0,
                        y1: 1,
                        x2: 0,
                        y2: 0
                    }).elem('stop', {
                        offset: 0,
                        'stop-color': 'rgba(132, 60, 247,1)'
                    }).parent().elem('stop', {
                        offset: 1,
                        'stop-color': 'rgba(56, 184, 242, 1)'
                    });
                    defs.elem('linearGradient', {
                        id: 'gradient7',
                        x1: 0,
                        y1: 1,
                        x2: 0,
                        y2: 0
                    }).elem('stop', {
                        offset: 0,
                        'stop-color': 'rgba(155, 60, 183,1)'
                    }).parent().elem('stop', {
                        offset: 1,
                        'stop-color': 'rgba(255, 57, 111, 1)'
                    });
                },
                draw: function (data) {
                    var barHorizontalCenter, barVerticalCenter, label, value;
                    if (data.type === 'bar') {
                        data.element.attr({
                            y1: 195,
                            x1: data.x1 + 0.001
                        });
                    }
                }
            },
        };
        // Bar chart configuration Ends
        // line chart configuration Starts
        this.WidgetlineChart = {
            type: 'Line', data: data['WidgetlineChart'],
            options: {
                axisX: {
                    showGrid: true,
                    showLabel: false,
                    offset: 0,
                },
                axisY: {
                    showGrid: false,
                    low: 40,
                    showLabel: false,
                    offset: 0,
                },
                lineSmooth: chartist__WEBPACK_IMPORTED_MODULE_2__["Interpolation"].cardinal({
                    tension: 0
                }),
                fullWidth: true,
            },
        };
        this.users = [];
        this.orders = [];
        this.total = {
            inc: 0,
            exp: 0,
            profit: 0,
            tax: 0
        };
    }
    Dashboard1Component.prototype.ngOnInit = function () {
        var _this = this;
        this.fbstatus = true;
        //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
        //Add 'implements OnInit' to the class.
        this.api.fb_val("user", function (result) {
            if (_this.fbstatus) {
                console.log(result.val());
                _this.load_users();
            }
        });
        this.api.fb_val("order", function (result) {
            if (_this.fbstatus) {
                _this.load_orders();
            }
        });
        this.api.check_login();
        this.load_total();
    };
    Dashboard1Component.prototype.load_orders = function () {
        var _this = this;
        this.api.getData("dashboard/load_orders").then(function (res) {
            console.log(res);
            _this.orders = res;
            _this.ref.detectChanges();
        });
    };
    // Line chart configuration Ends
    Dashboard1Component.prototype.load_users = function () {
        var _this = this;
        this.api.getData("dashboard/load_users").then(function (res) {
            console.log(res);
            _this.users = res;
            _this.ref.detectChanges();
        });
    };
    Dashboard1Component.prototype.load_total = function () {
        var _this = this;
        this.api.getData('dashboard/total').then(function (res) {
            _this.total = res;
            console.log(res);
            _this.ref.detectChanges();
        });
    };
    Dashboard1Component.prototype.ngOnDestroy = function () {
        this.fbstatus = false;
    };
    Dashboard1Component.ctorParameters = function () { return [
        { type: app_webapi_service__WEBPACK_IMPORTED_MODULE_3__["WebapiService"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"] },
        { type: _angular_fire_database__WEBPACK_IMPORTED_MODULE_4__["AngularFireDatabase"] }
    ]; };
    Dashboard1Component = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-dashboard1',
            template: __webpack_require__(/*! raw-loader!./dashboard1.component.html */ "./node_modules/raw-loader/index.js!./src/app/dashboard/dashboard1/dashboard1.component.html"),
            styles: [__webpack_require__(/*! ./dashboard1.component.scss */ "./src/app/dashboard/dashboard1/dashboard1.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [app_webapi_service__WEBPACK_IMPORTED_MODULE_3__["WebapiService"],
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"],
            _angular_fire_database__WEBPACK_IMPORTED_MODULE_4__["AngularFireDatabase"]])
    ], Dashboard1Component);
    return Dashboard1Component;
}());



/***/ }),

/***/ "./src/app/dashboard/dashboard2/dashboard2.component.scss":
/*!****************************************************************!*\
  !*** ./src/app/dashboard/dashboard2/dashboard2.component.scss ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host /deep/ .ct-grid {\n  stroke-dasharray: 0px;\n  stroke: rgba(0, 0, 0, 0.1); }\n\n:host /deep/ .ct-label {\n  font-size: 0.9rem; }\n\n:host /deep/ .WidgetlineChart .ct-point {\n  stroke-width: 0px; }\n\n:host /deep/ .WidgetlineChart .ct-line {\n  stroke: #fff; }\n\n:host /deep/ .WidgetlineChartShadow {\n  -webkit-filter: drop-shadow(0px 15px 5px rgba(0, 0, 0, 0.8));\n  filter: drop-shadow(0px 15px 5px rgba(0, 0, 0, 0.8));\n  /* Same syntax as box-shadow, except for the spread property */ }\n\n:host /deep/ .WidgetlineChart1 .ct-point {\n  stroke-width: 0px; }\n\n:host /deep/ .WidgetlineChart1 .ct-line {\n  stroke: url(\"/dashboard/dashboard2#widgradient\") !important; }\n\n:host /deep/ .WidgetlineChart1Shadow {\n  -webkit-filter: drop-shadow(0px 20px 5px rgba(0, 0, 0, 0.3));\n  filter: drop-shadow(0px 20px 5px rgba(0, 0, 0, 0.3));\n  /* Same syntax as box-shadow, except \n                                                       for the spread property */ }\n\n:host /deep/ .WidgetlineChart2 .ct-point {\n  stroke-width: 0px; }\n\n:host /deep/ .WidgetlineChart2 .ct-line {\n  stroke: url(\"/dashboard/dashboard2#widgradient1\") !important; }\n\n:host /deep/ .WidgetlineChart2 .ct-grid {\n  stroke-dasharray: 0px;\n  stroke: rgba(255, 255, 255, 0.2); }\n\n:host /deep/ .WidgetlineChart2Shadow {\n  -webkit-filter: drop-shadow(0px 20px 5px rgba(0, 0, 0, 0.3));\n  filter: drop-shadow(0px 20px 5px rgba(0, 0, 0, 0.3));\n  /* Same syntax as box-shadow, except \n                                                       for the spread property */ }\n\n:host /deep/ .donut1 .ct-label {\n  text-anchor: middle;\n  alignment-baseline: middle;\n  font-size: 60px;\n  fill: #009DA0; }\n\n:host /deep/ .donut1 .ct-outstanding .ct-slice-donut {\n  stroke: #eee; }\n\n:host /deep/ .donut1 .ct-done .ct-slice-donut {\n  stroke: #009DA0; }\n\n:host /deep/ .donut2 .ct-label {\n  text-anchor: middle;\n  alignment-baseline: middle;\n  font-size: 60px;\n  fill: #FF8D60; }\n\n:host /deep/ .donut2 .ct-outstanding .ct-slice-donut {\n  stroke: #eee; }\n\n:host /deep/ .donut2 .ct-done .ct-slice-donut {\n  stroke: #FF8D60; }\n\n:host /deep/ .donut3 .ct-label {\n  text-anchor: middle;\n  alignment-baseline: middle;\n  font-size: 60px;\n  fill: #FF586B; }\n\n:host /deep/ .donut3 .ct-outstanding .ct-slice-donut {\n  stroke: #eee; }\n\n:host /deep/ .donut3 .ct-done .ct-slice-donut {\n  stroke: #FF586A; }\n\n:host /deep/ .lineAreaChart .ct-series-a .ct-area {\n  fill: url(\"/dashboard/dashboard2#gradient\") !important; }\n\n:host /deep/ .lineAreaChart .ct-series-b .ct-area {\n  fill: #ff8d60;\n  fill-opacity: 0.1; }\n\n:host /deep/ .lineAreaChart .ct-point-circle {\n  stroke-width: 2px;\n  fill: white; }\n\n:host /deep/ .lineAreaChart .ct-series-b .ct-point-circle {\n  stroke: #ff8d60; }\n\n:host /deep/ .lineAreaChart .ct-series-a .ct-point-circle {\n  stroke: #31afb2; }\n\n:host /deep/ .lineAreaChart .ct-line {\n  fill: none;\n  stroke-width: 1px; }\n\n:host /deep/ .lineChart2 .ct-point-circle {\n  stroke-width: 2px;\n  fill: white; }\n\n:host /deep/ .lineChart2 .ct-series-b .ct-point-circle {\n  stroke: #ff8d60; }\n\n:host /deep/ .lineChart2 .ct-series-a .ct-point-circle {\n  stroke: #31afb2; }\n\n:host /deep/ .lineChart2 .ct-line {\n  fill: none;\n  stroke-width: 1px; }\n\n:host /deep/ .lineChart2Shadow {\n  -webkit-filter: drop-shadow(0px 25px 8px rgba(0, 0, 0, 0.3));\n  filter: drop-shadow(0px 25px 8px rgba(0, 0, 0, 0.3));\n  /* Same syntax as box-shadow, except \n                                                       for the spread property */ }\n\n:host /deep/ .lineChart1 .ct-line {\n  fill: none;\n  stroke-width: 3px; }\n\n:host /deep/ .lineChart1 .ct-point {\n  stroke-width: 0px; }\n\n:host /deep/ .lineChart1Shadow {\n  -webkit-filter: drop-shadow(0px 20px 6px rgba(0, 0, 0, 0.3));\n  filter: drop-shadow(0px 20px 6px rgba(0, 0, 0, 0.3));\n  /* Same syntax as box-shadow, except \n                                                       for the spread property */ }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZGFzaGJvYXJkL2Rhc2hib2FyZDIvQzpcXGRldmVsb3BlclxcYmFja2VuZC9zcmNcXGFwcFxcZGFzaGJvYXJkXFxkYXNoYm9hcmQyXFxkYXNoYm9hcmQyLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9kYXNoYm9hcmQvZGFzaGJvYXJkMi9kYXNoYm9hcmQyLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBO0VBQ0kscUJBQXFCO0VBQ3JCLDBCQUEwQixFQUFBOztBQUc5QjtFQUNJLGlCQUFpQixFQUFBOztBQUtyQjtFQUNJLGlCQUFpQixFQUFBOztBQUVyQjtFQUNJLFlBQVksRUFBQTs7QUFFaEI7RUFDSSw0REFBMkQ7RUFDbkQsb0RBQW1EO0VBQUUsOERBQUEsRUFBK0Q7O0FBTWhJO0VBQ0ksaUJBQWlCLEVBQUE7O0FBRXJCO0VBQ0ksMkRBQWlFLEVBQUE7O0FBR3JFO0VBQ0ksNERBQTJEO0VBQ25ELG9EQUFtRDtFQUFFO2dGQ1JlLEVEU0M7O0FBTWpGO0VBQ0ksaUJBQWlCLEVBQUE7O0FBRXJCO0VBQ0ksNERBQWlFLEVBQUE7O0FBRXJFO0VBQ0kscUJBQXFCO0VBQ3BCLGdDQUFnQyxFQUFBOztBQUVyQztFQUNJLDREQUEyRDtFQUNuRCxvREFBbUQ7RUFBRTtnRkNYZSxFRFlDOztBQU9qRjtFQUNJLG1CQUFtQjtFQUNuQiwwQkFBMEI7RUFDMUIsZUFBZTtFQUNmLGFBQWEsRUFBQTs7QUFHZjtFQUNFLFlBQVksRUFBQTs7QUFHZDtFQUNFLGVBQWUsRUFBQTs7QUFPakI7RUFDRSxtQkFBbUI7RUFDbkIsMEJBQTBCO0VBQzFCLGVBQWU7RUFDZixhQUFhLEVBQUE7O0FBR2Y7RUFDRSxZQUFZLEVBQUE7O0FBR2Q7RUFDRSxlQUFlLEVBQUE7O0FBT2pCO0VBQ0UsbUJBQW1CO0VBQ25CLDBCQUEwQjtFQUMxQixlQUFlO0VBQ2YsYUFBYSxFQUFBOztBQUdmO0VBQ0UsWUFBWSxFQUFBOztBQUdkO0VBQ0UsZUFBZSxFQUFBOztBQU9qQjtFQUNFLHNEQUE0RCxFQUFBOztBQUdoRTtFQUNJLGFBQWE7RUFDYixpQkFBaUIsRUFBQTs7QUFHckI7RUFDSSxpQkFBaUI7RUFDakIsV0FBVyxFQUFBOztBQUdmO0VBQ0ksZUFBZSxFQUFBOztBQUduQjtFQUNJLGVBQWUsRUFBQTs7QUFHbkI7RUFDSSxVQUFVO0VBQ1YsaUJBQWlCLEVBQUE7O0FBT3JCO0VBQ0ksaUJBQWlCO0VBQ2pCLFdBQVcsRUFBQTs7QUFHZjtFQUNJLGVBQWUsRUFBQTs7QUFHbkI7RUFDSSxlQUFlLEVBQUE7O0FBR25CO0VBQ0ksVUFBVTtFQUNWLGlCQUFpQixFQUFBOztBQUdyQjtFQUNJLDREQUEyRDtFQUNuRCxvREFBbUQ7RUFBRTtnRkNsRGUsRURtREM7O0FBTWpGO0VBQ0ksVUFBVTtFQUNWLGlCQUFpQixFQUFBOztBQUdyQjtFQUNJLGlCQUFpQixFQUFBOztBQUdyQjtFQUNJLDREQUEyRDtFQUNuRCxvREFBbUQ7RUFBRTtnRkN2RGUsRUR3REMiLCJmaWxlIjoic3JjL2FwcC9kYXNoYm9hcmQvZGFzaGJvYXJkMi9kYXNoYm9hcmQyLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiQGltcG9ydCBcIi4uLy4uLy4uL2Fzc2V0cy9zYXNzL3Njc3MvZ3JhZGllbnQtdmFyaWFibGVzXCI7XG5cbjpob3N0IC9kZWVwLyAuY3QtZ3JpZHtcbiAgICBzdHJva2UtZGFzaGFycmF5OiAwcHg7XG4gICAgc3Ryb2tlOiByZ2JhKDAsIDAsIDAsIDAuMSk7XG59XG5cbjpob3N0IC9kZWVwLyAuY3QtbGFiZWx7XG4gICAgZm9udC1zaXplOiAwLjlyZW07XG59XG5cbi8vd2lkZ2V0IExpbmUgQ2hhcnQgQ1NTIFN0YXJ0c1xuIFxuOmhvc3QgL2RlZXAvIC5XaWRnZXRsaW5lQ2hhcnQgLmN0LXBvaW50IHtcbiAgICBzdHJva2Utd2lkdGg6IDBweDtcbn1cbjpob3N0IC9kZWVwLyAuV2lkZ2V0bGluZUNoYXJ0IC5jdC1saW5le1xuICAgIHN0cm9rZTogI2ZmZjtcbn1cbjpob3N0IC9kZWVwLyAuV2lkZ2V0bGluZUNoYXJ0U2hhZG93IHtcbiAgICAtd2Via2l0LWZpbHRlcjogZHJvcC1zaGFkb3coIDBweCAxNXB4IDVweCByZ2JhKDAsMCwwLDAuOCkgKTtcbiAgICAgICAgICAgIGZpbHRlcjogZHJvcC1zaGFkb3coIDBweCAxNXB4IDVweCByZ2JhKDAsMCwwLDAuOCkgKTsgLyogU2FtZSBzeW50YXggYXMgYm94LXNoYWRvdywgZXhjZXB0IGZvciB0aGUgc3ByZWFkIHByb3BlcnR5ICovXG59XG5cbi8vd2lkZ2V0IExpbmUgQ2hhcnQgQ1NTIEVuZHNcblxuLy93aWRnZXQgTGluZSBDaGFydCAxIENTUyBTdGFydHNcbjpob3N0IC9kZWVwLyAuV2lkZ2V0bGluZUNoYXJ0MSAuY3QtcG9pbnQge1xuICAgIHN0cm9rZS13aWR0aDogMHB4O1xufVxuOmhvc3QgL2RlZXAvIC5XaWRnZXRsaW5lQ2hhcnQxIC5jdC1saW5le1xuICAgIHN0cm9rZTogdXJsKCRkYXNoYm9hcmQyLWdyYWRpZW50LXBhdGggKyAgI3dpZGdyYWRpZW50KSAhaW1wb3J0YW50OyAvLyAjMDA5REEwOyBcbn1cblxuOmhvc3QgL2RlZXAvIC5XaWRnZXRsaW5lQ2hhcnQxU2hhZG93IHtcbiAgICAtd2Via2l0LWZpbHRlcjogZHJvcC1zaGFkb3coIDBweCAyMHB4IDVweCByZ2JhKDAsMCwwLDAuMykgKTtcbiAgICAgICAgICAgIGZpbHRlcjogZHJvcC1zaGFkb3coIDBweCAyMHB4IDVweCByZ2JhKDAsMCwwLDAuMykgKTsgLyogU2FtZSBzeW50YXggYXMgYm94LXNoYWRvdywgZXhjZXB0IFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZvciB0aGUgc3ByZWFkIHByb3BlcnR5ICovXG59XG4vL3dpZGdldCBMaW5lIENoYXJ0IDEgQ1NTIEVuZHNcblxuLy93aWRnZXQgTGluZSBDaGFydCAyIENTUyBTdGFydHNcblxuOmhvc3QgL2RlZXAvIC5XaWRnZXRsaW5lQ2hhcnQyIC5jdC1wb2ludCB7XG4gICAgc3Ryb2tlLXdpZHRoOiAwcHg7XG59XG46aG9zdCAvZGVlcC8gLldpZGdldGxpbmVDaGFydDIgLmN0LWxpbmV7XG4gICAgc3Ryb2tlOnVybCgkZGFzaGJvYXJkMi1ncmFkaWVudC1wYXRoICsgICN3aWRncmFkaWVudDEpICFpbXBvcnRhbnQ7XG59XG46aG9zdCAvZGVlcC8gLldpZGdldGxpbmVDaGFydDIgLmN0LWdyaWQge1xuICAgIHN0cm9rZS1kYXNoYXJyYXk6IDBweDsgICAgXG4gICAgIHN0cm9rZTogcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjIpO1xufVxuOmhvc3QgL2RlZXAvIC5XaWRnZXRsaW5lQ2hhcnQyU2hhZG93IHtcbiAgICAtd2Via2l0LWZpbHRlcjogZHJvcC1zaGFkb3coIDBweCAyMHB4IDVweCByZ2JhKDAsMCwwLDAuMykgKTtcbiAgICAgICAgICAgIGZpbHRlcjogZHJvcC1zaGFkb3coIDBweCAyMHB4IDVweCByZ2JhKDAsMCwwLDAuMykgKTsgLyogU2FtZSBzeW50YXggYXMgYm94LXNoYWRvdywgZXhjZXB0IFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZvciB0aGUgc3ByZWFkIHByb3BlcnR5ICovXG59XG4vL3dpZGdldCBMaW5lIENoYXJ0IDIgQ1NTIEVuZHNcblxuXG4vL0RvbnV0IENoYXJ0IDEgQ1NTIFN0YXJ0c1xuXG46aG9zdCAvZGVlcC8gLmRvbnV0MSAuY3QtbGFiZWwge1xuICAgIHRleHQtYW5jaG9yOiBtaWRkbGU7XG4gICAgYWxpZ25tZW50LWJhc2VsaW5lOiBtaWRkbGU7XG4gICAgZm9udC1zaXplOiA2MHB4O1xuICAgIGZpbGw6ICMwMDlEQTA7XG4gIH1cblxuICA6aG9zdCAvZGVlcC8gLmRvbnV0MSAuY3Qtb3V0c3RhbmRpbmcgLmN0LXNsaWNlLWRvbnV0IHtcbiAgICBzdHJva2U6ICNlZWU7ICAgIFxuICB9XG5cbiAgOmhvc3QgL2RlZXAvIC5kb251dDEgLmN0LWRvbmUgLmN0LXNsaWNlLWRvbnV0IHtcbiAgICBzdHJva2U6ICMwMDlEQTA7XG4gIH1cblxuICAvL0RvbnV0IENoYXJ0IDEgQ1NTIEVuZHNcblxuICAvL0RvbnV0IENoYXJ0IDIgQ1NTIFN0YXJ0c1xuXG4gIDpob3N0IC9kZWVwLyAuZG9udXQyIC5jdC1sYWJlbCB7XG4gICAgdGV4dC1hbmNob3I6IG1pZGRsZTtcbiAgICBhbGlnbm1lbnQtYmFzZWxpbmU6IG1pZGRsZTtcbiAgICBmb250LXNpemU6IDYwcHg7XG4gICAgZmlsbDogI0ZGOEQ2MDtcbiAgfVxuXG4gIDpob3N0IC9kZWVwLyAuZG9udXQyIC5jdC1vdXRzdGFuZGluZyAuY3Qtc2xpY2UtZG9udXQge1xuICAgIHN0cm9rZTogI2VlZTsgICAgXG4gIH1cblxuICA6aG9zdCAvZGVlcC8gLmRvbnV0MiAuY3QtZG9uZSAuY3Qtc2xpY2UtZG9udXQge1xuICAgIHN0cm9rZTogI0ZGOEQ2MDtcbiAgfVxuXG4gIC8vRG9udXQgQ2hhcnQgMiBDU1MgRW5kc1xuXG4gIC8vRG9udXQgQ2hhcnQgMyBDU1MgU3RhcnRzXG5cbiAgOmhvc3QgL2RlZXAvIC5kb251dDMgLmN0LWxhYmVsIHtcbiAgICB0ZXh0LWFuY2hvcjogbWlkZGxlO1xuICAgIGFsaWdubWVudC1iYXNlbGluZTogbWlkZGxlO1xuICAgIGZvbnQtc2l6ZTogNjBweDtcbiAgICBmaWxsOiAjRkY1ODZCO1xuICB9XG5cbiAgOmhvc3QgL2RlZXAvIC5kb251dDMgLmN0LW91dHN0YW5kaW5nIC5jdC1zbGljZS1kb251dCB7XG4gICAgc3Ryb2tlOiAjZWVlOyAgICBcbiAgfVxuXG4gIDpob3N0IC9kZWVwLyAuZG9udXQzIC5jdC1kb25lIC5jdC1zbGljZS1kb251dCB7XG4gICAgc3Ryb2tlOiAjRkY1ODZBO1xuICB9XG5cbiAgLy9Eb251dCBDaGFydCAzIENTUyBFbmRzXG5cbiAgLy8gTGluZSBhcmVhIGNoYXJ0IENTUyBTdGFydHNcblxuICA6aG9zdCAvZGVlcC8gLmxpbmVBcmVhQ2hhcnQgLmN0LXNlcmllcy1hIC5jdC1hcmVhIHtcbiAgICBmaWxsOiB1cmwoJGRhc2hib2FyZDItZ3JhZGllbnQtcGF0aCArICAjZ3JhZGllbnQpICFpbXBvcnRhbnQ7XG59XG5cbjpob3N0IC9kZWVwLyAubGluZUFyZWFDaGFydCAuY3Qtc2VyaWVzLWIgLmN0LWFyZWEge1xuICAgIGZpbGw6ICNmZjhkNjA7XG4gICAgZmlsbC1vcGFjaXR5OiAwLjE7XG59XG5cbjpob3N0IC9kZWVwLyAubGluZUFyZWFDaGFydCAuY3QtcG9pbnQtY2lyY2xlIHtcbiAgICBzdHJva2Utd2lkdGg6IDJweDtcbiAgICBmaWxsOiB3aGl0ZTtcbn1cblxuOmhvc3QgL2RlZXAvIC5saW5lQXJlYUNoYXJ0IC5jdC1zZXJpZXMtYiAuY3QtcG9pbnQtY2lyY2xlIHtcbiAgICBzdHJva2U6ICNmZjhkNjA7XG59XG5cbjpob3N0IC9kZWVwLyAubGluZUFyZWFDaGFydCAuY3Qtc2VyaWVzLWEgLmN0LXBvaW50LWNpcmNsZSB7XG4gICAgc3Ryb2tlOiAjMzFhZmIyO1xufVxuXG46aG9zdCAvZGVlcC8gLmxpbmVBcmVhQ2hhcnQgLmN0LWxpbmUge1xuICAgIGZpbGw6IG5vbmU7XG4gICAgc3Ryb2tlLXdpZHRoOiAxcHg7XG59XG5cbi8vIExpbmUgYXJlYSBjaGFydCBDU1MgRW5kc1xuXG4vL0xpbmUgY2hhcnQgMiBDU1MgU3RhcnRzXG5cbjpob3N0IC9kZWVwLyAubGluZUNoYXJ0MiAuY3QtcG9pbnQtY2lyY2xlIHtcbiAgICBzdHJva2Utd2lkdGg6IDJweDtcbiAgICBmaWxsOiB3aGl0ZTtcbn1cblxuOmhvc3QgL2RlZXAvIC5saW5lQ2hhcnQyIC5jdC1zZXJpZXMtYiAuY3QtcG9pbnQtY2lyY2xlIHtcbiAgICBzdHJva2U6ICNmZjhkNjA7XG59XG5cbjpob3N0IC9kZWVwLyAubGluZUNoYXJ0MiAuY3Qtc2VyaWVzLWEgLmN0LXBvaW50LWNpcmNsZSB7XG4gICAgc3Ryb2tlOiAjMzFhZmIyO1xufVxuXG46aG9zdCAvZGVlcC8gLmxpbmVDaGFydDIgLmN0LWxpbmUge1xuICAgIGZpbGw6IG5vbmU7XG4gICAgc3Ryb2tlLXdpZHRoOiAxcHg7XG59XG5cbjpob3N0IC9kZWVwLyAubGluZUNoYXJ0MlNoYWRvdyB7XG4gICAgLXdlYmtpdC1maWx0ZXI6IGRyb3Atc2hhZG93KCAwcHggMjVweCA4cHggcmdiYSgwLDAsMCwwLjMpICk7XG4gICAgICAgICAgICBmaWx0ZXI6IGRyb3Atc2hhZG93KCAwcHggMjVweCA4cHggcmdiYSgwLDAsMCwwLjMpICk7IC8qIFNhbWUgc3ludGF4IGFzIGJveC1zaGFkb3csIGV4Y2VwdCBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBmb3IgdGhlIHNwcmVhZCBwcm9wZXJ0eSAqL1xufVxuLy9MaW5lIGNoYXJ0IDIgQ1NTIEVuZHNcblxuLy9MaW5lIENoYXJ0IDEgQ1NTIFN0YXJ0c1xuXG46aG9zdCAvZGVlcC8gLmxpbmVDaGFydDEgLmN0LWxpbmUge1xuICAgIGZpbGw6IG5vbmU7XG4gICAgc3Ryb2tlLXdpZHRoOiAzcHg7XG59XG5cbjpob3N0IC9kZWVwLyAubGluZUNoYXJ0MSAuY3QtcG9pbnQge1xuICAgIHN0cm9rZS13aWR0aDogMHB4O1xufVxuXG46aG9zdCAvZGVlcC8gLmxpbmVDaGFydDFTaGFkb3cge1xuICAgIC13ZWJraXQtZmlsdGVyOiBkcm9wLXNoYWRvdyggMHB4IDIwcHggNnB4IHJnYmEoMCwwLDAsMC4zKSApO1xuICAgICAgICAgICAgZmlsdGVyOiBkcm9wLXNoYWRvdyggMHB4IDIwcHggNnB4IHJnYmEoMCwwLDAsMC4zKSApOyAvKiBTYW1lIHN5bnRheCBhcyBib3gtc2hhZG93LCBleGNlcHQgXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZm9yIHRoZSBzcHJlYWQgcHJvcGVydHkgKi9cbn1cbi8vTGluZSBDaGFydCAxIENTUyBFbmRzIiwiOmhvc3QgL2RlZXAvIC5jdC1ncmlkIHtcbiAgc3Ryb2tlLWRhc2hhcnJheTogMHB4O1xuICBzdHJva2U6IHJnYmEoMCwgMCwgMCwgMC4xKTsgfVxuXG46aG9zdCAvZGVlcC8gLmN0LWxhYmVsIHtcbiAgZm9udC1zaXplOiAwLjlyZW07IH1cblxuOmhvc3QgL2RlZXAvIC5XaWRnZXRsaW5lQ2hhcnQgLmN0LXBvaW50IHtcbiAgc3Ryb2tlLXdpZHRoOiAwcHg7IH1cblxuOmhvc3QgL2RlZXAvIC5XaWRnZXRsaW5lQ2hhcnQgLmN0LWxpbmUge1xuICBzdHJva2U6ICNmZmY7IH1cblxuOmhvc3QgL2RlZXAvIC5XaWRnZXRsaW5lQ2hhcnRTaGFkb3cge1xuICAtd2Via2l0LWZpbHRlcjogZHJvcC1zaGFkb3coMHB4IDE1cHggNXB4IHJnYmEoMCwgMCwgMCwgMC44KSk7XG4gIGZpbHRlcjogZHJvcC1zaGFkb3coMHB4IDE1cHggNXB4IHJnYmEoMCwgMCwgMCwgMC44KSk7XG4gIC8qIFNhbWUgc3ludGF4IGFzIGJveC1zaGFkb3csIGV4Y2VwdCBmb3IgdGhlIHNwcmVhZCBwcm9wZXJ0eSAqLyB9XG5cbjpob3N0IC9kZWVwLyAuV2lkZ2V0bGluZUNoYXJ0MSAuY3QtcG9pbnQge1xuICBzdHJva2Utd2lkdGg6IDBweDsgfVxuXG46aG9zdCAvZGVlcC8gLldpZGdldGxpbmVDaGFydDEgLmN0LWxpbmUge1xuICBzdHJva2U6IHVybChcIi9kYXNoYm9hcmQvZGFzaGJvYXJkMiN3aWRncmFkaWVudFwiKSAhaW1wb3J0YW50OyB9XG5cbjpob3N0IC9kZWVwLyAuV2lkZ2V0bGluZUNoYXJ0MVNoYWRvdyB7XG4gIC13ZWJraXQtZmlsdGVyOiBkcm9wLXNoYWRvdygwcHggMjBweCA1cHggcmdiYSgwLCAwLCAwLCAwLjMpKTtcbiAgZmlsdGVyOiBkcm9wLXNoYWRvdygwcHggMjBweCA1cHggcmdiYSgwLCAwLCAwLCAwLjMpKTtcbiAgLyogU2FtZSBzeW50YXggYXMgYm94LXNoYWRvdywgZXhjZXB0IFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZvciB0aGUgc3ByZWFkIHByb3BlcnR5ICovIH1cblxuOmhvc3QgL2RlZXAvIC5XaWRnZXRsaW5lQ2hhcnQyIC5jdC1wb2ludCB7XG4gIHN0cm9rZS13aWR0aDogMHB4OyB9XG5cbjpob3N0IC9kZWVwLyAuV2lkZ2V0bGluZUNoYXJ0MiAuY3QtbGluZSB7XG4gIHN0cm9rZTogdXJsKFwiL2Rhc2hib2FyZC9kYXNoYm9hcmQyI3dpZGdyYWRpZW50MVwiKSAhaW1wb3J0YW50OyB9XG5cbjpob3N0IC9kZWVwLyAuV2lkZ2V0bGluZUNoYXJ0MiAuY3QtZ3JpZCB7XG4gIHN0cm9rZS1kYXNoYXJyYXk6IDBweDtcbiAgc3Ryb2tlOiByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuMik7IH1cblxuOmhvc3QgL2RlZXAvIC5XaWRnZXRsaW5lQ2hhcnQyU2hhZG93IHtcbiAgLXdlYmtpdC1maWx0ZXI6IGRyb3Atc2hhZG93KDBweCAyMHB4IDVweCByZ2JhKDAsIDAsIDAsIDAuMykpO1xuICBmaWx0ZXI6IGRyb3Atc2hhZG93KDBweCAyMHB4IDVweCByZ2JhKDAsIDAsIDAsIDAuMykpO1xuICAvKiBTYW1lIHN5bnRheCBhcyBib3gtc2hhZG93LCBleGNlcHQgXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZm9yIHRoZSBzcHJlYWQgcHJvcGVydHkgKi8gfVxuXG46aG9zdCAvZGVlcC8gLmRvbnV0MSAuY3QtbGFiZWwge1xuICB0ZXh0LWFuY2hvcjogbWlkZGxlO1xuICBhbGlnbm1lbnQtYmFzZWxpbmU6IG1pZGRsZTtcbiAgZm9udC1zaXplOiA2MHB4O1xuICBmaWxsOiAjMDA5REEwOyB9XG5cbjpob3N0IC9kZWVwLyAuZG9udXQxIC5jdC1vdXRzdGFuZGluZyAuY3Qtc2xpY2UtZG9udXQge1xuICBzdHJva2U6ICNlZWU7IH1cblxuOmhvc3QgL2RlZXAvIC5kb251dDEgLmN0LWRvbmUgLmN0LXNsaWNlLWRvbnV0IHtcbiAgc3Ryb2tlOiAjMDA5REEwOyB9XG5cbjpob3N0IC9kZWVwLyAuZG9udXQyIC5jdC1sYWJlbCB7XG4gIHRleHQtYW5jaG9yOiBtaWRkbGU7XG4gIGFsaWdubWVudC1iYXNlbGluZTogbWlkZGxlO1xuICBmb250LXNpemU6IDYwcHg7XG4gIGZpbGw6ICNGRjhENjA7IH1cblxuOmhvc3QgL2RlZXAvIC5kb251dDIgLmN0LW91dHN0YW5kaW5nIC5jdC1zbGljZS1kb251dCB7XG4gIHN0cm9rZTogI2VlZTsgfVxuXG46aG9zdCAvZGVlcC8gLmRvbnV0MiAuY3QtZG9uZSAuY3Qtc2xpY2UtZG9udXQge1xuICBzdHJva2U6ICNGRjhENjA7IH1cblxuOmhvc3QgL2RlZXAvIC5kb251dDMgLmN0LWxhYmVsIHtcbiAgdGV4dC1hbmNob3I6IG1pZGRsZTtcbiAgYWxpZ25tZW50LWJhc2VsaW5lOiBtaWRkbGU7XG4gIGZvbnQtc2l6ZTogNjBweDtcbiAgZmlsbDogI0ZGNTg2QjsgfVxuXG46aG9zdCAvZGVlcC8gLmRvbnV0MyAuY3Qtb3V0c3RhbmRpbmcgLmN0LXNsaWNlLWRvbnV0IHtcbiAgc3Ryb2tlOiAjZWVlOyB9XG5cbjpob3N0IC9kZWVwLyAuZG9udXQzIC5jdC1kb25lIC5jdC1zbGljZS1kb251dCB7XG4gIHN0cm9rZTogI0ZGNTg2QTsgfVxuXG46aG9zdCAvZGVlcC8gLmxpbmVBcmVhQ2hhcnQgLmN0LXNlcmllcy1hIC5jdC1hcmVhIHtcbiAgZmlsbDogdXJsKFwiL2Rhc2hib2FyZC9kYXNoYm9hcmQyI2dyYWRpZW50XCIpICFpbXBvcnRhbnQ7IH1cblxuOmhvc3QgL2RlZXAvIC5saW5lQXJlYUNoYXJ0IC5jdC1zZXJpZXMtYiAuY3QtYXJlYSB7XG4gIGZpbGw6ICNmZjhkNjA7XG4gIGZpbGwtb3BhY2l0eTogMC4xOyB9XG5cbjpob3N0IC9kZWVwLyAubGluZUFyZWFDaGFydCAuY3QtcG9pbnQtY2lyY2xlIHtcbiAgc3Ryb2tlLXdpZHRoOiAycHg7XG4gIGZpbGw6IHdoaXRlOyB9XG5cbjpob3N0IC9kZWVwLyAubGluZUFyZWFDaGFydCAuY3Qtc2VyaWVzLWIgLmN0LXBvaW50LWNpcmNsZSB7XG4gIHN0cm9rZTogI2ZmOGQ2MDsgfVxuXG46aG9zdCAvZGVlcC8gLmxpbmVBcmVhQ2hhcnQgLmN0LXNlcmllcy1hIC5jdC1wb2ludC1jaXJjbGUge1xuICBzdHJva2U6ICMzMWFmYjI7IH1cblxuOmhvc3QgL2RlZXAvIC5saW5lQXJlYUNoYXJ0IC5jdC1saW5lIHtcbiAgZmlsbDogbm9uZTtcbiAgc3Ryb2tlLXdpZHRoOiAxcHg7IH1cblxuOmhvc3QgL2RlZXAvIC5saW5lQ2hhcnQyIC5jdC1wb2ludC1jaXJjbGUge1xuICBzdHJva2Utd2lkdGg6IDJweDtcbiAgZmlsbDogd2hpdGU7IH1cblxuOmhvc3QgL2RlZXAvIC5saW5lQ2hhcnQyIC5jdC1zZXJpZXMtYiAuY3QtcG9pbnQtY2lyY2xlIHtcbiAgc3Ryb2tlOiAjZmY4ZDYwOyB9XG5cbjpob3N0IC9kZWVwLyAubGluZUNoYXJ0MiAuY3Qtc2VyaWVzLWEgLmN0LXBvaW50LWNpcmNsZSB7XG4gIHN0cm9rZTogIzMxYWZiMjsgfVxuXG46aG9zdCAvZGVlcC8gLmxpbmVDaGFydDIgLmN0LWxpbmUge1xuICBmaWxsOiBub25lO1xuICBzdHJva2Utd2lkdGg6IDFweDsgfVxuXG46aG9zdCAvZGVlcC8gLmxpbmVDaGFydDJTaGFkb3cge1xuICAtd2Via2l0LWZpbHRlcjogZHJvcC1zaGFkb3coMHB4IDI1cHggOHB4IHJnYmEoMCwgMCwgMCwgMC4zKSk7XG4gIGZpbHRlcjogZHJvcC1zaGFkb3coMHB4IDI1cHggOHB4IHJnYmEoMCwgMCwgMCwgMC4zKSk7XG4gIC8qIFNhbWUgc3ludGF4IGFzIGJveC1zaGFkb3csIGV4Y2VwdCBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBmb3IgdGhlIHNwcmVhZCBwcm9wZXJ0eSAqLyB9XG5cbjpob3N0IC9kZWVwLyAubGluZUNoYXJ0MSAuY3QtbGluZSB7XG4gIGZpbGw6IG5vbmU7XG4gIHN0cm9rZS13aWR0aDogM3B4OyB9XG5cbjpob3N0IC9kZWVwLyAubGluZUNoYXJ0MSAuY3QtcG9pbnQge1xuICBzdHJva2Utd2lkdGg6IDBweDsgfVxuXG46aG9zdCAvZGVlcC8gLmxpbmVDaGFydDFTaGFkb3cge1xuICAtd2Via2l0LWZpbHRlcjogZHJvcC1zaGFkb3coMHB4IDIwcHggNnB4IHJnYmEoMCwgMCwgMCwgMC4zKSk7XG4gIGZpbHRlcjogZHJvcC1zaGFkb3coMHB4IDIwcHggNnB4IHJnYmEoMCwgMCwgMCwgMC4zKSk7XG4gIC8qIFNhbWUgc3ludGF4IGFzIGJveC1zaGFkb3csIGV4Y2VwdCBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBmb3IgdGhlIHNwcmVhZCBwcm9wZXJ0eSAqLyB9XG4iXX0= */"

/***/ }),

/***/ "./src/app/dashboard/dashboard2/dashboard2.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/dashboard/dashboard2/dashboard2.component.ts ***!
  \**************************************************************/
/*! exports provided: Dashboard2Component */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Dashboard2Component", function() { return Dashboard2Component; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var chartist__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! chartist */ "./node_modules/chartist/dist/chartist.js");
/* harmony import */ var chartist__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(chartist__WEBPACK_IMPORTED_MODULE_2__);



var data = __webpack_require__(/*! ../../shared/data/chartist.json */ "./src/app/shared/data/chartist.json");
var Dashboard2Component = /** @class */ (function () {
    function Dashboard2Component() {
        // Line chart configuration Starts
        this.WidgetlineChart = {
            type: 'Line', data: data['WidgetlineChart2'],
            options: {
                axisX: {
                    showGrid: false,
                    showLabel: false,
                    offset: 0,
                },
                axisY: {
                    showGrid: false,
                    low: 50,
                    showLabel: false,
                    offset: 0,
                },
                fullWidth: true
            },
        };
        // Line chart configuration Ends
        // Line chart configuration Starts
        this.WidgetlineChart1 = {
            type: 'Line', data: data['WidgetlineChart3'],
            options: {
                axisX: {
                    showGrid: false,
                    showLabel: false,
                    offset: 0,
                },
                axisY: {
                    showGrid: false,
                    low: 50,
                    showLabel: false,
                    offset: 0,
                },
                fullWidth: true,
                chartPadding: { top: 0, right: 0, bottom: 10, left: 0 }
            },
            events: {
                created: function (data) {
                    var defs = data.svg.elem('defs');
                    defs.elem('linearGradient', {
                        id: 'widgradient',
                        x1: 0,
                        y1: 1,
                        x2: 0,
                        y2: 0
                    }).elem('stop', {
                        offset: 0,
                        'stop-color': 'rgba(132, 60, 247, 1)'
                    }).parent().elem('stop', {
                        offset: 1,
                        'stop-color': 'rgba(56, 184, 242, 1)'
                    });
                },
            },
        };
        // Line chart configuration Ends
        // Line chart configuration Starts
        this.WidgetlineChart2 = {
            type: 'Line', data: data['WidgetlineChart'],
            options: {
                axisX: {
                    showGrid: true,
                    showLabel: false,
                    offset: 0,
                },
                axisY: {
                    showGrid: false,
                    low: 40,
                    showLabel: false,
                    offset: 0,
                },
                lineSmooth: chartist__WEBPACK_IMPORTED_MODULE_2__["Interpolation"].cardinal({
                    tension: 0
                }),
                fullWidth: true
            },
            events: {
                created: function (data) {
                    var defs = data.svg.elem('defs');
                    defs.elem('linearGradient', {
                        id: 'widgradient1',
                        x1: 0,
                        y1: 1,
                        x2: 0,
                        y2: 0
                    }).elem('stop', {
                        offset: 0,
                        'stop-color': 'rgba(0, 201, 255,1)'
                    }).parent().elem('stop', {
                        offset: 1,
                        'stop-color': 'rgba(17,228,183, 1)'
                    });
                },
            },
        };
        // Line chart configuration Ends
        // Donut chart configuration Starts
        this.DonutChart1 = {
            type: 'Pie',
            data: data['DashboardDonut'],
            options: {
                donut: true,
                donutWidth: 3,
                startAngle: 0,
                chartPadding: 25,
                labelInterpolationFnc: function (value) {
                    return '\ue9c9';
                }
            },
            events: {
                draw: function (data) {
                    if (data.type === 'label') {
                        if (data.index === 0) {
                            data.element.attr({
                                dx: data.element.root().width() / 2,
                                dy: (data.element.root().height() + (data.element.height() / 4)) / 2,
                                class: 'ct-label',
                                'font-family': 'feather'
                            });
                        }
                        else {
                            data.element.remove();
                        }
                    }
                }
            }
        };
        // Donut chart configuration Ends
        // Donut chart configuration Starts
        this.DonutChart2 = {
            type: 'Pie',
            data: data['DashboardDonut'],
            options: {
                donut: true,
                donutWidth: 3,
                startAngle: 90,
                chartPadding: 25,
                labelInterpolationFnc: function (value) {
                    return '\ue9e7';
                }
            },
            events: {
                draw: function (data) {
                    if (data.type === 'label') {
                        if (data.index === 0) {
                            data.element.attr({
                                dx: data.element.root().width() / 2,
                                dy: (data.element.root().height() + (data.element.height() / 4)) / 2,
                                class: 'ct-label',
                                'font-family': 'feather'
                            });
                        }
                        else {
                            data.element.remove();
                        }
                    }
                }
            }
        };
        // Donut chart configuration Ends
        // Donut chart configuration Starts
        this.DonutChart3 = {
            type: 'Pie',
            data: data['DashboardDonut'],
            options: {
                donut: true,
                donutWidth: 3,
                startAngle: 270,
                chartPadding: 25,
                labelInterpolationFnc: function (value) {
                    return '\ue964';
                }
            },
            events: {
                draw: function (data) {
                    if (data.type === 'label') {
                        if (data.index === 0) {
                            data.element.attr({
                                dx: data.element.root().width() / 2,
                                dy: (data.element.root().height() + (data.element.height() / 4)) / 2,
                                class: 'ct-label',
                                'font-family': 'feather'
                            });
                        }
                        else {
                            data.element.remove();
                        }
                    }
                }
            }
        };
        // Donut chart configuration Ends
        // Line area chart configuration Starts
        this.lineAreaChart = {
            type: 'Line',
            data: data['lineArea3'],
            options: {
                low: 0,
                showArea: true,
                fullWidth: true,
                onlyInteger: true,
                axisY: {
                    low: 0,
                    scaleMinSpace: 50,
                },
                axisX: {
                    showGrid: false
                }
            },
            events: {
                created: function (data) {
                    var defs = data.svg.elem('defs');
                    defs.elem('linearGradient', {
                        id: 'gradient',
                        x1: 0,
                        y1: 1,
                        x2: 0,
                        y2: 0
                    }).elem('stop', {
                        offset: 0,
                        'stop-opacity': '0.2',
                        'stop-color': 'rgba(255, 255, 255, 1)'
                    }).parent().elem('stop', {
                        offset: 1,
                        'stop-opacity': '0.2',
                        'stop-color': 'rgba(38, 198, 218, 1)'
                    });
                },
                draw: function (data) {
                    var circleRadius = 6;
                    if (data.type === 'point') {
                        var circle = new chartist__WEBPACK_IMPORTED_MODULE_2__["Svg"]('circle', {
                            cx: data.x,
                            cy: data.y,
                            r: circleRadius,
                            class: 'ct-point-circle'
                        });
                        data.element.replace(circle);
                    }
                }
            },
        };
        // Line area chart configuration Ends
        // Line chart configuration Starts
        this.lineChart2 = {
            type: 'Line', data: data['line2'],
            options: {
                axisX: {
                    showGrid: false,
                },
                axisY: {
                    low: 0,
                    scaleMinSpace: 50,
                },
                fullWidth: true,
            },
            responsiveOptions: [
                ['screen and (max-width: 640px) and (min-width: 381px)', {
                        axisX: {
                            labelInterpolationFnc: function (value, index) {
                                return index % 2 === 0 ? value : null;
                            }
                        }
                    }],
                ['screen and (max-width: 380px)', {
                        axisX: {
                            labelInterpolationFnc: function (value, index) {
                                return index % 3 === 0 ? value : null;
                            }
                        }
                    }]
            ],
            events: {
                draw: function (data) {
                    var circleRadius = 6;
                    if (data.type === 'point') {
                        var circle = new chartist__WEBPACK_IMPORTED_MODULE_2__["Svg"]('circle', {
                            cx: data.x,
                            cy: data.y,
                            r: circleRadius,
                            class: 'ct-point-circle'
                        });
                        data.element.replace(circle);
                    }
                    else if (data.type === 'label') {
                        // adjust label position for rotation
                        var dX = data.width / 2 + (30 - data.width);
                        data.element.attr({ x: data.element.attr('x') - dX });
                    }
                }
            },
        };
        // Line chart configuration Ends
        // Line chart configuration Starts
        this.lineChart1 = {
            type: 'Line', data: data['line1'],
            options: {
                axisX: {
                    showGrid: false,
                },
                axisY: {
                    low: 0,
                    scaleMinSpace: 50,
                },
                fullWidth: true
            },
            events: {
                draw: function (data) {
                    if (data.type === 'label') {
                        // adjust label position for rotation
                        var dX = data.width / 2 + (30 - data.width);
                        data.element.attr({ x: data.element.attr('x') - dX });
                    }
                }
            },
        };
        // Line chart configuration Ends
    }
    Dashboard2Component = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-dashboard2',
            template: __webpack_require__(/*! raw-loader!./dashboard2.component.html */ "./node_modules/raw-loader/index.js!./src/app/dashboard/dashboard2/dashboard2.component.html"),
            styles: [__webpack_require__(/*! ./dashboard2.component.scss */ "./src/app/dashboard/dashboard2/dashboard2.component.scss")]
        })
    ], Dashboard2Component);
    return Dashboard2Component;
}());



/***/ }),

/***/ "./src/app/shared/directives/match-height.directive.ts":
/*!*************************************************************!*\
  !*** ./src/app/shared/directives/match-height.directive.ts ***!
  \*************************************************************/
/*! exports provided: MatchHeightDirective, MatchHeightModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MatchHeightDirective", function() { return MatchHeightDirective; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MatchHeightModule", function() { return MatchHeightModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var MatchHeightDirective = /** @class */ (function () {
    function MatchHeightDirective(el) {
        this.el = el;
    }
    MatchHeightDirective.prototype.ngAfterViewInit = function () {
        var _this = this;
        // call our matchHeight function here
        setTimeout(function () { _this.matchHeights(_this.el.nativeElement, _this.matchHeight); }, 300);
    };
    MatchHeightDirective.prototype.matchHeights = function (parent, className) {
        // match height logic here
        if (!parent)
            return;
        // step 1: find all the child elements with the selected class name
        var children = parent.getElementsByClassName(className);
        if (!children)
            return;
        //Pixinvent - Match hight - fix --- comment below code
        Array.from(children).forEach(function (x) {
            x.style.height = 'initial';
        });
        // step 2a: get all the child elements heights
        var itemHeights = Array.from(children)
            .map(function (x) { return x.getBoundingClientRect().height; });
        // step 2b: find out the tallest
        var maxHeight = itemHeights.reduce(function (prev, curr) {
            return curr > prev ? curr : prev;
        }, 0);
        // step 3: update all the child elements to the tallest height
        Array.from(children)
            .forEach(function (x) { return x.style.height = maxHeight + "px"; });
    };
    MatchHeightDirective.prototype.onResize = function () {
        // call our matchHeight function here
        this.matchHeights(this.el.nativeElement, this.matchHeight);
    };
    MatchHeightDirective.ctorParameters = function () { return [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], MatchHeightDirective.prototype, "matchHeight", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])('window:resize'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", []),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
    ], MatchHeightDirective.prototype, "onResize", null);
    MatchHeightDirective = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Directive"])({
            selector: '[matchHeight]'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"]])
    ], MatchHeightDirective);
    return MatchHeightDirective;
}());

var MatchHeightModule = /** @class */ (function () {
    function MatchHeightModule() {
    }
    MatchHeightModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [MatchHeightDirective],
            exports: [MatchHeightDirective]
        })
    ], MatchHeightModule);
    return MatchHeightModule;
}());



/***/ })

}]);
//# sourceMappingURL=dashboard-dashboard-module.js.map