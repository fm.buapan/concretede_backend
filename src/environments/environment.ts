// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyAscilrqRznimU8mwoLrufXfvo8m2VnQDc",
    authDomain: "concretedeliveryeasy-f16ad.firebaseapp.com",
    databaseURL: "https://concretedeliveryeasy-f16ad.firebaseio.com",
    projectId: "concretedeliveryeasy-f16ad",
    storageBucket: "concretedeliveryeasy-f16ad.appspot.com",
    messagingSenderId: "472114082705",
    appId: "1:472114082705:web:ba657314a370fce3936030",
    measurementId: "G-GV7WW66GQD"
  }

};

