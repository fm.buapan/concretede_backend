import { trigger } from '@angular/animations';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { Component, OnInit, Input, ViewChild, ElementRef, ChangeDetectorRef } from '@angular/core';
import { NgbModal, ModalDismissReasons, NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import swal from 'sweetalert2';
import { WebapiService } from 'app/webapi.service';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { CKEditorModule, ChangeEvent } from '@ckeditor/ckeditor5-angular';
@Component({
  selector: 'view-modal-news',
  templateUrl: './view_news.html',
  styleUrls: ['./view_news.scss']

})
export class viewModalNews implements OnInit {
  @Input() para;
  // newsForm: FormGroup;
  public viewnews :any;
  constructor(public activeModal: NgbActiveModal) {

  }
  close() {
    this.activeModal.dismiss();
  }
  Editor = ClassicEditor;
  editorConfig = {

  };
  ngOnInit() {
    console.log("view");
    console.log(this.para);
    this.viewnews=this.para
    // this.newsForm = new FormGroup({
    //   'email': new FormControl(null, [Validators.required, Validators.email]),
    //   'username': new FormControl(null, [Validators.required, Validators.minLength(4), Validators.maxLength(24)]),
    //   'password': new FormControl(null, [Validators.required, Validators.minLength(4), Validators.maxLength(24)]),
    //   'name': new FormControl(null, [Validators.required, Validators.email, Validators.minLength(4)]),
    //   'phone': new FormControl(null, [Validators.required, Validators.email, Validators.minLength(9), Validators.minLength(10)]),
    // }, { updateOn: 'blur' });
    // this.newsForm.patchValue({username:'xxx'});
  }
  c(val) {
    console.log(val);
  }
}

@Component({
  selector: 'ngbd-modal-news',
  templateUrl: './add_news.html',
  styleUrls: ['./add_news.scss']
})
export class NgbdModalNews implements OnInit {
  @Input() para;
  newsForm: FormGroup;
  @ViewChild("file", { static: false }) file: ElementRef;
  constructor(public activeModal: NgbActiveModal,
    private fb: FormBuilder, private cd: ChangeDetectorRef,
    public toastr: ToastrService,
    public http: HttpClient, public api: WebapiService) {

  }
  Editor = ClassicEditor;
  editorConfig = {

  };
  file_change(event) {
    const reader = new FileReader();
    if (event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      console.log(event.target.files);
      if (event.target.files[0].size < 2000000) {
        if (event.target.files[0].type == 'image/jpeg' || event.target.files[0].type == 'image/png') {
          reader.readAsDataURL(file);
          reader.onload = () => {
            this.api.postData('news/upload', { data: reader.result, type: event.target.files[0].type }).then((res: any) => {
              console.log(res);
              this.para.picture = res.url;
              this.cd.detectChanges();
            });

            this.cd.markForCheck();
          };
        } else {
          swal.fire(
            'ต้องเป็นไฟล์ jpg หรือ png เท่านั้น',
            '',
            'error'
          )
        }

      } else {
        swal.fire(
          'ไฟล์ต้องมีขนาดน้อยกกว่า 2 Mb',
          '',
          'error'
        )
      }

    }

  }
  change_picture() {
    console.log('changepic');
    this.file.nativeElement.click();
  }
  onReactiveFormSubmit() {
    let data = this.newsForm.value;
    data.n_id = this.para.n_id;
    this.api.postData('news/add_news', data).then((res: any) => {
      console.log(res);
      if (res == '1') {
        this.toastr.success('บันทึกเรียบรอ้แล้ว');
        this.activeModal.close();
      } else {
        this.toastr.error('มีปัญหาในการบันทึกข้อมูล');
      }
    });
  }
  close() {
    this.activeModal.dismiss();
  }
  ngOnInit() {
    
    if (this.para.t_id == '0') {
      this.para = {
        n_id: "0",
        title: { th: '', en: '' },
        content: { th: '', en: '' }
      }
      this.newsForm = new FormGroup({
        'title': new FormGroup({
          'th': new FormControl('', [Validators.required]),
          'en': new FormControl('', [Validators.required])
        }),
        'content': new FormGroup({
          'th': new FormControl('', [Validators.required]),
          'en': new FormControl('', [Validators.required])
        }),
        'show': new FormGroup({
          'show': new FormControl(true, ),
        })
      });

    }else{
      this.newsForm = new FormGroup({
        'title': new FormGroup({
          'th': new FormControl('', [Validators.required]),
          'en': new FormControl('', [Validators.required])
        }),
        'content': new FormGroup({
          'th': new FormControl('', [Validators.required]),
          'en': new FormControl('', [Validators.required])
        }),
        'show': new FormGroup({
          'show': new FormControl(this.para.show, ),
        }),
      });
    }

    this.newsForm.patchValue(this.para);
  }
  c(val) {
    console.log(val);
  }
}





@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss']
})
export class NewsComponent implements OnInit {
  closeResult: string;
  public news = [];
  newsForm: FormGroup;
  public searchForm: any;
  public all_page = 0;
  page: number = 1;
  constructor(private modalService: NgbModal, public api: WebapiService) {
    this.news = this.api.storage_get('news') || [];
    this.searchForm = new FormGroup({
      'search': new FormControl('')
    });
  }
  onReactiveFormSubmit() {
    let a = this.newsForm.value();
    console.log(a);
  }
  ngOnInit() {
    this.api.check_login();
    this.newsForm = new FormGroup({
      'inputEmail': new FormControl(null, [Validators.required, Validators.email]),
      'password': new FormControl(null, [Validators.required, Validators.minLength(4), Validators.maxLength(24)]),
      'textArea': new FormControl(null, [Validators.required]),
      'radioOption': new FormControl('Option one is this')
    }, { updateOn: 'blur' });
    this.load_data(1);
  }
  view(t) {
    console.log(t);
    const modal: NgbModalRef = this.modalService.open(viewModalNews, { size: 'lg', backdrop: 'static' });
    modal.componentInstance.para = t;
    modal.result.then((result) => {
      console.log(result);
    }, (reason) => {
    });
    modal.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  view_profile(t) {
    this.view(t);
    
  }
  load_data(page) {
    this.api.getData('news/load_news/' + page + "?search=" + this.searchForm.value.search).then((data: any) => {
      console.log(data);
      this.news = data.data;
      this.all_page = data.count / 20 * 20;
      this.api.storage_set('news', data.data);
    })
  }
  search() {
    console.log(this.searchForm.value);
    this.load_data(1);
  }
  open(t) {
    console.log(t);
    const modal: NgbModalRef = this.modalService.open(NgbdModalNews, { size: 'lg', backdrop: 'static' });
    modal.componentInstance.para = t;
    modal.result.then((result) => {
      this.load_data(this.page);
    }, (reason) => {
      console.log(reason);
    });
    modal.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  change_page(page) {
    this.load_data(page);
  }
  delete_member(t) {
    swal.fire({
      title: 'คุณต้องการลบจริงหรือไม่',
      text: "รายการนี้จะไม่สามารถนำกลับมาได้อีก",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#0CC27E',
      cancelButtonColor: '#FF586B',
      confirmButtonText: 'ลบออก',
      cancelButtonText: 'ปิดออก',
      confirmButtonClass: 'btn btn-success btn-raised mr-2',
      cancelButtonClass: 'btn btn-default btn-raised',
      buttonsStyling: false
    }).then((result) => {
      console.log(t);
      if (result.value) {
        this.api.postData('news/remove_news', { n_id: t.n_id }).then((res: any) => {
          console.log(res);
          this.load_data(this.page);
          swal.fire(
            'ลบเรียบร้อยแล้ว',
            '',
            'success'
          )
        })

      }

    });
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
}
