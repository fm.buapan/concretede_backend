import { Component, OnInit, Input, ChangeDetectorRef } from '@angular/core';
import { WebapiService } from 'app/webapi.service';
import { FormGroup, FormControl } from '@angular/forms';
import { NgbModalRef, ModalDismissReasons, NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AngularFireDatabase } from '@angular/fire/database'
import Swal from 'sweetalert2';
import { Lightbox } from 'ngx-lightbox';

@Component({
  selector: 'view-modal-content',
  templateUrl: './confirm_details.html',
  styleUrls: ['./confirm_details.scss']
})
export class viewModalConfirm implements OnInit {
  @Input() data:any = {
    sale_id:0
  }
  public admin: any = { name: '' };
  public admin_type = 0;
  constructor(
    public activeModal: NgbActiveModal,
    public api: WebapiService,
    public ref: ChangeDetectorRef,
    public firebase: AngularFireDatabase
  ) {

  }
  close() {
    this.activeModal.dismiss(this.data);
  }
  public imagePath: any = '';
  public url = '';
  public reader: any;
  public type: any;
  public formData = new FormData();
  ngOnInit(){
    console.log(this.data);
    this.admin = this.api.storage_get('logindata')
    this.api.getData('Confirm/salebank/'+this.data.sale_id).then((res:any)=>{
      this.data.bank = res;
      console.log(res);
      
    })
  }
  sbank(bb_id) {
    this.bb_id = bb_id
    console.log(bb_id);

  }
  public bb_id = ''
  confirm() {
    console.log(this.reader);
    if (this.bb_id != '') {
      if (this.reader != undefined) {
        this.api.postData('Confirm/confirm_sale', { img: this.reader.result, type: this.type, order_id: this.data.order_id, bb_id: this.bb_id, admin: this.admin.t_id, price: this.data.com_price }).then((res: any) => {
          if(res == 'success'){
            this.api.fb_set('order')
          Swal.fire(
            'สำเร็จ!',
            '',
            'success'
          )
          setTimeout(() => {
            this.close();
          }, 1000);
          
          }else{
            Swal.fire(
              'สำเร็จ!',
              '',
              'warning'
            )
            setTimeout(() => {
            this.close();
          }, 1000);
          }
          
        })
      } else {
        Swal.fire(
          'กรุณาเลือกหลักฐานก่อน',
          '',
          'warning'
        )
      }

    } else {
      Swal.fire(
        'กรุณาเลือกบัญชีธนาคารก่อน',
        '',
        'warning'
      )
    }

  }
  cancel(pm_id) {
    // console.log('Cancel by admin ID =',this.admin.t_id,' pm_id =',pm_id);
    // this.api.postData('Payment/confirm_payment/',{pm_id:pm_id,id:this.admin.t_id,confirm:-1}).then((res)=>{
    //   this.close()
    // })
  }
  changeImg(event): void {
    let data = {
      member_id: '',
      message: ''
    };
    console.log(event);
    this.formData.append('avatar', event.target.files[0]);
    this.reader = new FileReader();
    this.reader.onload = (event: any) => {
      this.url = event.target.result;
      this.ref.detectChanges()
    }
    this.reader.readAsDataURL(event.target.files[0]);
    this.type = event.target.files[0].type;

    console.log(this.reader.result);
    setTimeout(() => {


      // this.api.postData("Confirm/upload", { img: this.reader.result, type: this.type, member_id:member_id }).then((res: any) => {
      // })
    }, 1000);
  }
}

@Component({
  selector: 'app-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.scss']
})
export class ConfirmComponent implements OnInit {
  public searchForm: any;
  public data: any;
  public all_page = 0;
  public order: any;
  public bank: any;
  public buff = {
    bb_id: '',
    order_id: ''
  }
  closeResult: string;
  public admin: any = { name: '' }
  constructor(
    public api: WebapiService, 
    private modalService: NgbModal, 
    public firebase: AngularFireDatabase,
    private lightbox: Lightbox
    ) { }
  public fb = false
  ngOnInit() {
    this.fb = true
    this.searchForm = new FormGroup({
      'search': new FormControl('')
    });
    this.admin = this.api.storage_get('logindata')
    this.api.fb_val('order', (res:any) => {
      if (this.fb) {
        this.load_bank(1)
      }
      
    })
    
    // this.load_score(1)
  }
  ngOnDestroy(){
    this.fb = false
  }
  
  load_bank(page) {
    this.api.getData('Confirm/get_salebank/' + page + "?search=" + this.searchForm.value.search).then((res: any) => {
      this.order = res.order;
      console.log(res);
    })
  }
  load_confirm(page) {
    this.api.getData('Confirm/load_confirm/' + page + "?search=" + this.searchForm.value.search).then((res: any) => {
      console.log(res);
      this.data = res.data
      this.all_page = res.count / 20 * 20;
    })
  }
  search() {
    this.load_confirm(1)
  }
  change_page($event) {
    // console.log($event);

  }
  test(t) {
    console.log(t);
    const modal: NgbModalRef = this.modalService.open(viewModalConfirm, { size: 'lg' });
    modal.componentInstance.payment = t;
    modal.result.then((result) => {
      console.log(result);
    }, (reason) => {
    });
    modal.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      this.load_bank(1)
      // this.load_score(1)
    });
  }
  private getDismissReason(reason: any): string {
    // this.load_paymant(1)
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  sbank(bb_id, order_id) {
    console.log(bb_id + '/' + order_id);
    this.buff.bb_id = bb_id;
    this.buff.order_id = order_id;
    // console.log('asasdasdasfsjdhgfskjdf');

  }
  continue(data) {
    const modal: NgbModalRef = this.modalService.open(viewModalConfirm, { size: 'lg' });
    modal.componentInstance.data = data;
    modal.result.then((result) => {
      console.log(result);
    }, (reason) => {
    });
    modal.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  // ngOnInit() {
  //   this.searchForm = new FormGroup({
  //     'search': new FormControl('')
  //   });
  //   this.admin = this.api.storage_get('logindata')
  //   this.load_bank(1)
  // }
  // load_bank(page) {
  //   this.api.getData('Confirm/get_salebank/' + page + "?search=" + this.searchForm.value.search).then((res: any) => {
  //     this.order = res.order;
  //     console.log(res);
  //   })
  // }

  // load_confirm(page) {
  //   this.api.getData('Confirm/load_confirm/' + page + "?search=" + this.searchForm.value.search).then((res: any) => {
  //     console.log(res);
  //     this.data = res.data
  //     this.all_page = res.count / 20 * 20;
  //   })
  // }
  // change_page($event) {
  //   // console.log($event);

  // }
  // search() {
  //   this.load_confirm(1)
  // }

  // test(t) {
  //   console.log(t);
  //   const modal: NgbModalRef = this.modalService.open(viewModalConfirm, { size: 'lg' });
  //   modal.componentInstance.payment = t;
  //   modal.result.then((result) => {
  //     console.log(result);
  //   }, (reason) => {
  //   });
  //   modal.result.then((result) => {
  //     this.closeResult = `Closed with: ${result}`;
  //   }, (reason) => {
  //     this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
  //   });
  // }
  // private getDismissReason(reason: any): string {
  //   // this.load_paymant(1)
  //   if (reason === ModalDismissReasons.ESC) {
  //     return 'by pressing ESC';
  //   } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
  //     return 'by clicking on a backdrop';
  //   } else {
  //     return `with: ${reason}`;
  //   }
  // }
  // sbank(bb_id, order_id) {
  //   console.log(bb_id + '/' + order_id);
  //   this.buff.bb_id = bb_id;
  //   this.buff.order_id = order_id;
  //   // console.log('asasdasdasfsjdhgfskjdf');

  // }
  // confirm(order) {
  //   if (this.buff.order_id == order.order_id) {
  //     Swal.fire({
  //       title: 'คุณต้องการยืนยันรายการนี้จริงหรือไม่',
  //       text: "กรุณายืนยันเมื่อทำการโอนเงินสำเร็จแล้วเท่านั้น",
  //       type: 'warning',
  //       showCancelButton: true,
  //       confirmButtonColor: '#0CC27E',
  //       cancelButtonColor: '#FF586B',
  //       confirmButtonText: 'ยืนยัน',
  //       cancelButtonText: 'ปิดออก',
  //       confirmButtonClass: 'btn btn-success btn-raised mr-2',
  //       cancelButtonClass: 'btn btn-default btn-raised',
  //       buttonsStyling: false
  //     }).then((result) => {
  //       if (result.value) {
  //         this.api.postData('Confirm/confirm_sale', { order_id: order.order_id, bb_id: this.buff.bb_id, admin: this.admin.t_id,price:order.com_price }).then((res: any) => {
  //           console.log('OK');
  //           this.firebase.database.ref('order').set(Math.round(Math.random() * 100));
  //           this.load_bank(1);
  //           Swal.fire(
  //             'ยืนยันแล้ว',
  //             '',
  //             'success'
  //           )
  //         })
  //       }
  //     });
  //   }else {
  //     console.log('Select bank again');
  //     Swal.fire(
  //       'กรุณาเลือกบัญชีธนาคารก่อน',
  //       '',
  //       'warning'
  //     )

  //   }

  // }

}
