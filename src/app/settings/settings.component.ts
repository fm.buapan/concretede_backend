import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { Component, OnInit, Input, ViewChild, ElementRef, ChangeDetectorRef } from '@angular/core';
import { NgbModal, ModalDismissReasons, NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { WebapiService } from 'app/webapi.service';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { CKEditorModule, ChangeEvent } from '@ckeditor/ckeditor5-angular';
import { ToastrService } from 'ngx-toastr';
import { HttpClient } from '@angular/common/http';
import Swal from 'sweetalert2';
import { AngularFireDatabase } from '@angular/fire/database';

@Component({
  selector: 'ngbd-modal-content',
  templateUrl: './add_bank.html',
  styleUrls: ['./add_bank.scss']
})
export class NgbdModalBank implements OnInit {
  @Input() b;
  bankForm: FormGroup;
  public admin = {
    t_id: '0'
  }

  @ViewChild("file", { static: false }) file: ElementRef;
  constructor(public activeModal: NgbActiveModal,
    private fb: FormBuilder, private cd: ChangeDetectorRef,
    public toastr: ToastrService,
    public firebase: AngularFireDatabase,
    public http: HttpClient, public api: WebapiService) {

  }
  ngOnInit() {
    console.log('add bank');
    this.admin = this.api.storage_get('logindata')
    console.log(this.b);
    if (this.b == '') {
      this.b = {
        bb_id: "0"

      }
      console.log(this.b);
      this.bankForm = new FormGroup({
        'no': new FormControl('', [Validators.required]),
        'name': new FormControl('', [Validators.required]),
        'bank': new FormControl('', [Validators.required]),
        'show': new FormControl(true,),
      });
    } else {
      console.log(this.b);

      this.bankForm = new FormGroup({
        'no': new FormControl('', [Validators.required]),
        'name': new FormControl('', [Validators.required]),
        'bank': new FormControl('', [Validators.required]),
        'show': new FormControl(this.b.show,),
      });
    }
    this.bankForm.patchValue(this.b);



  }
  close() {
    this.activeModal.dismiss();
  }
  onBankSubmit() {
    // console.log(this.productForm.value);
    let data = this.bankForm.value;
    data.bb_id = this.b.bb_id;
    data.admin = this.admin.t_id;
    console.log(data);
    this.api.postData('Settings/add_bank', data).then((res: any) => {
      if (res == '1') {
        this.toastr.success('บันทึกเรียบร้อยแล้ว');
        this.activeModal.close();
      } else {
        this.toastr.error('มีปัญหาในการบันทึกข้อมูล');
      }
    })
    // this.api.postData('product/add_product', data).then((res: any) => {
    //   console.log(res);
    //   if (res == '1') {
    //     this.toastr.success('บันทึกเรียบรอ้แล้ว');
    //     this.activeModal.close();
    //   } else {
    //     this.toastr.error('มีปัญหาในการบันทึกข้อมูล');
    //   }
    // });
    // console.log("summit");
  }
}

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class settingsComponent implements OnInit {
  public setting_genaral = false
  public setting_rate = true
  public setting_account = true
  public setting_location = true
  public settingAll: any = {
    policy: { th: "", en: "" },
    service: { th: "", en: "" },
    up_date: '2020-01-01 00:00:00'
  };
  Editor = ClassicEditor;
  editorConfig = {

  };
  public bank: any;
  public rate: FormGroup;
  closeResult: string;
  public settings = [];
  public now_rate: any = { up_time: '2020-01-01 00:00:00' };
  public admin_id = 0;
  public searchForm: any;
  public all_page = 0;
  page: number = 1;
  constructor(
    private modalService: NgbModal,
    public firebase: AngularFireDatabase,
    public api: WebapiService,
    public ref: ChangeDetectorRef
  ) {
    this.settings = this.api.storage_get('settings') || [];
  }
  onReactiveFormSubmit() {
    let data = this.rate.value;
    console.log(data);
    this.api.postData('Settings/set_rate', { admin: this.admin_id, data: data }).then((res: any) => {
      this.get_rate();

    })
  }
  onBankSubmit() {
    let data = this.bank.value;
    console.log(data);

  }
  ngOnInit() {
    this.fb = true;
    this.api.check_login();
    this.api.fb_val('backend', (val: any) => {
      if (this.fb) {
        this.get_aboutus();
        this.get_rate();
        this.load_bank();
        this.load_province();
      }
    })
    this.rate = new FormGroup({
      'atOrder': new FormControl('',),
      'cus_rate': new FormControl('',),
      'sale_rate': new FormControl('',)
    });
    this.admin_id = this.api.storage_get('logindata').t_id
    setTimeout(() => { this.rate.patchValue(this.now_rate); }, 2000)
  }

  view(t) {
  }
  view_profile(t) {
    this.view(t);
  }
  public provinces: any;
  load_province() {
    this.api.getData('Settings/get_province').then((res: any) => {
      this.provinces = res;
      this.ref.detectChanges();
    })
  }
  pOpen(p) {
    Swal.fire({
      title: 'คุณต้องการเปิดพื้นที่บริการจริงหรือไม่',
      text: "พื้นที่ " + p.name_th + " จะสามารถใช้บริการได้ทันที",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#0CC27E',
      cancelButtonColor: '#FF586B',
      confirmButtonText: 'ยืนยัน',
      cancelButtonText: 'ปิดออก',
      confirmButtonClass: 'btn btn-success btn-raised mr-2',
      cancelButtonClass: 'btn btn-default btn-raised',
      buttonsStyling: false
    }).then((result) => {
      if (result.value) {
        this.api.getData('Settings/set_province/' + p.id + '/0').then((res: any) => {
          console.log(res);
          this.api.fb_set('backend')
          this.load_province()
          Swal.fire(
            'เปิดพื้นที่เรียบร้อยแล้ว',
            '',
            'success'
          )
        })

      }

    });
  }
  pClose(p) {
    Swal.fire({
      title: 'คุณต้องการปิดพื้นที่บริการจริงหรือไม่',
      text: "พื้นที่ " + p.name_th + " จะไม่สามารถใช้บริการได้",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#0CC27E',
      cancelButtonColor: '#FF586B',
      confirmButtonText: 'ยืนยัน',
      cancelButtonText: 'ปิดออก',
      confirmButtonClass: 'btn btn-success btn-raised mr-2',
      cancelButtonClass: 'btn btn-default btn-raised',
      buttonsStyling: false
    }).then((result) => {
      if (result.value) {
        this.api.getData('Settings/set_province/' + p.id + '/1').then((res: any) => {
          console.log(res);
          this.api.fb_set('backend')
          this.load_province()
          Swal.fire(
            'ปิดพื้นที่เรียบร้อยแล้ว',
            '',
            'success'
          )
        })

      }

    });
  }

  open(b) {
    console.log(b);
    const modal: NgbModalRef = this.modalService.open(NgbdModalBank);
    modal.componentInstance.b = b;
    modal.result.then((result) => {
      // this.load_data(this.page);
      this.load_bank();
    }, (reason) => {
      console.log(reason);
    });

  }
  load_bank() {
    this.api.getData("Settings/load_bank").then((res: any) => {
      // console.log(res);
      this.bank = res
      console.log(this.bank);
    })
  }
  autoGrowTextZone(e) {
    e.target.style.height = "0px";
    e.target.style.height = (e.target.scrollHeight + 25) + "px";
  }
  show_setting(s) {
    if (s == 1) {
      this.setting_genaral = !this.setting_genaral
    } else if (s == 2) {
      this.setting_rate = !this.setting_rate
    } else if (s == 3) {
      this.setting_account = !this.setting_account
    } else if (s == 4) {
      this.setting_location = !this.setting_location
    }
  }

  save() {
    this.api.postData('Settings/set_aboutus', this.settingAll).then((res: any) => {
      console.log(res);
    })
    console.log(this.settingAll);
  }

  public onChangePolicyTH({ editor }: ChangeEvent) {
    // const data = editor.getData();
    this.settingAll.policy.th = editor.getData();
    // console.log(data);
  }

  public onChangeServiceTH({ editor }: ChangeEvent) {
    // const data = editor.getData();
    this.settingAll.service.th = editor.getData();
    // console.log(data);
  }
  public onChangePolicyEN({ editor }: ChangeEvent) {
    // const data = editor.getData();
    this.settingAll.policy.en = editor.getData();
    // console.log(data);
  }

  public onChangeServiceEN({ editor }: ChangeEvent) {
    // const data = editor.getData();
    this.settingAll.service.en = editor.getData();
    // console.log(data);
  }
  ngOnChanges() {
    this.fb = true;
  }

  get_aboutus() {
    this.api.getData("Settings/get_aboutus").then((res: any) => {
      // console.log(res);
      this.settingAll = res
      console.log(this.settingAll);
    })
  }
  get_rate() {
    this.api.getData("Settings/get_rate").then((res: any) => {

      this.now_rate = res;

      console.log(this.now_rate);

    })
  }
  delete(b) {
    Swal.fire({
      title: 'คุณต้องการลบจริงหรือไม่',
      text: "รายการนี้จะไม่สามารถนำกลับมาได้อีก",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#0CC27E',
      cancelButtonColor: '#FF586B',
      confirmButtonText: 'ลบออก',
      cancelButtonText: 'ปิดออก',
      confirmButtonClass: 'btn btn-success btn-raised mr-2',
      cancelButtonClass: 'btn btn-default btn-raised',
      buttonsStyling: false
    }).then((result) => {
      console.log(b);
      console.log(b.bb_id);
      if (result.value) {
        this.api.postData('Settings/remove_bank', { bb_id: b.bb_id, admin: this.admin_id }).then((res: any) => {
          console.log(res);
          this.load_bank();
          Swal.fire(
            'ลบเรียบร้อยแล้ว',
            '',
            'success'
          )
        })

      }

    });
  }
  public fb = false;
  ngOnDestroy() {
    this.fb = false;
  }
}
