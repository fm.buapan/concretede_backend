import { CustomFormsModule } from 'ng2-validation';
import { FormsModule, ReactiveFormsModule, NgModel } from '@angular/forms';
import { NgbModalModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { NgbdModalBank,settingsComponent } from './settings.component';


@NgModule({
 
  entryComponents:[NgbdModalBank],
  declarations: [settingsComponent,NgbdModalBank],
  imports: [
    CKEditorModule,
    CustomFormsModule,
    CommonModule,
    ReactiveFormsModule,
    NgbModalModule.forRoot(),
    FormsModule
  ]
})

export class TeamModule { 
  public Editor = ClassicEditor;
}
