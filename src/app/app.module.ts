import { RouterLinkActive } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgModule } from "@angular/core";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

import { NgbModule, NgbModalModule } from "@ng-bootstrap/ng-bootstrap";
import { AppRoutingModule } from "./app-routing.module";
import { SharedModule } from "./shared/shared.module";
import { ToastrModule } from "ngx-toastr";
import { AgmCoreModule } from "@agm/core";
import { HttpClientModule, HttpClient } from "@angular/common/http";
import { TranslateModule, TranslateLoader } from "@ngx-translate/core";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
import { StoreModule } from "@ngrx/store";

import {
  PerfectScrollbarModule,
  PERFECT_SCROLLBAR_CONFIG,
  PerfectScrollbarConfigInterface
} from 'ngx-perfect-scrollbar';

import { AppComponent } from "./app.component";
import { ContentLayoutComponent } from "./layouts/content/content-layout.component";
import { FullLayoutComponent } from "./layouts/full/full-layout.component";

import { DragulaService } from "ng2-dragula";
import { AuthService } from "./shared/auth/auth.service";
import { AuthGuard } from "./shared/auth/auth-guard.service";
import { TeamComponent, NgbdModalContent, viewModalContent } from './team/team.component';
import { SallerComponent, NgbdModalContentSaller, viewModalContentSaller } from './saller/saller.component';
import { CustomerComponent, NgbdModalContentCustomer, viewModalContentCustomer } from './customer/customer.component';
import { AngularWebStorageModule } from 'angular-web-storage';
import { viewModalProduct, NgbdModalProduct, ProductComponent } from './product/product.component';
import {AngularFireModule} from '@angular/fire';
import {AngularFireDatabaseModule} from '@angular/fire/database';
import {environment} from "../environments/environment"; 

import { OrderComponent, viewModalOrder, NgbdModalOrder } from './order/order.component';
import { NewsComponent, NgbdModalNews, viewModalNews } from './news/news.component';
import { LoginPageComponent } from './pages/content-pages/login/login-page.component';
import { settingsComponent,NgbdModalBank } from './settings/settings.component';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { PaymentComponent, viewModalPayment } from './payment/payment.component';
import { ConfirmComponent, viewModalConfirm } from './confirm/confirm.component';
import { ScoreComponent, viewModalScore } from './score/score.component';
import { LightboxModule } from 'ngx-lightbox';
const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true,
  wheelPropagation: false
};
export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, "./assets/i18n/", ".json");
}

@NgModule({
  declarations: [AppComponent, FullLayoutComponent, OrderComponent, ContentLayoutComponent, NgbdModalContent, NewsComponent,
    TeamComponent, ProductComponent, viewModalContent, SallerComponent, CustomerComponent, NgbdModalContentSaller, 
    viewModalContentSaller, NgbdModalContentCustomer, viewModalContentCustomer,viewModalPayment, viewModalProduct, NgbdModalProduct, viewModalOrder, NgbdModalOrder,settingsComponent,
    NgbdModalNews, viewModalNews, PaymentComponent,NgbdModalBank, ConfirmComponent, ScoreComponent,viewModalScore,viewModalConfirm
  ],
  entryComponents: [NgbdModalContent, viewModalContent, NgbdModalContentSaller,
    viewModalContentSaller, NgbdModalContentCustomer, NgbdModalBank,
    viewModalContentCustomer,viewModalPayment, viewModalProduct, viewModalScore,viewModalConfirm,
    NgbdModalProduct, viewModalOrder, NgbdModalOrder, NgbdModalNews, viewModalNews],
  imports: [
    BrowserAnimationsModule,
    CKEditorModule,
    StoreModule.forRoot({}),
    AppRoutingModule,
    SharedModule,
    FormsModule,
    NgbModalModule,
    AngularWebStorageModule,
    HttpClientModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    ReactiveFormsModule,
    ToastrModule.forRoot(),
    NgbModule.forRoot(),
    LightboxModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient]
      }
    }),
    AgmCoreModule.forRoot({
      apiKey: "YOUR KEY"
    }),
    PerfectScrollbarModule
  ],
  providers: [
    AuthService,
    AuthGuard,
    DragulaService,
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
    },
    { provide: PERFECT_SCROLLBAR_CONFIG, useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
