import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { WebapiService } from 'app/webapi.service';

@Component({
    selector: 'app-login-page',
    templateUrl: './login-page.component.html',
    styleUrls: ['./login-page.component.scss']
})

export class LoginPageComponent {

    public loginForm: FormGroup;
    public loginfail = false;
    public loginsuccess = false;
    public error_text = '';
    constructor(private router: Router,
        public api: WebapiService,
        public rount2: Router,
        private route: ActivatedRoute) {
        this.loginForm = new FormGroup({
            'username': new FormControl('', Validators.compose([Validators.required, Validators.minLength(6), Validators.pattern(/^[ +|a-zA-Z0-9]+$/),])),
            'password': new FormControl('', Validators.compose([Validators.required, Validators.minLength(6)])),
        });
    }

    // On submit button click
    onSubmit() {
        console.log(this.loginForm.value);
        this.api.postData('team/login', this.loginForm.value).then((res: any) => {
            console.log(res);
            if (res.status == 'success') {
                this.api.storage_set("logindata", res.data);
                this.loginsuccess = true;
                this.loginfail = false;
                this.rount2.navigate(['/']);
            } else {
                this.error_text = res.error;
                this.loginsuccess = false;
                this.loginfail = true;

            }
        })
        // this.loginForm.reset();
    }
    // On Forgot password link click
    onForgotPassword() {
        this.router.navigate(['forgotpassword'], { relativeTo: this.route.parent });
    }
    // On registration link click
    onRegister() {
        this.router.navigate(['register'], { relativeTo: this.route.parent });
    }
}
