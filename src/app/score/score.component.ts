import { Component, OnInit, Input, ChangeDetectorRef } from '@angular/core';
import { WebapiService } from 'app/webapi.service';
import { AngularFireDatabase } from '@angular/fire/database'
import { NgbModal, NgbModalRef, ModalDismissReasons, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormControl } from '@angular/forms';
import { viewModalConfirm } from 'app/confirm/confirm.component';
import Swal from 'sweetalert2';
import { data } from 'app/shared/data/smart-data-table';

@Component({
  selector: 'app-score',
  templateUrl: './score.component.html',
  styleUrls: ['./score.component.scss']
})
export class ScoreComponent implements OnInit {
  public searchForm: any;
  public data: any;
  public all_page = 0;
  public order: any;
  public bank: any;
  public fb = false;
  public buff = {
    bb_id: '',
    order_id: ''
  }
  closeResult: string;
  public admin: any = { name: '' }
  public score: any = { member_id: '' }
  constructor(public api: WebapiService, private modalService: NgbModal, public firebase: AngularFireDatabase) { }

  ngOnInit() {
    this.searchForm = new FormGroup({
      'search': new FormControl('')
    });
    this.admin = this.api.storage_get('logindata')
    this.fb = true;
    this.api.fb_val('order', (res: any) => {
      if (this.fb) {
        this.load_bank(1)
        this.load_score(1)
      }
    });

  }
  load_bank(page) {
    this.api.getData('Confirm/get_salebank/' + page + "?search=" + this.searchForm.value.search).then((res: any) => {
      this.order = res.order;
      console.log(res);
    })
  }
  load_score(page) {
    this.api.getData('Score/expScore/' + page + "?search=" + this.searchForm.value.search).then((res: any) => {
      this.score = res;
      console.log(res);
    })
  }

  load_confirm(page) {
    this.api.getData('Confirm/load_confirm/' + page + "?search=" + this.searchForm.value.search).then((res: any) => {
      console.log(res);
      this.data = res.data
      this.all_page = res.count / 20 * 20;
    })
  }
  change_page($event) {
    // console.log($event);

  }
  search() {
    this.load_confirm(1)
  }

  test(t) {
    console.log(t);
    const modal: NgbModalRef = this.modalService.open(viewModalConfirm, { size: 'lg' });
    modal.componentInstance.payment = t;
    modal.result.then((result) => {
      console.log(result);
    }, (reason) => {
    });
    modal.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      this.load_bank(1)
      this.load_score(1)
    });
  }
  private getDismissReason(reason: any): string {
    // this.load_paymant(1)
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  sbank(bb_id, order_id) {
    console.log(bb_id + '/' + order_id);
    this.buff.bb_id = bb_id;
    this.buff.order_id = order_id;
    // console.log('asasdasdasfsjdhgfskjdf');

  }
  confirm(order) {
    if (this.buff.order_id == order.order_id) {
      Swal.fire({
        title: 'คุณต้องการยืนยันรายการนี้จริงหรือไม่',
        text: "กรุณายืนยันเมื่อทำการโอนเงินสำเร็จแล้วเท่านั้น",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#0CC27E',
        cancelButtonColor: '#FF586B',
        confirmButtonText: 'ยืนยัน',
        cancelButtonText: 'ปิดออก',
        confirmButtonClass: 'btn btn-success btn-raised mr-2',
        cancelButtonClass: 'btn btn-default btn-raised',
        buttonsStyling: false
      }).then((result) => {
        if (result.value) {
          this.api.postData('Confirm/confirm_sale', { order_id: order.order_id, bb_id: this.buff.bb_id, admin: this.admin.t_id, price: order.com_price }).then((res: any) => {
            console.log('OK');
            this.api.fb_set('order')
            this.load_bank(1);
            Swal.fire(
              'ยืนยันแล้ว',
              '',
              'success'
            )
          })
        }
      });
    } else {
      console.log('Select bank again');
      Swal.fire(
        'กรุณาเลือกบัญชีธนาคารก่อน',
        '',
        'warning'
      )

    }

  }
  public imagePath: any = '';
  public url = '';
  public reader: any;
  public type: any;
  public formData = new FormData();
  changeImg(event, member_id): void {
    let data = {
      member_id: '',
      message: ''
    };
    console.log(event);
    this.formData.append('avatar', event.target.files[0]);
    this.reader = new FileReader();
    this.reader.onload = (event: any) => {
      this.url = event.target.result;
    }
    this.reader.readAsDataURL(event.target.files[0]);
    this.type = event.target.files[0].type;

    console.log(this.reader.result);
    setTimeout(() => {
      console.log(member_id);

      // this.api.postData("Confirm/upload", { img: this.reader.result, type: this.type, member_id:member_id }).then((res: any) => {
      // })
    }, 1000);
  }
  continue(data) {
    const modal: NgbModalRef = this.modalService.open(viewModalScore, { size: 'lg' });
    modal.componentInstance.data = data;
    modal.result.then((result) => {
      console.log(result);
    }, (reason) => {
    });
    modal.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  ngOnDestroy() {
    this.fb = false
  }

}


@Component({
  selector: 'view-modal-content',
  templateUrl: './score_details.html',
  styleUrls: ['./score_details.scss']
})
export class viewModalScore implements OnInit {
  @Input() data;

  constructor(
    public activeModal: NgbActiveModal,
    public api: WebapiService,
    public firebase: AngularFireDatabase,
    public ref: ChangeDetectorRef,
  ) {
  }
  close() {
    this.activeModal.dismiss(this.data);
  }
  ngOnInit(): void {

  }
  public imagePath: any = '';
  public url = '';
  public reader: any;
  public type: any;
  public formData = new FormData();
  changeImg(event): void {
    let data = {
      member_id: '',
      message: ''
    };
    console.log(event);
    this.formData.append('avatar', event.target.files[0]);
    this.reader = new FileReader();
    this.reader.onload = (event: any) => {
      this.url = event.target.result;
      this.ref.detectChanges()
    }
    this.reader.readAsDataURL(event.target.files[0]);
    this.type = event.target.files[0].type;

    console.log(this.reader.result);
    setTimeout(() => {


      // this.api.postData("Confirm/upload", { img: this.reader.result, type: this.type, member_id:member_id }).then((res: any) => {
      // })
    }, 1000);
  }
  public bb_id = ''
  sbank(bb_id) {
    this.bb_id = bb_id
    console.log(bb_id);

  }
  confirm() {
    if (this.bb_id != '') {
      this.api.postData("Score/upload", { img: this.reader.result, type: this.type, bb_id: this.bb_id, price: this.data.exp, score_id: this.data.score_id }).then((res: any) => {
        this.api.fb_set('order')
        Swal.fire(
          'สำเร็จ!',
          '',
          'success'
        )
        setTimeout(() => {
          this.close();
        }, 1000);
      })
    } else {
      Swal.fire(
        'กรุณาเลือกบัญชีธนาคารก่อน',
        '',
        'warning'
      )
    }

  }
}
