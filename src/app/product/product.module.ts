import { CustomFormsModule } from 'ng2-validation';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductComponent, NgbdModalProduct } from './product.component';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';

@NgModule({

  entryComponents: [NgbdModalProduct],
  declarations: [ProductComponent, NgbdModalProduct],
  imports: [
    CustomFormsModule,
    CommonModule,
    ReactiveFormsModule,
    CKEditorModule,
    NgbModalModule.forRoot(),
    FormsModule
  ]
})

export class TeamModule { 
  public Editor = ClassicEditor;
}
