import { trigger } from '@angular/animations';
import { FormGroup, FormControl, Validators, FormBuilder, FormArray, FormArrayName } from '@angular/forms';
import { Component, OnInit, Input, ViewChild, ElementRef, ChangeDetectorRef } from '@angular/core';
import { NgbModal, ModalDismissReasons, NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import swal from 'sweetalert2';
import { WebapiService } from 'app/webapi.service';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
@Component({
  selector: 'view-modal-content',
  templateUrl: './view_product.html',
  styleUrls: ['./view_product.scss']

})
export class viewModalProduct implements OnInit {
  @Input() para;
  // productForm: FormGroup;
  constructor(public activeModal: NgbActiveModal) {

  }
  close() {
    this.activeModal.dismiss();
  }
  ngOnInit() {
    console.log("view");
    console.log(this.para);
    // this.productForm = new FormGroup({
    //   'email': new FormControl(null, [Validators.required, Validators.email]),
    //   'username': new FormControl(null, [Validators.required, Validators.minLength(4), Validators.maxLength(24)]),
    //   'password': new FormControl(null, [Validators.required, Validators.minLength(4), Validators.maxLength(24)]),
    //   'name': new FormControl(null, [Validators.required, Validators.email, Validators.minLength(4)]),
    //   'phone': new FormControl(null, [Validators.required, Validators.email, Validators.minLength(9), Validators.minLength(10)]),
    // }, { updateOn: 'blur' });
    // this.productForm.patchValue({username:'xxx'});
  }
  c(val) {
    console.log(val);
  }
}

@Component({
  selector: 'ngbd-modal-content',
  templateUrl: './add_product.html',
  styleUrls: ['./add_product.scss']
})
export class NgbdModalProduct implements OnInit {
  @Input() para;
  productForm: FormGroup;




  @ViewChild("file", { static: false }) file: ElementRef;
  constructor(public activeModal: NgbActiveModal,
    private fb: FormBuilder, private cd: ChangeDetectorRef,
    public toastr: ToastrService,
    public http: HttpClient, public api: WebapiService) {

  }
  Editor = ClassicEditor;
  editorConfig = {
  };
  ngOnInit() {
    console.log(this.para);
    if (this.para.p_id == '0') {
      this.para = {
        p_id: "0",
        p_name: { th: '', en: '' },
        how_to_use: { th: '', en: '' },
        p_img: "https://api.concretedeliveryeasy.com/uploads/p_df.jpg",
        show:'',
        sort:1
      }

      this.productForm = new FormGroup({
        'p_name': new FormGroup({
          'th': new FormControl('', [Validators.required]),
          'en': new FormControl('', [Validators.required])
        }),
        'how_to_use': new FormGroup({
          'th': new FormControl('', [Validators.required]),
          'en': new FormControl('', [Validators.required])
        }),
        'default_price': new FormGroup({
          'price': new FormControl('', [Validators.required]),
          'diff_sm': new FormControl('', [Validators.required]),
          'diff_lg': new FormControl('', [Validators.required])
        }),
        'show': new FormGroup({
          'show': new FormControl(true, ),
          'sort': new FormControl(1, ),
        }),
      });
    } else {
      this.productForm = new FormGroup({
        'p_name': new FormGroup({
          'th': new FormControl('', [Validators.required]),
          'en': new FormControl('', [Validators.required])
        }),
        'how_to_use': new FormGroup({
          'th': new FormControl('', [Validators.required]),
          'en': new FormControl('', [Validators.required])
        }),
        'default_price': new FormGroup({
          'price': new FormControl('', [Validators.required]),
          'diff_sm': new FormControl('', [Validators.required]),
          'diff_lg': new FormControl('', [Validators.required])
        }),
        'show': new FormGroup({
          'show': new FormControl(this.para.show, ),
          'sort': new FormControl(this.para.sort, ),
        }),
      });
    }


    this.productForm.patchValue(this.para);
  }
  file_change(event) {
    const reader = new FileReader();
    if (event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      console.log(event.target.files);
      if (event.target.files[0].size < 2000000) {
        if (event.target.files[0].type == 'image/jpeg' || event.target.files[0].type == 'image/png') {
          reader.readAsDataURL(file);
          reader.onload = () => {
            this.api.postData('team/upload', { data: reader.result, type: event.target.files[0].type }).then((res: any) => {
              console.log(res);
              this.para.p_img = res.url;
              this.cd.detectChanges();
            });

            this.cd.markForCheck();
          };
        } else {
          swal.fire(
            'ต้องเป็นไฟล์ jpg หรือ png เท่านั้น',
            '',
            'error'
          )
        }

      } else {
        swal.fire(
          'ไฟล์ต้องมีขนาดน้อยกกว่า 2 Mb',
          '',
          'error'
        )
      }

    }

  }
  search() {

  }
  change_picture() {
    console.log('changepic');
    this.file.nativeElement.click();
  }

  onReactiveFormSubmit() {
    // console.log(this.productForm.value);
    let data = this.productForm.value;
    data.p_id = this.para.p_id;
    data.p_img = this.para.p_img;
    console.log(data);

    this.api.postData('product/add_product', data).then((res: any) => {
      console.log(res);
      if (res == '1') {
        this.toastr.success('บันทึกเรียบร้อยแล้ว');
        this.activeModal.close();
      } else {
        this.toastr.error('มีปัญหาในการบันทึกข้อมูล');
      }
    });
    console.log("summit");
  }
  close() {
    this.activeModal.dismiss();
  }

  c(val) {
    console.log(val);
  }
}





@Component({
  selector: 'app-team',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {
  closeResult: string;
  public products = [];
  productForm: FormGroup;
  public all_page = 0;
  page: number = 1;
  public searchForm: any;
  constructor(private modalService: NgbModal, public api: WebapiService) {
    this.products = this.api.storage_get('products') || [];
  }
  onReactiveFormSubmit() {
    let a = this.productForm.value();
    console.log(a);
  }
  ngOnInit() {

    this.api.check_login();
    this.searchForm = new FormGroup({
      'search': new FormControl('')
    });
    this.productForm = new FormGroup({
      'inputEmail': new FormControl(null, [Validators.required, Validators.email]),
      'password': new FormControl(null, [Validators.required, Validators.minLength(4), Validators.maxLength(24)]),
      'textArea': new FormControl(null, [Validators.required]),
      'radioOption': new FormControl('Option one is this')
    }, { updateOn: 'blur' });
    // this.api.getData('team/load_team').then((data: any) => {
    //   console.log(data);
    //   this.products = data;
    //   this.api.storage_set('teams', data);
    // })
    this.load_data(1);
  }

  view(t) {
    console.log(t);
    const modal: NgbModalRef = this.modalService.open(viewModalProduct, { size: 'lg', backdrop: 'static' });
    modal.componentInstance.para = t;

    modal.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  view_profile(t) {
    this.view(t);
  }
  search() {
    this.load_data(1);
  }
  load_data(page) {
    this.api.getData('product/load_product/' + page + "?search=" + this.searchForm.value.search).then((data: any) => {
      this.products = data.data;
      this.all_page = data.count / 20 * 20;
      console.log(this.all_page);
      this.api.storage_set('products', data.data);
      console.log(data);
      
    })
  }
  change_page(page:number) {
    console.log('Change'+page);
    this.load_data(page);
  }
  open(t) {
    console.log(t);
    const modal: NgbModalRef = this.modalService.open(NgbdModalProduct, { size: 'lg', backdrop: 'static' });
    modal.componentInstance.para = t;
    modal.result.then((result) => {
      this.load_data(this.page);
    }, (reason) => {
      console.log(reason);
    });

  }
  delete_product(t) {
       
    swal.fire({
      title: 'คุณต้องการลบจริงหรือไม่',
      text: "รายการนี้จะไม่สามารถนำกลับมาได้อีก",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#0CC27E',
      cancelButtonColor: '#FF586B',
      confirmButtonText: 'ลบออก',
      cancelButtonText: 'ปิดออก',
      confirmButtonClass: 'btn btn-success btn-raised mr-2',
      cancelButtonClass: 'btn btn-default btn-raised',
      buttonsStyling: false
    }).then((result) => {
      console.log(t);
      console.log(t.p_id);
      if (result.value) {
        this.api.postData('product/remove_product', { p_id: t.p_id }).then((res: any) => {
          console.log(res);
          this.load_data(this.page);
          swal.fire(
            'ลบเรียบร้อยแล้ว',
            '',
            'success'
          )
        })

      }

    });
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
}
