import { CustomFormsModule } from 'ng2-validation';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomerComponent, NgbdModalContentCustomer } from './customer.component';
import { PaginationComponent } from 'app/components/bootstrap/pagination/pagination.component';

@NgModule({

  entryComponents: [NgbdModalContentCustomer],
  declarations: [CustomerComponent, NgbdModalContentCustomer, PaginationComponent],
  imports: [
    CustomFormsModule,
    CommonModule,
    
    ReactiveFormsModule,
    NgbModalModule.forRoot(),
    FormsModule
  ]
})

export class TeamModule { }
