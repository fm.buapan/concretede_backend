import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LocalStorageService } from 'angular-web-storage';
import { Router } from '@angular/router';
import { AngularFireDatabase } from '@angular/fire/database';

@Injectable({
  providedIn: 'root'
})
export class WebapiService {
  // public base_url = "https://api.concretedeliveryeasy.com/index.php/";
  public base_url = "https://api2.concretedeliveryeasy.com/index.php/";

  constructor(public http: HttpClient,
    public router: Router,
    public local: LocalStorageService,
    public firebase: AngularFireDatabase,
  ) {

  }
  public dev = 'dev/';
  fb_set(child) {
    this.firebase.database.ref(this.dev+child).set(Math.ceil(Math.random() * 100));
  }
  fb_setZero(child) {
    this.firebase.database.ref(this.dev+child).set(0);
  }
  fb_val(child, func) {
    this.firebase.database.ref(this.dev+child).on("value", (val:any) => {
      func(val.node_.value_);
    });
  }
  presentToast() {

  }
  storage_set(key, val) {
    this.local.set(key, val);
  }
  storage_get(key) {
    return this.local.get(key);
  }
  check_login() {
    if (!this.storage_get('logindata')) {
      this.router.navigate(['/pages/login']);
    }
  }

  postData(segment, objdata) {
    return new Promise((resolve, reject) => {
      var headers = new Headers();
      headers.append("Accept", 'application/json');
      headers.append('Content-Type', 'application/json');
      this.http.post(this.base_url + segment, JSON.stringify(objdata))
        .subscribe((res: any) => {
          resolve(res);

        }, (err) => {
          if (err.status == 0) {
            this.presentToast();
            reject(err);
          }
        });
    });
  }

  getData(segment) {
    return new Promise((resolve, reject) => {
      this.http.get(this.base_url + segment)
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          if (err.status == 0) {
            this.presentToast();
            reject(err);
          }
          reject(err);
        });
    });
  }
}
