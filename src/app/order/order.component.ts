import { trigger } from '@angular/animations';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { Component, OnInit, Input, ViewChild, ElementRef, ChangeDetectorRef } from '@angular/core';
import { NgbModal, ModalDismissReasons, NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { AngularFireDatabase } from '@angular/fire/database';
import swal from 'sweetalert2';
import { WebapiService } from 'app/webapi.service';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'view-modal-content',
  templateUrl: './view_order.html',
  styleUrls: ['./view_order.scss']

})
export class viewModalOrder implements OnInit {
  @Input() para;
  // teamForm: FormGroup;
  public details:any = {order_data:{order_id:'',p_name:{th:'',en:''}}};
  public action:any = [];
  public fbstatus= false;

  constructor(
    public ref: ChangeDetectorRef,
    public activeModal: NgbActiveModal,
    public firebase: AngularFireDatabase,
    public api: WebapiService
    ) {

  }
  close() {
    this.activeModal.dismiss();
  }
  ngOnInit() {
    console.log("view");
    // console.log(this.para);
    this.fbstatus = true;
    this.api.fb_val('order', (res:any) => {
      console.log(res.node_.value_);
      console.log('new auction!!');
      if (this.fbstatus) {
        this.load_auc();
      }
    });

  }
  ngOnDestroy(){
    this.fbstatus=false;
  }

  load_auc(){
    this.api.getData("order/get_vs_auction/"+this.para).then((res:any)=>{
      // console.log(res);
      this.details = res;
      this.action = res.auction_rec;
      console.log(this.action);
      this.ref.detectChanges();
    })
  }
  c(val) {
    console.log(val);
  }
}

@Component({
  selector: 'ngbd-modal-content',
  templateUrl: './add_order.html',
  styleUrls: ['./add_order.scss']
})
export class NgbdModalOrder implements OnInit {
  @Input() para;
  teamForm: FormGroup;
  @ViewChild("file", { static: false }) file: ElementRef;
  constructor(public activeModal: NgbActiveModal,
    public firebase: AngularFireDatabase,
    private fb: FormBuilder, private cd: ChangeDetectorRef,
    public toastr: ToastrService,
    public http: HttpClient, public api: WebapiService) {

  }
  file_change(event) {
    const reader = new FileReader();
    if (event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      console.log(event.target.files);
      if (event.target.files[0].size < 2000000) {
        if (event.target.files[0].type == 'image/jpeg' || event.target.files[0].type == 'image/png') {
          reader.readAsDataURL(file);
          reader.onload = () => {
            // console.log(reader.result);
            // this.para.picture = reader.result;
            // this.http.post(this.api.base_url + 'team/fileupload', { picture: reader.result }, {
            //   reportProgress: true,
            //   observe: 'events'
            // })
            this.api.postData('team/upload', { data: reader.result, type: event.target.files[0].type }).then((res: any) => {
              console.log(res);
              this.para.picture = res.url;
              this.cd.detectChanges();
            });

            // let selectedFile = event.target.files[0];
            // const uploadData = new FormData();
            // uploadData.append('photo', selectedFile, selectedFile.name);
            // this.http.post(this.api.base_url + 'team/fileupload', uploadData, {
            //   reportProgress: true,
            //   observe: 'events'
            // })
            //   .subscribe((e) => {
            //     console.log(e);
            //   })
            // this.formGroup.patchValue({
            //   file: reader.result
            // });

            // need to run CD since file load runs outside of zone
            this.cd.markForCheck();
          };
        } else {
          swal.fire(
            'ต้องเป็นไฟล์ jpg หรือ png เท่านั้น',
            '',
            'error'
          )
        }

      } else {
        swal.fire(
          'ไฟล์ต้องมีขนาดน้อยกกว่า 2 Mb',
          '',
          'error'
        )
      }

    }

  }
  change_picture() {
    console.log('changepic');
    this.file.nativeElement.click();
  }
  onReactiveFormSubmit() {
    // console.log(this.teamForm.value);
    let data = this.teamForm.value;
    data.t_id = this.para.t_id;
    data.picture = this.para.picture;
    this.api.postData('team/add_team', this.teamForm.value).then((res: any) => {
      console.log(res);
      if (res == '1') {
        this.api.fb_set('backend')
        this.toastr.success('บันทึกเรียบรอ้แล้ว');
        this.activeModal.close();
      } else {
        this.toastr.error('มีปัญหาในการบันทึกข้อมูล');
      }
    });
    console.log("summit");
  }
  close() {
    this.activeModal.dismiss();
  }
  ngOnInit() {
    console.log("xxxx");
    console.log(this.para);
    if (this.para.t_id == '0') {
      this.para = {
        t_id: "0", username: "", password: "", name: "", email: '', phone: "",
        gender: "m",
        picture: "https://image.flaticon.com/icons/svg/194/194938.svg",
        type: "1"
      }
      this.teamForm = new FormGroup({
        'username': new FormControl(null, [Validators.required, Validators.minLength(6), Validators.maxLength(24), Validators.pattern('^[a-zA-Z0-9._-]+$')]),
        'password': new FormControl(null, [Validators.required, Validators.minLength(6), Validators.maxLength(24)]),
        'name': new FormControl(null, [Validators.required, Validators.minLength(6)]),
        'email': new FormControl(null, [Validators.required, Validators.email]),

        'phone': new FormControl(null, [Validators.required, Validators.minLength(9), Validators.minLength(10), Validators.pattern('[0-9]+')]),
        gender: new FormControl(null, [Validators.required]),
        type: new FormControl(null, [Validators.required])
      }, { updateOn: 'blur' });
    } else {
      this.teamForm = new FormGroup({
        'username': new FormControl(null, [Validators.required, Validators.minLength(6), Validators.maxLength(24), Validators.pattern('^[a-zA-Z0-9._-]+$')]),
        'password': new FormControl(null, [Validators.minLength(6), Validators.maxLength(24)]),
        'name': new FormControl(null, [Validators.required, Validators.minLength(6)]),
        'email': new FormControl(null, [Validators.required, Validators.email]),

        'phone': new FormControl(null, [Validators.required, Validators.minLength(9), Validators.minLength(10), Validators.pattern('[0-9]+')]),
        gender: new FormControl(null, [Validators.required]),
        type: new FormControl(null, [Validators.required])
      }, { updateOn: 'blur' });
    }


    this.teamForm.patchValue(this.para);
    this.teamForm.patchValue({ password: '' })
  }
  c(val) {
    console.log(val);
  }
}





@Component({
  selector: 'app-team',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})
export class OrderComponent implements OnInit {
  closeResult: string;
  public orders = [];
  teamForm: FormGroup;
  public admin_id = 0;
  public all_page = 0;
  page: number = 1;
  public searchForm: any;
  constructor(
    private modalService: NgbModal,
    public firebase: AngularFireDatabase,
    public api: WebapiService
    ) {
    this.orders = this.api.storage_get('orders') || [];
  }
  onReactiveFormSubmit() {
    let a = this.teamForm.value();
    console.log(a);
  }
  ngOnInit() {
    this.api.check_login();
    this.searchForm = new FormGroup({
      'search': new FormControl('')
    });
    this.teamForm = new FormGroup({
      'inputEmail': new FormControl(null, [Validators.required, Validators.email]),
      'password': new FormControl(null, [Validators.required, Validators.minLength(4), Validators.maxLength(24)]),
      'textArea': new FormControl(null, [Validators.required]),
      'radioOption': new FormControl('Option one is this')
    }, { updateOn: 'blur' });
    this.load_data(1); 

    this.admin_id = this.api.storage_get('logindata').t_id

    this.api.fb_val('backend', (res:any) => {
      console.log(res.node_.value_);
      console.log('new order');
      this.load_data(1);
      // this.menuItems[1].badge = res.node_.value_ ;
      
    });
    
  }
  load_data(page:number) {
    this.api.getData('order/load_orders/' + page + "?search=" + this.searchForm.value.search).then((data: any) => {
      console.log(data);
      this.orders = data.data;
      this.all_page = data.count / 20 * 20;
      this.api.storage_set('orders', data.data);
    })
  }
  change_page(page:number){
    this.load_data(page)
  }
  
  view(t) {
    console.log(t);
    const modal: NgbModalRef = this.modalService.open(viewModalOrder, { size: 'lg' });
    modal.componentInstance.para = t;
    modal.result.then((result) => {
      console.log(result);
    }, (reason) => {
    });
    modal.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  search(){
    this.load_data(1)
  }

  view_profile(t) {
    this.view(t);
  }
  open(t) {
    console.log(t);
    const modal: NgbModalRef = this.modalService.open(NgbdModalOrder);
    modal.componentInstance.para = t;
    modal.result.then((result) => {
      this.load_data(this.page);
    }, (reason) => {
      console.log(reason);
    });
    modal.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  cancel_order(t) {
    swal.fire({
      title: 'คุณต้องการยกเลิกรายการนี้จริงหรือไม่',
      text: "รายการนี้จะไม่สามารถนำกลับมาได้อีก",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#0CC27E',
      cancelButtonColor: '#FF586B',
      confirmButtonText: 'ยืนยัน',
      cancelButtonText: 'ปิดออก',
      confirmButtonClass: 'btn btn-success btn-raised mr-2',
      cancelButtonClass: 'btn btn-default btn-raised',
      buttonsStyling: false
    }).then((result) => {
      console.log(t);

      if (result.value) {
        this.api.postData('Order/cancel_order', { order_id: t ,admin:this.admin_id}).then((res: any) => {
          this.api.fb_set('order')
          console.log(res);
          this.load_data(this.page);
          swal.fire(
            'ยกเลิกเรียบร้อยแล้ว',
            '',
            'success'
          )
        })

      }

    });
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
}
