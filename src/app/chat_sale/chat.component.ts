import { Component, ViewChild, ElementRef, OnInit, ChangeDetectionStrategy, Renderer2, ChangeDetectorRef } from '@angular/core';
import { ChatService } from './chat.service';
import { Chat } from './chat.model';
import { WebapiService } from 'app/webapi.service';
import { AngularFireDatabase } from '@angular/fire/database';


@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./chat.component.scss'],
  providers: [ChatService]
})
export class ChatComponent implements OnInit {

  chat: Chat[];
  activeChatUser: string;
  activeChatUserImg: string;
  @ViewChild('messageInput', { static: false }) messageInputRef: ElementRef;
  @ViewChild('chatSidebar', { static: false }) sidebar: ElementRef;
  @ViewChild('contentOverlay', { static: false }) overlay: ElementRef;
  users = [];
  messages = new Array();
  item: number = 0;
  user_active = { member_id: '0' };
  constructor(private elRef: ElementRef, private renderer: Renderer2, public firebase: AngularFireDatabase,
    public api: WebapiService, private ref: ChangeDetectorRef,
    private chatService: ChatService) {
    this.chat = chatService.chat1;
    this.activeChatUser = "Elizabeth Elliott";
    this.activeChatUserImg = "assets/img/portrait/small/avatar-s-3.png";
  }

  ngOnInit() {
    this.api.check_login();
    this.api.fb_val('chat', (res) => {
      this.load_data();
    });
    // $.getScript('./assets/js/chat.js');
  }
  load_data() {
    this.api.getData("chat/load_chat_users/2").then((res: any) => {
      console.log(res);
      this.users = res;
      this.users.forEach((element, key) => {
        if (element.member_id == this.user_active.member_id) {
          this.users[key].readed = 0;
          this.ref.detectChanges();
        }
      });
      if (this.user_active.member_id == '0' && res.length > 0) {
        this.user_active = res[0];
      }
      this.api.getData('chat/load_chat_message/' + this.user_active.member_id).then((res: any) => {
        this.chat = res;
        console.log(res);
        setTimeout(() => {
          this.ref.detectChanges();
        }, 100);
      })

    });
  }
  //send button function calls
  onAddMessage() {
    if (this.messageInputRef.nativeElement.value != "") {
      console.log(this.messageInputRef.nativeElement.value);

      this.api.postData("chat/add_admin", { message: this.messageInputRef.nativeElement.value, img: null, member_id: this.user_active.member_id }).then((res: any) => {
        console.log(res);
        this.api.fb_set("chat/" + this.user_active.member_id)
        this.api.fb_set("notification/user_" + this.user_active.member_id + '/chat')
        
        this.ref.detectChanges();
      })
      // this.messages.push(this.messageInputRef.nativeElement.value);
    }
    this.messageInputRef.nativeElement.value = "";
    this.messageInputRef.nativeElement.focus();
  }

  //chat user list click event function
  SetActive(event, user) {
    user.not_read = 0;
    this.user_active = user;
    var hElement: HTMLElement = this.elRef.nativeElement;
    //now you can simply get your elements with their class name
    var allAnchors = hElement.getElementsByClassName('list-group-item');
    //do something with selected elements
    [].forEach.call(allAnchors, function (item: HTMLElement) {
      item.setAttribute('class', 'list-group-item no-border');
    });
    //set active class for selected item
    event.currentTarget.setAttribute('class', 'list-group-item bg-blue-grey bg-lighten-5 border-right-primary border-right-2');
    this.messages = [];
    // if (chatId === 'chat1') {
    // this.chat = this.chatService.chat1;
    this.api.getData('chat/load_chat_message/' + user.member_id).then((res: any) => {
      this.chat = res;
      console.log(res);

      setTimeout(() => {
        this.ref.detectChanges();
      }, 100);
    })
    this.activeChatUser = user.name;
    this.activeChatUserImg = user.picture;
  }

  onSidebarToggle() {
    this.renderer.removeClass(this.sidebar.nativeElement, 'd-none');
    this.renderer.removeClass(this.sidebar.nativeElement, 'd-sm-none');
    this.renderer.addClass(this.sidebar.nativeElement, 'd-block');
    this.renderer.addClass(this.sidebar.nativeElement, 'd-sm-block');
    this.renderer.addClass(this.overlay.nativeElement, 'show');
  }

  onContentOverlay() {
    this.renderer.removeClass(this.overlay.nativeElement, 'show');
    this.renderer.removeClass(this.sidebar.nativeElement, 'd-block');
    this.renderer.removeClass(this.sidebar.nativeElement, 'd-sm-block');
    this.renderer.addClass(this.sidebar.nativeElement, 'd-none');
    this.renderer.addClass(this.sidebar.nativeElement, 'd-sm-none');

  }

  public imagePath: any = '';
  public url = '';
  public reader: any;
  public type: any;
  public formData = new FormData();
  changeImg(event): void {
    let data = {
      member_id: '',
      message: ''
    };
    console.log(event);
    this.formData.append('avatar', event.target.files[0]);
    this.reader = new FileReader();
    this.reader.onload = (event: any) => {
      this.url = event.target.result;
    }
    this.reader.readAsDataURL(event.target.files[0]);
    this.type = event.target.files[0].type;

    console.log(this.reader.result);
    setTimeout(() => {
      this.api.postData("chat/add_admin", { message: '', img: this.reader.result, type: this.type, member_id: this.user_active.member_id }).then((res: any) => {
        console.log(res);
        this.api.fb_set("chat/" + this.user_active.member_id)
        this.api.fb_set("notification/user_" + this.user_active.member_id + '/chat')
        this.ref.detectChanges();
      })
    }, 1000);
  }
}
