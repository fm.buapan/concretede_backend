import { Component, OnInit, OnDestroy, AfterContentInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Router, NavigationEnd, RouterLinkActive, ActivatedRoute } from '@angular/router';
import { filter } from 'rxjs/operators';
import * as BalloonEditor from '@ckeditor/ckeditor5-build-balloon';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html'
})
export class AppComponent implements OnInit, OnDestroy, AfterContentInit {

    subscription: Subscription;

    constructor(private router: Router, public routeractive: ActivatedRoute) {
    }
    ngAfterContentInit() {
        console.log(this.routeractive.toString());
    }
    ngOnInit() {
        this.subscription = this.router.events
            .pipe(
                filter(event => event instanceof NavigationEnd)
            )
            .subscribe(() => window.scrollTo(0, 0));
    }


    ngOnDestroy() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }



}