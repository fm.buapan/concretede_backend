import { Component, OnInit, Input, ChangeDetectorRef } from '@angular/core';
import { WebapiService } from 'app/webapi.service';
import { FormGroup, FormControl } from '@angular/forms';
import { NgbModalRef, ModalDismissReasons, NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AngularFireDatabase } from '@angular/fire/database';
import Swal from 'sweetalert2';
@Component({
  selector: 'view-modal-content',
  templateUrl: './payment_details.html',
  styleUrls: ['./payment_details.scss']
})
export class viewModalPayment implements OnInit {
  @Input() payment;
  public admin_id = 0;
  public admin_type = 0;
  constructor(public activeModal: NgbActiveModal, public api: WebapiService, public firebase: AngularFireDatabase) {

  }
  close() {
    this.activeModal.dismiss();
  }
  ngOnInit(): void {
    console.log("view");
    console.log(this.payment);
    this.admin_id = this.api.storage_get('logindata').t_id
    this.admin_type = this.api.storage_get('logindata').type
  }
  confirm(pm_id) {
    console.log('Confirm by admin ID =', this.admin_id, ' pm_id =', pm_id);
    this.api.postData('Payment/confirm_payment/', { pm_id: pm_id, id: this.admin_id, confirm: 1 }).then((res) => {
      this.api.fb_set('order')
      this.close()
    })

  }
  cancel(pm_id) {
    console.log('Cancel by admin ID =', this.admin_id, ' pm_id =', pm_id);
    this.api.postData('Payment/confirm_payment/', { pm_id: pm_id, id: this.admin_id, confirm: -1 }).then((res) => {
      this.api.fb_set('order')
      this.close()
    })
  }
}




@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss']
})
export class PaymentComponent implements OnInit {
  public searchForm: any;
  public status: any = 0;
  closeResult: string;
  public all_page = 0;
  page: number = 1;
  public admin_id = 0;
  public admin_type = 0;
  public payments: any = { name: '' }
  public pm_detail: any = {
    pm_id: '1',
    sum_price: '50000',
    img: '',
    sale_name: 'ตู่ รัฐบาป',
    cus_name: 'ป้อม รัฐบาป',
    product: 'รถถัง',
    count: '50',
    date_send: '22-6-2020',
    time_send: '17:00',
    address: 'ทำเนียบ',
    status: '0'
  }
  constructor(
    public api: WebapiService,
    public ref: ChangeDetectorRef,
    private modalService: NgbModal,
    public firebase: AngularFireDatabase
  ) { }
  public fb = false
  ngOnInit() {
    this.fb = true;
    this.searchForm = new FormGroup({
      'search': new FormControl('')
    });

    this.admin_id = this.api.storage_get('logindata').t_id
    this.admin_type = this.api.storage_get('logindata').type
    this.api.fb_val('order', (res: any) => {
      if (this.fb) {
        this.load_paymant(1);
      }
    });

  }

  public load_paymant(page) {
    console.log('OK pm');
    this.api.getData('Payment/load_payment/' + page + "?search=" + this.searchForm.value.search).then((res: any) => {
      console.log(res);
      this.payments = res
      this.all_page = res.count / 20 * 20;
      this.ref.detectChanges()
    })
  }

  confirm(pm_id) {
    Swal.fire({
      title: 'คุณต้องการยืนยันการชำระเงินนี้',
      text: "รายการนี้จะถูกบันทึกว่าได้รับชำระเงินแล้ว",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#0CC27E',
      cancelButtonColor: '#FF586B',
      confirmButtonText: 'ยืนยัน',
      cancelButtonText: 'ปิดออก',
      confirmButtonClass: 'btn btn-success btn-raised mr-2',
      cancelButtonClass: 'btn btn-default btn-raised',
      buttonsStyling: false
    }).then((result) => {
      if (result.value) {
        this.api.postData('Payment/confirm_payment/', { pm_id: pm_id, id: this.admin_id, confirm: 1 }).then((res) => {
          if (res == 'success') {
            this.api.fb_set('order')
            Swal.fire(
              'เรียบร้อยแล้ว',
              '',
              'success'
            )
          }

        })
      }
    })
  }

  cancel(pm_id) {
    console.log('canceled order id =' + pm_id);
    this.api.postData('Payment/confirm_payment/', { pm_id: pm_id, id: this.admin_id, confirm: -2 }).then((res) => {
      this.api.fb_set('order')
      this.load_paymant(1)
    })
  }

  search() {
    this.load_paymant(1)
  }

  change_page(page: number) {
    console.log('Change' + page);
    this.load_paymant(page);
  }

  test(t) {
    console.log('clicked');
    console.log(t);
    const modal: NgbModalRef = this.modalService.open(viewModalPayment, { size: 'lg' });
    modal.componentInstance.payment = t;
    modal.result.then((result) => {
      console.log(result);
    }, (reason) => {
    });
    modal.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  private getDismissReason(reason: any): string {
    this.load_paymant(1)
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  ngOnDestroy() {
    this.fb = false;
  }
}
